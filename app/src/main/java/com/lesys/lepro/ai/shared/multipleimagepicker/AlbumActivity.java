package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;

import java.util.ArrayList;

public class AlbumActivity extends BaseActivity {

//    Toolbar toolbar;
//    TextView mToolBarTitle;
    public static final String SELECTED_STRING_ARRAY_KEY = "CUSTOM_GALLERY_SELECTED";
    public static final String SEQUENCE_SELECTION_BOOLEAN_KEY = "CUSTOM_GALLERY_SEQUENCE_SELECTION";
    public static final String MAXIMUM_SELECTION_INT_KEY = "CUSTOM_GALLERY_MAXIMUM_SELECTION";
    GridView gridGallery;
    Handler handler;
    AlbumAdapter adapter;

    ImageView imgNoMedia;
    Button btnGalleryOk;

    String action;
    ArrayList<Album> mAlbumsList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.album);

        action = getIntent().getAction();
        if (action == null) {
            finish();
        }

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
//
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(null);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mToolBarTitle.setText(R.string.select_photo);
        init();
//        listFolder();

    }

    private ArrayList<Album> getAlbums() {

        mAlbumsList = new ArrayList<Album>();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, MediaStore.MediaColumns.DATE_ADDED + " DESC");

        ArrayList<String> ids = new ArrayList<String>();


        mAlbumsList.clear();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Album album = new Album();

                int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID);
                album.id = cursor.getString(columnIndex);
//				Log.d("Gallery", "ImagePath->" + cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)) );
                if (!ids.contains(album.id)) {
                    columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                    album.name = cursor.getString(columnIndex);

                    Log.d("Gallery", album.name);


                    columnIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    album.coverID = cursor.getLong(columnIndex);

                    columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    album.images.add(new CustomGallery(cursor.getString(columnIndex)));
                    album.thumbnail = cursor.getString(columnIndex);


                    mAlbumsList.add(album);
                    ids.add(album.id);
                } else {
                    mAlbumsList.get(ids.indexOf(album.id)).count++;
                    mAlbumsList.get(ids.indexOf(album.id)).images.add(new CustomGallery(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))));
                }
            }
            cursor.close();
        }
        return mAlbumsList;
    }


    private void init() {

        handler = new Handler();
        gridGallery = (GridView) findViewById(R.id.gridGallery);
        gridGallery.setFastScrollEnabled(true);
        if (getIntent().getBooleanExtra(SEQUENCE_SELECTION_BOOLEAN_KEY, false)) {
            adapter = new AlbumAdapter(getApplicationContext(), getIntent().getIntExtra(MAXIMUM_SELECTION_INT_KEY, 5));
        } else {
            adapter = new AlbumAdapter(getApplicationContext());
        }

//		PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader,
//				true, true);
//		gridGallery.setOnScrollListener(listener);

//        if (action.equalsIgnoreCase(Action.ACTION_MULTIPLE_PICK)) {
//
//            findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
//            gridGallery.setOnItemClickListener(mItemMulClickListener);
//            adapter.setMultiplePick(true);
//
//        } else if (action.equalsIgnoreCase(Action.ACTION_PICK)) {
//
//            findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
//            gridGallery.setOnItemClickListener(mItemSingleClickListener);
//            adapter.setMultiplePick(false);
//
//        }

//        findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
        gridGallery.setOnItemClickListener(mItemSingleClickListener);

        gridGallery.setAdapter(adapter);
        imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

//        btnGalleryOk = (Button) findViewById(R.id.btnGalleryOk);
//        btnGalleryOk.setOnClickListener(mOkClickListener);

        new Thread() {

            @Override
            public void run() {
                Looper.prepare();
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        adapter.addAll(getAlbums());
                        checkImageStatus();
                    }
                });
                Looper.loop();
            }

            ;

        }.start();

    }

    private void checkImageStatus() {
        if (adapter.isEmpty()) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }

    View.OnClickListener mOkClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            finish();
        }
    };

    AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            Intent intent = new Intent(AlbumActivity.this,CustomGalleryActivityV2.class);
            intent.putExtra(CustomGalleryActivityV2.INTENT_ALBUM_KEY, mAlbumsList.get(position));
            intent.putExtra(CustomGalleryActivityV2.MAXIMUM_SELECTION_INT_KEY, getIntent().getIntExtra(MAXIMUM_SELECTION_INT_KEY, 5));
            intent.putExtra(CustomGalleryActivityV2.SELECTED_STRING_ARRAY_KEY, getIntent().getStringArrayExtra(SELECTED_STRING_ARRAY_KEY));
            intent.putExtra(CustomGalleryActivityV2.SEQUENCE_SELECTION_BOOLEAN_KEY, getIntent().getBooleanExtra(SEQUENCE_SELECTION_BOOLEAN_KEY, false));
            intent.setAction(action);
            startActivityForResult(intent, CustomGalleryActivityV2.request_code);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CustomGalleryActivityV2.request_code && resultCode == RESULT_OK){
            setResult(RESULT_OK,data);
            finish();
        }
    }

    //    private ArrayList<CustomGallery> getGalleryPhotos() {
//        ArrayList<CustomGallery> galleryList = new ArrayList<CustomGallery>();
//        String[] selectedPaths = getIntent().getStringArrayExtra(SELECTED_STRING_ARRAY_KEY);
//        int addedCount = 0;
//        try {
//            final String[] columns = {MediaStore.Images.Media.DATA,
//                    MediaStore.Images.Media._ID};
//            final String orderBy = MediaStore.Images.Media._ID;
//
//            Cursor imagecursor = managedQuery(
//                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
//                    null, null, orderBy);
//
//            if (imagecursor != null && imagecursor.getCount() > 0) {
//
//                while (imagecursor.moveToNext()) {
//                    CustomGallery item = new CustomGallery();
//
//                    int dataColumnIndex = imagecursor
//                            .getColumnIndex(MediaStore.Images.Media.DATA);
//
//                    item.sdcardPath = imagecursor.getString(dataColumnIndex);
//                    for (int i = 0; i < selectedPaths.length; i++) {
////						System.out.println("Kelvyndebug: paths compare:" + item.sdcardPath + " vs " + selectedPaths[i]);
//                        if (item.sdcardPath.equals(selectedPaths[i])) {
//                            item.isSelected = true+"";
//                            item.selectionIndex = i;
//                            addedCount++;
//                            selectedPaths[i] = "";
//                            break;
//                        }
//                    }
//                    galleryList.add(item);
//                }
//            }
//            if (addedCount < selectedPaths.length) {
//                for (int i = 0; i < selectedPaths.length; i++) {
//                    if (selectedPaths[i].trim().length() > 0) {
//                        CustomGallery item = new CustomGallery();
//                        item.sdcardPath = selectedPaths[i];
//                        item.isSelected = true+"";
//                        item.selectionIndex = i;
//                        galleryList.add(item);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // show newest photo at beginning of the list
//        Collections.reverse(galleryList);
//        return galleryList;
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

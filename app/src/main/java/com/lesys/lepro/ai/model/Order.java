package com.lesys.lepro.ai.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 25/06/2017.
 */

public class Order implements Serializable {

    private long id;
    private String order_num;
    private String propertyStreet;
    private String propertyTown;
    private String propertyPostcode;
    private String propertyState;
    private String tenancyAgreementStatus;
    private String eSignStatus;
    private String eStampStatus;
    private String specialRequest;
    private String propertyPicture;
    private String previewDocumentPath;
    private List<Simple> tenants = new ArrayList<>();
    private List<Simple> landlords = new ArrayList<>();
    private String ipayBackendURL;
    private String ipayRemark;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public String getPropertyStreet() {
        return propertyStreet;
    }

    public void setPropertyStreet(String propertyStreet) {
        this.propertyStreet = propertyStreet;
    }

    public String getPropertyTown() {
        return propertyTown;
    }

    public void setPropertyTown(String propertyTown) {
        this.propertyTown = propertyTown;
    }

    public String getPropertyPostcode() {
        return propertyPostcode;
    }

    public void setPropertyPostcode(String propertyPostcode) {
        this.propertyPostcode = propertyPostcode;
    }

    public String getPropertyState() {
        return propertyState;
    }

    public void setPropertyState(String propertyState) {
        this.propertyState = propertyState;
    }

    public String getTenancyAgreementStatus() {
        return tenancyAgreementStatus;
    }

    public void setTenancyAgreementStatus(String tenancyAgreementStatus) {
        this.tenancyAgreementStatus = tenancyAgreementStatus;
    }

    public String geteSignStatus() {
        return eSignStatus;
    }

    public void seteSignStatus(String eSignStatus) {
        this.eSignStatus = eSignStatus;
    }

    public String geteStampStatus() {
        return eStampStatus;
    }

    public void seteStampStatus(String eStampStatus) {
        this.eStampStatus = eStampStatus;
    }

    public String getPropertyPicture() {
        return propertyPicture;
    }

    public void setPropertyPicture(String propertyPicture) {
        this.propertyPicture = propertyPicture;
    }

    public List<Simple> getTenants() {
        return tenants;
    }

    public void setTenants(List<Simple> tenants) {
        this.tenants = tenants;
    }

    public List<Simple> getLandlords() {
        return landlords;
    }

    public void setLandlords(List<Simple> landlords) {
        this.landlords = landlords;
    }

    public String getPreviewDocumentPath() {
        return previewDocumentPath;
    }

    public void setPreviewDocumentPath(String previewDocumentPath) {
        this.previewDocumentPath = previewDocumentPath;
    }

    public String getIpayBackendURL() {
        return ipayBackendURL;
    }

    public void setIpayBackendURL(String ipayBackendURL) {
        this.ipayBackendURL = ipayBackendURL;
    }

    public String getIpayRemark() {
        return ipayRemark;
    }

    public void setIpayRemark(String ipayRemark) {
        this.ipayRemark = ipayRemark;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

}

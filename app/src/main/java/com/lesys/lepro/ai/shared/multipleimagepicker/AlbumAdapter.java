package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesys.lepro.ai.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

public class AlbumAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater infalter;
    private ArrayList<Album> data = new ArrayList<Album>();

    private boolean isActionMultiplePick;
    private int maximumSelection;
    String[] dataT;
    ImageLoader imageLoader;

    public AlbumAdapter(Context c) {
        infalter = (LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        // clearCache();
    }

    public AlbumAdapter(Context c, int maximumSelection) {
        infalter = (LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;

        this.maximumSelection = maximumSelection;
        dataT = new String[maximumSelection];

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        // clearCache();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Album getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setMultiplePick(boolean isMultiplePick) {
        this.isActionMultiplePick = isMultiplePick;
    }



    public void addAll(ArrayList<Album> files) {

        try {
            this.data.clear();
            this.data.addAll(files);
        } catch (Exception e) {
            e.printStackTrace();
        }

        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {

            convertView = infalter.inflate(R.layout.album_item, null);
            holder = new ViewHolder();
            holder.imgQueue = (ImageView) convertView
                    .findViewById(R.id.imgQueue);

            holder.imgQueueMultiSelectionIndex = (TextView) convertView
                    .findViewById(R.id.imgQueueMultiSelectIndex);



            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imgQueue.setTag(position);

//        holder.imgQueueMultiSelected.setVisibility(View.GONE);

        try {

//            imageLoader.displayImage("file://" + data.get(position).sdcardPath,
//                    holder.imgQueue, new SimpleImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//                            holder.imgQueue
//                                    .setImageResource(R.drawable.placeholder_camera);
//                            super.onLoadingStarted(imageUri, view);
//                        }
//                    });

//            Picasso.with(mContext)
//                    .load("file://" + data.get(position).thumbnail)
//                    .fit()
//                    .centerInside()
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .error(R.drawable.placeholder_camera)
//                    .into(holder.imgQueue);

            imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage("file://" + data.get(position).thumbnail, holder.imgQueue);

            holder.imgQueueMultiSelectionIndex.setText(data.get(position).name);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public class ViewHolder {
        ImageView imgQueue;
        TextView imgQueueMultiSelectionIndex;
    }


    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }
}

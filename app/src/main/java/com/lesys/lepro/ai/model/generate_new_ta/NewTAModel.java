package com.lesys.lepro.ai.model.generate_new_ta;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lesys.lepro.ai.model.OrderFullDetail;
import com.lesys.lepro.ai.model.SpecialRequest;
import com.lesys.lepro.ai.model.SpecialRequestCover;
import com.lesys.lepro.ai.model.generate_new_ta.param_api.LandlordTenant;
import com.lesys.lepro.ai.model.generate_new_ta.param_api.LandlordTenantCover;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class NewTAModel implements Serializable {

    public long id;
    public String taPDF;
    public String uploadType;
    public PropertyDetailModel propertyDetail;
    public RentalDetailModel rentalDetail;
    public UtilitiesDetailsModel utilitiesDetailsModel;
    public PaymentDetailModel paymentDetailModel;
    //    public SpecialConditionModel specialCondition;
    public SpecialRequestCover mSpecialRequestCover;
    public FixtureFittingsModel fixtureFittingsModel;
    public boolean isTenant;
    public boolean isCommercial;
    public String stepCaching;
    public List<LandlordTenantDetailModel> ownerLandlordTenantDetailList = new ArrayList<>();
    public List<LandlordTenantDetailModel> nonOwnerLandlordTenantDetailList = new ArrayList<>();

    public NewTAModel() {

    }

    public NewTAModel syncServerTA(OrderFullDetail order) {
        id = order.getId();

        propertyDetail = new PropertyDetailModel();
        propertyDetail.syncServerTA(order);

        rentalDetail = new RentalDetailModel();
        rentalDetail.syncServerTA(order);

        if (!TextUtils.isEmpty(order.getSpecialRequest())) {
            try {
                JSONArray jsonArray = new JSONArray(order.getSpecialRequest());
                mSpecialRequestCover = new SpecialRequestCover();
                mSpecialRequestCover.getArrayList().clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    SpecialRequest specialRequest = new SpecialRequest();
                    specialRequest.setOriginal(jsonObject.getString("original"));
                    specialRequest.setEdited(jsonObject.getString("edited"));
                    mSpecialRequestCover.getArrayList().add(specialRequest);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            mSpecialRequestCover = new Gson().fromJson(order.getSpecialRequest(), SpecialRequestCover.class);
        }

        if (!TextUtils.isEmpty(order.getFixturesFitting())) {
            try {
                JSONArray jsonArray = new JSONArray(order.getFixturesFitting());
                fixtureFittingsModel = new FixtureFittingsModel();
                fixtureFittingsModel.fixtureFittingList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    fixtureFittingsModel.fixtureFittingList.add(jsonArray.getString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            fixtureFittingsModel = new Gson().fromJson(order.getFixturesFitting(), FixtureFittingsModel.class);
        }

        //Todo : still have payment
        paymentDetailModel = new PaymentDetailModel();
        paymentDetailModel.syncServerTA(order);

        isTenant = order.isLandlord() ? false : true;
        isCommercial = order.isResidential() ? false : true;

        if (isTenant) {
            for (LandlordTenant tenant : order.getTenants()) {
                ownerLandlordTenantDetailList.add(new LandlordTenantDetailModel(tenant));
            }
            for (LandlordTenant landlord : order.getLandlords()) {
                nonOwnerLandlordTenantDetailList.add(new LandlordTenantDetailModel(landlord));
            }
        } else {
            for (LandlordTenant tenant : order.getTenants()) {
                nonOwnerLandlordTenantDetailList.add(new LandlordTenantDetailModel(tenant));
            }
            for (LandlordTenant landlord : order.getLandlords()) {
                ownerLandlordTenantDetailList.add(new LandlordTenantDetailModel(landlord));
            }
        }


        return this;
    }
}

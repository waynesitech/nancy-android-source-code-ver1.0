package com.lesys.lepro.ai.module.tenancy_agreement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.DemoFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.OrderFullDetail;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatus1Fragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatus2Fragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatus3Fragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatus4Fragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatus5Fragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatusFixturesAndFittingsFragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.GenerateNewTaStatusPaymentDetailsFragment;
import com.lesys.lepro.ai.module.tenancy_agreement.fragment.PreviewFragment;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

import java.util.ArrayList;

/**
 * Created by Nicholas on 21/05/2017.
 */

public class GenerateNewTAActivity extends BaseActivity {
    private FragmentManager mFragmentManager;
    private Fragment[] mResidentialSlideFragments = {
            GenerateNewTaStatus1Fragment.newInstance(),
            GenerateNewTaStatus2Fragment.newInstance(true),
            GenerateNewTaStatus2Fragment.newInstance(false),
            GenerateNewTaStatus3Fragment.newInstance(),
            GenerateNewTaStatusPaymentDetailsFragment.newInstance(),
            GenerateNewTaStatus5Fragment.newInstance(),
            GenerateNewTaStatusFixturesAndFittingsFragment.newInstance(),
            PreviewFragment.newInstance()
    };
    private Fragment[] mCommercialSlideFragments = {
            GenerateNewTaStatus1Fragment.newInstance(),
            GenerateNewTaStatus2Fragment.newInstance(true),
            GenerateNewTaStatus2Fragment.newInstance(false),
            GenerateNewTaStatus3Fragment.newInstance(),
            GenerateNewTaStatus4Fragment.newInstance(),
            GenerateNewTaStatusPaymentDetailsFragment.newInstance(),
            GenerateNewTaStatus5Fragment.newInstance(),
            GenerateNewTaStatusFixturesAndFittingsFragment.newInstance(),
            PreviewFragment.newInstance()
    };
    LinearLayout mStepLayout;
    View[] mStatusViewArray;
    public NewTAModel newTAModel;
    public String[] mResidentialSlideFragmentTag = {
            GenerateNewTaStatus1Fragment.class.getName(),
            GenerateNewTaStatus2Fragment.class.getName() + "_ISOWNER",
            GenerateNewTaStatus2Fragment.class.getName(),
            GenerateNewTaStatus3Fragment.class.getName(),
            GenerateNewTaStatusPaymentDetailsFragment.class.getName(),
            GenerateNewTaStatus5Fragment.class.getName(),
            GenerateNewTaStatusFixturesAndFittingsFragment.class.getName(),
            PreviewFragment.class.getName()
    };
    public String[] mCommercialSlideFragmentTag = {
            GenerateNewTaStatus1Fragment.class.getName(),
            GenerateNewTaStatus2Fragment.class.getName() + "_ISOWNER",
            GenerateNewTaStatus2Fragment.class.getName(),
            GenerateNewTaStatus3Fragment.class.getName(),
            GenerateNewTaStatus4Fragment.class.getName(),
            GenerateNewTaStatusPaymentDetailsFragment.class.getName(),
            GenerateNewTaStatus5Fragment.class.getName(),
            GenerateNewTaStatusFixturesAndFittingsFragment.class.getName(),
            PreviewFragment.class.getName()
    };

    String uploadType;
    String taPdf;

    boolean isEdit;
    boolean isTARenew;
    public OrderFullDetail mOrderFullDetail;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment f : mResidentialSlideFragments)
            f.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (newTAModel.isCommercial) {
            for (int i = mCommercialSlideFragmentTag.length; i > 0; i--) {
                if (i == 1) {
                    super.onBackPressed();
                    break;
                } else if (i == 2 && ((GenerateNewTaStatus2Fragment) mCommercialSlideFragments[1]).getCurrentIndex() > 0) {
                    ((GenerateNewTaStatus2Fragment) mCommercialSlideFragments[1]).decreaseCurrentIndex();
                    break;
                } else if (i == 2 && ((GenerateNewTaStatus2Fragment) mCommercialSlideFragments[1]).isOnBackPressNeedDisplayUserTypeLayout()) {
                    break;
                } else if (i == 3 && ((GenerateNewTaStatus2Fragment) mCommercialSlideFragments[2]).getCurrentIndex() > 0) {
                    ((GenerateNewTaStatus2Fragment) mCommercialSlideFragments[2]).decreaseCurrentIndex();
                    break;
                }

                Fragment fragment = getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i - 1]);
                if (fragment != null) {
                    boolean isFragmentVisible = fragment.isVisible();
                    if (isFragmentVisible) {
                        pageController(mCommercialSlideFragmentTag[i - 2]);
                        break;
                    }
                }
            }
        } else {
            for (int i = mResidentialSlideFragmentTag.length; i > 0; i--) {
                if (i == 1) {
                    super.onBackPressed();
                    break;
                } else if (i == 2 && ((GenerateNewTaStatus2Fragment) mResidentialSlideFragments[1]).getCurrentIndex() > 0) {
                    ((GenerateNewTaStatus2Fragment) mResidentialSlideFragments[1]).decreaseCurrentIndex();
                    break;
                } else if (i == 2 && ((GenerateNewTaStatus2Fragment) mResidentialSlideFragments[1]).isOnBackPressNeedDisplayUserTypeLayout()) {
                    break;
                } else if (i == 3 && ((GenerateNewTaStatus2Fragment) mResidentialSlideFragments[2]).getCurrentIndex() > 0) {
                    ((GenerateNewTaStatus2Fragment) mResidentialSlideFragments[2]).decreaseCurrentIndex();
                    break;
                }
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i - 1]);
                if (fragment != null) {
                    boolean isFragmentVisible = fragment.isVisible();
                    if (isFragmentVisible) {
                        pageController(mResidentialSlideFragmentTag[i - 2]);
                        break;
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isEdit = getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);
        mOrderFullDetail = (OrderFullDetail) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_FULL_DETAIL_DATA);
        uploadType = getIntent().getStringExtra(GlobalParam.INTENT_UPLOAD_TYPE);
        taPdf = getIntent().getStringExtra(GlobalParam.INTENT_TA_PDF);
        setContentView(R.layout.activity_generate_new_ta);

        mStepLayout = (LinearLayout) findViewById(R.id.step_layout);

        if (isEdit || isTARenew) {
            if (mOrderFullDetail == null) {
                finish();
                return;
            }
            newTAModel = new NewTAModel();
            newTAModel.syncServerTA(mOrderFullDetail);
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(this, GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null) {
                newTAModel = new NewTAModel();
                newTAModel.uploadType = uploadType;
                if (!TextUtils.isEmpty(taPdf)) {
                    newTAModel.taPDF = taPdf;
                }
                String newTAModelJson = new Gson().toJson(newTAModel);
                SharedPreferencesUtil.setString(this, GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
            }
        }

        mFragmentManager = getSupportFragmentManager();

        if (!TextUtils.isEmpty(newTAModel.stepCaching)) {
            pageController(newTAModel.stepCaching);
        } else {
            pageController(GenerateNewTaStatus1Fragment.class.getName());
        }

    }

    void generateStepLayout() {
        if (!newTAModel.isCommercial) {
            mStatusViewArray = new View[mResidentialSlideFragmentTag.length - 1];
        } else {
            mStatusViewArray = new View[mCommercialSlideFragmentTag.length - 1];
        }

        mStepLayout.removeAllViews();
        for (int i = 0; i < mStatusViewArray.length; i++) {
            mStatusViewArray[i] = new View(this);
            mStatusViewArray[i].setBackgroundResource(R.drawable.bg_rounded_generate_ta_status);
            mStatusViewArray[i].setEnabled(false);

            mStepLayout.addView(mStatusViewArray[i]);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mStatusViewArray[i].getLayoutParams();
            params.width = Common.Helpers.getDp(this, 25);
            params.height = Common.Helpers.getDp(this, 25);
            mStatusViewArray[i].setLayoutParams(params);

            if (i != (mStatusViewArray.length - 1)) {
                View divider = new View(this);
                divider.setBackgroundColor(ContextCompat.getColor(this, R.color.generate_ta_status));
                mStepLayout.addView(divider);
                LinearLayout.LayoutParams dividerParams = (LinearLayout.LayoutParams) divider.getLayoutParams();
                dividerParams.width = Common.Helpers.getDp(this, 10);
                dividerParams.height = Common.Helpers.getDp(this, 4);
                divider.setLayoutParams(dividerParams);
            }
        }
    }

    public void pageController(String tag) {
        generateStepLayout();
        for (View v : mStatusViewArray) {
            v.setBackgroundResource(R.drawable.bg_rounded_generate_ta_status);
            v.setEnabled(false);
        }

        boolean isStopPoint = false;
        if (!newTAModel.isCommercial) {
            for (int i = 0; i < mResidentialSlideFragmentTag.length; i++) {
                if (!isStopPoint && i != (mResidentialSlideFragmentTag.length - 1)) {
                    mStatusViewArray[i].setBackgroundResource(R.drawable.bg_rounded_generate_ta_status);
                    mStatusViewArray[i].setEnabled(true);
                }
                if (tag.equals(mResidentialSlideFragmentTag[i])) {
                    if (i != (mResidentialSlideFragmentTag.length - 1)) {
                        mStatusViewArray[i].setBackgroundResource(R.drawable.bg_rounded_generate_ta_status_ongoing);
                    }
                    isStopPoint = true;
                    if (getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i]) == null) {
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.container, mResidentialSlideFragments[i], mResidentialSlideFragmentTag[i])
                                .commit();
                    } else {
                        getSupportFragmentManager().beginTransaction()
                                .show(getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i]))
                                .commit();
//                        getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i]).onHiddenChanged(true);
//                        try {
//                            if (i == (mResidentialSlideFragmentTag.length - 1)) {
//                                ((PreviewFragment) getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i])).loadLocalData();
//                            }
//                        } catch (Exception e) {
//
//                        }

                    }
                } else {
                    if (getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i]) != null) {
                        getSupportFragmentManager().beginTransaction()
                                .hide(getSupportFragmentManager().findFragmentByTag(mResidentialSlideFragmentTag[i]))
                                .commit();
                    }
                }
            }
        } else {
            for (int i = 0; i < mCommercialSlideFragmentTag.length; i++) {
                if (!isStopPoint && i != (mCommercialSlideFragmentTag.length - 1)) {
                    mStatusViewArray[i].setBackgroundResource(R.drawable.bg_rounded_generate_ta_status);
                    mStatusViewArray[i].setEnabled(true);
                }
                if (tag.equals(mCommercialSlideFragmentTag[i])) {
                    if (i != (mCommercialSlideFragmentTag.length - 1)) {
                        mStatusViewArray[i].setBackgroundResource(R.drawable.bg_rounded_generate_ta_status_ongoing);
                    }
                    isStopPoint = true;
                    if (getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i]) == null) {
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.container, mCommercialSlideFragments[i], mCommercialSlideFragmentTag[i])
                                .commit();
                    } else {
                        getSupportFragmentManager().beginTransaction()
                                .show(getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i]))
                                .commit();
//                        getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i]).onHiddenChanged(true);
//                        try {
//                            if (i == (mCommercialSlideFragments.length - 1)) {
//                                ((PreviewFragment) getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i])).loadLocalData();
//                            }
//                        } catch (Exception e) {
//
//                        }
                    }
                } else {
                    if (getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i]) != null) {
                        getSupportFragmentManager().beginTransaction()
                                .hide(getSupportFragmentManager().findFragmentByTag(mCommercialSlideFragmentTag[i]))
                                .commit();
                    }
                }
            }
        }
    }

    public void setPropertyType() {
        if (!isEdit || !isTARenew) {
            String keyNewTAModel = SharedPreferencesUtil.getString(this, GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
        }
    }

    public NewTAModel getNewTAModel() {
        return newTAModel;
    }

    public void setNewTAModel(NewTAModel newTAModel) {
        this.newTAModel = newTAModel;
    }

}

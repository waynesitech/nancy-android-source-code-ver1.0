package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class OrderResultCover extends MetaData implements Serializable {

    private OrderCover result;

    public OrderCover getResult() {
        return result;
    }

    public void setResult(OrderCover result) {
        this.result = result;
    }
}

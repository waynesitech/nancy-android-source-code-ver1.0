package com.lesys.lepro.ai.module.authentication;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.MainActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.UserCover;
import com.lesys.lepro.ai.module.nancy.CongratulationActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class PhoneVerificationActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = PhoneVerificationActivity.class.getName();

    TextView mVerifyDeviceDesc;
    TextView mResendSmsBtn;
    TextInputEditText mVerifyCode;
    Button mNextBtn;
    TextView mErrorMessage;
    User mUser = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        if (mUser.getUser(this) == null) {
            finish();
            return;
        }
        mVerifyDeviceDesc = (TextView) findViewById(R.id.verify_device_desc);
        mResendSmsBtn = (TextView) findViewById(R.id.resend_sms_btn);
        mVerifyCode = (TextInputEditText) findViewById(R.id.verify_code);
        mNextBtn = (Button) findViewById(R.id.next_btn);
        mErrorMessage = (TextView) findViewById(R.id.error_message);

        mResendSmsBtn.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);

        mUser = new User().getUser(this);
        mVerifyDeviceDesc.setText(getString(R.string.verify_device_desc, mUser.getMobile()));

        setErrorMessage1Style();
    }

    void setErrorMessage1Style() {
        String text = getString(R.string.verify_device_error_message_1);
        String word = "60";
        SpannableString myString = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };

        int startIndex = 0;
        for (int i = -1; (i = text.indexOf(word, i + 1)) != -1; ) {
            startIndex = i;
        }
        int lastIndex = startIndex + word.length();
        //For Click
        myString.setSpan(clickableSpan, startIndex, lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //For Bold
        myString.setSpan(new StyleSpan(Typeface.ITALIC), startIndex, lastIndex, 0);

        myString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.normal_text)), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mErrorMessage.setText(myString);
        mErrorMessage.setMovementMethod(LinkMovementMethod.getInstance());
    }

    void requestPhoneVerificationById() {
        BaseApi.init(this)
                .requestPhoneVerificationById(mUser.getId())
                .call(new DataHandler<MetaData>(MetaData.class) {
                    @Override
                    public void onSuccess(MetaData data) {

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });

    }

    void submitPhoneVerificationById() {
        if (TextUtils.isEmpty(mVerifyCode.getText().toString())) {
            mVerifyCode.setError(getString(R.string.verify_device_error_message_2), Common.Helpers.getErrorDrawable(this));
            mErrorMessage.setText(R.string.verify_device_error_message_2);
            return;
        }
        BaseApi.init(this)
                .submitPhoneVerificationById(mVerifyCode.getText().toString(), mUser.getId())
                .call(new DataHandler<UserCover>(UserCover.class) {
                    @Override
                    public void onSuccess(UserCover data) {
                        setResult(RESULT_OK);
                        mUser = new User().getUser(PhoneVerificationActivity.this);
                        mUser.setIsPhoneVerify(data.getResult().getIsPhoneVerify());
                        SharedPreferencesUtil.setString(PhoneVerificationActivity.this, GlobalParam.KEY_USER_DATA, new Gson().toJson(mUser));
                        Intent intent = new Intent(PhoneVerificationActivity.this, MainActivity.class);
                        intent.setAction(GlobalParam.ACTION_PHONE_VERIFIED);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_REGISTED: {
                    setResult(RESULT_OK);
                    finish();
                    break;
                }
            }
        }
    }

    void login() {
        BaseApi.init(this)
                .emailLogin("nic@gmail.com", "12341234")
                .call(new DataHandler<UserCover>(UserCover.class) {
                    @Override
                    public void onSuccess(UserCover data) {
                        setResult(RESULT_OK);
                        SharedPreferencesUtil.setString(PhoneVerificationActivity.this, GlobalParam.KEY_USER_DATA, new Gson().toJson(data.getResult()));
                        User mUser = new User().getUser(PhoneVerificationActivity.this);
                        Intent intent = new Intent(PhoneVerificationActivity.this, MainActivity.class);
                        intent.setAction(GlobalParam.ACTION_PHONE_VERIFIED);
                        startActivity(intent);
                        finish();
                        Log.e(TAG, "mUser = " + mUser.getEmail());
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.resend_sms_btn: {
                requestPhoneVerificationById();
                break;
            }
            case R.id.next_btn: {
                submitPhoneVerificationById();
//                login();
//                startActivityForResult(new Intent(this, CongratulationActivity.class), GlobalParam.RC_REGISTED);
                break;
            }
        }
    }
}

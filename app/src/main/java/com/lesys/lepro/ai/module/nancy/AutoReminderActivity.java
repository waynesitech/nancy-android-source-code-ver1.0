package com.lesys.lepro.ai.module.nancy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.module.e_stamp.EStampActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.TADetailActivity;
import com.lesys.lepro.ai.shared.GlobalParam;

/**
 * Created by Nicholas on 11/07/2017.
 */

public class AutoReminderActivity extends BaseActivity implements View.OnClickListener {

    TextView mTotalCharge;
    Button mCancelBtn;
    Button mAgreeBtn;

    Order mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_reminder);

        mOrder = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);

        mTotalCharge = (TextView) findViewById(R.id.total_charge);
        mCancelBtn = (Button) findViewById(R.id.cancel_btn);
        mAgreeBtn = (Button) findViewById(R.id.agree_btn);

        mTotalCharge.setText(getString(R.string.auto_reminder_title, "200.00"));

        mCancelBtn.setOnClickListener(this);
        mAgreeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_btn: {
                finish();
                break;
            }
            case R.id.agree_btn: {
                Intent intent = new Intent(this, EStampActivity.class);
                intent.putExtra(GlobalParam.INTENT_ORDER_DATA, mOrder);
                startActivity(intent);
                finish();
                break;
            }
        }
    }
}

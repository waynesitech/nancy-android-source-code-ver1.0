package com.lesys.lepro.ai.model.generate_new_ta;

import android.text.TextUtils;

import com.lesys.lepro.ai.model.OrderFullDetail;
import com.lesys.lepro.ai.model.generate_new_ta.param_api.OtherSecurityDepositCover;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class RentalDetailModel implements Serializable {

    public String commencementDate;
    public MonthYearModel termOfTheTenancy;
    public boolean isOptionToRenew;
    public MonthYearModel optionToRenew;
    public TermsOfRenewalModel termsOfRenewal;
    public MoneyModel rental;
    public MoneyModel advanceRental;
    public MoneyModel securityDepositRent;
    public MoneyModel securityDepositUtilities;
    public List<MoneyModel> securityDepositOtherList = new ArrayList<>();
    public MonthYearModel rentalFreePeriod;
    public boolean isRentalFreePeriodSpecialDate;
    public String rentalFreePeriodFromDate;
    public String rentalFreePeriodToDate;
    public String carParkAmount;
    public List<String> lotNumberList = new ArrayList<>();
    public DayHourMinModel operationHourFrom;
    public DayHourMinModel operationHourTo;

    public RentalDetailModel() {

    }

    public RentalDetailModel syncServerTA(OrderFullDetail order) {
        commencementDate = order.getCommencementDate();

        termOfTheTenancy = new MonthYearModel(
                "",
                String.valueOf(order.getTermsOfTenancyMonths()),
                String.valueOf(order.getTermsOfTenancyYears())
        );
        isOptionToRenew = order.isOptionToRenew();
        optionToRenew = new MonthYearModel(
                "",
                String.valueOf(order.getOptionToRenewMonths()),
                String.valueOf(order.getOptionToRenewYears())
        );
        termsOfRenewal = new TermsOfRenewalModel();
        termsOfRenewal.syncServerTA(order);
        rental = new MoneyModel(
                "",
                order.getRental()
        );
        advanceRental = new MoneyModel(
                "",
                order.getAdvanceRental()
        );
        securityDepositRent = new MoneyModel(
                "",
                order.getSecurityDepositRent()
        );
        securityDepositUtilities = new MoneyModel(
                "",
                order.getSecurityDepositUtilities()
        );

        OtherSecurityDepositCover otherSecurityDepositCover = new OtherSecurityDepositCover();
        securityDepositOtherList = otherSecurityDepositCover.fromJson(order.getOtherSecurityDeposit());

        rentalFreePeriod = new MonthYearModel(
                order.getRentalFreePeriodWeeks(),
                order.getRentalFreePeriodMonths(),
                order.getRentalFreePeriodYears()
        );
//        isRentalFreePeriodSpecialDate = order.is
        rentalFreePeriodFromDate = order.getRentalFreePeriodStart();
        rentalFreePeriodToDate = order.getRentalFreePeriodEnd();

        if (!TextUtils.isEmpty(order.getCarParkLots())) {
            try {
                JSONArray jsonArray = new JSONArray(order.getCarParkLots());
                for (int i = 0; i < jsonArray.length(); i++) {
                    lotNumberList.add(jsonArray.getString(i));
                }
                carParkAmount = lotNumberList.size() + "";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return this;
    }
}

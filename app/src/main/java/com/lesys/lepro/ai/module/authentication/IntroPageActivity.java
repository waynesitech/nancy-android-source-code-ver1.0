package com.lesys.lepro.ai.module.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.shared.GlobalParam;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class IntroPageActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_page);

        findViewById(R.id.sign_in_btn).setOnClickListener(this);
        findViewById(R.id.sign_up_btn).setOnClickListener(this);
        findViewById(R.id.e_sign_btn).setOnClickListener(this);
//        splashScreenCountDown();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_LOGIN:
                case GlobalParam.RC_VERIFIED: {
                    finish();
                    break;
                }
                case GlobalParam.RC_REGISTED: {
                    startActivityForResult(new Intent(this, PhoneVerificationActivity.class), GlobalParam.RC_VERIFIED);
                    break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.sign_in_btn: {
                intent.setClass(this, LoginActivity.class);
                startActivityForResult(intent, GlobalParam.RC_LOGIN);
                break;
            }
            case R.id.sign_up_btn: {
                intent.setClass(this, SignUpActivity.class);
                startActivityForResult(intent, GlobalParam.RC_REGISTED);
                break;
            }
            case R.id.e_sign_btn: {
                intent.setClass(this, PasscodeSignInActivity.class);
                startActivityForResult(intent, GlobalParam.RC_ESIGN);
                break;
            }
        }
    }
}

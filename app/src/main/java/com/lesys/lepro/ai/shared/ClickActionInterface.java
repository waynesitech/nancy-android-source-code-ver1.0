package com.lesys.lepro.ai.shared;

/**
 * Created by NicholasTan on 19/01/2017.
 */

public class ClickActionInterface {

    public interface DialogActionInterface {
        void onPositiveButtonClick();
    }

    public interface RetryActionInterface {
        void onRetryClickListener();
    }

}

package com.lesys.lepro.ai.shared;

import android.app.ProgressDialog;
import android.content.Context;

import com.lesys.lepro.ai.R;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

import java.io.File;

/**
 * Created by NicholasTan on 03/03/2016.
 */
public abstract class FileDownloader {
    String url;
    String fileName;
    File file;
    ProgressDialog pDialog;
    HttpHandler handler;
    final String TAG = "FileDownloader";

    public FileDownloader(Context context, String url, String fileName) {
        this.url = url;
        this.fileName = fileName;
        file = new File(fileName);
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.loading));
//        pDialog.setIndeterminate(false);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
    }

    public void download() {
        HttpUtils http = new HttpUtils();
        handler = http.download(url,
                fileName,
                true, // 如果目标文件存在，接着未完成的部分继续下载。服务器不支持RANGE时将从新下载。
                true, // 如果从请求返回信息中获取到文件名，下载完成后自动重命名。
                new RequestCallBack<File>() {

                    @Override
                    public void onStart() {
                        pDialog.show();
                    }

                    @Override
                    public void onLoading(long total, long current, boolean isUploading) {
//                        pDialog.setMax((int)total);
//                        pDialog.show();
//                        Log.d(TAG, "current = " + current + "/total = " + total);
//                        Log.d(TAG, "current1 % = " + ((current / total) * 100));
//                        if (current > 0)
//                            Log.d(TAG, "current2 % = " + ((int) (total * 100 / current)));
//                        pDialog.setProgress((int)current);
                    }

                    @Override
                    public void onSuccess(ResponseInfo<File> responseInfo) {
//                        pDialog.setProgress(100);
                        pDialog.dismiss();
                        onSuccessDownloaded();
                    }


                    @Override
                    public void onFailure(HttpException error, String msg) {
                        if (msg.equals("maybe the file has downloaded completely")) {
                            onSuccessDownloaded();
                        } else {
                            if (file != null && file.exists()) {
                                file.delete();
                            }
                        }
                        pDialog.dismiss();
                    }
                });
    }

    public void cancel() {
        if (handler != null) handler.cancel();
        if (pDialog != null) pDialog.dismiss();
    }

    public abstract void onSuccessDownloaded();
}

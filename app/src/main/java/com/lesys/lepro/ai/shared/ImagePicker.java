package com.lesys.lepro.ai.shared;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.lesys.lepro.ai.BuildConfig;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by karhong on 11/27/14.
 */
public class ImagePicker {

    private static final String TAG = "ImagePicker";
    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private static final String CAMERA_DIR = "/dcim/";
    private static final String ALBUM_NAME = "NancyAI";

    public static final int RC_IMAGE_CAPTURE = 901;
    public static final int RC_IMAGE_SELECT = 902;

    private File mPhotoFile;
    private String mImagePath;
    private Uri mImageUri;
    private InputStream mInputSteam;
    String mCurrentPhotoPath;

    private Context mContext;
    private Fragment mFragment;

    public ImagePicker(Context context) {
        mContext = context;
    }

    public ImagePicker(Fragment fragment) {
        mContext = fragment.getActivity();
        mFragment = fragment;
    }

    /*    public String getImagePath() {
            return mImagePath;
        }

        public void setImagePath(String path) {
            mImagePath = path;
        }
        */
    public Uri getImageUri() {
        return mImageUri;
    }

/*    public File getImageFile() {
        return mPhotoFile;
    }

    public void setImageFile(File file) {
        mPhotoFile = file;
    }*/

    public InputStream getInputStream() {
        return mInputSteam;
    }

    public void setInputStream(InputStream file) {
        mInputSteam = file;
    }

    public void pickByCamera() {

        if (Common.Helpers.checkCameraPermission((Activity) mContext)) {
            Common.Helpers.openPermissionDialog((Activity) mContext).show();
            return;
        }

        mPhotoFile = null;
        mImagePath = null;
        mInputSteam = null;

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {

            // Create the File where the photo should go
            try {
                //mPhotoFile = createImageFile();
//                mPhotoFile = createImageFile();
//                mImagePath = mPhotoFile.getAbsolutePath();
                Uri photoURI;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    photoURI = FileProvider.getUriForFile(mContext,
                            BuildConfig.APPLICATION_ID + ".provider",
                            createImageFileNougat());
                } else {
                    mPhotoFile = createImageFile();
                    mImagePath = mPhotoFile.getAbsolutePath();
                    photoURI = Uri.fromFile(mPhotoFile);
                }

//                mPhotoFile = new File(photoURI.getPath());
//                mImagePath = mPhotoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPhotoFile));


                ((Activity) mContext).startActivityForResult(takePictureIntent, RC_IMAGE_CAPTURE);

            } catch (IOException e) {
                e.printStackTrace();
                mPhotoFile = null;
                mImagePath = null;
                mInputSteam = null;
            }

        }
    }

    public void pickByGallery() {
        //mPhotoFile = null;
        mImagePath = null;
        mInputSteam = null;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), RC_IMAGE_SELECT);

    }

    public void showPickerDialog() {
        final CharSequence[] items = {"Take Photo", "Choose Existing Photo",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    pickByCamera();
                } else if (items[item].equals("Choose Existing Photo")) {
                    pickByGallery();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_IMAGE_SELECT) {
            if (data != null) {
                Uri uri = data.getData();
                mImageUri = uri;
                mImagePath = uri.toString();
                mInputSteam = getPath(uri);
                //mPhotoFile = inputStreamToFile(mInputSteam, getFileExtension(uri));
                //Log.d(TAG, "" + mPhotoFile.toString());

            }
        } else if (requestCode == RC_IMAGE_CAPTURE) {

            if (resultCode != Activity.RESULT_OK && mPhotoFile != null && mPhotoFile.exists()) {
                mImageUri = null;
                mPhotoFile.delete();
            } else {
                try {
                    galleryAddPic();
//                    Log.d(TAG, "" + mPhotoFile.toString());
//                    mInputSteam = bitmapToInputStream(BitmapFactory.decodeFile(mImagePath));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }
    }

    private void startActivityForResult(Intent intent, int requestCode) {
        if (mFragment != null)
            mFragment.startActivityForResult(intent, requestCode);
        else
            ((Activity) mContext).startActivityForResult(intent, requestCode);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
    }

    private File createImageFileNougat() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public File getAlbumDir() {

        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    ALBUM_NAME
            );

            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d(TAG, "failed to create directory");
                    return null;
                }
            }

        } else {
            Log.v(TAG, "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private void galleryAddPic() {
        try {
            Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
            //File f = new File(mImagePath);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                Uri imageUri = Uri.parse(mCurrentPhotoPath);
                mImagePath = imageUri.getPath();
                mPhotoFile = new File(mImagePath);
            }

            System.out.println("image mPhotoFile-" + mPhotoFile);
            System.out.println("image mInputSteam-" + mInputSteam);

            System.out.println("image file size (1) -" + mPhotoFile.length());

            Bitmap b = BitmapFactory.decodeFile(mImagePath);
            int i = (int) (b.getHeight() * (720.0 / b.getWidth()));
            Bitmap bitmap = Bitmap.createScaledBitmap(b, 720, i, true);


            OutputStream os;
            try {
                os = new FileOutputStream(mPhotoFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }
            System.out.println("image file size (2) -" + mPhotoFile.length());


            Uri contentUri = Uri.fromFile(mPhotoFile);
            mImageUri = contentUri;
            mInputSteam = getPath(contentUri);
            //mImagePath = "file://" + mImagePath;
            mediaScanIntent.setData(contentUri);
            mContext.sendBroadcast(mediaScanIntent);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public InputStream getPath(Uri uri) {
        try {
            return mContext.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public File inputStreamToFile(InputStream inputStream, String extension) {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File file = new File(mContext.getCacheDir(), "PlusixtyImage_" + timeStamp + ".jpg");

        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;

            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            out.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    public InputStream bitmapToInputStream(Bitmap bitmap) {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();

        return new ByteArrayInputStream(bitmapdata);
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = mContext.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
}

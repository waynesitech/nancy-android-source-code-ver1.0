package com.lesys.lepro.ai.model;

import java.io.Serializable;

public class JMsg implements Serializable {

    public String subj;
    public String body;
}

package com.lesys.lepro.ai.module.e_stamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.EStampPricing;
import com.lesys.lepro.ai.model.EStampPricingCover;
import com.lesys.lepro.ai.model.EStampPricingResultCover;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.module.tenancy_agreement.ViewPDFActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.ipay.IPayUtil;

import java.text.DecimalFormat;

/**
 * Created by Nicholas on 12/07/2017.
 */

public class EStampActivity extends BaseActivity implements View.OnClickListener {

    TextView mStampingFee;
    LinearLayout mPenaltyLayout;
    TextView mPenaltyFee;
    TextView mDuplicates;
    AppCompatSpinner mTotalCopies;
    TextView mTotal1;
    TextView mServiceCharge;
    TextView mTotal2;
    Button mNextBtn;

    Order mOrder;
    EStampPricingCover mEStampPricingCover;
    EStampPricing mPricing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_stamp);

        mOrder = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);

        mStampingFee = (TextView) findViewById(R.id.stamping_fee);
        mPenaltyLayout = (LinearLayout) findViewById(R.id.penalty_layout);
        mPenaltyFee = (TextView) findViewById(R.id.penalty_fee);
        mDuplicates = (TextView) findViewById(R.id.duplicates);
        mTotalCopies = (AppCompatSpinner) findViewById(R.id.total_copies);
        mTotal1 = (TextView) findViewById(R.id.total_1);
        mServiceCharge = (TextView) findViewById(R.id.service_charge);
        mTotal2 = (TextView) findViewById(R.id.total_2);
        mNextBtn = (Button) findViewById(R.id.next_btn);

        mNextBtn.setOnClickListener(this);

        getEstampingPrice();
    }

    void getEstampingPrice() {
        BaseApi.init(this)
                .getEstampingPrice(mOrder.getId())
                .call(new DataHandler<EStampPricingResultCover>(EStampPricingResultCover.class) {
                    @Override
                    public void onSuccess(EStampPricingResultCover data) {
                        mEStampPricingCover = data.getResult();
                        mPricing = mEStampPricingCover.getPricing();

                        mTotalCopies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                recalculatePricing();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        mTotalCopies.setSelection(1);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    void recalculatePricing() {
        mStampingFee.setText(getString(R.string.rm_param_2, Common.Helpers.getIntegerToDouble2Decimal(mPricing.getStampingFee())));
        mPenaltyFee.setText(getString(R.string.rm_param_2, Common.Helpers.getIntegerToDouble2Decimal(mPricing.getPenaltyFee())));
        mServiceCharge.setText(getString(R.string.rm_param_2, Common.Helpers.getIntegerToDouble2Decimal(mPricing.getServiceFee())));

        int duplicatesFee = mPricing.getStampingDuplicateFee() * Integer.valueOf(mTotalCopies.getSelectedItem().toString());
        mDuplicates.setText(getString(R.string.rm_param_2, Common.Helpers.getIntegerToDouble2Decimal(duplicatesFee)));

        int total1Price = mPricing.getStampingFee() +
                mPricing.getPenaltyFee() +
                duplicatesFee;
        int total2Price = total1Price + mPricing.getServiceFee();
        mTotal1.setText(getString(R.string.rm_param_2, Common.Helpers.getIntegerToDouble2Decimal(total1Price)));
        mTotal2.setText(getString(R.string.rm_param_2, Common.Helpers.getIntegerToDouble2Decimal(total2Price)));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_PAYMENT: {
                    finish();
                    break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                int duplicatesFee = mPricing.getStampingDuplicateFee() * Integer.valueOf(mTotalCopies.getSelectedItem().toString());
                int total1Price = mPricing.getStampingFee() +
                        mPricing.getPenaltyFee() +
                        duplicatesFee;
                int total2Price = total1Price + mPricing.getServiceFee();
                new IPayUtil(this).paymentProcess(mOrder, mEStampPricingCover, total2Price + "");
                break;
            }
        }
    }
}

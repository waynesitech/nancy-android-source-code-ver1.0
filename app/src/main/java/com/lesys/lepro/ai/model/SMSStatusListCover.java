package com.lesys.lepro.ai.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 02/07/2017.
 */

public class SMSStatusListCover implements Serializable {

    private List<SMSStatus> smsStatus = new ArrayList<>();

    public List<SMSStatus> getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(List<SMSStatus> smsStatus) {
        this.smsStatus = smsStatus;
    }

}

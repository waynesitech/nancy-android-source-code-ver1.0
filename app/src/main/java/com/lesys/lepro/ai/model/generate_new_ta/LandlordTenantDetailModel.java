package com.lesys.lepro.ai.model.generate_new_ta;

import com.lesys.lepro.ai.model.OrderFullDetail;
import com.lesys.lepro.ai.model.generate_new_ta.param_api.LandlordTenant;

import java.io.Serializable;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class LandlordTenantDetailModel implements Serializable {

    public String name;
    public String icNum;
    public String address;
    public String countryCode;
    public String contactNum;
    public String email;
    public boolean isCompany;
    public String companyName;
    public String companyRegNum;

    public LandlordTenantDetailModel() {

    }

    public LandlordTenantDetailModel(LandlordTenant landlordTenant) {

        //Todo : still have contactNum
        this.name = landlordTenant.getName();
        this.icNum = landlordTenant.getIcNum();
        this.address = landlordTenant.getAddress();
        this.contactNum = landlordTenant.getContactNum();
        this.email = landlordTenant.getEmail();
        this.isCompany = landlordTenant.isCompany();
        this.companyName = landlordTenant.getCompanyName();
        this.companyRegNum = landlordTenant.getCompanyRegNum();
    }
}

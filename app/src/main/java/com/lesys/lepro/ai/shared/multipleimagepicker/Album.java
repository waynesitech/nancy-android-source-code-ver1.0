package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by yongtat on 7/24/15.
 */
public class Album implements Parcelable {

    public String id;
    public String name;
    public String thumbnail;
    public Long coverID;
    public int count;
    public ArrayList<CustomGallery> images = new ArrayList<>();

    public static final Creator CREATOR = new Creator() {
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.thumbnail);
        dest.writeLong(this.coverID);
        dest.writeInt(this.count);
        dest.writeTypedList(images);
    }

    public Album() {
    }

    public Album(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.thumbnail = in.readString();
        this.coverID = in.readLong();
        this.count = in.readInt();
        in.readTypedList(this.images, CustomGallery.CREATOR);
    }
}

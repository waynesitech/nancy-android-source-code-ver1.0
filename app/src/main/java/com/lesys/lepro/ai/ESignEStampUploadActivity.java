package com.lesys.lepro.ai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.noelchew.permisowrapper.PermisoWrapper;

import java.io.File;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

/**
 * Created by Nicholas on 09/07/2017.
 */

public class ESignEStampUploadActivity extends BaseActivity implements View.OnClickListener {

    TextView mFileName;
    ProgressBar mProgressBar;
    Button mInternalStorageBtn;
    Button mNextBtn;

    String title;
    String uploadType;
    File file;
    private ArrayList<String> docPaths = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_sign_e_stamp_upload);

        title = getIntent().getStringExtra(GlobalParam.INTENT_TITLE);
        uploadType = getIntent().getStringExtra(GlobalParam.INTENT_UPLOAD_TYPE);
        Permiso.getInstance().setActivity(this);
        setTitle(title);

        mFileName = (TextView) findViewById(R.id.file_name);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mInternalStorageBtn = (Button) findViewById(R.id.internal_storage_btn);
        mNextBtn = (Button) findViewById(R.id.next_btn);

        mInternalStorageBtn.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);
    }

    public void onPickDoc() {
        String[] pdfs = {".pdf"};
        FilePickerBuilder.getInstance().setMaxCount(1)
//                .setSelectedFiles(docPaths)
                .setActivityTheme(R.style.FilePickerTheme)
                .addFileSupport("PDF", pdfs)
                .enableDocSupport(false)
                .pickFile(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths.clear();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                break;
        }

        if (docPaths.size() != 0) {
            mFileName.setText(docPaths.get(0));
            file = new File(docPaths.get(0));
        }
    }

    int currentProgress;
    CountDownTimer mCountDownTimer;

    void dummyUpdateProgress() {

        if (mCountDownTimer == null) {
            mCountDownTimer = new CountDownTimer(10, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    mProgressBar.setProgress(currentProgress++);
                    if (currentProgress >= 100) {
                        startActivity(new Intent(ESignEStampUploadActivity.this, GenerateNewTAActivity.class));
                        finish();
                    } else {
                        dummyUpdateProgress();
                    }
                }
            };
        } else {
            mCountDownTimer.cancel();
        }
        mCountDownTimer.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                if (mNextBtn.getText().toString().equals(getString(R.string.next))) {
                    mNextBtn.setText(R.string.cancel);
                    if (file == null || file != null && !file.exists()) {
                        Toast.makeText(this, R.string.toast_please_choose_a_pdf_from_internal_storage, Toast.LENGTH_LONG).show();
                        return;
                    }
                    currentProgress = 0;
                    dummyUpdateProgress();
                } else {
                    mNextBtn.setText(R.string.next);
                    mProgressBar.setProgress(0);
                    if (mCountDownTimer != null) {
                        mCountDownTimer.cancel();
                    }
                }
                break;
            }
            case R.id.internal_storage_btn: {
                PermisoWrapper.getPermissionSaveMediaToStorage(this, new PermisoWrapper.PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        onPickDoc();
                    }

                    @Override
                    public void onPermissionDenied() {

                    }
                });
                break;
            }
        }
    }
}

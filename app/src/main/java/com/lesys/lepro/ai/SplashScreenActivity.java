package com.lesys.lepro.ai;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.module.authentication.LoginActivity;
import com.lesys.lepro.ai.module.authentication.PasscodeSignInActivity;
import com.lesys.lepro.ai.module.authentication.PhoneVerificationActivity;
import com.lesys.lepro.ai.module.authentication.SignUpActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.widget.SplashScreenView;
import com.lesys.lepro.ai.widget.SplashScreenViewInterface;

import org.json.JSONObject;

public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = SplashScreenActivity.class.getName();

    SplashScreenView mSplashScreenView;
    CountDownTimer mCountDownTimer;
    User mUser = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
////        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//        View decorView = getWindow().getDecorView();
//        // Hide the status bar.
//         int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN; decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mSplashScreenView = (SplashScreenView) findViewById(R.id.splashscreen_view);
        mSplashScreenView.setSplashScreenViewInterface(new SplashScreenViewInterface() {
            @Override
            public void onClickLogin() {
                Intent intent = new Intent();
                intent.setClass(SplashScreenActivity.this, LoginActivity.class);
                startActivityForResult(intent, GlobalParam.RC_LOGIN);
            }

            @Override
            public void onClickSignUp() {
                Intent intent = new Intent();
                intent.setClass(SplashScreenActivity.this, SignUpActivity.class);
                startActivityForResult(intent, GlobalParam.RC_REGISTED);
            }

            @Override
            public void onClickESign() {
                Intent intent = new Intent();
                intent.setClass(SplashScreenActivity.this, PasscodeSignInActivity.class);
                startActivityForResult(intent, GlobalParam.RC_ESIGN);
            }
        });
        try {
            if (!Common.Helpers.checkPermission(this)) {
//                checkAppVersion();
                splashScreenCountDown();
            }
        } catch (Exception e) {
//            checkAppVersion();
            splashScreenCountDown();
        }
//        splashScreenCountDown();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
//                checkAppVersion();
                splashScreenCountDown();
                break;
        }
    }

    void splashScreenCountDown() {
        mCountDownTimer = new CountDownTimer(1000, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    mUser = new User().getUser(SplashScreenActivity.this);
                    if (mUser.getId() != 0 &&
                            (!mUser.getIsPhoneVerify().equals("false") &&
                                    mUser.getRole().getName().equals(GlobalParam.UserRole.ROLE_MEMBER)) ||
                            mUser.getRole().getName().equals(GlobalParam.UserRole.ROLE_LESYS_TEAM)) {
                        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        finish();
                    } else {
                        mSplashScreenView.startBtnDisplay();
//                    Intent intent = new Intent(SplashScreenActivity.this, SignInActivity.class);
//                    startActivity(intent);
//                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mSplashScreenView.startBtnDisplay();
                }

            }
        }.start();
    }

    private void checkAppVersion() {
        BaseApi.init(this).
                getAppVersion().
                call(new BaseApi.JsonResponseListener() {

                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            if (response.has("ANDROID_VERSION_CODE")) {

                                int latestVersionCode = response.getInt("ANDROID_VERSION_CODE");

                                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                                int currentVersionCode = pInfo.versionCode;

                                System.out.println("kelvyndebug:latestVersionCode->" + latestVersionCode);
                                System.out.println("kelvyndebug:currentVersionCode->" + currentVersionCode);

                                if (currentVersionCode >= latestVersionCode) {
//                            startMainPage();
                                    splashScreenCountDown();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);

                                    builder.setTitle("New app version available");
                                    builder.setCancelable(false);
                                    builder.setMessage(getResources().getString(R.string.confirm_update));
                                    builder.setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setData(Uri.parse("market://details?id=com.lesys.lepro"));
                                            startActivity(intent);
                                            finish();
                                        }
                                    });
                                    builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    });
                                    builder.show();
                                }

                            } else {
                                splashScreenCountDown();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            splashScreenCountDown();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, JSONObject response) {
                        splashScreenCountDown();
                    }


                });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_LOGIN:
                case GlobalParam.RC_VERIFIED: {
                    finish();
                    break;
                }
                case GlobalParam.RC_REGISTED: {
                    startActivityForResult(new Intent(this, PhoneVerificationActivity.class), GlobalParam.RC_VERIFIED);
                    break;
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();
    }


    @Override
    protected void onStop() {
        super.onStop();

    }
}

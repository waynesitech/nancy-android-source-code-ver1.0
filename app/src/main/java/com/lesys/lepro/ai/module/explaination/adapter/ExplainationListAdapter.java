package com.lesys.lepro.ai.module.explaination.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.MaterialCommunityIcons;
import com.karumi.headerrecyclerview.HeaderRecyclerViewAdapter;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.Explanation;
import com.lesys.lepro.ai.shared.MediaPlayerUtil;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

/**
 * Created by NicholasTan on 28/04/2016.
 */
public class ExplainationListAdapter extends
        HeaderRecyclerViewAdapter<RecyclerView.ViewHolder, String, Explanation, String> {

    private final String TAG = ExplainationListAdapter.class.getName();
    private Context mContext;
    Transformation transformation = new RoundedTransformationBuilder()
            .borderColor(Color.WHITE)
            .borderWidthDp(2)
            .cornerRadiusDp(180)
            .oval(false)
            .build();
    private MediaPlayerUtil mMediaPlayerUtil;
    int currentPlayPosition = -1;

    Drawable mStopDrawable;
    Drawable mPlayDrawable;

    public ExplainationListAdapter(final Context context, MediaPlayerUtil mMediaPlayerUtil) {
        this.mContext = context;
        this.mMediaPlayerUtil = mMediaPlayerUtil;

        mStopDrawable = new IconDrawable(mContext, MaterialCommunityIcons.mdi_stop)
                .colorRes(R.color.colorAccent).sizeDp(25);
        mPlayDrawable = new IconDrawable(mContext, MaterialCommunityIcons.mdi_play)
                .colorRes(R.color.colorAccent).sizeDp(25);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_explaination_list, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_explaination_list, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loadmore, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
    }


    @Override
    protected void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CustomViewHolder viewHolder = (CustomViewHolder) holder;
        final Explanation data = getItem(position);
        viewHolder.itemViewControl(data, position);
    }


    @Override
    protected void onBindFooterViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    protected void onHeaderViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onItemViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onFooterViewRecycled(RecyclerView.ViewHolder holder) {
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        protected View mRootView;
        protected ImageView mVoiceBtn;
        protected TextView mIndex;
        protected TextView mDesc;

        public CustomViewHolder(View v) {
            super(v);
            try {
                this.mRootView = v;
                this.mVoiceBtn = (ImageView) v.findViewById(R.id.voice_btn);
                this.mIndex = (TextView) v.findViewById(R.id.index);
                this.mDesc = (TextView) v.findViewById(R.id.desc);
            } catch (Exception e) {

            }
        }

        public void itemViewControl(final Explanation data, final int position) {
            try {
                mIndex.setText(data.getExplanationNumber());
                mDesc.setText(data.getExplanationText());
                mVoiceBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentPlayPosition != position) {
                            currentPlayPosition = position;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mMediaPlayerUtil.playAudio(data.getExplanationURL(), position, onMediaPlayerListener);
                                }
                            }).start();
                        } else {
                            if (mMediaPlayerUtil.isAudioPlaying()) {
                                mMediaPlayerUtil.stopAudio(onMediaPlayerListener);
                                currentPlayPosition = -1;
                            } else {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mMediaPlayerUtil.playAudio(data.getExplanationURL(), position, onMediaPlayerListener);
                                    }
                                }).start();
                            }
                        }

                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (currentPlayPosition != position) {
//                                    currentPlayPosition = position;
//                                    mMediaPlayerUtil.playAudio(data.getExplanationURL());
//                                } else {
//                                    if (mMediaPlayerUtil.isAudioPlaying()) {
//                                        mMediaPlayerUtil.stopAudio();
//                                        currentPlayPosition = -1;
//                                    } else {
//                                        mMediaPlayerUtil.playAudio(data.getExplanationURL());
//                                    }
//                                }
//
//                                if (currentPlayPosition != -1) {
//                                    mMediaPlayerUtil.audioListener(new MediaPlayerUtil.OnMediaPlayerListener() {
//                                        @Override
//                                        public void onStop() {
//                                            currentPlayPosition = -1;
//                                            ((Activity) mContext).runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    notifyDataSetChanged();
//                                                }
//                                            });
//                                        }
//                                    });
//                                }
//
//                                ((Activity) mContext).runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        notifyDataSetChanged();
//                                    }
//                                });
//                            }
//                        }).start();
                    }
                });

                if (currentPlayPosition == position) {
                    mVoiceBtn.setImageDrawable(mStopDrawable);
                } else {
                    mVoiceBtn.setImageDrawable(mPlayDrawable);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public MediaPlayerUtil.OnMediaPlayerListener onMediaPlayerListener = new MediaPlayerUtil.OnMediaPlayerListener() {
        @Override
        public void onStop() {
            currentPlayPosition = -1;
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onPlay(int position) {
            currentPlayPosition = position;
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }


    };
}
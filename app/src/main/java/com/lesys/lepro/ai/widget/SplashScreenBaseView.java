package com.lesys.lepro.ai.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by NicholasTan on 20/04/2016.
 */
public abstract class SplashScreenBaseView extends RelativeLayout {

    public SplashScreenBaseView(Context context) {
        super(context);

    }

    public SplashScreenBaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        try {
            // Init game
            getPageContent();
        } catch (Exception e) {
            // bug
            e.printStackTrace();
        }
    }

    //
    abstract protected void getPageContent();
}

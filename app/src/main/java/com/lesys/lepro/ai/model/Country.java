package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class Country implements Serializable {

    private String id;
    private String country_name;
    private String country_code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

}

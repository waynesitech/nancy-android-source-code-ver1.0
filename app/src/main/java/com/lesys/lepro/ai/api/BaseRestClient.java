package com.lesys.lepro.ai.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Toast;

import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.shared.Common;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.Header;
import org.apache.http.cookie.Cookie;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class BaseRestClient {


    public static final String BASE_URL = "https://nancy.com.my:443/api";
    //    public static final String BASE_URL = "https://staging.lesystenancy.com:443/api";
    //        public static final String BASE_URL = "https://staging.lesystenancy.com:443/api";//live
    public static final String WEB_URL = "https://staging.lesystenancy.com";
    private static final String TAG = BaseRestClient.class.getName();

    public static AsyncHttpClient client = new AsyncHttpClient();
    public static PersistentCookieStore cookieStore;
    private Context mContext;

    RequestParams mParams = new RequestParams();

    public BaseRestClient(Context context) {
        this.mContext = context;
        cookieStore = new PersistentCookieStore(context);
        setCookieStore();
        client.setTimeout(30000);
    }

    public BaseRestClient syncHttpClient() {
        client = new SyncHttpClient();
        client.setCookieStore(cookieStore);
        return this;
    }

    public static String getImage(int imageId) {
        return BASE_URL + "/viewImage/" + imageId + "/" + 250;
    }

    public void addParam(String name, String value) {
        mParams.put(name, value);
    }

    public void addParam(String name, Bitmap value) {
        mParams.put(name, value);
    }

    public void addParam(String name, double value) {
        mParams.put(name, String.valueOf(value));
    }

    public void addParam(String name, long value) {
        mParams.put(name, String.valueOf(value));
    }

    public void addParam(String name, int value) {
        mParams.put(name, String.valueOf(value));
    }


    public void addParam(String name, ArrayList<Integer> value) {
        mParams.put(name, value);
    }

    public void addParam(String name, int[] things) {
        mParams.put(name, things);
    }

    public void addParam(String name, File value) {
        try {
            mParams.put(name, value);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void addParam(String name, boolean value) {
        mParams.put(name, value);
    }

    public void addParam(String name, Drawable things) {
        mParams.put(name, things);
    }

    public void addParam(String name, byte[] things) {
        mParams.put(name, things);
    }

    public void addParam(String name, InputStream value) {
        mParams.put(name, value);
    }

    public void addParam(String name, List<InputStream> value) {
        mParams.put(name, value);
    }

    public void setCookieStore() {
        client.setCookieStore(cookieStore);
    }

    public PersistentCookieStore getCookieStore() {
        return cookieStore;
    }

    public List<Cookie> getCookies() {
        return cookieStore.getCookies();
    }

    public void clearCookies() {
        cookieStore.clear();
    }

    public void get(String url, AsyncHttpResponseHandler responseHandler) {

        if (Common.Helpers.checkInternet(mContext)) {
            Log.d(TAG, getAbsoluteUrl(url));
            Log.d(TAG, mParams.toString());
            client.get(getAbsoluteUrl(url), mParams, responseHandler);
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.please_connect_to_the_internet), Toast.LENGTH_LONG).show();
            try {
                ((JsonHttpResponseHandler) responseHandler).onFailure(400, new Header[0], new Throwable(), new JSONObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void post(String url, AsyncHttpResponseHandler responseHandler) {
        if (Common.Helpers.checkInternet(mContext)) {
            Log.d(TAG, getAbsoluteUrl(url));
            Log.d(TAG, mParams.toString());
            client.post(getAbsoluteUrl(url), mParams, responseHandler);

        } else {
            Toast.makeText(mContext, mContext.getString(R.string.please_connect_to_the_internet), Toast.LENGTH_LONG).show();
            try {
                ((JsonHttpResponseHandler) responseHandler).onFailure(400, new Header[0], new Throwable(), new JSONObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getCustom(String url, AsyncHttpResponseHandler responseHandler) {

        if (Common.Helpers.checkInternet(mContext)) {
            Log.d(TAG, getCustomUrl(url));
            Log.d(TAG, mParams.toString());
            if (header != null && header.length() != 0 && value != null && value.length() != 0)
                client.addHeader(header, value);
            client.get(getCustomUrl(url), mParams, responseHandler);

        } else {
            Toast.makeText(mContext, mContext.getString(R.string.please_connect_to_the_internet), Toast.LENGTH_LONG).show();
            try {
                ((JsonHttpResponseHandler) responseHandler).onFailure(400, new Header[0], new Throwable(), new JSONObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    String header, value;

    public void setHeader(String header, String value) {
        this.header = header;
        this.value = value;
    }


    public void postCustom(String url, AsyncHttpResponseHandler responseHandler) {
        if (Common.Helpers.checkInternet(mContext)) {
            Log.d(TAG, getCustomUrl(url));
            Log.d(TAG, mParams.toString());
            if (header != null && header.length() != 0 && value != null && value.length() != 0)
                client.addHeader(header, value);
            client.post(getCustomUrl(url), mParams, responseHandler);


        } else {
            Toast.makeText(mContext, mContext.getString(R.string.please_connect_to_the_internet), Toast.LENGTH_LONG).show();
            try {
                ((JsonHttpResponseHandler) responseHandler).onFailure(400, new Header[0], new Throwable(), new JSONObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

	/*  public static void post(String url, AsyncHttpResponseHandler responseHandler) {
          client.post(getAbsoluteUrl(url), responseHandler);
	  } */

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    private static String getCustomUrl(String relativeUrl) {
        return relativeUrl;
    }

}

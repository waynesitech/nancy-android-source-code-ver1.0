package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class OrderCover implements Serializable {

    private Order order;
    private String orderUserID;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getOrderUserID() {
        return orderUserID;
    }

    public void setOrderUserID(String orderUserID) {
        this.orderUserID = orderUserID;
    }
}

package com.lesys.lepro.ai;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.MaterialCommunityModule;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

/**
 * Created by NicholasTan on 16/02/2016.
 */
public class BaseApplication extends Application {
    private static BaseApplication mInstance = null;

    public synchronized static BaseApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Iconify.with(new MaterialCommunityModule());
//                .with(new FontAwesomeModule())
//                .with(new EntypoModule())
//                .with(new TypiconsModule())
//                .with(new MaterialModule())
//                .with(new MeteoconsModule())
//                .with(new WeathericonsModule())
//                .with(new SimpleLineIconsModule())
//                .with(new IoniconsModule());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

}

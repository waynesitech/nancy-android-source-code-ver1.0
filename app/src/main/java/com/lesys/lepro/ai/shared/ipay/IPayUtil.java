package com.lesys.lepro.ai.shared.ipay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.ipay.Ipay;
import com.ipay.IpayAcitivity;
import com.ipay.IpayPayment;
import com.lesys.lepro.ai.model.EStampPricingCover;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.model.User;

/**
 * Created by Nicholas on 02/07/2017.
 */

public class IPayUtil {

    private static final String TAG = IPayUtil.class.getName();
    public IPayPaymentListener mIPayPaymentListener;
    Class<IpayAcitivity> mIPayActivity;
    Activity mActivity;

    public IPayUtil(Activity mContext) {
        this.mActivity = mContext;
    }

    public void paymentProcess(Order order) {
        User mUser = new User().getUser(mActivity);

        IpayPayment payment = new IpayPayment();
        payment.setMerchantKey("Mp7FIn6fFM");
        payment.setMerchantCode("M08747");
        payment.setPaymentId("16");
        payment.setCurrency("MYR");
        payment.setRefNo(order.getOrder_num());
//                                payment.setAmount(appPreferences.getOrderSubmissionPrice());
        payment.setAmount("1");
        payment.setProdDesc("Order Submission");
        payment.setUserName(mUser.getName());
        payment.setUserEmail(mUser.getEmail());
        payment.setUserContact(mUser.getMobile());
        payment.setRemark(order.getIpayRemark());
        payment.setCountry("MY");
        payment.setBackendPostURL(order.getIpayBackendURL());

        mIPayActivity = IpayAcitivity.class;

        mIPayPaymentListener = new IPayPaymentListener() {
            @Override
            public void onSuccessed() {
                Log.d(TAG, "Payment onSuccessed");
                mActivity.setResult(Activity.RESULT_OK);
                mActivity.finish();
            }

            @Override
            public void onFailed(String error) {
                Log.d(TAG, "Payment onFailed = " + error);
                Toast.makeText(mActivity, error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCanceled() {
                Log.d(TAG, "Payment onCanceled");
            }

        };

        IPayResultDelegateCover.ResultDelegate resultDelegate = new IPayResultDelegateCover.ResultDelegate(mIPayPaymentListener);
        Intent checkoutIntent = Ipay.getInstance().checkout(payment, mActivity, resultDelegate);
        mActivity.startActivityForResult(checkoutIntent, 1);
    }

    public void paymentProcess(Order order, EStampPricingCover eStampPricingCover, String price) {
        User mUser = new User().getUser(mActivity);

        IpayPayment payment = new IpayPayment();
        payment.setMerchantKey("Mp7FIn6fFM");
        payment.setMerchantCode("M08747");
        payment.setPaymentId("16");
        payment.setCurrency("MYR");
        payment.setRefNo(order.getOrder_num());
//                                payment.setAmount(appPreferences.getOrderSubmissionPrice());
        payment.setAmount(price);
        payment.setProdDesc("E-stamping Subscription");
        payment.setUserName(mUser.getName());
        payment.setUserEmail(mUser.getEmail());
        payment.setUserContact(mUser.getMobile());
        payment.setRemark(eStampPricingCover.getIpayRemark());
        payment.setCountry("MY");
        payment.setBackendPostURL(eStampPricingCover.getIpayEstampingBackendURL());

        mIPayActivity = IpayAcitivity.class;

        mIPayPaymentListener = new IPayPaymentListener() {
            @Override
            public void onSuccessed() {
                Log.d(TAG, "Payment onSuccessed");
                mActivity.setResult(Activity.RESULT_OK);
                mActivity.finish();
            }

            @Override
            public void onFailed(String error) {
                Log.d(TAG, "Payment onFailed = " + error);
                Toast.makeText(mActivity, error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCanceled() {
                Log.d(TAG, "Payment onCanceled");
            }

        };

        IPayResultDelegateCover.ResultDelegate resultDelegate = new IPayResultDelegateCover.ResultDelegate(mIPayPaymentListener);
        Intent checkoutIntent = Ipay.getInstance().checkout(payment, mActivity, resultDelegate);
        mActivity.startActivityForResult(checkoutIntent, 1);
    }

}

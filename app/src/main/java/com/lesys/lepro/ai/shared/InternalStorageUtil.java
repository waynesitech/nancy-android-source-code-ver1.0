package com.lesys.lepro.ai.shared;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class InternalStorageUtil {
    private String sdPath;
    private String imgPath;
    private String audioPath;
    private String videoPath;
    private String pdfPath;
    private String signaturePath;

    public InternalStorageUtil(Context mContext) {
        sdPath = Common.Helpers.getFilesDirectory(mContext).getPath();
        imgPath = sdPath + "/Image/";
        audioPath = sdPath + "/Audio/";
        videoPath = sdPath + "/Video/";
        pdfPath = sdPath + "/PDF/";
        signaturePath = sdPath + "/SIGNATURE/";
    }

    public void createFolder(String path) {
        File path1 = new File(path);
        if (!path1.exists()) {
            path1.mkdirs();
        }
    }

    public String saveImagePath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(imgPath);
        return imgPath + filePath;
    }

    public String saveAudioPath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(audioPath);
        return audioPath + filePath;
    }

    public String saveVideoPath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(videoPath);
        return videoPath + filePath;
    }

    public String savePDFPath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(pdfPath);
        return pdfPath + filePath;
    }

    public String getPDFFile(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(pdfPath);

        File file = new File(pdfPath + filePath);
        if (file.exists()) {
            return pdfPath + filePath;
        }
        return null;
    }

    public String saveSignaturePath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(signaturePath);
        return signaturePath + filePath;
    }

    public String getSignatureFile(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(signaturePath);

        File file = new File(signaturePath + filePath);
        if (file.exists()) {
            return signaturePath + filePath;
        }
        return null;
    }

    public String getImagePath() {
        createFolder(imgPath);
        return imgPath;
    }

    public String getAudioPath() {
        createFolder(audioPath);
        return audioPath;
    }

    public String getVideoPath() {
        createFolder(videoPath);
        return videoPath;
    }

    public String getPDFPath() {
        createFolder(pdfPath);
        return pdfPath;
    }
}

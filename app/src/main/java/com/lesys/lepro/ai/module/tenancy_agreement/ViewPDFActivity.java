package com.lesys.lepro.ai.module.tenancy_agreement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.ScrollBar;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.model.OrderFullDetail;
import com.lesys.lepro.ai.model.OrderFullDetailResultCover;
import com.lesys.lepro.ai.module.e_sign.DigitalSignatureActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.FileDownloader;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.ipay.IPayUtil;
import com.noelchew.permisowrapper.PermisoWrapper;

import java.io.File;

/**
 * Created by NicholasTan on 03/03/2016.
 */
public class ViewPDFActivity extends BaseActivity implements
        OnPageChangeListener,
        View.OnClickListener {
    public static final int RC_DIGITAL_SIGNATURE = 1001;
    PDFView pdfView;
    Button mChangeBtn;
    Button mConfirmBtn;
    LinearLayout mBottomLayout;

    Integer pageNumber = 0;
    Menu menu;

    Order order;
    String pdfPath;
    boolean isLoginWithPassCode;
    String orderUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_pdf);
        Permiso.getInstance().setActivity(this);

        order = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);
        orderUserID = getIntent().getStringExtra(GlobalParam.INTENT_ORDER_USER_ID);
        pdfPath = getIntent().getStringExtra(GlobalParam.INTENT_PDF_PATH);
        isLoginWithPassCode = getIntent().getBooleanExtra(GlobalParam.INTENT_IS_LOGIN_WITH_PASSCODE, false);

        pdfView = (PDFView) findViewById(R.id.pdfView);
        mChangeBtn = (Button) findViewById(R.id.change_btn);
        mConfirmBtn = (Button) findViewById(R.id.confirm_btn);
        mBottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);

        mChangeBtn.setOnClickListener(this);
        mConfirmBtn.setOnClickListener(this);

        pdfView.useBestQuality(true);
        pdfView.fromFile(new File(pdfPath))
                .enableSwipe(true)
                .enableDoubletap(true)
                .swipeVertical(false)
                .defaultPage(1)
                .showMinimap(false)
                .onPageChange(this)
                .enableAnnotationRendering(false)
                .password(null)
                .showPageWithAnimation(true)
                .load();

        mBottomLayout.setVisibility(!order.getTenancyAgreementStatus().equals(GlobalParam.TAStatus.READY) || isLoginWithPassCode
                ? View.GONE : View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
//            case R.id.menu_sign_agreement:
//                clickSignStep1Dialog();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s / %s", page, pageCount));
        try {
            menu.clear();
        } catch (Exception e) {

        }

        if (page == pageCount && isLoginWithPassCode) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_view_pdf, menu);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.dialog_process_to_sign_agreement)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ViewPDFActivity.this, DigitalSignatureActivity.class);
                            intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                            intent.putExtra(GlobalParam.INTENT_ORDER_USER_ID, orderUserID);
                            startActivityForResult(intent, GlobalParam.RC_DIGITAL_SIGNATURE);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.create().show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isLoginWithPassCode && Common.Helpers.isAppIsInBackground(this)) {
            Intent intent = new Intent();
            intent.setAction(GlobalParam.ACTION_BACKGROUND);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_DIGITAL_SIGNATURE: {
                    Intent intent = new Intent();
                    if (data.getAction() != null &&
                            data.getAction().equals(GlobalParam.ACTION_BACKGROUND)) {
                        intent.setAction(GlobalParam.ACTION_BACKGROUND);
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                }
                case GlobalParam.RC_PAYMENT: {
                    setResult(RESULT_OK);
                    finish();
                    break;
                }
            }
        }
    }

    void displayConfirmDialog() {
        PermisoWrapper.getPermissionGetPhoneNumber(this, new PermisoWrapper.PermissionListener() {
            @Override
            public void onPermissionGranted() {
                new AlertDialog.Builder(ViewPDFActivity.this)
                        .setMessage(R.string.dialog_confirm_pdf)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new IPayUtil(ViewPDFActivity.this).paymentProcess(order);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create().show();
            }

            @Override
            public void onPermissionDenied() {

            }
        });

    }

    void getFullDetail() {
        BaseApi.init(this)
                .getFullDetail(order.getId())
                .call(new DataHandler<OrderFullDetailResultCover>(OrderFullDetailResultCover.class) {
                    @Override
                    public void onSuccess(OrderFullDetailResultCover data) {
                        Intent intent = new Intent(ViewPDFActivity.this, GenerateNewTAActivity.class);
                        intent.putExtra(GlobalParam.INTENT_IS_TA_EDIT, true);
                        intent.putExtra(GlobalParam.INTENT_ORDER_FULL_DETAIL_DATA, data.getResult().getOrder());
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Toast.makeText(ViewPDFActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_btn: {
                getFullDetail();
                break;
            }
            case R.id.confirm_btn: {
                displayConfirmDialog();
                break;
            }
        }
    }
}

package com.lesys.lepro.ai.model;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class User implements Serializable {

    private long id;
    private String name;
    private String email;
    private String mobile;
    private String image;
    private Simple role;
    private String isPhoneVerify;
    private int status;

    public void saveUser(Context mContext, User user) {
        SharedPreferencesUtil.setString(mContext, GlobalParam.KEY_USER_DATA, new Gson().toJson(user).toString());
    }

    public User getUser(Context mContext) {
        String userData = SharedPreferencesUtil.getString(mContext, GlobalParam.KEY_USER_DATA);
        if (TextUtils.isEmpty(userData)) {
            return new User();
        }

        return new Gson().fromJson(userData, User.class);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Simple getRole() {
        return role;
    }

    public void setRole(Simple role) {
        this.role = role;
    }

    public String getIsPhoneVerify() {
        return isPhoneVerify;
    }

    public void setIsPhoneVerify(String isPhoneVerify) {
        this.isPhoneVerify = isPhoneVerify;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}

package com.lesys.lepro.ai.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Nicholas on 20/08/2017.
 */

public class SpecialRequestCover implements Serializable {

    private ArrayList<SpecialRequest> arrayList = new ArrayList<>();

    public ArrayList<SpecialRequest> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<SpecialRequest> arrayList) {
        this.arrayList = arrayList;
    }

}

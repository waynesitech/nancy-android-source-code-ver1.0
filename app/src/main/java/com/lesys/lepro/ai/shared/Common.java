package com.lesys.lepro.ai.shared;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;

import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.listener.OnFileCopyListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {

    public static final String TAG = "Common";
    public static final String PLATFORM = "android";

    public static class Permission {
        public static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
        public static final String PERMISSION_READ_CONTACTS = Manifest.permission.READ_CONTACTS;
        public static final String PERMISSION_GET_ACCOUNTS = Manifest.permission.GET_ACCOUNTS;
        public static final String PERMISSION_ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
        public static final String PERMISSION_ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
        public static final String PERMISSION_RECORD_AUDIO = Manifest.permission.RECORD_AUDIO;
        public static final String PERMISSION_SEND_SMS = Manifest.permission.SEND_SMS;
        public static final String PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        public static final String PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
        public static final String PERMISSION_CALL_PHONE = Manifest.permission.CALL_PHONE;
        public static final String PERMISSION_READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;

        public static String[] getAllPermission() {

            return new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.GET_ACCOUNTS,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_PHONE_STATE
            };
        }
    }

    public static class AppPreferences {

        private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();
        private static SharedPreferences _sharedPrefs;
        private static SharedPreferences.Editor _prefsEditor;

//        public static final String PREF_KEY_LASTUSER = "key.last_user";
//        public static final String PREF_KEY_LOGIN_STATUS = "key.login.status";
//
//        //user info
//        public static final String PREF_KEY_USER_DATA = "chopink.user.data";
//        public static final String PREF_KEY_USER_ID = "chopink.user.id";
//        public static final String PREF_KEY_USER_EMAIL = "chopink.user.email";
//        public static final String PREF_KEY_USER_NAME = "chopink.user.name";
//        public static final String PREF_KEY_USER_JSON_DATA = "chopink.user.json.data";
//        public static final String PREF_KEY_HOME_JSON_DATA = "chopink.home.json.data";
//
//        public static final String PREF_KEY_USER_FB_ID = "chopink.user.fb.id";
//        public static final String PREF_KEY_USER_IS_FIRST_TIME_LOGIN = "chopink.user.is_first_time_login";
//
//        public static final String PREF_GCM_TOKEN = "key.gcm.token";
//
//        public static final String PREF_STATUS_BAR_HEIGHT = "key.status.bar.height";

        public AppPreferences(Context context) {
            try {
                AppPreferences._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
                AppPreferences._prefsEditor = _sharedPrefs.edit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        public ArrayList<String> getNotificationIds(String key) {
//            int userID = getUserId();
//            key = key + userID;
//
//            String ids = _sharedPrefs.getString(key, "");
//            Log.d("AppPreferences", "getNotificationIds:" + ids + "/" + key);
//            ArrayList<String> arrayList = new ArrayList<>();
//            if (ids != null) {
//                for (String anIds_array : ids.split(",")) {
//                    if (!anIds_array.equals(""))
//                        arrayList.add(anIds_array.replace("_", ""));
//                }
//                Log.d("AppPreferences", "getNotificationIds(Array):" + arrayList.toString() + "/" + key);
//                return arrayList;
//            }
//
//            return new ArrayList<>();
//        }
//
//        public void saveNotificationId(String key, String id) {
//            Log.d("AppPreferences", "saveNotificationId:" + id + "/" + key);
//            int userID = getUserId();
//            key = key + userID;
//            try {
//
//                String ids = _sharedPrefs.getString(key, "");
//                if (ids == null) {
//                    Log.d("AppPreferences", "saveNotificationId is null");
//
//                    _prefsEditor.putString(key, _sharedPrefs.getString(key, "") + "_" + id + ",");
//                    _prefsEditor.commit();
//                } else if (!ids.contains("_" + id + ",") && !id.equals("0")) {
//
//                    Log.d("AppPreferences", ids + " not contain _" + id + ",");
//
//                    _prefsEditor.putString(key, _sharedPrefs.getString(key, "") + "_" + id + ",");
//                    _prefsEditor.commit();
//                }
//            } catch (NullPointerException e) {
//                Log.d("AppPreferences", "saveNotificationId NullPointerException " + e);
//
//                _prefsEditor.putString(key, _sharedPrefs.getString(key, "") + "_" + id + ",");
//                _prefsEditor.commit();
//            }
//        }
//
//        public void clearNotificationIds(String key, String id) {
//            int userID = getUserId();
//            key = key + userID;
//            _prefsEditor.putString(key, "");
//            _prefsEditor.commit();
//        }
//
//        public void removeNotificationId(String key, String id) {
//            int userID = getUserId();
//            key = key + userID;
//            String ids = _sharedPrefs.getString(key, "");
//            if (ids != null) {
//                _prefsEditor.putString(key, ids.replace("_" + id + ",", ""));
//                _prefsEditor.commit();
//            }
//        }
    }

    public static final class Helpers {

        public static boolean checkInternet(Context ctx) {
            ConnectivityManager connec = (ConnectivityManager) ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            // Check if wifi or mobile network is available or not. If any of them is
            // available or connected then it will return true, otherwise false;
            return wifi.isConnected() || mobile.isConnected();
        }

        public static String getPathFromUri(Context context, Uri uri) {
            if (uri == null)
                return null;
            String scheme = uri.getScheme();
            if (scheme.equals(ContentResolver.SCHEME_FILE)) {
                return uri.getPath();
            } else if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                Cursor cursor = context.getContentResolver().query(uri, null, null, null,
                        null);
                if (cursor == null)
                    return uri.getPath();
                else {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                    try {
                        return cursor.getString(idx);
                    } catch (Exception exception) {
                        return null;
                    }
                }
            }
            return uri.toString();
        }


        public static Uri getVideoContentUri(Context context, File videoFile) {
            String filePath = videoFile.getAbsolutePath();
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Video.Media._ID},
                    MediaStore.Video.Media.DATA + "=? ",
                    new String[]{filePath}, null);
            if (cursor != null && cursor.moveToFirst()) {
                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
                return Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, "" + id);
            } else {
                if (videoFile.exists()) {
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Video.Media.DATA, filePath);
                    return context.getContentResolver().insert(
                            MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                } else {
                    return null;
                }
            }
        }

        public static Point getScreenSize(Activity activity) {
            Point point = new Point();
            try {
                Display display = activity.getWindowManager().getDefaultDisplay();


                if (Build.VERSION.SDK_INT >= 13) {
                    display.getSize(point);
                } else {
                    point.x = display.getHeight();
                    point.y = display.getWidth();
                }
            } catch (Exception e) {

            }


            return point;
        }

        public static int getDp(Context context, int value) {

            return (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    value,
                    context.getResources().getDisplayMetrics());
        }

        public static int getPx(Context context, int dp) {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            float px = dp * (metrics.densityDpi / 160f);

            return (int) px;
        }

        public boolean isValidEmail(String email) {
            return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }

        public static String getFinalDate(String date) {
//            Sep 22, 2009 4:47:08 PM
            //Tue Sep 22 16:47:08 GMT+08:00 2009
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE MMM d HH:mm:ss z yyyy", Locale.ENGLISH);
            Date myDate = null;
            try {
                myDate = dateFormat.parse(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat("dd.M.yyyy", Locale.ENGLISH);
            String finalDate = timeFormat.format(myDate);
            return finalDate;
        }

        public static String getFinalDate(String date, String customDateFormat) {
//            Sep 22, 2009 4:47:08 PM
            //Tue Sep 22 16:47:08 GMT+08:00 2009
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "dd/MM/yyyy", Locale.ENGLISH);
            Date myDate = null;
            try {
                myDate = dateFormat.parse(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat(customDateFormat, Locale.ENGLISH);
            String finalDate = timeFormat.format(myDate);
            return finalDate;
        }

        public static Date getDateFromStr(String inputDateString, String dateFormatFromServer) {
            SimpleDateFormat sdf;
            if (!TextUtils.isEmpty(dateFormatFromServer)) {
                sdf = new SimpleDateFormat(dateFormatFromServer);
            } else {
                sdf = new SimpleDateFormat("dd/MM/yyyy");
            }
            try {
                return sdf.parse(inputDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return Calendar.getInstance().getTime();
        }

        public static String parseTodaysDate(String time) {

            String inputPattern = "MMM d, yyyy h:mm:ss a";

            inputPattern = "MMM d, yyyy H:mm:ss a";

            String outputPattern = "dd.M.yyyy";

            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);

                Log.i("mini", "Converted Date Today:" + str);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }

        public static String getDateTimeForDisplay(Context mContext, long timestamp) {
//            Log.d("Common.Helpers", "timestamp->" + timestamp);
            Calendar msgDate = Calendar.getInstance();
            Calendar today = Calendar.getInstance();
            msgDate.setTimeInMillis(timestamp);
            today.setTimeInMillis(System.currentTimeMillis());

            int day = today.get(Calendar.DAY_OF_MONTH) - msgDate.get(Calendar.DAY_OF_MONTH);
            int month = today.get(Calendar.MONTH) - msgDate.get(Calendar.MONTH);
            int year = today.get(Calendar.YEAR) - msgDate.get(Calendar.YEAR);

            if (day == 0 && month == 0 && year == 0) {
                return Common.Helpers.getTimeFromTimestamp(timestamp);
            } else {
                return Common.Helpers.getDateFromTimestamp(timestamp);
            }
        }

        public static String getDateFromTimestamp(long timestamp) {
            Date date = new Date(timestamp);
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            return format.format(date);
        }

        public static String getTimeFromTimestamp(long timestamp) {
            Date date = new Date(timestamp);
            SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
            return format.format(date);
        }

        public static long getTimestampFromStrDateTime(String datetime, String format) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
            try {
                Date date = dateFormat.parse(datetime);
                return date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }

        }

        public static Uri getAudioContentUri(Context context, File audioFile) {
            String filePath = audioFile.getAbsolutePath();
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Media._ID},
                    MediaStore.Audio.Media.DATA + "=? ",
                    new String[]{filePath}, null);
            if (cursor != null && cursor.moveToFirst()) {
                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
                return Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, "" + id);
            } else {
                if (audioFile.exists()) {
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Audio.Media.DATA, filePath);
                    return context.getContentResolver().insert(
                            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, values);
                } else {
                    return null;
                }
            }
        }

        public static void hideSoftKeyboard(Activity activity) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null)
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

        public static void backToParent(Activity context, Intent parentIntent, boolean isFromHouzcare) {
            if (NavUtils.shouldUpRecreateTask(context, parentIntent)) {
                // This activity is NOT part of this app's task, so create a new task
                // when navigating up, with a synthesized back stack.
                TaskStackBuilder.create(context)
                        // Add all of this activity's parents to the back stack
                        .addNextIntentWithParentStack(parentIntent)
                        // Navigate up to the closest parent
                        .startActivities();
                Log.d("backToParent", "recreate");
                context.finish();
            } else {
                // This activity is part of this app's task, so simply
                // navigate up to the logical parent activity.
                if (isFromHouzcare) {
                    Log.d("backToParent", "navigate up");
                    NavUtils.navigateUpTo(context, parentIntent);
                } else {
                    context.finish();
                }
            }
        }

        public static String getMinuteFormat(long millis) {
            return String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        }

        public static String getPathFromAudioUri(Context context, Uri uri) {
            if (uri == null)
                return null;
            String scheme = uri.getScheme();
            if (scheme.equals(ContentResolver.SCHEME_FILE)) {
                return uri.getPath();
            } else if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                Cursor cursor = context.getContentResolver().query(uri, null, null, null,
                        null);
                if (cursor == null)
                    return uri.getPath();
                else {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                    try {
                        return cursor.getString(idx);
                    } catch (Exception exception) {
                        return null;
                    }
                }
            }
            return uri.toString();
        }

        public static AlertDialog showAlertDialog(Context context, String message, String title, DialogInterface.OnClickListener listener) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            if (message.length() > 0) {
                builder1.setMessage(message);
            } else {
                return null;
            }
            builder1.setCancelable(true);
            if (title.length() > 0) {
                builder1.setTitle("Success");
            }
            if (listener != null) {
                builder1.setPositiveButton("OK", listener);
            } else {
                builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
            AlertDialog alert11 = builder1.create();
            alert11.show();

            return alert11;
        }

        public static void showOptions(String[] options, String title, Context c,
                                       DialogInterface.OnClickListener listener) {
            new AlertDialog.Builder(c)
                    .setTitle(title)
                    .setNegativeButton(c.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setItems(options, listener).show();
        }

        public static AlertDialog showConfirmation(Context context, String message, String title, String yes, String no, DialogInterface.OnClickListener listener) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            if (message.length() > 0) {
                builder1.setMessage(message);
            } else {
                return null;
            }
//            builder1.setCancelable(false);


            if (title.length() > 0) {
                builder1.setTitle("Success");
            }
            if (listener != null) {
                builder1.setPositiveButton(yes, listener);
                builder1.setNegativeButton(no, listener);
            } else {
                builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
            AlertDialog alert11 = builder1.create();
            alert11.show();

            return alert11;
        }

        public static String getAudioPathFromUri(Context context, Uri uri) {
            if (uri == null)
                return null;
            String scheme = uri.getScheme();
            try {
                if (scheme.equals(ContentResolver.SCHEME_FILE)) {
                    return uri.getPath();
                } else if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                    Cursor cursor = context.getContentResolver().query(uri, null, null, null,
                            null);
                    if (cursor == null)
                        return uri.getPath();
                    else {
                        cursor.moveToFirst();
                        int idx = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                        try {
                            return cursor.getString(idx);
                        } catch (Exception exception) {
                            return null;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return uri.toString();
        }

        public static String getExtensionFromURL(String url) {
            if (url.lastIndexOf(".") > -1) {
                return url.substring(url.lastIndexOf("."));
            } else {
                throw new IllegalArgumentException();
            }

        }

        public static void moveFile(String inputPath, String outputPath) {

            InputStream in = null;
            OutputStream out = null;
            try {

                //create output directory if it doesn't exist
                String outputDir = outputPath.substring(0, outputPath.lastIndexOf("/") + 1);
                File dir = new File(outputDir);
                if (!dir.exists()) {
                    dir.mkdirs();
                }


                in = new FileInputStream(inputPath);
                out = new FileOutputStream(outputPath);

                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();
                in = null;

                // write the output file
                out.flush();
                out.close();
                out = null;

                // delete the original file
                new File(inputPath).delete();
            } catch (FileNotFoundException fnfe1) {
                Log.e("Helper.MoveFile", fnfe1.getMessage());
                fnfe1.printStackTrace();
            } catch (Exception e) {
                Log.e("Helper.MoveFile", e.getMessage());
                e.printStackTrace();
            }
        }

        public static void sendAnalytics(Activity context, String screenName) {
//            // Obtain the shared Tracker instance.
//            try {
//                KepoApplication application = (KepoApplication) context.getApplication();
//                Tracker mTracker = application.getDefaultTracker();
//                Log.i("Common.sendAnalytics", "Setting screen name:" + screenName);
//                mTracker.setScreenName(screenName);
//                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }

        public static void logKeyHash(Context context) {
            PackageInfo info;
            try {
                info = context.getPackageManager().getPackageInfo("com.lesys.lepro", PackageManager.GET_SIGNATURES);
                for (android.content.pm.Signature signature : info.signatures) {
                    MessageDigest md;
                    md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    String something = new String(Base64.encode(md.digest(), 0));
                    //String something = new String(Base64.encodeBytes(md.digest()));
                    Log.e("hash key", something);
                }
            } catch (PackageManager.NameNotFoundException e1) {
                Log.e("name not found", e1.toString());
            } catch (NoSuchAlgorithmException e) {
                Log.e("no such an algorithm", e.toString());
            } catch (Exception e) {
                Log.e("exception", e.toString());
            }
        }

        public static ProgressDialog showProgessDialog(Context context) {
            ProgressDialog progressDialog = new ProgressDialog(context);
//            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            return progressDialog;
        }

        public static void hideKeyboard(Context context, View view) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        public static void showOptionsNoCancel(String[] options, String title, Context c,
                                               DialogInterface.OnClickListener listener) {
            new AlertDialog.Builder(c)
                    .setTitle(title)
                    .setItems(options, listener).show().setCanceledOnTouchOutside(true);
        }

        public static boolean checkPermission(Activity context) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                ActivityCompat.requestPermissions(context, Common.Permission.getAllPermission(), 1);
                return true;
            }
            return false;
        }

        public static AlertDialog openPermissionDialog(final Activity context) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setMessage(context.getResources().getString(R.string.dialog_permission_deny))
                    .setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(context.getResources().getString(R.string.setting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", context.getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    });
            return builder.create();
        }

        public static boolean checkPhonePermission(Activity context) {
            if (ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            return false;
        }

        public static boolean checkRecordAudioPermission(final Activity context) {
            if (ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            return false;
        }

        public static boolean checkCameraPermission(final Activity context) {
            if (ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            return false;
        }

        public static boolean checkStoragePermission(final Activity context) {
            if (ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ) {
                return true;
            }
            return false;
        }

        public static boolean checkContactPermission(final Activity context) {
            if (ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_READ_CONTACTS) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Permission.PERMISSION_GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {

                return true;
            }
            return false;
        }

        public static void samsungCleanNotification(Context context) {
            try {
                ContentValues cv = new ContentValues();
                cv.put("badgecount", 0);
                context.getContentResolver().update(Uri.parse("content://com.sec.badge/apps"), cv, "package=?", new String[]{context.getPackageName()});
            } catch (Exception e) {
            }
        }

        public static int getStatusBarHeight(Activity mContext) {
            Rect rectangle = new Rect();
            Window window = mContext.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
            int statusBarHeight = rectangle.top;
            int contentViewTop =
                    window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
            int titleBarHeight = contentViewTop - statusBarHeight;

            return statusBarHeight;
        }

        public static int getStatusBarHeight2(Activity mContext) {
            int result = 0;
            int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = mContext.getResources().getDimensionPixelSize(resourceId);
            }
            return result;
        }

        public static boolean isEmailValid(String email) {
            boolean isValid = false;

            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = email;

            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                isValid = true;
            }
            return isValid;
        }

        public static void materialViewVisible(final View myView) {
            materialViewVisible(myView, 0);
        }

        public static void materialViewVisible(final View myView, int duration) {
            if (myView != null)
                try {
//                myView.setVisibility(View.GONE);
                    // get the center for the clipping circle
                    int cx = myView.getMeasuredWidth() / 2;
                    int cy = myView.getMeasuredHeight() / 2;

                    // get the final radius for the clipping circle
                    int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / 2;
                    // create the animator for this view (the start radius is zero)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
                        if (duration != 0)
                            anim.setDuration(duration);
                        else
                            anim.setDuration(500);
                        anim.setInterpolator(new AccelerateInterpolator());
                        if (anim != null)
                            anim.start();
                    }

                    myView.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    myView.setVisibility(View.VISIBLE);
                }
        }

        public static void materialViewGone(final View myView) {
            materialViewGone(myView, 0);
        }

        public static void materialViewGone(final View myView, int duration) {
            if (myView != null)
                try {
                    // get the center for the clipping circle
                    int cx = myView.getMeasuredWidth() / 2;
                    int cy = myView.getMeasuredHeight() / 2;

                    // get the initial radius for the clipping circle
                    int initialRadius = myView.getWidth() / 2;

                    // create the animation (the final radius is zero)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);
                        if (duration != 0)
                            anim.setDuration(duration);
                        else
                            anim.setDuration(500);
                        anim.setInterpolator(new AccelerateInterpolator());
                        // make the view invisible when the animation is done
                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                myView.setVisibility(View.GONE);
                            }
                        });

                        // start the animation

                        anim.start();
                    } else {
                        myView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    myView.setVisibility(View.GONE);
                }
        }

        public static void displayRevealAnimation(Activity mContext, final View view) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mContext.findViewById(android.R.id.content).getRootView().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        v.removeOnLayoutChangeListener(this);
                        Common.Helpers.materialViewVisible(view);
                    }
                });
            }
        }

        public static boolean isAppIsInBackground(Context context) {
            boolean isInBackground = true;
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                }
            }

            return isInBackground;
        }

        public static String displayTimeZone(TimeZone tz) {

            long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                    - TimeUnit.HOURS.toMinutes(hours);
            // avoid -4:-30 issue
            minutes = Math.abs(minutes);

            String result = "";
            if (hours > 0) {
                result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
            } else {
                result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
            }

            return result;

        }

        public static float dpToPx(Context context, float valueInDp) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
        }

        public static Drawable getErrorDrawable(Context mContext) {
            Drawable d = ContextCompat.getDrawable(mContext, R.drawable.ic_error);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());

            return d;
        }

        public static void copyTestDocToSdCard(final Activity mContext, final String fileName, final File testImageOnSdCard, final OnFileCopyListener onFileCopyListener) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        InputStream is = mContext.getAssets().open(fileName);
                        FileOutputStream fos = new FileOutputStream(testImageOnSdCard);
                        byte[] buffer = new byte[8192];
                        int read;
                        try {
                            while ((read = is.read(buffer)) != -1) {
                                fos.write(buffer, 0, read);
                            }
                        } finally {
                            fos.flush();
                            fos.close();
                            is.close();
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (onFileCopyListener != null)
                                        onFileCopyListener.onSuccess(fileName);
                                }
                            });
                        }
                    } catch (IOException e) {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (onFileCopyListener != null)
                                    onFileCopyListener.onFailed();
                            }
                        });
                    }
                }
            }).start();
        }

        public static File getFilesDirectory(Context context) {
            File appFilesDir = null;
            if (Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                appFilesDir = getExternalFilesDir(context);
            }
            if (appFilesDir == null) {
                appFilesDir = context.getFilesDir();
            }
            return appFilesDir;
        }

        public static File getExternalFilesDir(Context context) {
            File dataDir = new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data");
            File appFilesDir = new File(new File(dataDir, context.getPackageName()), "files");
            if (!appFilesDir.exists()) {
                if (!appFilesDir.mkdirs()) {
                    //L.w("Unable to create external cache directory");
                    return null;
                }
                try {
                    new File(appFilesDir, ".nomedia").createNewFile();
                } catch (IOException e) {
                    //L.i("Can't create \".nomedia\" file in application external cache directory");
                    return null;
                }
            }
            return appFilesDir;
        }

        public static boolean isNumber(String s) {
            String regexStr = "^[0-9]*$";
            if (!TextUtils.isEmpty(s) && s.matches(regexStr)) {
                return true;
            }
            return false;
        }

        public static String getIntegerToDouble2Decimal(int i) {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(i);
        }
    }
}

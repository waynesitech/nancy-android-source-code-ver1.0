package com.lesys.lepro.ai.widget;

/**
 * Created by Nicholas on 14/05/2017.
 */

public interface SplashScreenViewInterface {

    void onClickLogin();

    void onClickSignUp();

    void onClickESign();
}

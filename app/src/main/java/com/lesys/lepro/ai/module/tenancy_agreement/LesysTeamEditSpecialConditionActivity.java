package com.lesys.lepro.ai.module.tenancy_agreement;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.model.SpecialRequest;
import com.lesys.lepro.ai.model.SpecialRequestCover;
import com.lesys.lepro.ai.model.generate_new_ta.SpecialConditionModel;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Nicholas on 20/08/2017.
 */

public class LesysTeamEditSpecialConditionActivity extends BaseActivity implements
        View.OnClickListener {

    LinearLayout mSpecialConditionLayout;
    Button mSaveBtn;

    SpecialRequestCover mSpecialRequestCover;
    Order mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesys_team_edit_special_condition);

        mOrder = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);
        if (mOrder == null) {
            finish();
            return;
        }

        mSpecialConditionLayout = (LinearLayout) findViewById(R.id.special_conditions_layout);
        mSaveBtn = (Button) findViewById(R.id.save_btn);

        mSpecialConditionLayout.removeAllViews();

        try {
            JSONArray jsonArray = new JSONArray(mOrder.getSpecialRequest());
            for (int i = 0; i < jsonArray.length(); i++) {
                generateSpecialConditionView(jsonArray.getString(i));
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        mSaveBtn.setOnClickListener(this);
    }

    void generateSpecialConditionView(String s) {
        SpecialRequest specialRequest;
        try {
            specialRequest = new Gson().fromJson(s, SpecialRequest.class);
        } catch (Exception e) {
            specialRequest = null;
        }

        View v = LayoutInflater.from(this).inflate(R.layout.item_lesys_team_edit_special_condition, null);

        TextView mOriginalIndex = (TextView) v.findViewById(R.id.original_index);
        TextView mOriginal = (TextView) v.findViewById(R.id.original);
        final TextView mIndex = (TextView) v.findViewById(R.id.index);
        TextInputEditText mSpecialConditionText = (TextInputEditText) v.findViewById(R.id.special_condition_text);
        final int index = mSpecialConditionLayout.getChildCount();

        mOriginalIndex.setText((index + 1) + ".");
        mIndex.setText((index + 1) + ".");

        if (specialRequest != null) {
            mOriginal.setText(specialRequest.getOriginal());
            mSpecialConditionText.setText(specialRequest.getEdited());
        } else {
            mOriginal.setText(s);
        }

        mSpecialConditionLayout.addView(v);
    }

    void updateSpecialRequest() {
        String jsonStr = null;
        try {
            jsonStr = new Gson().toJson(mSpecialRequestCover.getArrayList());
        } catch (Exception e) {

        }
        if (TextUtils.isEmpty(jsonStr)) {
            return;
        }
        BaseApi.init(this)
                .updateSpecialRequest(mOrder.getId(), jsonStr)
                .call(new DataHandler<MetaData>(MetaData.class) {
                    @Override
                    public void onSuccess(MetaData data) {
                        Common.Helpers.hideSoftKeyboard(LesysTeamEditSpecialConditionActivity.this);
                        Toast.makeText(LesysTeamEditSpecialConditionActivity.this, R.string.toast_special_condition_edited_successfully, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_btn: {
                mSpecialRequestCover = new SpecialRequestCover();
                mSpecialRequestCover.getArrayList().clear();
                for (int i = 0; i < mSpecialConditionLayout.getChildCount(); i++) {
                    TextView mOriginal = (TextView) mSpecialConditionLayout.getChildAt(i).findViewById(R.id.original);
                    TextInputEditText mEditText = (TextInputEditText) mSpecialConditionLayout.getChildAt(i).findViewById(R.id.special_condition_text);
                    SpecialRequest specialRequest = new SpecialRequest();
                    specialRequest.setOriginal(mOriginal.getText().toString());
                    specialRequest.setEdited(mEditText.getText().toString());
                    mSpecialRequestCover.getArrayList().add(specialRequest);
                }

                updateSpecialRequest();
                break;
            }
        }
    }
}

package com.lesys.lepro.ai.shared;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.lesys.lepro.ai.BaseApplication;
import com.lesys.lepro.ai.model.NcLocation;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by aio-synergy on 2/4/16.
 */
public class LocationUtil {

    private static LocationUtil mInstance = null;
    private static final String TAG = "LocationModule";
    private static final String IP_API_URL = "http://ip-api.com/json";

    private static Context mContext;
    private static LocationManager locationManager;
    private static LocationModuleListener mLocationModuleListener;

    private static NcLocation ncLocation;
    private static boolean booFulfilledLocationRequest = true;

    private static int FIFTEEN_MINUTES_MILLISECONDS = 15 * 60 * 1000;

    public static LocationUtil getInstance() {
        if (mInstance == null) {
            mInstance = getSync();
        }
        return mInstance;
    }

    private static synchronized LocationUtil getSync() {
        if (mInstance == null) {
            mInstance = new LocationUtil();
        }
        return mInstance;
    }

    private LocationUtil() {
        mContext = BaseApplication.getInstance().getApplicationContext();
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        updateLocation();
    }

    public static void getLocation(LocationModuleListener listener) {
        booFulfilledLocationRequest = false;
        mLocationModuleListener = listener;
        if (ncLocation != null) {
            listener.OnGetLocationSuccess(ncLocation);
            booFulfilledLocationRequest = true;
        }
    }

    public static void cancelLocationListenerCallback() {
        booFulfilledLocationRequest = true;
    }

//    private static void updateLocationWithApiIfOnWifi(RequestQueue requestQueue) {
//        if (NetworkUtil.isWifiOnAndConnected(mContext)) {
//            Log.d(TAG, "Wifi is on and connected. Obtaining location by API.");
//            getLocationByIP(requestQueue);
//        } else if (NetworkUtil.isNetworkAvailable(mContext)) {
//            Log.d(TAG, "Wifi is not connected. Network is connected. Obtaining location by Network.");
//            getLocationByNetwork(mContext);
//        } else {
//            Log.d(TAG, "Network is not connected. Obtaining location by GPS.");
//            getLocationByGps(mContext);
//        }
//    }

    private static void updateLocation() {
        if (NetworkUtil.isNetworkAvailable(mContext)) {
            Log.d(TAG, "Wifi is not connected. Network is connected. Obtaining location by Network.");
            getLocationByNetwork(mContext);
        } else {
            Log.d(TAG, "Network is not connected. Obtaining location by GPS.");
            getLocationByGps(mContext);
        }
    }

//    private static void getLocationByIP(RequestQueue requestQueue) {
//        JsonObjectRequest request = new JsonObjectRequest(
//                IP_API_URL,
//                null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject json) {
//                        IPLocation ipLocation = IPLocation.fromString(json.toString());
//                        if (ipLocation.status.equalsIgnoreCase(IPLocation.STATUS_SUCCESS)) {
//                            ncLocation = new NcLocation(ipLocation.country, ipLocation.countryCode, ipLocation.lat, ipLocation.lon);
//                            if (!booFulfilledLocationRequest && mLocationModuleListener != null) {
//                                mLocationModuleListener.OnGetLocationSuccess(ncLocation);
//                                booFulfilledLocationRequest = true;
//                            }
//                            UserCountryISOCodeData.saveUserCountryISOCode(mContext, ncLocation.countryISOCode);
//                        } else {
//                            Log.e(TAG, "Get location by API failed. Message: " + ipLocation.message);
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        if (!TextUtils.isEmpty(volleyError.getMessage())) {
//                            Log.e(TAG, volleyError.getMessage());
//                        } else {
//                            Log.e(TAG, "Get location by API failed.");
//                        }
//                    }
//                });
//
//        requestQueue.add(request);
//    }

    private static void getLocationByNetwork(Context context) {
        if (!PermissionUtil.hasSelfPermission(context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})) {
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "getLocationByNetwork - Permission denied.");
            return;
        }
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Log.d(TAG, "Requesting location updates.");
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    FIFTEEN_MINUTES_MILLISECONDS, 1000000, locationListener);
        } else {
            Log.e(TAG, "Location Provider (Network) doesn't exists.");
        }

    }

    private static void getLocationByGps(Context context) {
        if (!PermissionUtil.hasSelfPermission(context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})) {
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "getLocationByGps() - Permission denied.");
            return;
        }

        mContext = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        Log.d(TAG, "Requesting location updates.");
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                FIFTEEN_MINUTES_MILLISECONDS, 1000000, locationListener);
    }

    private static LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged location: " + location.toString());
            Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                // e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                ncLocation = new NcLocation(
                        address.getCountryName(),
                        address.getCountryCode(),
                        (float) location.getLatitude(),
                        (float) location.getLongitude()
                );
                if (!booFulfilledLocationRequest && mLocationModuleListener != null) {
                    mLocationModuleListener.OnGetLocationSuccess(ncLocation);
                    booFulfilledLocationRequest = true;
                }
            } else {
                Log.e(TAG, "No address found.");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "onStatusChanged provider: " + provider.toString() +
                    "\nstatus: " + status +
                    "\nextras: " + extras.toString());
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "onProviderEnabled provider: " + provider.toString());
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (ActivityCompat.checkSelfPermission(BaseApplication.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(BaseApplication.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onProviderDisabled - Permission denied.");
                return;
            }
            Log.d(TAG, "onProviderDisabled provider: " + provider.toString());
            Log.d(TAG, "Removing listener.");
            locationManager.removeUpdates(this);
            if (provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
                getLocationByGps(mContext);
            }
        }
    };

    public interface LocationModuleListener {
        void OnGetLocationSuccess(NcLocation ncLocation);

        void OnGetLocationFail(String errorMessage);
    }
}

package com.lesys.lepro.ai.model.generate_new_ta;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class MoneyModel implements Serializable {

    public String name;
    public String ringgitMalaysia;
    public String cent;

    public MoneyModel() {

    }

    public MoneyModel(String name, double price) {
        this.name = name;
        String strOfPrice = String.format("%.2f", price);
        if (strOfPrice.contains(".")) {
            int decimalPosition = strOfPrice.indexOf('.');
            this.ringgitMalaysia = strOfPrice.substring(0, decimalPosition);
            this.cent = strOfPrice.substring(decimalPosition + 1);
            Log.d("zzz", "lol");
        } else {
            this.cent = "00";
        }
    }
}

package com.lesys.lepro.ai.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 14/07/2017.
 */

public class ExplanationResultCover extends MetaData implements Serializable {

    private List<Explanation> result = new ArrayList<>();


    public List<Explanation> getResult() {
        return result;
    }

    public void setResult(List<Explanation> result) {
        this.result = result;
    }

}

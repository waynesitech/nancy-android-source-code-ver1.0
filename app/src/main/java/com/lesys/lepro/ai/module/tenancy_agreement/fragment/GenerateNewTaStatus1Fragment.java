package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.MainActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.PropertyDetailModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.PickAndCrop;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;
import com.lesys.lepro.ai.shared.TransformationUtil;
import com.noelchew.permisowrapper.PermisoWrapper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatus1Fragment extends BaseFragment implements View.OnClickListener {

    ImageView mImage;
    AppCompatRadioButton mCheckBoxResidential;
    AppCompatRadioButton mCheckBoxCommercial;
    TextInputEditText mStreetName;
    TextInputEditText mTown;
    TextInputEditText mPostCode;
    Spinner mStates;
    TextView mCountry;
    Button mNextBtn;
//    Button mPreviousBtn;

    File mProfileImageFile;
    private PickAndCrop mPickAndCrop;
    String mStringURL = "";
    int ratioX = 1;
    int ratioY = 1;
    String[] statesArray;

    GenerateNewTAActivity generateNewTAActivity;
    public NewTAModel newTAModel;

    boolean isEdit;
    boolean isTARenew;

    public static GenerateNewTaStatus1Fragment newInstance() {
        GenerateNewTaStatus1Fragment f = new GenerateNewTaStatus1Fragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_1, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);

        mImage = (ImageView) v.findViewById(R.id.image);
        mCheckBoxResidential = (AppCompatRadioButton) v.findViewById(R.id.checkBox_residential);
        mCheckBoxCommercial = (AppCompatRadioButton) v.findViewById(R.id.checkBox_commercial);
        mStreetName = (TextInputEditText) v.findViewById(R.id.street_name);
        mTown = (TextInputEditText) v.findViewById(R.id.town);
        mPostCode = (TextInputEditText) v.findViewById(R.id.postcodes);
        mStates = (Spinner) v.findViewById(R.id.states);
        mCountry = (TextView) v.findViewById(R.id.country);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());
        statesArray = getResources().getStringArray(R.array.states_array);

        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mPickAndCrop = new PickAndCrop(this);
        Permiso.getInstance().setActivity(getActivity());
        if (!newTAModel.isCommercial) {
            mCheckBoxResidential.setChecked(true);
        } else {
            mCheckBoxCommercial.setChecked(true);
        }

        mCheckBoxResidential.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    newTAModel.isCommercial = false;
                    mCheckBoxCommercial.setChecked(false);
                }
            }
        });
        mCheckBoxCommercial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    newTAModel.isCommercial = true;
                    mCheckBoxResidential.setChecked(false);
                }
            }
        });

        mImage.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);
//        mPreviousBtn.setOnClickListener(this);

        Picasso.with(getActivity())
                .load(R.drawable.ic_generate_ta_camera)
                .transform(TransformationUtil.transformationRounded())
                .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                .into(mImage);
        loadLocalData();

        return v;
    }

    public void loadLocalData() {
        if (newTAModel != null) {
            if (newTAModel.propertyDetail != null) {
                PropertyDetailModel propertyDetailModel = newTAModel.propertyDetail;
                if (!TextUtils.isEmpty(propertyDetailModel.imagePath)) {
                    if (propertyDetailModel.imagePath.contains("http")) {
                        Picasso.with(getActivity())
                                .load(propertyDetailModel.imagePath)
                                .transform(TransformationUtil.transformationRounded())
                                .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                                .into(mImage);
                    } else {
                        Picasso.with(getActivity())
                                .load(new File(propertyDetailModel.imagePath))
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .transform(TransformationUtil.transformationRounded())
                                .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                                .into(mImage);
                    }
                } else {
                    Picasso.with(getActivity())
                            .load(R.drawable.ic_generate_ta_camera)
                            .transform(TransformationUtil.transformationRounded())
                            .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                            .into(mImage);
                }

                mStringURL = propertyDetailModel.imagePath;
                mStreetName.setText(propertyDetailModel.streetName);
                mTown.setText(propertyDetailModel.town);
                mPostCode.setText(propertyDetailModel.postCode);
                if (!TextUtils.isEmpty(propertyDetailModel.states)) {
                    for (int i = 0; i < statesArray.length; i++) {
                        if (statesArray[i].equals(propertyDetailModel.states)) {
                            mStates.setSelection(i);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mPickAndCrop != null)
            mPickAndCrop.handleResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case 6709: {
                    try {
                        mStringURL = mPickAndCrop.getImageUri().getPath();
                        Picasso.with(getActivity())
                                .load(mPickAndCrop.getImageUri())
                                .transform(TransformationUtil.transformationRounded())
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .into(mImage);
                        mProfileImageFile = new File(mStringURL);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return;
                }

            }
        }
    }

    void updateNewTAModel() {
        newTAModel.propertyDetail = new PropertyDetailModel();
        newTAModel.propertyDetail.imagePath = mStringURL;
        newTAModel.propertyDetail.streetName = mStreetName.getText().toString();
        newTAModel.propertyDetail.postCode = mPostCode.getText().toString();
        newTAModel.propertyDetail.town = mTown.getText().toString();
        newTAModel.propertyDetail.states = mStates.getSelectedItem().toString();
        newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[1] : generateNewTAActivity.mResidentialSlideFragmentTag[1];

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image: {
                PermisoWrapper.getPermissionTakePicture(getActivity(), new PermisoWrapper.PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        mPickAndCrop.selectImage(ratioX, ratioY);
                    }

                    @Override
                    public void onPermissionDenied() {

                    }
                });
                break;
            }
            case R.id.next_btn: {
                if (TextUtils.isEmpty(mStreetName.getText().toString()) ||
                        TextUtils.isEmpty(mTown.getText().toString()) ||
                        TextUtils.isEmpty(mPostCode.getText().toString())) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }

                updateNewTAModel();
                generateNewTAActivity.setPropertyType();
                generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[1] : generateNewTAActivity.mResidentialSlideFragmentTag[1]);
                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

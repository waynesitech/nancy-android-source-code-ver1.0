package com.lesys.lepro.ai.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.os.Message;
import android.view.View;

/**
 * Created by NicholasTan on 20/04/2016.
 */
public class MBookView extends View {
    final String TAG = "MBookView";
    // MyHandler myHandler = new MyHandler();
    private static int DEFAULT_FLIP_VALUE = 20;
    private static int FLIP_SPEED = 30;

    private long mMoveDelay = 1000/60;

    float xTouchValue = DEFAULT_FLIP_VALUE, yTouchValue = DEFAULT_FLIP_VALUE;

    class FlippingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            MBookView.this.flip();
        }

        public void sleep(long delayMillis) {

            this.removeMessages(0);

            sendMessageDelayed(obtainMessage(0), delayMillis);

        }
    }

    FlippingHandler flippingHandler;
    //
    int width;
    int height;

    float oldTouchX, oldTouchY;
    boolean flipping = false;
    boolean next;

    Point A, B, C, D, E, F,G;

    Bitmap visiblePage;
    Bitmap invisiblePage;
    Paint flipPagePaint;

    boolean flip = false;

    Context context;
    //
    int loadedPages = 0;
    long timeToLoad = 0;
    //

    // boolean loadingDone = false;

    boolean onloading = true;

    boolean onMoving = false;
    int flippedPaperRange = 0;

    FlipDirection flipDirection = FlipDirection.LEFT;

    public enum FlipDirection {
        LEFT, RIGHT
    }

    public MBookView(Context context, int width, int height) {
        super(context);
        this.context = context;
        this.width = width;
        this.height = height;
    }

    public void init(Bitmap page1, Bitmap page2) {
        flippingHandler = new FlippingHandler();
        flipPagePaint = new Paint();
        flipPagePaint.setColor(Color.parseColor("#f15a23"));
//        flipPagePaint.setColor(Color.GRAY);

        A = new Point(10, 0);
        B = new Point(width, height);
        C = new Point(width, 0);
        D = new Point(0, 0);
        E = new Point(0, 0);
        F = new Point(0, 0);
        G = new Point(0, 0);

        xTouchValue = yTouchValue = DEFAULT_FLIP_VALUE;
        visiblePage = page1;
        invisiblePage = page2;
        onMoving = false;
        flipping = false;

        if(flipDirection.equals(FlipDirection.LEFT)){
            flippedPaperRange = width;
        }else{
            flippedPaperRange = 0;
        }

        loadData();

    }

    private void loadData() {
        onloading = false;
    }

    private View visibleView, invisibleView;

    public void setView(View view1, View view2) {
        visibleView = view1;
        invisibleView = view2;
        visibleView.setVisibility(VISIBLE);
        invisibleView.setVisibility(GONE);
        this.setVisibility(GONE);
    }

    public void next() {
        visibleView.setVisibility(GONE);
        this.setVisibility(VISIBLE);
        flipping = true;
        next = true;
        flip();
    }

    public void flip() {
        if (flipping) {
            if (xTouchValue > height || xTouchValue < DEFAULT_FLIP_VALUE) {
                flipping = false;
                if (!flipping) {
                    if (next) {
                        swap2Page();
                    }
                    flip = false;
                    xTouchValue = DEFAULT_FLIP_VALUE;
                    yTouchValue = DEFAULT_FLIP_VALUE;
                    swap2View();
                }
                return;
            }
            if (next) {
                yTouchValue += FLIP_SPEED;
            } else {
                yTouchValue -= FLIP_SPEED;

            }
            this.invalidate();

            flippingHandler.sleep(mMoveDelay);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        width = getWidth();
        height = getHeight();

        Paint paint = new Paint();

        canvas.drawColor(Color.GRAY);
        canvas.drawBitmap(visiblePage, 0, 0, paint);

        if(flipDirection.equals(FlipDirection.LEFT)){
            Path pathX2 = pathOfFlippedPaperFromLeft();
            canvas.drawPath(pathX2, flipPagePaint);
            // Second Page Render
            Path pathX = pathOfTheMaskFromLeft();
            canvas.clipPath(pathX);
        }else{
            Path pathX2 = pathOfFlippedPaperFromRight();
            canvas.drawPath(pathX2, flipPagePaint);
            // Second Page Render
            Path pathX = pathOfTheMaskFromRight();
            canvas.clipPath(pathX);
        }
        canvas.drawBitmap(invisiblePage, 0, 0, paint);
        try {
            canvas.restore();
        }catch(Exception e){

        }

    }

    private Path pathOfTheMaskFromRight() {
        Path path = new Path();

        A.x = width - (width / 3) - yTouchValue;
        A.y = height;
        B.x = width;
        B.y = height;
        C.x = width;
        C.y = height - yTouchValue;
        D.x = width - (width / 3);
        D.y = height - yTouchValue;

        if (A.x <= 0) {
            A.x = 0;
            D.x = 0;

            D.y = height - xTouchValue;
        }else{
            D.x = width;
            D.y = height - yTouchValue;
        }

        path.moveTo(A.x, A.y);
        path.lineTo(B.x, B.y);
        path.lineTo(C.x, C.y);
        path.lineTo(D.x, D.y);
        path.lineTo(A.x, A.y);

        return path;
    }

    private Path pathOfFlippedPaperFromRight() {
        Path path = new Path();

        A.x = width - (width / 3) - yTouchValue;
        A.y = height;
        E.x = width;
        E.y = height - yTouchValue;


        F.x = width - (width / 3) - yTouchValue;
        F.y = height - yTouchValue;
        G.x = width - (width / 3) - yTouchValue;
        G.y = height - yTouchValue;


        if (A.x < 0) {
            A.x = 0;

            xTouchValue += FLIP_SPEED;
            flippedPaperRange += 5;
            F.x =  flippedPaperRange;
            F.y = (height - yTouchValue)-xTouchValue;
            G.x = 0;
        }

        path.moveTo(A.x, A.y);
        path.lineTo(E.x, E.y);
        path.lineTo(F.x, F.y);
        path.lineTo(G.x, G.y);
        path.lineTo(A.x, A.y);
        return path;
    }

    private Path pathOfTheMaskFromLeft() {
        Path path = new Path();

        A.x = 0;
        A.y = height;
        B.x = yTouchValue;
        B.y = height;
        C.x = yTouchValue;
        C.y = height;
        D.x = 0;
        D.y = height - yTouchValue;

        if (B.x > width) {
            B.x = width;
            C.x = width;

            C.y = height - xTouchValue;
        } else {
            C.x = yTouchValue;
            C.y = height;
        }

        path.moveTo(A.x, A.y);
        path.lineTo(B.x, B.y);
        path.lineTo(C.x, C.y);
        path.lineTo(D.x, D.y);
        path.lineTo(A.x, A.y);

        return path;
    }

    private Path pathOfFlippedPaperFromLeft() {
        Path path = new Path();

        A.x = yTouchValue;
        A.y = height;
        E.x = 0;
        E.y = height - yTouchValue;


        F.x = yTouchValue;
        F.y = height - yTouchValue;
        G.x = yTouchValue;
        G.y = height - yTouchValue;


        if (G.x > width) {
            G.x = width;
            A.x = width;

            xTouchValue += FLIP_SPEED;
            flippedPaperRange -= 5;
            F.x = flippedPaperRange;
            F.y = (height - yTouchValue) - xTouchValue;
        }


        path.moveTo(A.x, A.y);
        path.lineTo(E.x, E.y);
        path.lineTo(F.x, F.y);
        path.lineTo(G.x, G.y);
        path.lineTo(A.x, A.y);
        return path;
    }

    private void swap2Page() {
        Bitmap temp = visiblePage;
        visiblePage = invisiblePage;
        invisiblePage = temp;
        flippedPaperRange = 0;
        if(flipDirection.equals(FlipDirection.LEFT)){
            flippedPaperRange = width;
        }else{
            flippedPaperRange = 0;
        }
        temp = null;
    }

    private void swap2View() {
        View t = visibleView;
        visibleView = invisibleView;
        invisibleView = t;
        t = null;
        visibleView.setVisibility(VISIBLE);
        this.setVisibility(GONE);
    }


    public class Point {
        float x;
        float y;
        public Point(float x, float y){
            this.x = x;
            this.y = y;
        }

    }
}
package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 14/07/2017.
 */

public class EStampPricingResultCover extends MetaData implements Serializable {

    private EStampPricingCover result;

    public EStampPricingCover getResult() {
        return result;
    }

    public void setResult(EStampPricingCover result) {
        this.result = result;
    }

}

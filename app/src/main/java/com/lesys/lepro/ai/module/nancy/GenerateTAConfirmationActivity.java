package com.lesys.lepro.ai.module.nancy;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.MainActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.UserCover;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateTAConfirmationActivity extends BaseActivity {

    private static final String TAG = GenerateTAConfirmationActivity.class.getName();

    TextView mMsg;
    Button mGoBackBtn;
    Button mAgreeBtn;

    NewTAModel newTAModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String keyNewTAModel = SharedPreferencesUtil.getString(this, GlobalParam.KEY_NEW_TA_MODEL);
        if (!TextUtils.isEmpty(keyNewTAModel)) {
            newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
        }
        if (newTAModel != null) {
            Intent intent = new Intent(this, GenerateNewTAActivity.class);
            intent.putExtra(GlobalParam.INTENT_UPLOAD_TYPE, GlobalParam.TAUploadType.UPLOAD_TYPE_NORMAL);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_generate_ta_confirmation);

        mMsg = (TextView) findViewById(R.id.msg);
        mGoBackBtn = (Button) findViewById(R.id.go_back_btn);
        mAgreeBtn = (Button) findViewById(R.id.agree_btn);

        mMsg.setText(getString(R.string.generate_ta_confirmation_msg, "100"));
        mGoBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mAgreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateTAConfirmationActivity.this, GenerateNewTAActivity.class);
                intent.putExtra(GlobalParam.INTENT_UPLOAD_TYPE, GlobalParam.TAUploadType.UPLOAD_TYPE_NORMAL);
                startActivity(intent);
                finish();
            }
        });
    }

}

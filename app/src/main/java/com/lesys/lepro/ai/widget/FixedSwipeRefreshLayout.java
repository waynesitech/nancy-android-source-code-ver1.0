package com.lesys.lepro.ai.widget;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

/**
 * Created by NicholasTan on 04/05/2016.
 */
public class FixedSwipeRefreshLayout extends SwipeRefreshLayout {

    private RecyclerView recyclerView;
    private ListView mListView;
    private NestedScrollView mNestedScrollView;
    private RelativeLayout relativeLayout;

    private ScrollView mScrollView;

    public FixedSwipeRefreshLayout(Context context) {
        super(context);
    }

    public FixedSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public void setListView(ListView mListView) {
        this.mListView = mListView;
    }

    public void setmNestedScrollView(NestedScrollView mNestedScrollView) {
        this.mNestedScrollView = mNestedScrollView;
    }

    public void setRelativeLayout(RelativeLayout relativeLayout) {
        this.relativeLayout = relativeLayout;
    }

    public void setScrollView(ScrollView mScrollView) {
        this.mScrollView = mScrollView;
    }


    @Override
    public boolean canChildScrollUp() {
        if (recyclerView != null) {
            return recyclerView.canScrollVertically(-1);
        }
        if (mListView != null) {
            return mListView.canScrollVertically(-1);
        }
        if (mNestedScrollView != null) {
            return mNestedScrollView.canScrollVertically(-1);
        }
        if (relativeLayout != null) {
            return relativeLayout.canScrollVertically(-1);
        }
        if (mScrollView != null) {
            return mScrollView.canScrollVertically(-1);
        }
        return false;
    }

}

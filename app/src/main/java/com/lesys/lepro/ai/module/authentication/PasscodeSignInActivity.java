package com.lesys.lepro.ai.module.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.model.OrderResultCover;
import com.lesys.lepro.ai.module.e_sign.DigitalSignatureActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.TADetailActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NicholasTan on 13/07/2016.
 */
public class PasscodeSignInActivity extends BaseActivity implements View.OnClickListener {

    EditText mPasscode1, mPasscode2, mPasscode3, mPasscode4;
    EditText mNRICPasscode1, mNRICPasscode2, mNRICPasscode3, mNRICPasscode4;
    Button mVerifyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_passcode_sign_in);

        mPasscode1 = (EditText) findViewById(R.id.passcode1);
        mPasscode2 = (EditText) findViewById(R.id.passcode2);
        mPasscode3 = (EditText) findViewById(R.id.passcode3);
        mPasscode4 = (EditText) findViewById(R.id.passcode4);
        mNRICPasscode1 = (EditText) findViewById(R.id.nric_passcode1);
        mNRICPasscode2 = (EditText) findViewById(R.id.nric_passcode2);
        mNRICPasscode3 = (EditText) findViewById(R.id.nric_passcode3);
        mNRICPasscode4 = (EditText) findViewById(R.id.nric_passcode4);
        mVerifyBtn = (Button) findViewById(R.id.verify_btn);

//        mPasscode1.setText("8");
//        mPasscode2.setText("d");
//        mPasscode3.setText("3");
//        mPasscode4.setText("6");
//        mNRICPasscode1.setText("0");
//        mNRICPasscode2.setText("2");
//        mNRICPasscode3.setText("2");
//        mNRICPasscode4.setText("1");

        mPasscode1.addTextChangedListener(new OnTextWatcher(mPasscode1));
        mPasscode2.addTextChangedListener(new OnTextWatcher(mPasscode2));
        mPasscode3.addTextChangedListener(new OnTextWatcher(mPasscode3));
        mPasscode4.addTextChangedListener(new OnTextWatcher(mPasscode4));
        mNRICPasscode1.addTextChangedListener(new OnTextWatcher(mNRICPasscode1));
        mNRICPasscode2.addTextChangedListener(new OnTextWatcher(mNRICPasscode2));
        mNRICPasscode3.addTextChangedListener(new OnTextWatcher(mNRICPasscode3));
        mNRICPasscode4.addTextChangedListener(new OnTextWatcher(mNRICPasscode4));

        mPasscode1.setOnKeyListener(new OnPasscodeListener());
        mPasscode2.setOnKeyListener(new OnPasscodeListener());
        mPasscode3.setOnKeyListener(new OnPasscodeListener());
        mPasscode4.setOnKeyListener(new OnPasscodeListener());
        mNRICPasscode1.setOnKeyListener(new OnPasscodeListener());
        mNRICPasscode2.setOnKeyListener(new OnPasscodeListener());
        mNRICPasscode3.setOnKeyListener(new OnPasscodeListener());
        mNRICPasscode4.setOnKeyListener(new OnPasscodeListener());

        mVerifyBtn.setOnClickListener(this);

    }

    public class OnPasscodeListener implements View.OnKeyListener {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DEL:
                        if (mPasscode1.isFocused()) {
                            mPasscode1.setText("");
                        } else if (mPasscode2.isFocused()) {
                            mPasscode2.setText("");
                            mPasscode1.requestFocus();
//                            mPasscode1.setText("");
                        } else if (mPasscode3.isFocused()) {
                            mPasscode3.setText("");
                            mPasscode2.requestFocus();
//                            mPasscode2.setText("");
                        } else if (mPasscode4.isFocused()) {
                            mPasscode4.setText("");
                            mPasscode3.requestFocus();
//                            mPasscode3.setText("");
                        } else if (mNRICPasscode1.isFocused()) {
                            mNRICPasscode1.setText("");
//                            mPasscode4.requestFocus();
//                            mPasscode4.setText("");
                        } else if (mNRICPasscode2.isFocused()) {
                            mNRICPasscode2.setText("");
                            mNRICPasscode1.requestFocus();
//                            mNRICPasscode1.setText("");
                        } else if (mNRICPasscode3.isFocused()) {
                            mNRICPasscode3.setText("");
                            mNRICPasscode2.requestFocus();
//                            mNRICPasscode2.setText("");
                        } else if (mNRICPasscode4.isFocused()) {
                            mNRICPasscode4.setText("");
                            mNRICPasscode3.requestFocus();
//                            mNRICPasscode3.setText("");
                        }
//                        if (v == mPasscode2) {
//                            mPasscode2.setText("");
//                            mPasscode1.setFocusable(true);
//                            mPasscode1.requestFocus();
//                        } else if (v == mPasscode3) {
//                            mPasscode3.setText("");
//                            mPasscode2.setFocusable(true);
//                            mPasscode2.requestFocus();
//                        } else if (v == mPasscode4) {
//                            mPasscode4.setText("");
//                            mPasscode3.setFocusable(true);
//                            mPasscode3.requestFocus();
//                        } else {
//                            mPasscode1.setText("");
//                        }
                        return true;
                    default:
                        if (mPasscode1.isFocused()) {
                            mPasscode2.requestFocus();
                        } else if (mPasscode2.isFocused()) {
                            mPasscode3.requestFocus();
                        } else if (mPasscode3.isFocused()) {
                            mPasscode4.requestFocus();
                        } else if (mPasscode4.isFocused()) {
                            mNRICPasscode1.requestFocus();
                        } else if (mNRICPasscode1.isFocused()) {
                            mNRICPasscode2.requestFocus();
                        } else if (mNRICPasscode2.isFocused()) {
                            mNRICPasscode3.requestFocus();
                        } else if (mNRICPasscode3.isFocused()) {
                            mNRICPasscode4.requestFocus();
                        }
                        break;
                }
            }
            return false;
        }
    }

    public class OnTextWatcher implements TextWatcher {

        EditText mEditText;

        public OnTextWatcher(EditText mEditText) {
            this.mEditText = mEditText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0) {
                if (mEditText == mPasscode1) {
                    mPasscode2.setFocusable(true);
                    mPasscode2.requestFocus();
                } else if (mEditText == mPasscode2) {
                    mPasscode3.setFocusable(true);
                    mPasscode3.requestFocus();
                } else if (mEditText == mPasscode3) {
                    mPasscode4.setFocusable(true);
                    mPasscode4.requestFocus();
                } else if (mEditText == mPasscode4) {
                    mNRICPasscode1.setFocusable(true);
                    mNRICPasscode1.requestFocus();
                } else if (mEditText == mNRICPasscode1) {
                    mNRICPasscode2.setFocusable(true);
                    mNRICPasscode2.requestFocus();
                } else if (mEditText == mNRICPasscode2) {
                    mNRICPasscode3.setFocusable(true);
                    mNRICPasscode3.requestFocus();
                } else if (mEditText == mNRICPasscode3) {
                    mNRICPasscode4.setFocusable(true);
                    mNRICPasscode4.requestFocus();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    void loginViaPasscode(String passCode, String icCode) {
//        startActivity(new Intent(this, DigitalSignatureActivity.class));
//        if (Common.Helpers.checkReadPhoneStatePermission(this)) {
//            Common.Helpers.openPermissionDialog(this).show();
//            return;
//        }

        BaseApi.init(this)
                .loginViaPasscode(passCode, icCode)
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        if (data.getResult().getOrder() != null) {

                            Intent intent = new Intent(PasscodeSignInActivity.this, TADetailActivity.class);
                            intent.putExtra(GlobalParam.INTENT_ORDER_DATA, data.getResult().getOrder());
                            intent.putExtra(GlobalParam.INTENT_ORDER_USER_ID, data.getResult().getOrderUserID());
                            intent.putExtra(GlobalParam.INTENT_IS_LOGIN_WITH_PASSCODE, true);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verify_btn: {
                String passCode;
                String icCode;
                passCode = mPasscode1.getText().toString().trim().length() != 0 ? mPasscode1.getText().toString() : "0";
                passCode = passCode + (mPasscode2.getText().toString().trim().length() != 0 ? mPasscode2.getText().toString() : "0");
                passCode = passCode + (mPasscode3.getText().toString().trim().length() != 0 ? mPasscode3.getText().toString() : "0");
                passCode = passCode + (mPasscode4.getText().toString().trim().length() != 0 ? mPasscode4.getText().toString() : "0");
                icCode = mNRICPasscode1.getText().toString().trim().length() != 0 ? mNRICPasscode1.getText().toString() : "0";
                icCode = icCode + (mNRICPasscode2.getText().toString().trim().length() != 0 ? mNRICPasscode2.getText().toString() : "0");
                icCode = icCode + (mNRICPasscode3.getText().toString().trim().length() != 0 ? mNRICPasscode3.getText().toString() : "0");
                icCode = icCode + (mNRICPasscode4.getText().toString().trim().length() != 0 ? mNRICPasscode4.getText().toString() : "0");

                loginViaPasscode(passCode, icCode);
                break;
            }
        }
    }
}

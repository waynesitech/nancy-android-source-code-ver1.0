package com.lesys.lepro.ai.model.generate_new_ta.param_api;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lesys.lepro.ai.model.generate_new_ta.LandlordTenantDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.MoneyModel;
import com.lesys.lepro.ai.shared.Common;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 25/06/2017.
 */

public class LandlordTenantCover implements Serializable {

    private List<LandlordTenant> landlordTenantList = new ArrayList<>();

    public String getJson(List<LandlordTenantDetailModel> landlordTenantDetailList) {
        landlordTenantList.clear();
        for (LandlordTenantDetailModel model : landlordTenantDetailList) {
            LandlordTenant landlordTenant = new LandlordTenant();
            landlordTenant.setName(model.name);
            landlordTenant.setAddress(model.address);
            landlordTenant.setCompany(model.isCompany);
            landlordTenant.setCompanyName(model.companyName);
            landlordTenant.setCompanyRegNum(model.companyRegNum);
            landlordTenant.setEmail(model.email);
            landlordTenant.setIcNum(model.icNum);
            landlordTenant.setContactNum(model.countryCode + model.contactNum);

            landlordTenantList.add(landlordTenant);
        }
        if (landlordTenantList.size() != 0) {
            return new Gson().toJson(landlordTenantList);
        } else {
            return null;
        }
    }
}

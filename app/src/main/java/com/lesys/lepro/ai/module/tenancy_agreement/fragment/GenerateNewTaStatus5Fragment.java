package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.joanzapata.iconify.widget.IconTextView;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.SpecialRequest;
import com.lesys.lepro.ai.model.SpecialRequestCover;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.SpecialConditionModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatus5Fragment extends BaseFragment implements View.OnClickListener {

    ScrollView mScrollView;
    TextView mPropertyType;
    LinearLayout mSpecialCondition;
    Button mNextBtn;
//    Button mPreviousBtn;

    public NewTAModel newTAModel;
    GenerateNewTAActivity generateNewTAActivity;

    boolean isEdit;
    boolean isTARenew;

    public static GenerateNewTaStatus5Fragment newInstance() {
        GenerateNewTaStatus5Fragment f = new GenerateNewTaStatus5Fragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_5, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);

        mScrollView = (ScrollView) v.findViewById(R.id.scrollView);
        mPropertyType = (TextView) v.findViewById(R.id.property_type);
        mSpecialCondition = (LinearLayout) v.findViewById(R.id.special_conditions_layout);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);

        mNextBtn.setOnClickListener(this);
//        mPreviousBtn.setOnClickListener(this);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        loadLocalData();
    }

    public void loadLocalData() {
        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mPropertyType.setText(!newTAModel.isCommercial ? R.string.residential : R.string.commercial);

        mSpecialCondition.removeAllViews();
        if (newTAModel.mSpecialRequestCover != null &&
                newTAModel.mSpecialRequestCover.getArrayList() != null &&
                newTAModel.mSpecialRequestCover.getArrayList().size() != 0) {
            for (int i = 0; i < newTAModel.mSpecialRequestCover.getArrayList().size(); i++) {
                String s = newTAModel.mSpecialRequestCover.getArrayList().get(i).getOriginal();
                generateSpecialConditionView(i, s);
            }
            return;
        }

        generateSpecialConditionView(0, "");
    }

    void generateSpecialConditionView(int position, String s) {
        final View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_special_condition_view, null);

        final TextView mIndex = (TextView) v.findViewById(R.id.index);
        TextInputEditText mSpecialConditionText = (TextInputEditText) v.findViewById(R.id.special_condition_text);
        IconTextView mAddOnBtn = (IconTextView) v.findViewById(R.id.add_on_btn);
        IconTextView mRemoveBtn = (IconTextView) v.findViewById(R.id.remove_btn);
        final int index = mSpecialCondition.getChildCount();
        mSpecialConditionText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        mSpecialConditionText.setText(s);
        mSpecialConditionText.requestFocus();
        mIndex.setText((index + 1) + ".");
        mIndex.setTag(position);
        mAddOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSpecialCondition.getChildCount() >= 10) {
                    Toast.makeText(getActivity(), R.string.toast_max_special_conditions, Toast.LENGTH_LONG).show();
                    return;
                }
                generateSpecialConditionView(((int) mIndex.getTag() + 1), "");
            }
        });
        mRemoveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View btnView) {
                mSpecialCondition.removeView(v);
                for (int i = 0; i < mSpecialCondition.getChildCount(); i++) {
                    IconTextView mRemoveButton = (IconTextView) mSpecialCondition.getChildAt(i).findViewById(R.id.remove_btn);
                    TextView mIndexText = (TextView) mSpecialCondition.getChildAt(i).findViewById(R.id.index);
                    mIndexText.setText((i + 1) + ".");
                    mIndexText.setTag(i);

                    mRemoveButton.setVisibility(mSpecialCondition.getChildCount() != 1 ? View.VISIBLE : View.GONE);
                }
            }
        });
        mSpecialConditionText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSpecialCondition.addView(v, position);
        mScrollView.fullScroll(View.FOCUS_DOWN);

        for (int i = 0; i < mSpecialCondition.getChildCount(); i++) {
            IconTextView mRemoveButton = (IconTextView) mSpecialCondition.getChildAt(i).findViewById(R.id.remove_btn);
            TextView mIndexText = (TextView) mSpecialCondition.getChildAt(i).findViewById(R.id.index);
            mIndexText.setText((i + 1) + ".");
            mIndexText.setTag(i);

            mRemoveButton.setVisibility(mSpecialCondition.getChildCount() != 1 ? View.VISIBLE : View.GONE);
        }
    }

    void updateNewTAModel() {
        newTAModel.mSpecialRequestCover = new SpecialRequestCover();
        for (int i = 0; i < mSpecialCondition.getChildCount(); i++) {
            TextInputEditText mEditText = (TextInputEditText) mSpecialCondition.getChildAt(i).findViewById(R.id.special_condition_text);
            if (!TextUtils.isEmpty(mEditText.getText().toString())) {
                SpecialRequest mSpecialRequest = new SpecialRequest();
                mSpecialRequest.setEdited("");
                mSpecialRequest.setOriginal(mEditText.getText().toString());
                newTAModel.mSpecialRequestCover.getArrayList().add(mSpecialRequest);
            }
        }
        newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[7] : generateNewTAActivity.mResidentialSlideFragmentTag[6];

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                updateNewTAModel();
                generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[7] : generateNewTAActivity.mResidentialSlideFragmentTag[6]);
                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

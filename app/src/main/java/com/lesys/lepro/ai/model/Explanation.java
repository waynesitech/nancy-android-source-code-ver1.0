package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 14/07/2017.
 */

public class Explanation implements Serializable {

    private String explanationNumber;
    private String explanationText;
    private String explanationURL;

    public String getExplanationNumber() {
        return explanationNumber;
    }

    public void setExplanationNumber(String explanationNumber) {
        this.explanationNumber = explanationNumber;
    }

    public String getExplanationText() {
        return explanationText;
    }

    public void setExplanationText(String explanationText) {
        this.explanationText = explanationText;
    }

    public String getExplanationURL() {
        return explanationURL;
    }

    public void setExplanationURL(String explanationURL) {
        this.explanationURL = explanationURL;
    }

}

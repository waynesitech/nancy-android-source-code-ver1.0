package com.lesys.lepro.ai.module.authentication;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.greysonparrelli.permiso.Permiso;
import com.heetch.countrypicker.Country;
import com.heetch.countrypicker.CountryPickerCallbacks;
import com.heetch.countrypicker.CountryPickerDialog;
import com.heetch.countrypicker.Utils;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.NcLocation;
import com.lesys.lepro.ai.model.UserCover;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.LocationUtil;
import com.lesys.lepro.ai.shared.CustomPhoneNumberUtil;
import com.lesys.lepro.ai.shared.PickAndCrop;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.noelchew.permisowrapper.PermisoWrapper;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.util.List;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onResume() {
        super.onResume();
        Permiso.getInstance().setActivity(this);
    }

    private static final String TAG = SignUpActivity.class.getName();

    //    ImageView mAvatar;
    TextInputEditText mUserName;
    TextInputEditText mICNo;
    TextInputEditText mEmail;
    TextInputEditText mPassword;
    TextInputEditText mReenterPassword;
    TextInputEditText mMobileNumber;
    AppCompatCheckBox mTnCCheckBox;
    TextView mTnC;
    Button mSignUpBtn;
    TextView mHaveAnAccount;
    TextView mWarningText;
    Button mCountryCodeBtn;


    File mProfileImageFile;
    private PickAndCrop mPickAndCrop;
    String mStringURL = "";
    int ratioX = 1;
    int ratioY = 1;
    Transformation transformation = new RoundedTransformationBuilder()
            .borderColor(Color.WHITE)
            .borderWidthDp(2)
            .cornerRadiusDp(180)
            .oval(false)
            .build();
    String currentCountryCode;
    private String mCountryISOCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

//        mAvatar = (ImageView) findViewById(R.id.avatar);
        mUserName = (TextInputEditText) findViewById(R.id.username);
        mICNo = (TextInputEditText) findViewById(R.id.ic_no);
        mEmail = (TextInputEditText) findViewById(R.id.email);
        mPassword = (TextInputEditText) findViewById(R.id.password);
        mReenterPassword = (TextInputEditText) findViewById(R.id.reenter_password);
        mMobileNumber = (TextInputEditText) findViewById(R.id.mobile_number);
        mTnCCheckBox = (AppCompatCheckBox) findViewById(R.id.tnc_check_box);
        mTnC = (TextView) findViewById(R.id.tnc);
        mSignUpBtn = (Button) findViewById(R.id.sign_up_btn);
        mHaveAnAccount = (TextView) findViewById(R.id.have_an_account);
        mWarningText = (TextView) findViewById(R.id.warning_text);
        mCountryCodeBtn = (Button) findViewById(R.id.country_code_btn);
        mWarningText.setVisibility(View.GONE);

        mPickAndCrop = new PickAndCrop(this);
        Permiso.getInstance().setActivity(this);

//        mAvatar.setOnClickListener(this);
        mCountryCodeBtn.setOnClickListener(this);
        mSignUpBtn.setOnClickListener(this);
        mUserName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        setTnCStyle();
        setHaveAnAccountStyle();
        currentCountryCode = mCountryCodeBtn.getText().toString();

        PermisoWrapper.getPermissionLocateUser(this, new PermisoWrapper.PermissionListener() {
            @Override
            public void onPermissionGranted() {
                LocationUtil.getInstance().getLocation(locationModuleListener);
            }

            @Override
            public void onPermissionDenied() {

            }
        });
    }

    private LocationUtil.LocationModuleListener locationModuleListener = new LocationUtil.LocationModuleListener() {
        @Override
        public void OnGetLocationSuccess(NcLocation ncLocation) {
            setTvCountryCodeValue(ncLocation.countryISOCode);
        }

        @Override
        public void OnGetLocationFail(String errorMessage) {
            // do nothing
        }
    };

    private void setTvCountryCodeValue(String countryISOCode) {
        String countryCode = countryISOCode;
        String countryDialCode = "";
        List<Country> countries = Utils.parseCountries(Utils.getCountriesJSON(this));
        for (Country country : countries) {
            if (country.getIsoCode().equalsIgnoreCase(countryCode)) {
                countryDialCode = country.getDialingCode();
                break;
            }
        }
        Country country = new Country(countryCode, countryDialCode);
        mCountryCodeBtn.setText(country.getDialingCode());
        currentCountryCode = mCountryCodeBtn.getText().toString();
//        mCountryISOCode = country.getIsoCode();
    }

    void setTnCStyle() {
        String text = getString(R.string.i_have_read_tnc);
        String word = getString(R.string.i_have_read_tnc_matching_word);
        SpannableString myString = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };

        int startIndex = 0;
        for (int i = -1; (i = text.indexOf(word, i + 1)) != -1; ) {
            startIndex = i;
        }
        int lastIndex = startIndex + word.length();
        //For Click
        myString.setSpan(clickableSpan, startIndex, lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //For Bold
        myString.setSpan(new StyleSpan(Typeface.ITALIC), startIndex, lastIndex, 0);

        myString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.normal_text)), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mTnC.setText(myString);
        mTnC.setMovementMethod(LinkMovementMethod.getInstance());
    }

    void setHaveAnAccountStyle() {
        String text = getString(R.string.have_an_account);
        String word = getString(R.string.have_an_account_matching_word);
        SpannableString myString = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        int startIndex = 0;
        for (int i = -1; (i = text.indexOf(word, i + 1)) != -1; ) {
            startIndex = i;
        }
        int lastIndex = startIndex + word.length();
        //For Click
        myString.setSpan(clickableSpan, startIndex, lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //For Bold
        myString.setSpan(new StyleSpan(Typeface.BOLD), startIndex, lastIndex, 0);

        myString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.normal_text)), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mHaveAnAccount.setText(myString);
        mHaveAnAccount.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private String getPhoneNumber() {
        String phoneNum = "+" + currentCountryCode + mMobileNumber.getText().toString();
        return CustomPhoneNumberUtil.getFormattedPhoneNumber(phoneNum, mCountryISOCode);
    }

    void signUp() {

        String phoneNumber = getPhoneNumber();

        if (!mTnCCheckBox.isChecked()) {
            Toast.makeText(this, R.string.toast_request_agree_tnc, Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(mEmail.getText().toString())) {
            mEmail.setError(getString(R.string.toast_this_filed_is_required), Common.Helpers.getErrorDrawable(this));
            mEmail.setHint(getString(R.string.email) + "**");
        }
        if (TextUtils.isEmpty(mICNo.getText().toString())) {
            mICNo.setError(getString(R.string.toast_this_filed_is_required), Common.Helpers.getErrorDrawable(this));
            mICNo.setHint(getString(R.string.ic_no) + "**");
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            mPassword.setError(getString(R.string.toast_this_filed_is_required), Common.Helpers.getErrorDrawable(this));
            mPassword.setHint(getString(R.string.password) + "**");
        }
        if (TextUtils.isEmpty(mReenterPassword.getText().toString())) {
            mReenterPassword.setError(getString(R.string.toast_this_filed_is_required), Common.Helpers.getErrorDrawable(this));
            mReenterPassword.setHint(getString(R.string.reenter_password) + "**");
        }
        if (TextUtils.isEmpty(mUserName.getText().toString())) {
            mUserName.setError(getString(R.string.toast_this_filed_is_required), Common.Helpers.getErrorDrawable(this));
            mUserName.setHint(getString(R.string.username) + "**");
        }
        if (TextUtils.isEmpty(mMobileNumber.getText().toString())) {
            mMobileNumber.setError(getString(R.string.toast_this_filed_is_required), Common.Helpers.getErrorDrawable(this));
            mMobileNumber.setHint(getString(R.string.mobile_number) + "**");
        }

        if (TextUtils.isEmpty(mEmail.getText().toString()) ||
                TextUtils.isEmpty(mICNo.getText().toString()) ||
                TextUtils.isEmpty(mPassword.getText().toString()) ||
                TextUtils.isEmpty(mReenterPassword.getText().toString()) ||
                TextUtils.isEmpty(mUserName.getText().toString()) ||
                TextUtils.isEmpty(mMobileNumber.getText().toString())) {
            mWarningText.setVisibility(View.VISIBLE);
            return;
        }

        if (!mPassword.getText().toString().equals(mReenterPassword.getText().toString())) {
            mPassword.setError(getString(R.string.toast_password_not_match), Common.Helpers.getErrorDrawable(this));
            mReenterPassword.setError(getString(R.string.toast_password_not_match), Common.Helpers.getErrorDrawable(this));
            return;
        }

        phoneNumber = phoneNumber.replaceAll("\\W", "");

        mWarningText.setVisibility(View.GONE);
        BaseApi.init(this)
                .registerAccount(
                        mEmail.getText().toString(),
                        mPassword.getText().toString(),
                        mICNo.getText().toString(),
                        mUserName.getText().toString(),
                        phoneNumber
                )
                .call(new DataHandler<UserCover>(UserCover.class) {
                    @Override
                    public void onSuccess(UserCover data) {
                        setResult(RESULT_OK);
                        SharedPreferencesUtil.setString(SignUpActivity.this, GlobalParam.KEY_USER_DATA, new Gson().toJson(data.getResult()));
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });

    }

    private void countryCodeSelected(Country country) {
        mCountryCodeBtn.setText(country.getDialingCode());
        mCountryISOCode = country.getIsoCode();
        currentCountryCode = mCountryCodeBtn.getText().toString();
        LocationUtil.cancelLocationListenerCallback();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "lemon debug : requestCode = " + requestCode);
        if (mPickAndCrop != null)
            mPickAndCrop.handleResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
//                case 6709: {
//                    try {
//                        mStringURL = mPickAndCrop.getImageUri().getPath();
//                        Picasso.with(this)
//                                .load(mPickAndCrop.getImageUri())
//                                .transform(transformation)
////                                .error(R.drawable.placeholder_user)
////                                .placeholder(R.drawable.placeholder_user)
//                                .networkPolicy(NetworkPolicy.NO_CACHE)
//                                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                                .into(mAvatar);
//                        mProfileImageFile = new File(mStringURL);
//                    } catch (Exception ex) {
//                        ex.printStackTrace();
//                    }
//                    return;
//                }
                case GlobalParam.RC_REGISTED: {
                    setResult(RESULT_OK);
                    finish();
                    break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatar: {
//                PermisoWrapper.getPermissionTakePicture(this, new PermisoWrapper.PermissionListener() {
//                    @Override
//                    public void onPermissionGranted() {
//                        mPickAndCrop.selectImage(ratioX, ratioY);
//                    }
//
//                    @Override
//                    public void onPermissionDenied() {
//
//                    }
//                });
                break;
            }
            case R.id.country_code_btn: {
                CountryPickerDialog countryPicker =
                        new CountryPickerDialog(this, new CountryPickerCallbacks() {
                            @Override
                            public void onCountrySelected(Country country, int flagResId) {
                                countryCodeSelected(country);
                            }
                        });
                countryPicker.show();
                break;
            }
            case R.id.sign_up_btn: {
                signUp();
                break;
            }
        }
    }
}

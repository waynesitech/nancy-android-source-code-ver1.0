package com.lesys.lepro.ai.shared;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.Phonenumber;

/**
 * Created by aio-synergy on 2/3/16.
 */
public class CustomPhoneNumberUtil {

    // will not return "+" in front of the number
    public static String getFormattedPhoneNumber(String phoneNumber, String countryIsoCode) {
        com.google.i18n.phonenumbers.PhoneNumberUtil phoneUtil = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber pn = phoneUtil.parse(phoneNumber, countryIsoCode);
            String pnE164 = phoneUtil.format(pn, com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat.E164);
            return pnE164.replace("+", "");
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            return "";
        }
    }

    public static String getDisplayPhoneNumber(String phoneNumber, String countryIsoCode) {
        com.google.i18n.phonenumbers.PhoneNumberUtil phoneUtil = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber pn = phoneUtil.parse(phoneNumber, countryIsoCode);
            String internationalFormat = phoneUtil.format(pn, com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            return internationalFormat;
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            return "";
        }
    }
}

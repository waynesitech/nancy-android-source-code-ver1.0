package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class SMSStatusListResultCover extends MetaData implements Serializable {

    private SMSStatusListCover result;

    public SMSStatusListCover getResult() {
        return result;
    }

    public void setResult(SMSStatusListCover result) {
        this.result = result;
    }

}

package com.lesys.lepro.ai.model.generate_new_ta;

import java.io.Serializable;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class DayHourMinModel implements Serializable {

    public String day;
    public String hour;
    public String min;
}

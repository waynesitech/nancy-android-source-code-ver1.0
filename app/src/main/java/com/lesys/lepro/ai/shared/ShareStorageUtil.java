package com.lesys.lepro.ai.shared;

import android.os.Environment;

import java.io.File;

public class ShareStorageUtil {
    public static String sdPath = String.valueOf(Environment.getExternalStorageDirectory());
    public static String path = sdPath + "/Nancy/";
    public static String imgPath = path + "Image/";
    public static String audioPath = path + "Audio/";
    public static String videoPath = path + "Video/";
    public static String pdfPath = path + "PDF/";

    public static void createFolder(String path) {
        File path1 = new File(path);
        if (!path1.exists()) {
            path1.mkdirs();
        }
    }

    public static String saveImagePath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(imgPath);
        return imgPath + filePath;
    }

    public static String saveAudioPath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(audioPath);
        return audioPath + filePath;
    }

    public static String saveVideoPath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(videoPath);
        return videoPath + filePath;
    }

    public static String savePDFPath(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(pdfPath);
        return pdfPath + filePath;
    }

    public static String getPDFFile(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(pdfPath);

        File file = new File(pdfPath + filePath);
        if (file.exists()) {
            return pdfPath + filePath;
        }
        return null;
    }

    public static String saveImageFile(String filePath) {
        if (filePath != null)
            if (filePath.startsWith("/"))
                filePath = filePath.substring(1);
        createFolder(imgPath);
        return imgPath + filePath;
    }

    public static String getImageFile(String fileName) {
        if (fileName != null)
            if (fileName.startsWith("/"))
                fileName = fileName.substring(1);
        createFolder(imgPath);

        File file = new File(imgPath + fileName);
        if (file.exists()) {
            return imgPath + fileName;
        }
        return null;
    }

    public static String getImagePath() {
        createFolder(imgPath);
        return imgPath;
    }

    public static String getAudioPath() {
        createFolder(audioPath);
        return audioPath;
    }

    public static String getVideoPath() {
        createFolder(videoPath);
        return videoPath;
    }

    public static String getPDFPath() {
        createFolder(pdfPath);
        return pdfPath;
    }
}

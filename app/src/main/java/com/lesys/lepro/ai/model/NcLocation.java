package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by aio-synergy on 2/4/16.
 */
public class NcLocation implements Serializable {

    public String country;
    public String countryISOCode;
    public float latitude;
    public float longitude;

    public NcLocation(String country, String countryISOCode, float latitude, float longitude) {
        this.country = country;
        this.countryISOCode = countryISOCode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

}

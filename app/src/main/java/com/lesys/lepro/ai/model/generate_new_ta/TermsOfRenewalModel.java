package com.lesys.lepro.ai.model.generate_new_ta;

import com.lesys.lepro.ai.model.OrderFullDetail;

import java.io.Serializable;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class TermsOfRenewalModel implements Serializable {

    public boolean isMarkerPrevailingRate;
    public boolean isIncreaseOf10Percent;
    public boolean isIncreaseOf15Percent;
    public boolean isAtMutuallyAgreeRate;
    public boolean isOther;
    public String totalOtherPercentage;

    public TermsOfRenewalModel() {

    }

    public TermsOfRenewalModel syncServerTA(OrderFullDetail order) {
        switch (order.getOptionToRenewCondition()) {
            case "isMarkerPrevailingRate": {
                isMarkerPrevailingRate = true;
                break;
            }
            case "isIncreaseOf10percent": {
                isIncreaseOf10Percent = true;
                break;
            }
            case "isIncreaseOf15percent": {
                isIncreaseOf15Percent = true;
                break;
            }
            case "isMutuallyAgreedRate": {
                isAtMutuallyAgreeRate = true;
                break;
            }
            case "isOtherPercent": {
                isOther = true;
                totalOtherPercentage = order.getOtherTermsofRenewal();
                break;
            }
        }
        return this;
    }
}

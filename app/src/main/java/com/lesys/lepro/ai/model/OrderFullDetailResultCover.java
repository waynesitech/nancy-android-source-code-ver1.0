package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class OrderFullDetailResultCover extends MetaData implements Serializable {

    private OrderFullDetailCover result;

    public OrderFullDetailCover getResult() {
        return result;
    }

    public void setResult(OrderFullDetailCover result) {
        this.result = result;
    }
}

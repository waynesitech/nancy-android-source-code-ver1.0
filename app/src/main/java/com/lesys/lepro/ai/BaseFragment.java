package com.lesys.lepro.ai;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.lesys.lepro.ai.shared.ClickActionInterface;

/**
 * Created by NicholasTan on 29/06/2016.
 */
public class BaseFragment extends Fragment {

    protected SwipeRefreshLayout mSwipeRefreshLayout;
    private final String TAG = "BaseFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated");
    }

    protected ActionBar getSupportActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    protected void onRefreshStart() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void displayLoading(final View v, final boolean isArrayListEmpty) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isArrayListEmpty) {
                        v.findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
                        v.findViewById(R.id.loading).setVisibility(View.VISIBLE);
                        v.findViewById(R.id.retry_layout).setVisibility(View.GONE);
                        v.findViewById(R.id.no_data_msg).setVisibility(View.GONE);
                    }
                }
            });

        } catch (Exception e) {
        }
    }

    public void displayRetryLayout(final View v, final boolean isArrayListEmpty, final ClickActionInterface.RetryActionInterface action) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isArrayListEmpty) {
                        v.findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
                        v.findViewById(R.id.loading).setVisibility(View.GONE);
                        v.findViewById(R.id.retry_layout).setVisibility(View.VISIBLE);
                        v.findViewById(R.id.no_data_msg).setVisibility(View.GONE);
//                        CommonUtil.setRadiusColor(getActivity(), v.findViewById(R.id.retry_btn), 80, R.color.button_primary, R.color.button_primary_light, R.color.grey);
                        v.findViewById(R.id.retry_btn).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (action != null)
                                    action.onRetryClickListener();
                            }
                        });
                    }
                }
            });

        } catch (Exception e) {
        }
    }

    public void dismissLoading(final View v, final boolean isArrayListEmpty) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    v.findViewById(R.id.loading_layout).setVisibility(isArrayListEmpty ? View.VISIBLE : View.GONE);
                    v.findViewById(R.id.loading).setVisibility(View.GONE);
                    v.findViewById(R.id.retry_layout).setVisibility(View.GONE);
                    v.findViewById(R.id.no_data_msg).setVisibility(isArrayListEmpty ? View.VISIBLE : View.GONE);
                }
            });
        } catch (Exception e) {

        }
    }

}
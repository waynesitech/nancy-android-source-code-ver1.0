package com.lesys.lepro.ai;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.module.nancy.GenerateTAConfirmationActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.TenancyAgreementListActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;
import com.noelchew.permisowrapper.PermisoWrapper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class MainFunctionFragment extends BaseFragment {

    RecyclerView mRecyclerView;

    MainFunctionAdapter mAdapter;
    String[] nancyMainFunctionArray;
    User mUser;
    NewTAModel newTAModel;

    public static MainFunctionFragment newInstance() {
        MainFunctionFragment f = new MainFunctionFragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        mUser = new User().getUser(getActivity());
        nancyMainFunctionArray = getResources().getStringArray(R.array.nancy_main_function_array);
        mRecyclerView.setHasFixedSize(true);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0)
                    return 2;
                else
                    return 1;
            }
        });
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MainFunctionAdapter(getActivity()) {
            @Override
            public void onItemClick(int position) {
                switch (position) {
                    case 1: {
                        startActivity(new Intent(getActivity(), TenancyAgreementListActivity.class));
                        break;
                    }
                    case 2: {
                        newTAModel = null;
                        String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
                        if (!TextUtils.isEmpty(keyNewTAModel)) {
                            newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
                        }
                        if (newTAModel != null) {
                            dialogTAUnsubmmited();
                        } else {
                            startActivity(new Intent(getActivity(), GenerateTAConfirmationActivity.class));
                        }
                        break;
                    }
                    case 3:
                    case 4:
                    case 5: {
                        String uploadType;
                        Intent intent = new Intent(getActivity(), ESignEStampSelectionActivity.class);
                        intent.putExtra(GlobalParam.INTENT_TITLE, nancyMainFunctionArray[position - 1]);
                        if (nancyMainFunctionArray[position - 1].equals(getString(R.string.slidemenu_e_sign))) {
                            uploadType = GlobalParam.TAUploadType.UPLOAD_TYPE_E_SIGN;
                        } else if (nancyMainFunctionArray[position - 1].equals(getString(R.string.slidemenu_e_stamp))) {
                            uploadType = GlobalParam.TAUploadType.UPLOAD_TYPE_E_STAMP;
                        } else if (nancyMainFunctionArray[position - 1].equals(getString(R.string.slidemenu_atuo_reminder))) {
                            uploadType = GlobalParam.TAUploadType.UPLOAD_TYPE_AUTO_REMINDER;
                        } else {
                            uploadType = GlobalParam.TAUploadType.UPLOAD_TYPE_NORMAL;
                        }
                        intent.putExtra(GlobalParam.INTENT_UPLOAD_TYPE, uploadType);
                        startActivity(intent);
                        break;
                    }
                }
            }
        };
        mAdapter.setHeader(mUser);
        mAdapter.setItems(new ArrayList<>(Arrays.asList(nancyMainFunctionArray)));
        mRecyclerView.setAdapter(mAdapter);

        return v;
    }

    void dialogTAUnsubmmited() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.dialog_ta_unsubmitted)
                .setPositiveButton(R.string.continue_editing, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getActivity(), GenerateNewTAActivity.class));
                    }
                })
                .setNegativeButton(R.string.discard, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, "");
                        startActivity(new Intent(getActivity(), GenerateTAConfirmationActivity.class));
                    }
                })
                .setNeutralButton(R.string.cancel, null);
        builder.create().show();
    }
}

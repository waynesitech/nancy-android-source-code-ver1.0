package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.model.OrderResultCover;
import com.lesys.lepro.ai.model.SpecialRequestCover;
import com.lesys.lepro.ai.model.generate_new_ta.CollectionByLandlordModel;
import com.lesys.lepro.ai.model.generate_new_ta.DirectBankInModel;
import com.lesys.lepro.ai.model.generate_new_ta.FixtureFittingsModel;
import com.lesys.lepro.ai.model.generate_new_ta.LandlordTenantDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.MoneyModel;
import com.lesys.lepro.ai.model.generate_new_ta.MonthYearModel;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.PaymentDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.PostDatedChequeModel;
import com.lesys.lepro.ai.model.generate_new_ta.PropertyDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.RentalDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.SpecialConditionModel;
import com.lesys.lepro.ai.model.generate_new_ta.TermsOfRenewalModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.TADetailActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.ViewPDFActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.FileDownloader;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.ShareStorageUtil;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;
import com.lesys.lepro.ai.shared.TransformationUtil;
import com.noelchew.permisowrapper.PermisoWrapper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class PreviewFragment extends BaseFragment implements View.OnClickListener {

    ScrollView mScrollView;
    Button mNextBtn;

    /**
     * Property Details
     */
    ImageView mImage;
    TextView mTypeOfProperty;
    TextView mAddress;

    /**
     * Landlord Tenant Details
     */
    TextView mOwnerTenantLandlordTitle;
    LinearLayout mOwnerTenantLandlordLayout;
    TextView mNonOwnerTenantLandlordTitle;
    LinearLayout mNonOwnerTenantLandlordLayout;

    /**
     * Rental Details
     */
    TextView mCommencementDate;
    TextView mTermOfTenancy;
    TextView mOptionToRenew;
    TableRow mTermsOfRenewalLayout;
    TextView mTermsOfRenewal;
    TextView mRental;
    TextView mAdvanceRental;
    LinearLayout mSecurityDepositLayout;
    TextView mRentalFreePeriod;
    TextView mCarkPark;
    LinearLayout mCarParkLayout;

    /**
     * Payment Details
     */
    TableLayout mDirectBankInLayout;
    TextView mNameOfAccountHolder;
    TextView mBank;
    TextView mBankAccountNumber;
    TextView mMethodOfNotification;
    TableLayout mPostDatedChequeLayout;
    TextView mPostDateCheque;
    TableLayout mCollectionByLandlordLayout;
    TextView mCollectionByLandlord;

    /**
     * Special Condition
     */
    LinearLayout mSpecialCondtionsLayout;

    /**
     * Fixtures & Fittings
     */
    LinearLayout mFixtureFittingsLayout;

    public NewTAModel newTAModel;
    GenerateNewTAActivity generateNewTAActivity;

    boolean isEdit;
    boolean isTARenew;
    String fileName;

    public static PreviewFragment newInstance() {
        PreviewFragment f = new PreviewFragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_preview, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);
        fileName = (System.currentTimeMillis() / 1000) + "";
        Permiso.getInstance().setActivity(getActivity());

        mScrollView = (ScrollView) v.findViewById(R.id.scrollView);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);

        /**
         * Property Details
         */
        mImage = (ImageView) v.findViewById(R.id.image);
        mTypeOfProperty = (TextView) v.findViewById(R.id.type_of_property);
        mAddress = (TextView) v.findViewById(R.id.address);

        /**
         * Landlord Tenant Details
         */
        mOwnerTenantLandlordTitle = (TextView) v.findViewById(R.id.owner_tenant_landlord_title);
        mOwnerTenantLandlordLayout = (LinearLayout) v.findViewById(R.id.owner_tenant_landlord_layout);
        mNonOwnerTenantLandlordTitle = (TextView) v.findViewById(R.id.non_owner_tenant_landlord_title);
        mNonOwnerTenantLandlordLayout = (LinearLayout) v.findViewById(R.id.non_owner_tenant_landlord_layout);

        /**
         * Rental Details
         */
        mCommencementDate = (TextView) v.findViewById(R.id.commencement_date);
        mTermOfTenancy = (TextView) v.findViewById(R.id.term_of_tenancy);
        mOptionToRenew = (TextView) v.findViewById(R.id.option_to_renew);
        mTermsOfRenewalLayout = (TableRow) v.findViewById(R.id.terms_of_renewal_layout);
        mTermsOfRenewal = (TextView) v.findViewById(R.id.terms_of_renewal);
        mRental = (TextView) v.findViewById(R.id.rental);
        mAdvanceRental = (TextView) v.findViewById(R.id.advance_rental);
        mSecurityDepositLayout = (LinearLayout) v.findViewById(R.id.security_deposit_layout);
        mRentalFreePeriod = (TextView) v.findViewById(R.id.rental_free_period);
        mCarkPark = (TextView) v.findViewById(R.id.car_park);
        mCarParkLayout = (LinearLayout) v.findViewById(R.id.car_park_layout);

        /**
         * Payment Details
         */
        mDirectBankInLayout = (TableLayout) v.findViewById(R.id.direct_bank_in_layout);
        mNameOfAccountHolder = (TextView) v.findViewById(R.id.name_of_account_holder);
        mBank = (TextView) v.findViewById(R.id.bank);
        mBankAccountNumber = (TextView) v.findViewById(R.id.bank_account_number);
        mMethodOfNotification = (TextView) v.findViewById(R.id.method_of_notification_upon_payment_made);
        mPostDatedChequeLayout = (TableLayout) v.findViewById(R.id.post_dated_cheque_layout);
        mPostDateCheque = (TextView) v.findViewById(R.id.post_dated_cheque);
        mCollectionByLandlordLayout = (TableLayout) v.findViewById(R.id.collection_by_landlord_layout);
        mCollectionByLandlord = (TextView) v.findViewById(R.id.collection_by_landlord);

        /**
         * Special Condition
         */
        mSpecialCondtionsLayout = (LinearLayout) v.findViewById(R.id.special_conditions_layout);

        /**
         * Fixtures & Fittings
         */
        mFixtureFittingsLayout = (LinearLayout) v.findViewById(R.id.fixture_fittings_layout);

        mNextBtn.setOnClickListener(this);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        Picasso.with(getActivity())
                .load(R.drawable.ic_generate_ta_camera)
                .transform(TransformationUtil.transformationRounded())
                .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                .into(mImage);
        loadLocalData();
        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    public void loadLocalData() {
        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        if (newTAModel == null)
            return;
        Log.d(PreviewFragment.class.getName(), "loadLocalData");
        /**
         * Property Details
         */
        mTypeOfProperty.setText(!newTAModel.isCommercial ? R.string.residential : R.string.commercial);
        if (newTAModel.propertyDetail != null) {
            PropertyDetailModel propertyDetailModel = newTAModel.propertyDetail;
            if (!TextUtils.isEmpty(propertyDetailModel.imagePath)) {
                if (propertyDetailModel.imagePath.contains("http")) {
                    Picasso.with(getActivity())
                            .load(propertyDetailModel.imagePath)
                            .transform(TransformationUtil.transformationRounded())
                            .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                            .into(mImage);
                } else {
                    Picasso.with(getActivity())
                            .load(new File(propertyDetailModel.imagePath))
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .transform(TransformationUtil.transformationRounded())
                            .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                            .into(mImage);
                }
            } else {
                Picasso.with(getActivity())
                        .load(R.drawable.ic_generate_ta_camera)
                        .transform(TransformationUtil.transformationRounded())
                        .resize(Common.Helpers.getDp(getActivity(), 200), Common.Helpers.getDp(getActivity(), 200))
                        .into(mImage);
            }

            mAddress.setText(propertyDetailModel.streetName + "\n" +
                    propertyDetailModel.town + "\n" +
                    propertyDetailModel.postCode + " " +
                    propertyDetailModel.states + "\n" +
                    getString(R.string.malaysia));
        }

        /**
         * Landlord Tenant Details
         */
        mOwnerTenantLandlordLayout.removeAllViews();
        if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() != 0) {
            for (int i = 0; i < newTAModel.ownerLandlordTenantDetailList.size(); i++) {
                generateOnwerTenantLandlordView(i, newTAModel, newTAModel.ownerLandlordTenantDetailList.get(i));
            }
        }
        mNonOwnerTenantLandlordLayout.removeAllViews();
        if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() != 0) {
            for (int i = 0; i < newTAModel.nonOwnerLandlordTenantDetailList.size(); i++) {
                generateNonOnwerTenantLandlordView(i, newTAModel, newTAModel.nonOwnerLandlordTenantDetailList.get(i));
            }
        }

        /**
         * Rental Details
         */
        if (newTAModel.rentalDetail != null) {
            RentalDetailModel rentalDetailModel = newTAModel.rentalDetail;
            mCommencementDate.setText(rentalDetailModel.commencementDate);
            if (rentalDetailModel.termOfTheTenancy != null) {
                MonthYearModel monthYearModel = rentalDetailModel.termOfTheTenancy;
                mTermOfTenancy.setText(getString(R.string.param_year_param_month, monthYearModel.year, monthYearModel.month));
            }
            if (rentalDetailModel.isOptionToRenew) {
                if (rentalDetailModel.optionToRenew != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.optionToRenew;
                    mOptionToRenew.setText(getString(R.string.param_year_param_month, monthYearModel.year, monthYearModel.month));
                }
                mTermsOfRenewalLayout.setVisibility(View.VISIBLE);
                if (rentalDetailModel.termsOfRenewal != null) {
                    String termsOfRenewalStr = "";
                    TermsOfRenewalModel termsOfRenewalModel = rentalDetailModel.termsOfRenewal;
                    if (termsOfRenewalModel.isMarkerPrevailingRate) {
                        termsOfRenewalStr = getString(R.string.marker_prevailing_rate);
                    } else if (termsOfRenewalModel.isIncreaseOf10Percent) {
                        termsOfRenewalStr = getString(R.string.increase_of_10_percentage_in_last_rental_rate);
                    } else if (termsOfRenewalModel.isIncreaseOf15Percent) {
                        termsOfRenewalStr = getString(R.string.increase_of_15_percentage_in_last_rental_rate);
                    } else if (termsOfRenewalModel.isAtMutuallyAgreeRate) {
                        termsOfRenewalStr = getString(R.string.at_a_mutually_agreed_rate);
                    } else if (termsOfRenewalModel.isOther) {
                        termsOfRenewalStr = termsOfRenewalModel.totalOtherPercentage;
                    }
                    mTermsOfRenewal.setText(termsOfRenewalStr);
                }
            } else {
                mOptionToRenew.setText(R.string.no);
                mTermsOfRenewalLayout.setVisibility(View.GONE);
            }
            if (rentalDetailModel.rental != null) {
                MoneyModel moneyModel = rentalDetailModel.rental;
                mRental.setText(getString(R.string.rm_param, moneyModel.ringgitMalaysia, moneyModel.cent));
            }
            if (rentalDetailModel.advanceRental != null) {
                MoneyModel moneyModel = rentalDetailModel.advanceRental;
                mAdvanceRental.setText(getString(R.string.rm_param, moneyModel.ringgitMalaysia, moneyModel.cent));
            }
            if (!rentalDetailModel.isRentalFreePeriodSpecialDate) {
                if (rentalDetailModel.rentalFreePeriod != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.rentalFreePeriod;
                    mRentalFreePeriod.setText(getString(R.string.param_year_param_month, monthYearModel.year, monthYearModel.month));
                }
            } else {
                if (!TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodFromDate) && !TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodToDate)) {
                    mRentalFreePeriod.setText(rentalDetailModel.rentalFreePeriodFromDate + " - " + rentalDetailModel.rentalFreePeriodToDate);
                }
            }

            mCarkPark.setText(getString(R.string.preview_car_park_param, rentalDetailModel.carParkAmount));

            mSecurityDepositLayout.removeAllViews();
            if (rentalDetailModel.securityDepositRent != null) {
                generateSecurityDepositView(getString(R.string.rent), rentalDetailModel.securityDepositRent);
            } else {
                generateSecurityDepositView(getString(R.string.rent), null);
            }
            if (rentalDetailModel.securityDepositUtilities != null) {
                generateSecurityDepositView(getString(R.string.utilities), rentalDetailModel.securityDepositUtilities);
            } else {
                generateSecurityDepositView(getString(R.string.utilities), null);
            }
            for (MoneyModel moneyModel : rentalDetailModel.securityDepositOtherList) {
                generateSecurityDepositView(null, moneyModel);
            }

            mCarParkLayout.removeAllViews();
            for (String s : rentalDetailModel.lotNumberList) {
                generateCarParkView(s);
            }
        }

        /**
         * Payment Details
         */
        if (newTAModel.paymentDetailModel != null) {
            PaymentDetailModel paymentDetailModel = newTAModel.paymentDetailModel;
            mDirectBankInLayout.setVisibility(paymentDetailModel.isDirectBankIn ? View.VISIBLE : View.GONE);
            mPostDatedChequeLayout.setVisibility(paymentDetailModel.isPostDatedCheque ? View.VISIBLE : View.GONE);
            mCollectionByLandlordLayout.setVisibility(paymentDetailModel.isCollectionByLandlord ? View.VISIBLE : View.GONE);

            if (paymentDetailModel.directBankInModel != null) {
                DirectBankInModel directBankInModel = paymentDetailModel.directBankInModel;
                mNameOfAccountHolder.setText(directBankInModel.nameOfAccountHolder);
                mBank.setText(directBankInModel.bank);
                mBankAccountNumber.setText(directBankInModel.bankAccountNumber);
                if (directBankInModel.isWhatsappText) {
                    mMethodOfNotification.setText(R.string.whatsapp_text);
                } else if (directBankInModel.isSMS) {
                    mMethodOfNotification.setText(R.string.sms);
                } else if (directBankInModel.isEmail) {
                    mMethodOfNotification.setText(R.string.email);
                }
            }

            if (paymentDetailModel.postDatedChequeModel != null) {
                PostDatedChequeModel postDatedChequeModel = paymentDetailModel.postDatedChequeModel;
                if (postDatedChequeModel.is6Months) {
                    mPostDateCheque.setText(R.string.months_6);
                } else if (postDatedChequeModel.is12Months) {
                    mPostDateCheque.setText(R.string.months_12);
                } else if (postDatedChequeModel.is24Months) {
                    mPostDateCheque.setText(R.string.months_24);
                }
            }

            if (paymentDetailModel.collectionByLandlordModel != null) {
                CollectionByLandlordModel collectionByLandlordModel = paymentDetailModel.collectionByLandlordModel;
                mCollectionByLandlord.setText(collectionByLandlordModel.isCollectionByLandlord ? R.string.yes : R.string.no);
            }
        }

        /**
         * Special Condition
         */
        mSpecialCondtionsLayout.removeAllViews();
        if (newTAModel.mSpecialRequestCover != null) {
            SpecialRequestCover mSpecialRequestCover = newTAModel.mSpecialRequestCover;
//            SpecialConditionModel specialConditionModel = newTAModel.specialCondition;
            if (mSpecialRequestCover != null &&
                    mSpecialRequestCover.getArrayList() != null &&
                    mSpecialRequestCover.getArrayList().size() != 0) {
                for (int i = 0; i < mSpecialRequestCover.getArrayList().size(); i++) {
                    generateSpecialConditionView(i + 1, mSpecialRequestCover.getArrayList().get(i).getOriginal());
                }
            }
        }

        /**
         * Fixtures & Fittings
         */
        mFixtureFittingsLayout.removeAllViews();
        if (newTAModel.fixtureFittingsModel != null) {
            FixtureFittingsModel fixtureFittingsModel = newTAModel.fixtureFittingsModel;
            if (fixtureFittingsModel.fixtureFittingList != null && fixtureFittingsModel.fixtureFittingList.size() != 0) {
                for (int i = 0; i < fixtureFittingsModel.fixtureFittingList.size(); i++) {
                    generateFixtureFittings(i + 1, fixtureFittingsModel.fixtureFittingList.get(i));
                }
            }
        }
    }

    void generateOnwerTenantLandlordView(int index, NewTAModel newTAModel, LandlordTenantDetailModel model) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_tenant_landlord, null);

        TextView mTitle = (TextView) v.findViewById(R.id.title);
        TableRow mCompanyNameLayout = (TableRow) v.findViewById(R.id.company_name_layout);
        TableRow mCompanyRegNumLayout = (TableRow) v.findViewById(R.id.company_reg_no_layout);
        TextView mCompanyName = (TextView) v.findViewById(R.id.company_name);
        TextView mCompanyRegNum = (TextView) v.findViewById(R.id.company_reg_no);
        TextView mPersonInCharge = (TextView) v.findViewById(R.id.person_in_charge);
        TextView mICNumPassportNum = (TextView) v.findViewById(R.id.ic_no_passport_no);
        TextView mAddress = (TextView) v.findViewById(R.id.address);
        TextView mContactNum = (TextView) v.findViewById(R.id.contact_number);
        TextView mEmail = (TextView) v.findViewById(R.id.email);

        if (model != null) {
            if (newTAModel.isTenant) {
                mTitle.setText(getString(R.string.preview_tenant_details_param,
                        (index + 1) + "",
                        model.isCompany ? getString(R.string.company) : getString(R.string.individual)));
            } else {
                mTitle.setText(getString(R.string.preview_landlord_details_param,
                        (index + 1) + "",
                        model.isCompany ? getString(R.string.company) : getString(R.string.individual)));
            }
            mCompanyNameLayout.setVisibility(model.isCompany ? View.VISIBLE : View.GONE);
            mCompanyRegNumLayout.setVisibility(model.isCompany ? View.VISIBLE : View.GONE);
            mCompanyName.setText(model.companyName);
            mCompanyRegNum.setText(model.companyRegNum);
            mPersonInCharge.setText(model.name);
            mICNumPassportNum.setText(model.icNum);
            mAddress.setText(model.address);
            mContactNum.setText(model.countryCode + model.contactNum);
            mEmail.setText(model.email);
        }

        mOwnerTenantLandlordLayout.addView(v);
    }

    void generateNonOnwerTenantLandlordView(int index, NewTAModel newTAModel, LandlordTenantDetailModel model) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_tenant_landlord, null);

        TextView mTitle = (TextView) v.findViewById(R.id.title);
        TableRow mCompanyNameLayout = (TableRow) v.findViewById(R.id.company_name_layout);
        TableRow mCompanyRegNumLayout = (TableRow) v.findViewById(R.id.company_reg_no_layout);
        TextView mCompanyName = (TextView) v.findViewById(R.id.company_name);
        TextView mCompanyRegNum = (TextView) v.findViewById(R.id.company_reg_no);
        TextView mPersonInCharge = (TextView) v.findViewById(R.id.person_in_charge);
        TextView mICNumPassportNum = (TextView) v.findViewById(R.id.ic_no_passport_no);
        TextView mAddress = (TextView) v.findViewById(R.id.address);
        TextView mContactNum = (TextView) v.findViewById(R.id.contact_number);
        TextView mEmail = (TextView) v.findViewById(R.id.email);

        if (model != null) {
            if (!newTAModel.isTenant) {
                mTitle.setText(getString(R.string.preview_tenant_details_param,
                        (index + 1) + "",
                        model.isCompany ? getString(R.string.company) : getString(R.string.individual)));
            } else {
                mTitle.setText(getString(R.string.preview_landlord_details_param,
                        (index + 1) + "",
                        model.isCompany ? getString(R.string.company) : getString(R.string.individual)));
            }
            mCompanyNameLayout.setVisibility(model.isCompany ? View.VISIBLE : View.GONE);
            mCompanyRegNumLayout.setVisibility(model.isCompany ? View.VISIBLE : View.GONE);
            mCompanyName.setText(model.companyName);
            mCompanyRegNum.setText(model.companyRegNum);
            mPersonInCharge.setText(model.name);
            mICNumPassportNum.setText(model.icNum);
            mAddress.setText(model.address);
            mContactNum.setText(model.countryCode + model.contactNum);
            mEmail.setText(model.email);
        }

        mNonOwnerTenantLandlordLayout.addView(v);
    }

    void generateSecurityDepositView(String name, MoneyModel model) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_preview_security_deposit, null);
        TextView mName = (TextView) v.findViewById(R.id.name);
        TextView mValue = (TextView) v.findViewById(R.id.value);

        if (!TextUtils.isEmpty(name)) {
            mName.setText(name);
        } else {
            mName.setText(model.name);
        }
        if (model != null) {
            mValue.setText(getString(R.string.rm_param, model.ringgitMalaysia, model.cent));
        }
        mSecurityDepositLayout.addView(v);
    }

    void generateCarParkView(String s) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_preview_car_park, null);
        TextView mValue = (TextView) v.findViewById(R.id.value);

        mValue.setText(s);
        mCarParkLayout.addView(v);
    }

    void generateSpecialConditionView(int index, String s) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_preview_special_condition, null);
        TextView mValue = (TextView) v.findViewById(R.id.value);

        mValue.setText(index + ". " + s);
        mSpecialCondtionsLayout.addView(v);
    }

    void generateFixtureFittings(int index, String s) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_preview_special_condition, null);
        TextView mValue = (TextView) v.findViewById(R.id.value);

        mValue.setText(index + ". " + s);
        mFixtureFittingsLayout.addView(v);
    }

    void createOrder() {
        if (newTAModel == null)
            return;
        BaseApi.init(getActivity())
                .createOrder(newTAModel)
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, "");
                        Intent intent = new Intent(getActivity(), TADetailActivity.class);
                        intent.putExtra(GlobalParam.INTENT_ORDER_DATA, data.getResult().getOrder());
                        startActivity(intent);
                        generateNewTAActivity.finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    void createOrderWithPDF() {
        if (newTAModel == null)
            return;
        BaseApi.init(getActivity())
                .createOrderWithPDF(newTAModel)
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, "");
                        Intent intent = new Intent(getActivity(), TADetailActivity.class);
                        intent.putExtra(GlobalParam.INTENT_ORDER_DATA, data.getResult().getOrder());
                        startActivity(intent);
                        generateNewTAActivity.finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    void editOrder() {
        if (newTAModel == null)
            return;
        BaseApi.init(getActivity())
                .editOrder(newTAModel)
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        generateNewTAActivity.finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                new AlertDialog.Builder(getActivity())
                        .setMessage(isEdit ? R.string.dialog_confirm_edit_info : R.string.dialog_rental_your_ta)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (isEdit) {
                                    editOrder();
                                } else {
                                    if (isTARenew && newTAModel.propertyDetail.imagePath.contains("http")) {
                                        PermisoWrapper.getPermissionSaveMediaToStorage(getActivity(), new PermisoWrapper.PermissionListener() {
                                            @Override
                                            public void onPermissionGranted() {
                                                if (ShareStorageUtil.getImageFile(fileName) != null) {
                                                    createOrder();
                                                } else {
                                                    new FileDownloader(getActivity(),
                                                            newTAModel.propertyDetail.imagePath,
                                                            ShareStorageUtil.saveImageFile(fileName)) {
                                                        @Override
                                                        public void onSuccessDownloaded() {
                                                            newTAModel.propertyDetail.imagePath = ShareStorageUtil.getImageFile(fileName);
                                                            createOrder();
                                                        }
                                                    }.download();
                                                }
                                            }

                                            @Override
                                            public void onPermissionDenied() {

                                            }
                                        });
                                    } else {
                                        if (isTARenew || newTAModel.uploadType.equals(GlobalParam.TAUploadType.UPLOAD_TYPE_NORMAL)) {
                                            createOrder();
                                        } else {
                                            createOrderWithPDF();
                                        }
                                    }
                                }

                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
                break;
            }
        }
    }
}

package com.lesys.lepro.ai.module.nancy.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.MainActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.User;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class WelcomeBackFragment extends BaseFragment {

    TextView mWelcomeBackMsg;

    User mUser;

    public static WelcomeBackFragment newInstance() {
        WelcomeBackFragment f = new WelcomeBackFragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_welcome_back, container, false);

        mWelcomeBackMsg = (TextView) v.findViewById(R.id.welcome_back_msg);

        mUser = new User().getUser(getActivity());
        mWelcomeBackMsg.setText(getString(R.string.welcome_back, mUser.getName()));

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).openMainFragment();
            }
        });

        return v;
    }
}

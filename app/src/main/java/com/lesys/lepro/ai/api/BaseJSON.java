package com.lesys.lepro.ai.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.module.authentication.IntroPageActivity;
import com.lesys.lepro.ai.module.authentication.LoginActivity;
import com.lesys.lepro.ai.shared.ClickActionInterface;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

import org.json.JSONObject;

public class BaseJSON {

    private static final String TAG = BaseJSON.class.getName();
    public static final String INTENT_AUTHORIZATION_FAILED = "INTENT_AUTHORIZATION_FAILED";
    public static final String INTENT_AUTHORIZATION_FAILED_MSG = "INTENT_AUTHORIZATION_FAILED_MSG";

    private int code;
    private int resultCode = GlobalParam.RC_LOGIN;
    private String body, subject;
    private Context context;
    private boolean checkGPS = true, logout = true;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return body;
    }

    public BaseJSON(Context context) {
        this.context = context;
    }

    public BaseJSON get(JSONObject jsonObject, ClickActionInterface.DialogActionInterface dialogAction) {

        try {
//            Log.d(TAG, jsonObject.toString());
            try {
                JSONObject messageObject = jsonObject.getJSONObject("meta");
                code = messageObject.getInt("stat");
                messageObject = messageObject.getJSONObject("msg");
                subject = messageObject.getString("subj");
                body = messageObject.getString("body");

                if (!body.equals("")) showDialog(dialogAction);

                if (code == 401 && context != null) handle401Error();

            } catch (Exception e) {
                Log.e(TAG, "Error converting JSON", e);
            }
        } catch (NullPointerException ex) {
            Log.d(TAG, "JSON response return NULL");
            body = "";
            code = 0;
        }


        return this;
    }

    public void handle401Error() {
        SharedPreferencesUtil.setBoolean(context, GlobalParam.KEY_IS_LOGINED, false);
        new BaseRestClient(context).clearCookies();

        Log.e(TAG, "handle401Error");
        if (logout) {
            SharedPreferencesUtil.setString(context, GlobalParam.KEY_USER_DATA, null);
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(context, IntroPageActivity.class);
            context.startActivity(intent);
            finish();
        }
    }

    public void finish() {
        ((Activity) context).finish();
    }

    public BaseJSON resultCode(int resultCode) {
        this.resultCode = resultCode;
        return this;
    }

    public BaseJSON checkGPS(boolean checkGPS) {
        this.checkGPS = checkGPS;
        return this;
    }

    public BaseJSON noLogOut() {
        this.logout = false;
        return this;
    }

    public BaseJSON showDialog() {
        return showDialog(null);
    }

    public BaseJSON showDialog(final ClickActionInterface.DialogActionInterface dialogAction) {
        if (!((Activity) context).isFinishing()) {
            if (code > 0 && !body.equals("")) {
                try {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(subject);
                    alertDialog.setMessage(body);
                    alertDialog.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (dialogAction != null) {
                                dialogAction.onPositiveButtonClick();
                            }
                            // if (code == 401 && context != null)
                            //     handle401Error();
                        }
                    });
                    alertDialog.show();
                }catch(Exception e){

                }
            }
        }
        return this;
    }

    public BaseJSON toast() {
        Toast.makeText(context,
                body, Toast.LENGTH_SHORT).show();
        return this;
    }

    public BaseJSON toast(int length) {
        Toast.makeText(context,
                body, length).show();
        return this;
    }

}

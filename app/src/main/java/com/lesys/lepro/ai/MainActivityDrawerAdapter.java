package com.lesys.lepro.ai;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.karumi.headerrecyclerview.HeaderRecyclerViewAdapter;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.shared.TransformationUtil;
import com.squareup.picasso.Picasso;

/**
 * Created by NicholasTan on 28/04/2016.
 */
public abstract class MainActivityDrawerAdapter extends
        HeaderRecyclerViewAdapter<RecyclerView.ViewHolder, User, String, String> {

    private final String TAG = MainActivityDrawerAdapter.class.getName();
    private Context mContext;

    public MainActivityDrawerAdapter(final Context context) {
        this.mContext = context;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_main_drawer, null);

        return new HeaderViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_drawer, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loadmore, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        final HeaderViewHolder viewHolder = (HeaderViewHolder) holder;
        final User user = getHeader();
        viewHolder.itemViewControl(user, position);
    }


    @Override
    protected void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CustomViewHolder viewHolder = (CustomViewHolder) holder;
        final String data = getItem(position);
        viewHolder.itemViewControl(data, position);
    }


    @Override
    protected void onBindFooterViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    protected void onHeaderViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onItemViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onFooterViewRecycled(RecyclerView.ViewHolder holder) {
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected View mRootView;
        protected ImageView mAvatar;
        protected TextView mUserName;

        public HeaderViewHolder(View v) {
            super(v);
            try {
                mRootView = v;
                this.mAvatar = (ImageView) v.findViewById(R.id.avatar);
                this.mUserName = (TextView) v.findViewById(R.id.username);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void itemViewControl(final User mUser, final int position) {
            try {
                mUserName.setText(mUser.getName());
                Picasso.with(mContext).load(mUser.getImage()).transform(TransformationUtil.transformationRounded()).into(mAvatar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        protected View mRootView;
        protected TextView mName;

        public CustomViewHolder(View v) {
            super(v);
            try {
                mRootView = v;
                this.mName = (TextView) v.findViewById(R.id.name);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void itemViewControl(final String data, final int position) {
            try {
                mName.setText(data);
                mRootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClick(position);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public abstract void onItemClick(int position);
}
package com.lesys.lepro.ai.module.tenancy_agreement;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.model.OrderListCover;
import com.lesys.lepro.ai.model.OrderListResultCover;
import com.lesys.lepro.ai.model.OrderResultCover;
import com.lesys.lepro.ai.module.tenancy_agreement.adapter.TenancyAgreementListAdapter;
import com.lesys.lepro.ai.shared.ClickActionInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.logging.Handler;

/**
 * Created by Nicholas on 18/05/2017.
 */

public class TenancyAgreementListActivity extends BaseActivity implements
        SwipeRefreshLayout.OnRefreshListener {

    FrameLayout mFragmentContainer;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    LinearLayout mRetryLayout;
    TextView mRetryBtn;
    RelativeLayout mLoadingLayout;
    ProgressBar mLoading;
    TextView mNoDataFound;

    TenancyAgreementListAdapter mAdapter;
    ArrayList<Order> arrayList = new ArrayList<>();

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    final int length = 10;
    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenancy_agreement_list);

        mFragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLoadingLayout = (RelativeLayout) findViewById(R.id.loading_layout);
        mRetryLayout = (LinearLayout) findViewById(R.id.retry_layout);
        mRetryBtn = (TextView) findViewById(R.id.retry_btn);
        mLoading = (ProgressBar) findViewById(R.id.loading);
        mNoDataFound = (TextView) findViewById(R.id.no_data_msg);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mRecyclerView.setBackgroundResource(R.color.default_bg);

        mAdapter = null;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    void displayList() {
        page = 1;
        isLoading = true;
        displayLoading(arrayList.size() == 0);
        BaseApi.init(this)
                .listOrder(
                        length,
                        page
                )
                .call(new DataHandler<OrderListResultCover>(OrderListResultCover.class) {
                    @Override
                    public void onSuccess(OrderListResultCover data) {
                        arrayList.clear();
                        try {
                            arrayList.addAll(data.getResult().getOrders());
                            mAdapter = new TenancyAgreementListAdapter(TenancyAgreementListActivity.this);
//                            mAdapter.setHeader("");
                            mAdapter.setItems(arrayList);
                            if (data.getResult().getOrders().size() != 0) {
                                isLoading = false;
                                mAdapter.setFooter("");
                                mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                    @Override
                                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                        super.onScrollStateChanged(recyclerView, newState);
                                    }

                                    @Override
                                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                        super.onScrolled(recyclerView, dx, dy);
                                        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                                        totalItemCount = linearLayoutManager.getItemCount();
                                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                            page++;
                                            isLoading = true;
                                            loadMore();
                                        }
                                    }
                                });
                            } else {
                                isLoading = true;
                            }
                            mRecyclerView.setAdapter(mAdapter);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        mSwipeRefreshLayout.setRefreshing(false);
                        dismissLoading(arrayList.size() == 0);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        displayRetryLayout(arrayList.size() == 0, new ClickActionInterface.RetryActionInterface() {
                            @Override
                            public void onRetryClickListener() {
                                displayList();
                            }
                        });
                    }
                });
    }

    void loadMore() {
        BaseApi.init(this)
                .listOrder(
                        length,
                        page
                )
                .call(new DataHandler<OrderListResultCover>(OrderListResultCover.class) {
                    @Override
                    public void onSuccess(OrderListResultCover data) {
                        try {
                            arrayList.addAll(data.getResult().getOrders());
                            if (data.getResult().getOrders().size() != 0) {
                                mAdapter.showFooter();
                                isLoading = false;
                            } else {
                                mAdapter.hideFooter();
                                isLoading = true;
                            }

                            mAdapter.notifyDataSetChanged();
                            mSwipeRefreshLayout.setRefreshing(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        if (mAdapter != null) {
                            mAdapter.hideFooter();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }


    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        displayList();
    }


}

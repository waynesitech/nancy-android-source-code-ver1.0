package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.PropertyDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.UtilitiesDetailsModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatus4Fragment extends BaseFragment implements View.OnClickListener {

    RadioButton mWaterRadioBtn1;
    RadioButton mWaterRadioBtn2;
    RadioButton mElectricityRadioBtn1;
    RadioButton mElectricityRadioBtn2;
    RadioButton mSewerageRadioBtn1;
    RadioButton mSewerageRadioBtn2;
    RadioButton mInternetRadioBtn1;
    RadioButton mInternetRadioBtn2;
    Button mNextBtn;
    Button mPreviousBtn;

    public NewTAModel newTAModel;
    GenerateNewTAActivity generateNewTAActivity;

    public static GenerateNewTaStatus4Fragment newInstance() {
        GenerateNewTaStatus4Fragment f = new GenerateNewTaStatus4Fragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_4, container, false);

        mWaterRadioBtn1 = (RadioButton) v.findViewById(R.id.water_radio_btn_1);
        mWaterRadioBtn2 = (RadioButton) v.findViewById(R.id.water_radio_btn_2);
        mElectricityRadioBtn1 = (RadioButton) v.findViewById(R.id.electricity_radio_btn_1);
        mElectricityRadioBtn2 = (RadioButton) v.findViewById(R.id.electricity_radio_btn_2);
        mSewerageRadioBtn1 = (RadioButton) v.findViewById(R.id.sewerage_radio_btn_1);
        mSewerageRadioBtn2 = (RadioButton) v.findViewById(R.id.sewerage_radio_btn_2);
        mInternetRadioBtn1 = (RadioButton) v.findViewById(R.id.internet_radio_btn_1);
        mInternetRadioBtn2 = (RadioButton) v.findViewById(R.id.internet_radio_btn_2);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);

        mWaterRadioBtn1.setOnCheckedChangeListener(listener);
        mWaterRadioBtn2.setOnCheckedChangeListener(listener);
        mElectricityRadioBtn1.setOnCheckedChangeListener(listener);
        mElectricityRadioBtn2.setOnCheckedChangeListener(listener);
        mSewerageRadioBtn1.setOnCheckedChangeListener(listener);
        mSewerageRadioBtn2.setOnCheckedChangeListener(listener);
        mInternetRadioBtn1.setOnCheckedChangeListener(listener);
        mInternetRadioBtn2.setOnCheckedChangeListener(listener);
        mNextBtn.setOnClickListener(this);
//        mPreviousBtn.setOnClickListener(this);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        loadLocalData();
        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    public void loadLocalData() {
        String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
        if (!TextUtils.isEmpty(keyNewTAModel)) {
            newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
        }
        if (newTAModel == null)
            newTAModel = new NewTAModel();
        if (newTAModel != null) {
            if (newTAModel.utilitiesDetailsModel != null) {
                UtilitiesDetailsModel utilitiesDetailsModel = newTAModel.utilitiesDetailsModel;
                mWaterRadioBtn1.setChecked(utilitiesDetailsModel.water == 1 ? true : false);
                mWaterRadioBtn2.setChecked(utilitiesDetailsModel.water == 2 ? true : false);

                mElectricityRadioBtn1.setChecked(utilitiesDetailsModel.electricity == 1 ? true : false);
                mElectricityRadioBtn2.setChecked(utilitiesDetailsModel.electricity == 2 ? true : false);

                mSewerageRadioBtn1.setChecked(utilitiesDetailsModel.sewereae == 1 ? true : false);
                mSewerageRadioBtn2.setChecked(utilitiesDetailsModel.sewereae == 2 ? true : false);

                mInternetRadioBtn1.setChecked(utilitiesDetailsModel.internet == 1 ? true : false);
                mInternetRadioBtn2.setChecked(utilitiesDetailsModel.internet == 2 ? true : false);
            }
        }
    }

    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                switch (buttonView.getId()) {
                    case R.id.water_radio_btn_1: {
                        mWaterRadioBtn2.setChecked(false);
                        break;
                    }
                    case R.id.water_radio_btn_2: {
                        mWaterRadioBtn1.setChecked(false);
                        break;
                    }
                    case R.id.electricity_radio_btn_1: {
                        mElectricityRadioBtn2.setChecked(false);
                        break;
                    }
                    case R.id.electricity_radio_btn_2: {
                        mElectricityRadioBtn1.setChecked(false);
                        break;
                    }
                    case R.id.sewerage_radio_btn_1: {
                        mSewerageRadioBtn2.setChecked(false);
                        break;
                    }
                    case R.id.sewerage_radio_btn_2: {
                        mSewerageRadioBtn1.setChecked(false);
                        break;
                    }
                    case R.id.internet_radio_btn_1: {
                        mInternetRadioBtn2.setChecked(false);
                        break;
                    }
                    case R.id.internet_radio_btn_2: {
                        mInternetRadioBtn1.setChecked(false);
                        break;
                    }
                }
            }
        }
    };

    void updateNewTAModel() {
        newTAModel.utilitiesDetailsModel = new UtilitiesDetailsModel();
        if (mWaterRadioBtn1.isChecked()) {
            newTAModel.utilitiesDetailsModel.water = 1;
        } else if (mWaterRadioBtn2.isChecked()) {
            newTAModel.utilitiesDetailsModel.water = 2;
        }

        if (mElectricityRadioBtn1.isChecked()) {
            newTAModel.utilitiesDetailsModel.electricity = 1;
        } else if (mElectricityRadioBtn2.isChecked()) {
            newTAModel.utilitiesDetailsModel.electricity = 2;
        }

        if (mSewerageRadioBtn1.isChecked()) {
            newTAModel.utilitiesDetailsModel.sewereae = 1;
        } else if (mSewerageRadioBtn2.isChecked()) {
            newTAModel.utilitiesDetailsModel.sewereae = 2;
        }

        if (mInternetRadioBtn1.isChecked()) {
            newTAModel.utilitiesDetailsModel.internet = 1;
        } else if (mInternetRadioBtn2.isChecked()) {
            newTAModel.utilitiesDetailsModel.internet = 2;
        }
        newTAModel.stepCaching = generateNewTAActivity.mCommercialSlideFragmentTag[5];

        String newTAModelJson = new Gson().toJson(newTAModel);
        SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                updateNewTAModel();
                generateNewTAActivity.pageController(generateNewTAActivity.mCommercialSlideFragmentTag[5]);
                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

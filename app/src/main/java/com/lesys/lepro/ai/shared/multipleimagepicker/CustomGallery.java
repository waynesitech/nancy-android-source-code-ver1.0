package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.os.Parcel;
import android.os.Parcelable;


public class CustomGallery implements Parcelable {

	public String sdcardPath;
	public String isSelected = "false";
	public int selectionIndex = -1;

	public CustomGallery(String sdcardPath) {
		this.sdcardPath = sdcardPath;
	}

	public CustomGallery() {
	}

	public CustomGallery(Parcel in) {
		this.sdcardPath = in.readString();
		this.isSelected = in.readString();
		this.selectionIndex = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.sdcardPath);
		dest.writeString(this.isSelected);
		dest.writeInt(this.selectionIndex);
	}

	public static final Creator CREATOR = new Creator() {
		public CustomGallery createFromParcel(Parcel in) {
			return new CustomGallery(in);
		}

		public CustomGallery[] newArray(int size) {
			return new CustomGallery[size];
		}
	};


	public boolean isSelected() {
		return isSelected.equalsIgnoreCase("true");
	}
}

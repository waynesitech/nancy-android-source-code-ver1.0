package com.lesys.lepro.ai.module.explaination;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.Explanation;
import com.lesys.lepro.ai.model.ExplanationResultCover;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.module.explaination.adapter.ExplainationListAdapter;
import com.lesys.lepro.ai.module.tenancy_agreement.adapter.TenancyAgreementListAdapter;
import com.lesys.lepro.ai.shared.ClickActionInterface;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.MediaPlayerUtil;

import java.util.ArrayList;

/**
 * Created by Nicholas on 18/05/2017.
 */

public class ExplainationListActivity extends BaseActivity implements
        SwipeRefreshLayout.OnRefreshListener {

    FrameLayout mFragmentContainer;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    LinearLayout mRetryLayout;
    TextView mRetryBtn;
    RelativeLayout mLoadingLayout;
    ProgressBar mLoading;
    TextView mNoDataFound;

    ExplainationListAdapter mAdapter;
    ArrayList<Explanation> arrayList = new ArrayList<>();
    private MediaPlayerUtil mMediaPlayerUtil;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    final int length = 10;
    int page = 1;

    boolean isEnglish;
    Order mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenancy_agreement_list);

        mOrder = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);
        isEnglish = getIntent().getBooleanExtra(GlobalParam.INTENT_IS_ENGLISH, false);

        mFragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLoadingLayout = (RelativeLayout) findViewById(R.id.loading_layout);
        mRetryLayout = (LinearLayout) findViewById(R.id.retry_layout);
        mRetryBtn = (TextView) findViewById(R.id.retry_btn);
        mLoading = (ProgressBar) findViewById(R.id.loading);
        mNoDataFound = (TextView) findViewById(R.id.no_data_msg);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mRecyclerView.setBackgroundResource(R.color.default_bg);

        mAdapter = null;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMediaPlayerUtil = new MediaPlayerUtil();

        displayList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null)
            mMediaPlayerUtil.stopAudio(mAdapter.onMediaPlayerListener);
        else
            mMediaPlayerUtil.stopAudio(null);
    }

    void displayList() {
        page = 1;
        isLoading = true;
        displayLoading(arrayList.size() == 0);

        BaseApi.init(this)
                .getExplanationAudio(mOrder.getId(), isEnglish ? "en" : "cn")
                .call(new DataHandler<ExplanationResultCover>(ExplanationResultCover.class) {
                    @Override
                    public void onSuccess(ExplanationResultCover data) {
                        arrayList.clear();
                        arrayList.addAll(data.getResult());
                        mAdapter = new ExplainationListAdapter(ExplainationListActivity.this, mMediaPlayerUtil);
                        mAdapter.setHeader("");
                        mAdapter.setItems(arrayList);
                        mRecyclerView.setAdapter(mAdapter);
                        mSwipeRefreshLayout.setRefreshing(false);

                        dismissLoading(arrayList.size() == 0);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        displayRetryLayout(arrayList.size() == 0, new ClickActionInterface.RetryActionInterface() {
                            @Override
                            public void onRetryClickListener() {
                                displayList();
                            }
                        });
                    }
                });
    }

    @Override
    public void onRefresh() {
        displayList();
    }


}

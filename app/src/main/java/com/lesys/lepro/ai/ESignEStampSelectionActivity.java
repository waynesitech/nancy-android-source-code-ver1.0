package com.lesys.lepro.ai;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.TenancyAgreementListActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;
import com.noelchew.permisowrapper.PermisoWrapper;

import java.io.File;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

/**
 * Created by Nicholas on 09/07/2017.
 */

public class ESignEStampSelectionActivity extends BaseActivity implements View.OnClickListener {

    Button mExistingTaBtn;
    Button mUploadTaBtn;
    TextView mChooseOne;

    String title;
    String uploadType;
    File file;
    private ArrayList<String> docPaths = new ArrayList<>();
    NewTAModel newTAModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_sign_e_stamp_selection);

        Permiso.getInstance().setActivity(this);

        title = getIntent().getStringExtra(GlobalParam.INTENT_TITLE);
        uploadType = getIntent().getStringExtra(GlobalParam.INTENT_UPLOAD_TYPE);

        setTitle(title);

        mChooseOne = (TextView) findViewById(R.id.choose_one);
        mExistingTaBtn = (Button) findViewById(R.id.existing_ta_btn);
        mUploadTaBtn = (Button) findViewById(R.id.upload_ta_btn);

        mExistingTaBtn.setVisibility(uploadType.equals(GlobalParam.TAUploadType.UPLOAD_TYPE_AUTO_REMINDER) ? View.GONE : View.VISIBLE);
        mChooseOne.setVisibility(uploadType.equals(GlobalParam.TAUploadType.UPLOAD_TYPE_AUTO_REMINDER) ? View.GONE : View.VISIBLE);
        mExistingTaBtn.setOnClickListener(this);
        mUploadTaBtn.setOnClickListener(this);
    }

    public void onPickDoc() {
        String[] pdfs = {".pdf"};
        FilePickerBuilder.getInstance().setMaxCount(1)
//                .setSelectedFiles(docPaths)
                .setActivityTheme(R.style.FilePickerTheme)
                .addFileSupport("PDF", pdfs)
                .enableDocSupport(false)
                .pickFile(this);
    }

    void dialogTAUnsubmmited() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_ta_unsubmitted)
                .setPositiveButton(R.string.continue_editing, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ESignEStampSelectionActivity.this, GenerateNewTAActivity.class));
                        finish();
                    }
                })
                .setNegativeButton(R.string.discard, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferencesUtil.setString(ESignEStampSelectionActivity.this, GlobalParam.KEY_NEW_TA_MODEL, "");
                        PermisoWrapper.getPermissionSaveMediaToStorage(ESignEStampSelectionActivity.this, new PermisoWrapper.PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                onPickDoc();
                            }

                            @Override
                            public void onPermissionDenied() {

                            }
                        });
                    }
                })
                .setNeutralButton(R.string.cancel, null);
        builder.create().show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths.clear();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                break;
        }

        if (docPaths.size() != 0) {
            file = new File(docPaths.get(0));
            Intent intent = new Intent(ESignEStampSelectionActivity.this, GenerateNewTAActivity.class);
            intent.putExtra(GlobalParam.INTENT_UPLOAD_TYPE, uploadType);
            intent.putExtra(GlobalParam.INTENT_TA_PDF, docPaths.get(0));
            startActivity(intent);
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.existing_ta_btn: {
                startActivity(new Intent(this, TenancyAgreementListActivity.class));
                finish();
                break;
            }
            case R.id.upload_ta_btn: {
                newTAModel = null;
                String keyNewTAModel = SharedPreferencesUtil.getString(this, GlobalParam.KEY_NEW_TA_MODEL);
                if (!TextUtils.isEmpty(keyNewTAModel)) {
                    newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
                }
                if (newTAModel != null) {
                    dialogTAUnsubmmited();
                } else {
                    PermisoWrapper.getPermissionSaveMediaToStorage(this, new PermisoWrapper.PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            onPickDoc();
                        }

                        @Override
                        public void onPermissionDenied() {

                        }
                    });
                }
                break;
            }
        }
    }
}

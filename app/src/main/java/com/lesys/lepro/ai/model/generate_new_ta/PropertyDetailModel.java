package com.lesys.lepro.ai.model.generate_new_ta;

import com.lesys.lepro.ai.model.OrderFullDetail;

import java.io.Serializable;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class PropertyDetailModel implements Serializable {

    public String imagePath;
    public String streetName;
    public String town;
    public String postCode;
    public String states;

    public PropertyDetailModel() {

    }

    public PropertyDetailModel syncServerTA(OrderFullDetail order) {
        imagePath = order.getPropertyPicture();
        streetName = order.getPropertyStreet();
        town = order.getPropertyTown();
        postCode = order.getPropertyPostcode();
        states = order.getPropertyState();
        return this;
    }
}

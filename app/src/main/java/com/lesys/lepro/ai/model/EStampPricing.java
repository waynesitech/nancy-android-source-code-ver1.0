package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 14/07/2017.
 */

public class EStampPricing implements Serializable {

    private int stampingFee;
    private int penaltyFee;
    private int stampingDuplicateFee;
    private int serviceFee;


    public int getStampingFee() {
        return stampingFee;
    }

    public void setStampingFee(int stampingFee) {
        this.stampingFee = stampingFee;
    }

    public int getPenaltyFee() {
        return penaltyFee;
    }

    public void setPenaltyFee(int penaltyFee) {
        this.penaltyFee = penaltyFee;
    }

    public int getStampingDuplicateFee() {
        return stampingDuplicateFee;
    }

    public void setStampingDuplicateFee(int stampingDuplicateFee) {
        this.stampingDuplicateFee = stampingDuplicateFee;
    }

    public int getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(int serviceFee) {
        this.serviceFee = serviceFee;
    }

}

package com.lesys.lepro.ai.shared.ipay;

/**
 * Created by NicholasTan on 19/07/2016.
 */
public interface IPayPaymentListener {
    void onSuccessed();

    void onFailed(String error);

    void onCanceled();
}

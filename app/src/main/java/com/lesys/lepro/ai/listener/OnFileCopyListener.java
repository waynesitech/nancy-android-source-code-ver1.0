package com.lesys.lepro.ai.listener;

/**
 * Created by Nicholas on 04/06/2017.
 */

public interface OnFileCopyListener {

    void onSuccess(String filename);

    void onFailed();
}

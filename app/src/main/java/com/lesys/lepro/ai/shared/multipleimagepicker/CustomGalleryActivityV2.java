package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;

import java.util.ArrayList;

public class CustomGalleryActivityV2 extends BaseActivity {

//    Toolbar toolbar;
//    TextView mToolBarTitle;
    public static final String SELECTED_STRING_ARRAY_KEY = "CUSTOM_GALLERY_SELECTED";
    public static final String SEQUENCE_SELECTION_BOOLEAN_KEY = "CUSTOM_GALLERY_SEQUENCE_SELECTION";
    public static final String MAXIMUM_SELECTION_INT_KEY = "CUSTOM_GALLERY_MAXIMUM_SELECTION";
    public static final String INTENT_ALBUM_KEY = "INTENT_ALBUM_KEY";
    public static final int request_code = 177;

    GridView gridGallery;
    Handler handler;
    GalleryAdapter adapter;

    ImageView imgNoMedia;
    Button btnGalleryOk;

    String action;
    ArrayList<CustomGallery> mImageList;
    Album mAlbum;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.gallery);
        action = getIntent().getAction();

        mAlbum = getIntent().getParcelableExtra(INTENT_ALBUM_KEY);
        if (mAlbum == null) {
            finish();
            return;
        } else {
            mImageList = mAlbum.images;
        }

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
//
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(null);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mToolBarTitle.setText(R.string.select_photo);


        init();
//		listFolder();

    }

    private void init() {

        handler = new Handler();
        gridGallery = (GridView) findViewById(R.id.gridGallery);
        gridGallery.setFastScrollEnabled(true);
        if (getIntent().getBooleanExtra(SEQUENCE_SELECTION_BOOLEAN_KEY, false)) {
            adapter = new GalleryAdapter(CustomGalleryActivityV2.this, getIntent().getIntExtra(MAXIMUM_SELECTION_INT_KEY, 5));
        } else {
            adapter = new GalleryAdapter(CustomGalleryActivityV2.this);
        }

//		PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader,
//				true, true);
//		gridGallery.setOnScrollListener(listener);

        if (action.equalsIgnoreCase(Action.ACTION_MULTIPLE_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
            gridGallery.setOnItemClickListener(mItemMulClickListener);
            adapter.setMultiplePick(true);

        } else if (action.equalsIgnoreCase(Action.ACTION_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
            gridGallery.setOnItemClickListener(mItemSingleClickListener);
            adapter.setMultiplePick(false);

        }

        gridGallery.setAdapter(adapter);
        imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

        btnGalleryOk = (Button) findViewById(R.id.btnGalleryOk);
        btnGalleryOk.setOnClickListener(mOkClickListener);

        new Thread() {

            @Override
            public void run() {
                Looper.prepare();
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        adapter.addAll(mImageList);
                        checkImageStatus();
                    }
                });
                Looper.loop();
            }

            ;

        }.start();

    }



    private void checkImageStatus() {

        if (adapter.isEmpty()) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }

    View.OnClickListener mOkClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String[] allPath;
            if (getIntent().getBooleanExtra(SEQUENCE_SELECTION_BOOLEAN_KEY, false)) {
                allPath = adapter.getSequenceSelection();
            } else {
                ArrayList<CustomGallery> selected = adapter.getSelected();

                allPath = new String[selected.size()];
                for (int i = 0; i < allPath.length; i++) {
                    allPath[i] = selected.get(i).sdcardPath;
                }
            }


            Intent data = new Intent().putExtra("all_path", allPath);
            setResult(RESULT_OK, data);
            finish();

        }
    };
    AdapterView.OnItemClickListener mItemMulClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            adapter.changeSelection(v, position);

        }
    };

    AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            CustomGallery item = adapter.getItem(position);
            Intent data = new Intent().putExtra("single_path", item.sdcardPath);
            setResult(RESULT_OK, data);
            finish();
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 20/08/2017.
 */

public class SpecialRequest implements Serializable {

    private String original;
    private String edited;


    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

}

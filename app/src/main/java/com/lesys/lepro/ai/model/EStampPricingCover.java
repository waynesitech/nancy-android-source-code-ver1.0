package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 14/07/2017.
 */

public class EStampPricingCover implements Serializable {

    private String ipayEstampingBackendURL;
    private String ipayRemark;
    private EStampPricing pricing;


    public String getIpayEstampingBackendURL() {
        return ipayEstampingBackendURL;
    }

    public void setIpayEstampingBackendURL(String ipayEstampingBackendURL) {
        this.ipayEstampingBackendURL = ipayEstampingBackendURL;
    }

    public String getIpayRemark() {
        return ipayRemark;
    }

    public void setIpayRemark(String ipayRemark) {
        this.ipayRemark = ipayRemark;
    }

    public EStampPricing getPricing() {
        return pricing;
    }

    public void setPricing(EStampPricing pricing) {
        this.pricing = pricing;
    }

}

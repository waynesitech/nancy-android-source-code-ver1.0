package com.lesys.lepro.ai.model.generate_new_ta;

import com.lesys.lepro.ai.model.OrderFullDetail;

import java.io.Serializable;

/**
 * Created by Nicholas on 04/06/2017.
 */

public class PaymentDetailModel implements Serializable {

    public boolean isDirectBankIn;
    public boolean isPostDatedCheque;
    public boolean isCollectionByLandlord;
    public DirectBankInModel directBankInModel;
    public PostDatedChequeModel postDatedChequeModel;
    public CollectionByLandlordModel collectionByLandlordModel;

    public PaymentDetailModel() {

    }

    public PaymentDetailModel syncServerTA(OrderFullDetail order) {

        switch (order.getPaymentMethod()) {
            case "isDirectBankIn": {
                isDirectBankIn = true;
                isPostDatedCheque = false;
                isCollectionByLandlord = false;
                directBankInModel = new DirectBankInModel();
                directBankInModel.syncServerTA(order);
                break;
            }
            case "isPostDatedCheque": {
                isDirectBankIn = false;
                isPostDatedCheque = true;
                isCollectionByLandlord = false;
                postDatedChequeModel = new PostDatedChequeModel();
                postDatedChequeModel.syncServerTA(order);
                break;
            }
            case "isCollectionByLandlord": {
                isDirectBankIn = false;
                isPostDatedCheque = false;
                isCollectionByLandlord = true;
                collectionByLandlordModel = new CollectionByLandlordModel();
                collectionByLandlordModel.isCollectionByLandlord = true;
                break;
            }
        }

        return this;
    }
}

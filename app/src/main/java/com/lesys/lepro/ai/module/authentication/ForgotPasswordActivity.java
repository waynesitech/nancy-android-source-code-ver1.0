package com.lesys.lepro.ai.module.authentication;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.shared.Common;

/**
 * Created by Nicholas on 21/05/2017.
 */

public class ForgotPasswordActivity extends BaseActivity {

    private static final String TAG = ForgotPasswordActivity.class.getName();

    TextInputEditText mEmail;
    Button mNextBtn;
    TextView mWarningText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mEmail = (TextInputEditText) findViewById(R.id.email);
        mNextBtn = (Button) findViewById(R.id.next_btn);
        mWarningText = (TextView) findViewById(R.id.warning_text);

        mWarningText.setVisibility(View.INVISIBLE);
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });
    }

    void forgotPassword() {
        if (TextUtils.isEmpty(mEmail.getText().toString())) {
            mWarningText.setVisibility(View.VISIBLE);
            mEmail.setError(mWarningText.getText().toString(), Common.Helpers.getErrorDrawable(this));
            return;
        }
        mWarningText.setVisibility(View.INVISIBLE);

        BaseApi.init(this)
                .resetPassword(mEmail.getText().toString())
                .call(new DataHandler<MetaData>(MetaData.class) {
                    @Override
                    public void onSuccess(MetaData data) {
                        resetPasswordSuccessDialog();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });

    }

    void resetPasswordSuccessDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.dialog_title_reset_password_success, mEmail.getText().toString()))
                .setMessage(getString(R.string.dialog_message_reset_password_success))
                .setPositiveButton(R.string.dialog_btn_positive_reset_password_success, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.dialog_btn_negative_reset_password_success, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_APP_EMAIL);
                        startActivity(intent);
                        finish();
                    }
                })
                .show();
    }

}

package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class OrderFullDetailCover implements Serializable {

    private OrderFullDetail order;

    public OrderFullDetail getOrder() {
        return order;
    }

    public void setOrder(OrderFullDetail order) {
        this.order = order;
    }

}

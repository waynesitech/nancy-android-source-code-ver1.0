package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class OrderListResultCover extends MetaData implements Serializable {

    private OrderListCover result;

    public OrderListCover getResult() {
        return result;
    }

    public void setResult(OrderListCover result) {
        this.result = result;
    }

}

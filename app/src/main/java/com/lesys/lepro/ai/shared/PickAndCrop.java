package com.lesys.lepro.ai.shared;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.shared.multipleimagepicker.Action;
import com.lesys.lepro.ai.shared.multipleimagepicker.CustomGalleryActivity;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by karhong on 4/7/15.
 */
public class PickAndCrop {

    private Context mContext;

    private Uri mImageUri;
    private File mImageFile;

    private int mCount;

    private boolean mIsMultiple;

    private boolean needToCrop;

    private ImagePicker mImagePicker;

    private Fragment mFragment;

    private PickAndCropCallback mPickAndCropCallback;
    private int mRatioX = 1;
    private int mRatioY = 1;

    public PickAndCrop(Context context) {
        init(context);
        mImagePicker = new ImagePicker(context);
    }

    public PickAndCrop(Context context, PickAndCropCallback callback) {
        init(context);
        this.mPickAndCropCallback = callback;
        mImagePicker = new ImagePicker(context);
    }

    public PickAndCrop(Fragment fragment) {
        init(fragment.getActivity());
        mFragment = fragment;
        mImagePicker = new ImagePicker(fragment);
    }

    public PickAndCrop(Fragment fragment, PickAndCropCallback callback) {
        init(fragment.getActivity());
        mFragment = fragment;
        this.mPickAndCropCallback = callback;
        mImagePicker = new ImagePicker(fragment);
    }

    private void init(Context context) {
        mContext = context;
        mIsMultiple = false;
    }

    public File getImageFile() {
        return mImageFile;
    }

    public void setImageFile(File imageFile) {
        mImageFile = imageFile;
    }


    public boolean isNeedToCrop() {
        return needToCrop;
    }

    public void setNeedToCrop(boolean needToCrop) {
        this.needToCrop = needToCrop;
    }

    public Uri getImageUri() {
        System.out.println("Kelvyndebug:mImageUri->" + mImageUri);
        return mImageUri;
    }

    public Uri getImageUriFromPicker() {
        return mImagePicker.getImageUri();
    }


    public void isMultiple(boolean isMultiple) {
        mIsMultiple = isMultiple;
    }

    public void selectImage(int ratioX, int ratioY) {
        mRatioX = ratioX;
        mRatioY = ratioY;

        final CharSequence[] items;
        if (mPickAndCropCallback != null) {

            items = new CharSequence[4];
            items[0] = mContext.getString(R.string.take_photo);
            items[1] = mContext.getString(R.string.choose_photo);
            items[2] = mContext.getString(R.string.delete);
            items[3] = mContext.getString(R.string.cancel);

        } else {
            items = new CharSequence[3];
            items[0] = mContext.getString(R.string.take_photo);
            items[1] = mContext.getString(R.string.choose_photo);
            items[2] = mContext.getString(R.string.cancel);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getString(R.string.take_photo))) {
                    mImagePicker.pickByCamera();
                } else if (items[item].equals(mContext.getString(R.string.choose_photo))) {
                    if (Common.Helpers.checkStoragePermission((Activity) mContext)) {
                        Common.Helpers.openPermissionDialog((Activity) mContext).show();
                        return;
                    }
                    Crop.pickImage((Activity) mContext);
                } else if (items[item].equals(mContext.getString(R.string.cancel))) {
                    dialog.dismiss();
                } else if (items[item].equals(mContext.getString(R.string.delete))) {
                    if (mPickAndCropCallback != null) {
                        mPickAndCropCallback.onRemoved();
                    }
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void selectImage(final int maxSelection, boolean haveDelete) {
        selectImage(maxSelection, haveDelete, false);
    }

    /**
     * maxSelection = maximum number of photos user can select at a time
     * haveDelete = to indicate if "Delete" is needed, if delete is not needed then select Video option will be added.
     */
    public void selectImage(final int maxSelection, boolean haveDelete, boolean haveVideo) {
        final CharSequence[] items = {mContext.getString(R.string.take_photo), mContext.getString(R.string.choose_photo), mContext.getString(R.string.delete),
                mContext.getString(R.string.cancel)};
        final CharSequence[] itemsNoDelete = {mContext.getString(R.string.take_photo),
                mContext.getString(R.string.choose_photo),
                mContext.getString(R.string.cancel)};
        final CharSequence[] itemsWithVideo = {mContext.getString(R.string.take_photo),
                mContext.getString(R.string.choose_photo),
                mContext.getString(R.string.select_video),
                mContext.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        if (haveDelete) {
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {

                    if (items[item].equals(mContext.getString(R.string.take_photo))) {
                        mImagePicker.pickByCamera();
                    } else if (items[item].equals(mContext.getString(R.string.choose_photo))) {
                        if (Common.Helpers.checkStoragePermission((Activity) mContext)) {
                            Common.Helpers.openPermissionDialog((Activity) mContext).show();
                            return;
                        }
                        Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
//                    i.putExtra(CustomGalleryActivity.SELECTED_STRING_ARRAY_KEY, selectedImages);
                        i.putExtra(CustomGalleryActivity.SEQUENCE_SELECTION_BOOLEAN_KEY, true);
                        i.putExtra(CustomGalleryActivity.MAXIMUM_SELECTION_INT_KEY, maxSelection);
                        ((Activity) mContext).startActivityForResult(i, 200);
                    } else if (items[item].equals(mContext.getString(R.string.cancel))) {
                        dialog.dismiss();
                    } else if (items[item].equals(mContext.getString(R.string.delete))) {
                        if (mPickAndCropCallback != null) {
                            mPickAndCropCallback.onRemoved();
                        }
                        dialog.dismiss();
                    }
                }
            });
        } else {
            if (haveVideo) {
//                builder.setItems(itemsWithVideo, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int item) {
//
//                        if (itemsWithVideo[item].equals(mContext.getString(R.string.take_photo))) {
//                            mImagePicker.pickByCamera();
//                        } else if (itemsWithVideo[item].equals(mContext.getString(R.string.choose_photo))) {
//                            if (Common.Helpers.checkStoragePermission((Activity) mContext)) {
//                                Common.Helpers.openPermissionDialog((Activity) mContext).show();
//                                return;
//                            }
//                            Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
////                    i.putExtra(CustomGalleryActivity.SELECTED_STRING_ARRAY_KEY, selectedImages);
//                            i.putExtra(CustomGalleryActivity.SEQUENCE_SELECTION_BOOLEAN_KEY, true);
//                            i.putExtra(CustomGalleryActivity.MAXIMUM_SELECTION_INT_KEY, maxSelection);
//                            ((Activity) mContext).startActivityForResult(i, 200);
//                        } else if (itemsWithVideo[item].equals(mContext.getString(R.string.cancel))) {
//                            dialog.dismiss();
//                        } else if (itemsWithVideo[item].equals(mContext.getString(R.string.select_video))) {
//                            if (Common.Helpers.checkStoragePermission((Activity) mContext)) {
//                                Common.Helpers.openPermissionDialog((Activity) mContext).show();
//                                return;
//                            }
//                            Intent i = new Intent(mContext, TrimVideoActivity.class);
//                            ((Activity) mContext).startActivityForResult(i, TrimVideoActivity.REQUEST_CODE);
//                        }
//                    }
//                });
            } else {
                builder.setItems(itemsNoDelete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (itemsNoDelete[item].equals(mContext.getString(R.string.take_photo))) {
                            mImagePicker.pickByCamera();
                        } else if (itemsNoDelete[item].equals(mContext.getString(R.string.choose_photo))) {
                            if (Common.Helpers.checkStoragePermission((Activity) mContext)) {
                                Common.Helpers.openPermissionDialog((Activity) mContext).show();
                                return;
                            }
                            Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
//                    i.putExtra(CustomGalleryActivity.SELECTED_STRING_ARRAY_KEY, selectedImages);
                            i.putExtra(CustomGalleryActivity.SEQUENCE_SELECTION_BOOLEAN_KEY, true);
                            i.putExtra(CustomGalleryActivity.MAXIMUM_SELECTION_INT_KEY, maxSelection);
                            ((Activity) mContext).startActivityForResult(i, 200);
                        } else if (itemsNoDelete[item].equals(mContext.getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
            }
        }
        builder.show();
    }

    public void handleResult(int requestCode, int resultCode, Intent result) {
        mImagePicker.onActivityResult(requestCode, resultCode, result);

        if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK) {
            beginCrop(result.getData());
        } else if (requestCode == ImagePicker.RC_IMAGE_CAPTURE) {
//            if (isNeedToCrop())
                beginCrop(mImagePicker.getImageUri());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, result);
        }
    }

    private void beginCrop(Uri source) {

        Uri outputUri;
        if (!mIsMultiple)
            outputUri = Uri.fromFile(new File(mContext.getCacheDir(), "cropped.jpg"));
        else {
            outputUri = Uri.fromFile(new File(mContext.getCacheDir(), "cropped_" + mCount + ".jpg"));
            mCount++;
        }
        new Crop(source).output(outputUri).withDefaultNoCrop(true).withAspect(mRatioX, mRatioY).start((Activity) mContext);
    }

    private void handleCrop(int resultCode, Intent result) {

        if (resultCode == Activity.RESULT_OK) {
            mImageUri = Crop.getOutput(result);
            mImageFile = new File(mImageUri.getPath());
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(mContext, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void clearCache() {
        File file = new File(mContext.getCacheDir(), "cropped.jpg");
        if (file.exists())
            file.delete();

        for (int i = 0; i < 5; i++) {
            File file1 = new File(mContext.getCacheDir(), "cropped_" + i + ".jpg");
            if (file1.exists())
                file1.delete();
        }
    }

    public File bitmapToFile(Bitmap bitmap, String name) {
//        File filesDir = mContext.getFilesDir();
        File filesDir = mImagePicker.getAlbumDir();
        File imageFile = new File(filesDir, name);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }

        return imageFile;
    }

    public interface PickAndCropCallback {
        void onRemoved();
    }
}

package com.lesys.lepro.ai.shared;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Nicholas on 14/07/2017.
 */

public class MediaPlayerUtil {
    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    private MediaPlayer mMediaPlayer;

    public void stopAudio(final OnMediaPlayerListener onMediaPlayerListener) {
        if (mMediaPlayer != null) {
            try {
                if (onMediaPlayerListener != null) {
                    onMediaPlayerListener.onStop();
                }
                mMediaPlayer.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void releaseAudio() {
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.release();
                mMediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playAudio(String url, int position, final OnMediaPlayerListener onMediaPlayerListener) {
        stopAudio(onMediaPlayerListener);
        releaseAudio();
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepare();
            audioListener(onMediaPlayerListener);
            mMediaPlayer.start();
            if (onMediaPlayerListener != null) {
                onMediaPlayerListener.onPlay(position);
            }
        } catch (IOException e) {
            stopAudio(onMediaPlayerListener);
            e.printStackTrace();
        }
    }

    public void audioListener(final OnMediaPlayerListener onMediaPlayerListener) {
        if (onMediaPlayerListener == null)
            return;
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        onMediaPlayerListener.onStop();
                    }
                });
                mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        onMediaPlayerListener.onStop();
                        return true;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            onMediaPlayerListener.onStop();
        }
    }

    public boolean isAudioPlaying() {
        if (mMediaPlayer != null) {
            return mMediaPlayer.isPlaying();
        }
        return false;
    }

    public interface OnMediaPlayerListener {
        void onStop();

        void onPlay(int position);
    }
}

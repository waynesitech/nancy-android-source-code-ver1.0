package com.lesys.lepro.ai.module.authentication;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.MainActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.UserCover;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 07/05/2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getName();

    TextInputEditText mEmail;
    TextInputEditText mPassword;
    Button mLoginBtn;
    TextView mSignUpBtn;
    TextView mForgotPasswordBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginBtn = (Button) findViewById(R.id.sign_in_btn);
        mEmail = (TextInputEditText) findViewById(R.id.email);
        mPassword = (TextInputEditText) findViewById(R.id.password);
        mSignUpBtn = (TextView) findViewById(R.id.sign_up_btn);
        mForgotPasswordBtn = (TextView) findViewById(R.id.forgot_password_btn);

        mEmail.setText("lnicholaslemonl@gmail.com");
        mPassword.setText("123123");

//        mEmail.setText("lesys@nancy.com");
//        mPassword.setText("12341234");

//        mEmail.setText("demo@nancy.com.my");
//        mPassword.setText("password");

        mLoginBtn.setOnClickListener(this);
        mForgotPasswordBtn.setOnClickListener(this);

        setSignUpBtnStyle();
    }

    void setSignUpBtnStyle() {
        String text = getString(R.string.dont_have_an_account);
        String word = getString(R.string.dont_have_an_account_matching_word);
        SpannableString myString = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivityForResult(new Intent(LoginActivity.this, SignUpActivity.class), GlobalParam.RC_REGISTED);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        int startIndex = 0;
        for (int i = -1; (i = text.indexOf(word, i + 1)) != -1; ) {
            startIndex = i;
        }
        int lastIndex = startIndex + word.length();
        //For Click
        myString.setSpan(clickableSpan, startIndex, lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //For Bold
        myString.setSpan(new StyleSpan(Typeface.BOLD), startIndex, lastIndex, 0);

        myString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.normal_text)), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mSignUpBtn.setText(myString);
        mSignUpBtn.setMovementMethod(LinkMovementMethod.getInstance());
    }

    void login() {
        BaseApi.init(this)
                .emailLogin(mEmail.getText().toString(), mPassword.getText().toString())
                .call(new DataHandler<UserCover>(UserCover.class) {
                    @Override
                    public void onSuccess(UserCover data) {
                        SharedPreferencesUtil.setString(LoginActivity.this, GlobalParam.KEY_USER_DATA, new Gson().toJson(data.getResult()));
                        User mUser = new User().getUser(LoginActivity.this);
                        if (mUser.getRole().getName().equals(GlobalParam.UserRole.ROLE_MEMBER) && mUser.getIsPhoneVerify().equals("false")) {
                            startActivityForResult(new Intent(LoginActivity.this, PhoneVerificationActivity.class), GlobalParam.RC_VERIFIED);
                        } else {
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            setResult(RESULT_OK);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_REGISTED: {
                    startActivityForResult(new Intent(this, PhoneVerificationActivity.class), GlobalParam.RC_VERIFIED);
                    break;
                }
                case GlobalParam.RC_VERIFIED: {
                    setResult(RESULT_OK);
                    finish();
                    break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_btn: {
                login();
                break;
            }
            case R.id.forgot_password_btn: {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            }
        }
    }
}

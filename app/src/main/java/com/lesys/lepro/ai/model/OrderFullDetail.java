package com.lesys.lepro.ai.model;

import com.lesys.lepro.ai.model.generate_new_ta.param_api.LandlordTenant;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nicholas on 10/09/2017.
 */

public class OrderFullDetail implements Serializable {

    private long id;
    private String order_num;
    private String propertyStreet;
    private String propertyTown;
    private String propertyPostcode;
    private String propertyState;
    private String propertyPicture;
    private boolean isResidential;
    private boolean isLandlord;
    private String commencementDate;
    private int termsOfTenancyMonths;
    private int termsOfTenancyYears;
    private boolean isOptionToRenew;
    private int optionToRenewMonths;
    private int optionToRenewYears;
    private String optionToRenewCondition;
    private String otherTermsofRenewal;
    private double rental;
    private double advanceRental;
    private double securityDepositRent;
    private double securityDepositUtilities;
    private String otherSecurityDeposit;
    private String rentalFreePeriodWeeks;
    private String rentalFreePeriodMonths;
    private String rentalFreePeriodYears;
    private String rentalFreePeriodStart;
    private String rentalFreePeriodEnd;
    private String carParkLots;
    private String specialRequest;
    private String fixturesFitting;
    private String previewDocumentPath;
    private String paymentMethod;
    private String paymentAccountName;
    private String bank;
    private String bankAccountNum;
    private String notificationMethod;
    private String postDatedMethod;
    private List<LandlordTenant> tenants;
    private List<LandlordTenant> landlords;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public String getPropertyStreet() {
        return propertyStreet;
    }

    public void setPropertyStreet(String propertyStreet) {
        this.propertyStreet = propertyStreet;
    }

    public String getPropertyTown() {
        return propertyTown;
    }

    public void setPropertyTown(String propertyTown) {
        this.propertyTown = propertyTown;
    }

    public String getPropertyPostcode() {
        return propertyPostcode;
    }

    public void setPropertyPostcode(String propertyPostcode) {
        this.propertyPostcode = propertyPostcode;
    }

    public String getPropertyState() {
        return propertyState;
    }

    public void setPropertyState(String propertyState) {
        this.propertyState = propertyState;
    }

    public String getPropertyPicture() {
        return propertyPicture;
    }

    public void setPropertyPicture(String propertyPicture) {
        this.propertyPicture = propertyPicture;
    }

    public String getCommencementDate() {
        return commencementDate;
    }

    public void setCommencementDate(String commencementDate) {
        this.commencementDate = commencementDate;
    }

    public int getTermsOfTenancyMonths() {
        return termsOfTenancyMonths;
    }

    public void setTermsOfTenancyMonths(int termsOfTenancyMonths) {
        this.termsOfTenancyMonths = termsOfTenancyMonths;
    }

    public int getTermsOfTenancyYears() {
        return termsOfTenancyYears;
    }

    public void setTermsOfTenancyYears(int termsOfTenancyYears) {
        this.termsOfTenancyYears = termsOfTenancyYears;
    }

    public int getOptionToRenewMonths() {
        return optionToRenewMonths;
    }

    public void setOptionToRenewMonths(int optionToRenewMonths) {
        this.optionToRenewMonths = optionToRenewMonths;
    }

    public int getOptionToRenewYears() {
        return optionToRenewYears;
    }

    public void setOptionToRenewYears(int optionToRenewYears) {
        this.optionToRenewYears = optionToRenewYears;
    }

    public String getOptionToRenewCondition() {
        return optionToRenewCondition;
    }

    public void setOptionToRenewCondition(String optionToRenewCondition) {
        this.optionToRenewCondition = optionToRenewCondition;
    }

    public String getOtherTermsofRenewal() {
        return otherTermsofRenewal;
    }

    public void setOtherTermsofRenewal(String otherTermsofRenewal) {
        this.otherTermsofRenewal = otherTermsofRenewal;
    }

    public double getRental() {
        return rental;
    }

    public void setRental(double rental) {
        this.rental = rental;
    }

    public double getAdvanceRental() {
        return advanceRental;
    }

    public void setAdvanceRental(double advanceRental) {
        this.advanceRental = advanceRental;
    }

    public double getSecurityDepositRent() {
        return securityDepositRent;
    }

    public void setSecurityDepositRent(double securityDepositRent) {
        this.securityDepositRent = securityDepositRent;
    }

    public double getSecurityDepositUtilities() {
        return securityDepositUtilities;
    }

    public void setSecurityDepositUtilities(double securityDepositUtilities) {
        this.securityDepositUtilities = securityDepositUtilities;
    }

    public String getOtherSecurityDeposit() {
        return otherSecurityDeposit;
    }

    public void setOtherSecurityDeposit(String otherSecurityDeposit) {
        this.otherSecurityDeposit = otherSecurityDeposit;
    }

    public String getRentalFreePeriodWeeks() {
        return rentalFreePeriodWeeks;
    }

    public void setRentalFreePeriodWeeks(String rentalFreePeriodWeeks) {
        this.rentalFreePeriodWeeks = rentalFreePeriodWeeks;
    }

    public String getRentalFreePeriodMonths() {
        return rentalFreePeriodMonths;
    }

    public void setRentalFreePeriodMonths(String rentalFreePeriodMonths) {
        this.rentalFreePeriodMonths = rentalFreePeriodMonths;
    }

    public String getRentalFreePeriodYears() {
        return rentalFreePeriodYears;
    }

    public void setRentalFreePeriodYears(String rentalFreePeriodYears) {
        this.rentalFreePeriodYears = rentalFreePeriodYears;
    }

    public String getRentalFreePeriodStart() {
        return rentalFreePeriodStart;
    }

    public void setRentalFreePeriodStart(String rentalFreePeriodStart) {
        this.rentalFreePeriodStart = rentalFreePeriodStart;
    }

    public String getRentalFreePeriodEnd() {
        return rentalFreePeriodEnd;
    }

    public void setRentalFreePeriodEnd(String rentalFreePeriodEnd) {
        this.rentalFreePeriodEnd = rentalFreePeriodEnd;
    }

    public String getCarParkLots() {
        return carParkLots;
    }

    public void setCarParkLots(String carParkLots) {
        this.carParkLots = carParkLots;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public String getPreviewDocumentPath() {
        return previewDocumentPath;
    }

    public void setPreviewDocumentPath(String previewDocumentPath) {
        this.previewDocumentPath = previewDocumentPath;
    }

    public String getFixturesFitting() {
        return fixturesFitting;
    }

    public void setFixturesFitting(String fixturesFitting) {
        this.fixturesFitting = fixturesFitting;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentAccountName() {
        return paymentAccountName;
    }

    public void setPaymentAccountName(String paymentAccountName) {
        this.paymentAccountName = paymentAccountName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAccountNum() {
        return bankAccountNum;
    }

    public void setBankAccountNum(String bankAccountNum) {
        this.bankAccountNum = bankAccountNum;
    }

    public String getNotificationMethod() {
        return notificationMethod;
    }

    public void setNotificationMethod(String notificationMethod) {
        this.notificationMethod = notificationMethod;
    }

    public String getPostDatedMethod() {
        return postDatedMethod;
    }

    public void setPostDatedMethod(String postDatedMethod) {
        this.postDatedMethod = postDatedMethod;
    }


    public boolean isResidential() {
        return isResidential;
    }

    public void setResidential(boolean residential) {
        isResidential = residential;
    }

    public boolean isLandlord() {
        return isLandlord;
    }

    public void setLandlord(boolean landlord) {
        isLandlord = landlord;
    }

    public boolean isOptionToRenew() {
        return isOptionToRenew;
    }

    public void setOptionToRenew(boolean optionToRenew) {
        isOptionToRenew = optionToRenew;
    }

    public List<LandlordTenant> getTenants() {
        return tenants;
    }

    public void setTenants(List<LandlordTenant> tenants) {
        this.tenants = tenants;
    }

    public List<LandlordTenant> getLandlords() {
        return landlords;
    }

    public void setLandlords(List<LandlordTenant> landlords) {
        this.landlords = landlords;
    }

}

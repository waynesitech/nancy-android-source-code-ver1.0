package com.lesys.lepro.ai.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.JMeta;
import com.lesys.lepro.ai.model.JMsg;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.model.SpecialRequestCover;
import com.lesys.lepro.ai.model.generate_new_ta.DirectBankInModel;
import com.lesys.lepro.ai.model.generate_new_ta.FixtureFittingsModel;
import com.lesys.lepro.ai.model.generate_new_ta.MoneyModel;
import com.lesys.lepro.ai.model.generate_new_ta.MonthYearModel;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.PaymentDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.PostDatedChequeModel;
import com.lesys.lepro.ai.model.generate_new_ta.PropertyDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.RentalDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.SpecialConditionModel;
import com.lesys.lepro.ai.model.generate_new_ta.TermsOfRenewalModel;
import com.lesys.lepro.ai.model.generate_new_ta.param_api.LandlordTenantCover;
import com.lesys.lepro.ai.model.generate_new_ta.param_api.OtherSecurityDepositCover;
import com.lesys.lepro.ai.shared.ClickActionInterface;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class BaseApi {

    private static final String TAG = BaseApi.class.getName();

    public static Builder init(Context context) {
        return new Builder(context);
    }

    public interface DialogResponseListener {
        void onSuccess(JSONObject response);

        void onFailure(Throwable throwable, JSONObject response);
    }

    public interface JsonResponseListener {
        void onSuccess(JSONObject response);

        void onFailure(Throwable throwable, JSONObject response);
    }

    public interface BigFileResponseListener {
        void onSuccess(JSONObject response);

        void onProgress(int bytesWritten, int totalSize);

        void onFailure(Throwable throwable, JSONObject response);
    }

    public static class Builder {

        private Context mContext;
        private BaseRestClient mClient;
        private String mUrl, mMethod;
        private DialogResponseListener mDialogResponseListener;
        private JsonResponseListener mJsonResponseListener;
        private BigFileResponseListener mBigFileResponseListener;
        private ProgressDialog mProgressDialog;
        private boolean mKeepProgressDialog = false, mShowMessageDialog = true;
        private boolean mIgnore403 = false, mIsLogout = false;
        Gson gson = new Gson();
        private ClickActionInterface.DialogActionInterface dialogAction;
        DataHandler mHandler;
        public boolean withProgressDialog = false;

        public ClickActionInterface.DialogActionInterface getDialogAction() {
            return dialogAction;
        }

        public void setDialogAction(ClickActionInterface.DialogActionInterface dialogAction) {
            this.dialogAction = dialogAction;
        }

        private JsonHttpResponseHandler mJsonHttpResponseHandler = new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                if (mShowMessageDialog)
                    new BaseJSON(mContext).get(response, dialogAction);

                if (mHandler != null)
                    mHandler.onReady(response.toString());

                if (mDialogResponseListener != null)
                    mDialogResponseListener.onSuccess(response);

                if (mJsonResponseListener != null)
                    mJsonResponseListener.onSuccess(response);

                if (mBigFileResponseListener != null)
                    mBigFileResponseListener.onSuccess(response);

                try {
                    if (mProgressDialog != null && !mKeepProgressDialog) mProgressDialog.dismiss();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                if (mIgnore403) {
                    SharedPreferencesUtil.setBoolean(mContext, GlobalParam.KEY_IS_LOGINED, true);
                }

                if (mIsLogout) {
                    SharedPreferencesUtil.setBoolean(mContext, GlobalParam.KEY_IS_LOGINED, false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {

                Log.e(TAG, "onFailure 1 : " + throwable.toString());
                if (mDialogResponseListener != null)
                    mDialogResponseListener.onFailure(throwable, response);

                if (mJsonResponseListener != null)
                    mJsonResponseListener.onFailure(throwable, response);

                if (mBigFileResponseListener != null)
                    mBigFileResponseListener.onFailure(throwable, response);

                if (mHandler != null)
                    try {
                        mHandler.onFail(statusCode, headers, response.toString(), throwable);
                    } catch (Exception e) {
                        mHandler.onFail(statusCode, headers, "", throwable);
                    }

                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                BaseJSON baseJSON = new BaseJSON(mContext);

                if (mIgnore403) baseJSON.noLogOut();

                if (mShowMessageDialog)
                    baseJSON.get(response, null);

                if (throwable != null && throwable.toString().equals("java.net.SocketTimeoutException"))
                    Toast.makeText(mContext, R.string.connecting_to_server, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onProgress(int bytesWritten, int totalSize) {
                if (mBigFileResponseListener != null)
                    mBigFileResponseListener.onProgress(bytesWritten, totalSize);


                super.onProgress(bytesWritten, totalSize);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Log.e(TAG, "onFailure 2 : " + throwable.toString());
                MetaData metaData = new MetaData();
                metaData.meta = new JMeta();
                metaData.meta.msg = new JMsg();
                metaData.meta.msg.body = mContext.getString(R.string.connecting_to_server);
                metaData.meta.msg.subj = mContext.getString(R.string.network_error);
                metaData.meta.stat = "500";

                String jsonString = gson.toJson(metaData);

                try {
                    JSONObject response = new JSONObject(jsonString);

                    if (mDialogResponseListener != null)
                        mDialogResponseListener.onFailure(throwable, response);

                    if (mJsonResponseListener != null)
                        mJsonResponseListener.onFailure(throwable, response);

                    if (mBigFileResponseListener != null)
                        mBigFileResponseListener.onFailure(throwable, response);

                    if (mHandler != null)
                        mHandler.onFail(statusCode, headers, responseString, throwable);

                    try {
                        if (mProgressDialog != null) mProgressDialog.dismiss();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Toast.makeText(mContext, R.string.connecting_to_server, Toast.LENGTH_LONG).show();
            }
        };

        public Builder(Context context) {
            mContext = context;
            mClient = new BaseRestClient(context);
        }

        public Builder syncHttpClient(boolean isSync) {
            if (isSync)
                mClient = new BaseRestClient(mContext).syncHttpClient();
            return this;
        }

        public void call(AsyncHttpResponseHandler responseHandler) {

            if (mMethod.equals("get")) mClient.get(mUrl, responseHandler);
            else if (mMethod.equals("post")) mClient.post(mUrl, responseHandler);
        }

        public void callCustom(AsyncHttpResponseHandler responseHandler) {

            if (mMethod.equals("get")) mClient.getCustom(mUrl, responseHandler);
            else if (mMethod.equals("post")) mClient.postCustom(mUrl, responseHandler);
        }

        public Builder call(DialogResponseListener responseHandler) {

            //setup progress dialog
            if (mProgressDialog == null) {
                try {
                    mProgressDialog = new ProgressDialog(mContext);
                    mProgressDialog.setMessage(mContext.getResources().getString(R.string.loading));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.show();
                } catch (Exception e) {

                }
            }
            mDialogResponseListener = responseHandler;
            call(mJsonHttpResponseHandler);

            return this;
        }

        public Builder call(JsonResponseListener responseHandler) {
            mJsonResponseListener = responseHandler;
            call(mJsonHttpResponseHandler);

            return this;
        }

        public <T> Builder call(final DataHandler<T> handler) {
            if (withProgressDialog && mProgressDialog == null) {
                try {
                    mProgressDialog = new ProgressDialog(mContext);
                    mProgressDialog.setMessage(mContext.getResources().getString(R.string.loading));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.show();
                } catch (Exception e) {

                }
            }

            this.mHandler = handler;
            call(mJsonHttpResponseHandler);

            return this;
        }

        public Builder call(BigFileResponseListener responseHandler) {
            mBigFileResponseListener = responseHandler;
            call(mBigFileResponseListener);

            return this;
        }

        public Builder showMessageDialog(boolean show) {
            mShowMessageDialog = show;
            return this;
        }

        public Builder ignore403(boolean ignore) {
            mIgnore403 = ignore;
            return this;
        }

        public Builder keepProgressDialog() {
            mKeepProgressDialog = true;
            return this;
        }

        public ProgressDialog getProgressDialog() {
            return mProgressDialog;
        }

        public Builder setProgressDialog(ProgressDialog progressDialog) {
            mProgressDialog = progressDialog;
            return this;
        }

        public Builder emailLogin(
                String email,
                String password
        ) {

            mIgnore403 = true;
            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/login";
//            mUrl = "/users/adminLogin";

            mClient.addParam("email", email);
            mClient.addParam("password", password);

            PackageInfo pInfo = null;
            try {
                pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
                int currentVersionCode = pInfo.versionCode;

                mClient.addParam("platform", "android");
                mClient.addParam("currentVersionCode", currentVersionCode);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            return this;
        }

        public Builder registerAccount(
                String email,
                String password,
                String ic_number,
                String name,
                String mobile
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/register";

            mClient.addParam("name", name);
            mClient.addParam("email", email);
            mClient.addParam("password", password);
            mClient.addParam("ic_number", ic_number);
            mClient.addParam("mobile", mobile);

            return this;
        }

        public Builder resendActivationCode(
                String email
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/resendActivationCode";

            mClient.addParam("email", email);

            return this;
        }

        public Builder requestPhoneVerificationById(
                long userID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/requestPhoneVerificationById";

            mClient.addParam("userID", userID);

            return this;
        }

        public Builder submitPhoneVerificationById(
                String verificationCode,
                long userID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/submitPhoneVerificationById";

            mClient.addParam("verificationCode", verificationCode);
            mClient.addParam("userID", userID);

            return this;
        }

        public Builder resetPassword(
                String email
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/reset";

            mClient.addParam("email", email);

            return this;
        }

        public Builder logout() {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/users/logout";

            return this;
        }

        public Builder getAppVersion() {
            mMethod = "get";
            withProgressDialog = false;
            mUrl = "/getAppVersion";

            PackageInfo pInfo = null;
            try {
                pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
                int currentVersionCode = pInfo.versionCode;

                String locale = SharedPreferencesUtil.getString(mContext, GlobalParam.KEY_LOCALE);
                if (locale != null) {
                    mClient.addParam("lang", locale);
                }
                mClient.addParam("platform", "android");
                mClient.addParam("currentVersionCode", currentVersionCode);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            return this;
        }

        public Builder createOrder(NewTAModel newTAModel) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/new";

            /**
             * Property Detail
             */
            mClient.addParam("isResidential", !newTAModel.isCommercial);
            if (newTAModel.propertyDetail != null) {
                PropertyDetailModel propertyDetailModel = newTAModel.propertyDetail;
                mClient.addParam("propertyStreet", propertyDetailModel.streetName);
                mClient.addParam("propertyTown", propertyDetailModel.town);
                mClient.addParam("propertyPostcode", propertyDetailModel.postCode);
                mClient.addParam("propertyState", propertyDetailModel.states);
                mClient.addParam("propertyPicture", new File(propertyDetailModel.imagePath));
            }

            /**
             * Owner Landlord Tenant Detail List
             */
            mClient.addParam("isLandlord", !newTAModel.isTenant);
            if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() != 0) {
                LandlordTenantCover landlordTenantCover = new LandlordTenantCover();
                String json = landlordTenantCover.getJson(newTAModel.ownerLandlordTenantDetailList);
                if (!TextUtils.isEmpty(json)) {
                    if (newTAModel.isTenant) {
                        mClient.addParam("tenants", json);
                    } else {
                        mClient.addParam("landlords", json);
                    }
                }
            }

            /**
             * Non Owner Landlord Tenant Detail List
             */
            if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() != 0) {
                LandlordTenantCover landlordTenantCover = new LandlordTenantCover();
                String json = landlordTenantCover.getJson(newTAModel.nonOwnerLandlordTenantDetailList);
                if (!TextUtils.isEmpty(json)) {
                    if (!newTAModel.isTenant) {
                        mClient.addParam("tenants", json);
                    } else {
                        mClient.addParam("landlords", json);
                    }
                }
            }

            /**
             * Rental Detail
             */
            if (newTAModel.rentalDetail != null) {
                RentalDetailModel rentalDetailModel = newTAModel.rentalDetail;
                mClient.addParam("commencementDate",
                        Common.Helpers.getFinalDate(rentalDetailModel.commencementDate, "yyyy-MM-dd"));
                if (rentalDetailModel.termOfTheTenancy != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.termOfTheTenancy;
                    mClient.addParam("termsOfTenancyMonths", monthYearModel.month);
                    mClient.addParam("termsOfTenancyYears", monthYearModel.year);
                }
                mClient.addParam("isOptionToRenew", rentalDetailModel.isOptionToRenew);
                if (rentalDetailModel.optionToRenew != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.optionToRenew;
                    mClient.addParam("optionToRenewMonths", monthYearModel.month);
                    mClient.addParam("optionToRenewYears", monthYearModel.year);
                }
                if (rentalDetailModel.termsOfRenewal != null) {
                    TermsOfRenewalModel termsOfRenewalModel = rentalDetailModel.termsOfRenewal;
                    String optionToRenewCondition = null;
                    if (termsOfRenewalModel.isMarkerPrevailingRate) {
                        optionToRenewCondition = "isMarketPrevaillingRate";
                    } else if (termsOfRenewalModel.isIncreaseOf10Percent) {
                        optionToRenewCondition = "isIncreaseOf10percent";
                    } else if (termsOfRenewalModel.isIncreaseOf15Percent) {
                        optionToRenewCondition = "isIncreaseOf15percent";
                    } else if (termsOfRenewalModel.isAtMutuallyAgreeRate) {
                        optionToRenewCondition = "isMutuallyAgreedRate";
                    } else if (termsOfRenewalModel.isOther) {
                        optionToRenewCondition = "isOtherPercent";
                        mClient.addParam("otherTermsofRenewal", termsOfRenewalModel.totalOtherPercentage);
                    }
                    if (!TextUtils.isEmpty(optionToRenewCondition)) {
                        mClient.addParam("optionToRenewCondition", optionToRenewCondition);
                    }
                }
                if (rentalDetailModel.rental != null) {
                    MoneyModel moneyModel = rentalDetailModel.rental;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("rental", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.advanceRental != null) {
                    MoneyModel moneyModel = rentalDetailModel.advanceRental;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("advanceRental", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositRent != null) {
                    MoneyModel moneyModel = rentalDetailModel.securityDepositRent;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("securityDepositRent", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositUtilities != null) {
                    MoneyModel moneyModel = rentalDetailModel.securityDepositUtilities;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("securityDepositUtilities", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositOtherList != null && rentalDetailModel.securityDepositOtherList.size() != 0) {
                    OtherSecurityDepositCover otherSecurityDepositCover = new OtherSecurityDepositCover();
                    String json = otherSecurityDepositCover.getJson(rentalDetailModel.securityDepositOtherList);
                    if (!TextUtils.isEmpty(json)) {
                        mClient.addParam("otherSecurityDeposit", json);
                    }
                }

                if (rentalDetailModel.isRentalFreePeriodSpecialDate) {
                    if (!TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodFromDate) && !TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodToDate)) {
                        mClient.addParam("rentalFreePeriodStart",
                                Common.Helpers.getFinalDate(rentalDetailModel.rentalFreePeriodFromDate, "yyyy-MM-dd"));
                        mClient.addParam("rentalFreePeriodEnd",
                                Common.Helpers.getFinalDate(rentalDetailModel.rentalFreePeriodToDate, "yyyy-MM-dd"));
                    }
                } else {
                    if (rentalDetailModel.rentalFreePeriod != null) {
                        MonthYearModel monthYearModel = rentalDetailModel.rentalFreePeriod;
                        mClient.addParam("rentalFreePeriodWeeks", monthYearModel.week);
                        mClient.addParam("rentalFreePeriodMonths", monthYearModel.month);
                        mClient.addParam("rentalFreePeriodYears", monthYearModel.year);
                    }
                }

                if (rentalDetailModel.lotNumberList != null && rentalDetailModel.lotNumberList.size() != 0) {
                    mClient.addParam("carParkLots", new Gson().toJson(rentalDetailModel.lotNumberList));
                } else {
                    mClient.addParam("carParkLots", "[]");
                }
            }

            /**
             * Payment Detail
             */
            if (newTAModel.paymentDetailModel != null) {
                PaymentDetailModel paymentDetailModel = newTAModel.paymentDetailModel;
                String paymentMethod = null;
                if (paymentDetailModel.isDirectBankIn) {
                    paymentMethod = "isDirectBankIn";
                    if (paymentDetailModel.directBankInModel != null) {
                        DirectBankInModel directBankInModel = paymentDetailModel.directBankInModel;
                        mClient.addParam("paymentAccountName", directBankInModel.nameOfAccountHolder);
                        mClient.addParam("bank", directBankInModel.bank);
                        mClient.addParam("bankAccountNum", directBankInModel.bankAccountNumber);
                        String notificationMethod = null;
                        if (directBankInModel.isEmail) {
                            notificationMethod = "isEmail";
                        } else if (directBankInModel.isSMS) {
                            notificationMethod = "isSMS";
                        } else if (directBankInModel.isWhatsappText) {
                            notificationMethod = "isWhatsapp";
                        }
                        if (!TextUtils.isEmpty(notificationMethod)) {
                            mClient.addParam("notificationMethod", notificationMethod);
                        }
                    }
                } else if (paymentDetailModel.isPostDatedCheque) {
                    paymentMethod = "isPostDatedCheque";
                    if (paymentDetailModel.postDatedChequeModel != null) {
                        PostDatedChequeModel postDatedChequeModel = paymentDetailModel.postDatedChequeModel;
                        String postDatedMethod = null;
                        if (postDatedChequeModel.is6Months) {
                            postDatedMethod = "is6Months";
                        } else if (postDatedChequeModel.is12Months) {
                            postDatedMethod = "is12Months";
                        } else if (postDatedChequeModel.is24Months) {
                            postDatedMethod = "is24Months";
                        }
                        if (!TextUtils.isEmpty(postDatedMethod)) {
                            mClient.addParam("postDatedMethod", postDatedMethod);
                        }
                    }
                } else if (paymentDetailModel.isCollectionByLandlord) {
                    paymentMethod = "isCollectionByLandlord";
                }
                if (!TextUtils.isEmpty(paymentMethod)) {
                    mClient.addParam("paymentMethod", paymentMethod);
                }
            }

            /**
             * SpecialCondition
             */
            if (newTAModel.mSpecialRequestCover != null) {
                SpecialRequestCover mSpecialRequestCover = newTAModel.mSpecialRequestCover;
//                SpecialConditionModel specialConditionModel = newTAModel.specialCondition;
                if (mSpecialRequestCover != null &&
                        mSpecialRequestCover.getArrayList() != null &&
                        mSpecialRequestCover.getArrayList().size() != 0) {
                    mClient.addParam("specialRequest", new Gson().toJson(mSpecialRequestCover.getArrayList()));
                } else {
                    mClient.addParam("specialRequest", "[]");
                }
            } else {
                mClient.addParam("specialRequest", "[]");
            }

            /**
             * FixtureFittings
             */
            if (newTAModel.fixtureFittingsModel != null) {
                FixtureFittingsModel fixtureFittingsModel = newTAModel.fixtureFittingsModel;
                if (fixtureFittingsModel.fixtureFittingList != null && fixtureFittingsModel.fixtureFittingList.size() != 0) {
                    mClient.addParam("fixturesFitting", new Gson().toJson(fixtureFittingsModel.fixtureFittingList));
                } else {
                    mClient.addParam("fixturesFitting", "[]");
                }
            } else {
                mClient.addParam("fixturesFitting", "[]");
            }
            return this;
        }

        public Builder createOrderWithPDF(NewTAModel newTAModel) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/newWithPDF";

            mClient.addParam("taPDF", new File(newTAModel.taPDF));
            mClient.addParam("uploadType", newTAModel.uploadType);
            /**
             * Property Detail
             */
            mClient.addParam("isResidential", !newTAModel.isCommercial);
            if (newTAModel.propertyDetail != null) {
                PropertyDetailModel propertyDetailModel = newTAModel.propertyDetail;
                mClient.addParam("propertyStreet", propertyDetailModel.streetName);
                mClient.addParam("propertyTown", propertyDetailModel.town);
                mClient.addParam("propertyPostcode", propertyDetailModel.postCode);
                mClient.addParam("propertyState", propertyDetailModel.states);
                mClient.addParam("propertyPicture", new File(propertyDetailModel.imagePath));
            }

            /**
             * Owner Landlord Tenant Detail List
             */
            mClient.addParam("isLandlord", !newTAModel.isTenant);
            if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() != 0) {
                LandlordTenantCover landlordTenantCover = new LandlordTenantCover();
                String json = landlordTenantCover.getJson(newTAModel.ownerLandlordTenantDetailList);
                if (!TextUtils.isEmpty(json)) {
                    if (newTAModel.isTenant) {
                        mClient.addParam("tenants", json);
                    } else {
                        mClient.addParam("landlords", json);
                    }
                }
            }

            /**
             * Non Owner Landlord Tenant Detail List
             */
            if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() != 0) {
                LandlordTenantCover landlordTenantCover = new LandlordTenantCover();
                String json = landlordTenantCover.getJson(newTAModel.nonOwnerLandlordTenantDetailList);
                if (!TextUtils.isEmpty(json)) {
                    if (!newTAModel.isTenant) {
                        mClient.addParam("tenants", json);
                    } else {
                        mClient.addParam("landlords", json);
                    }
                }
            }

            /**
             * Rental Detail
             */
            if (newTAModel.rentalDetail != null) {
                RentalDetailModel rentalDetailModel = newTAModel.rentalDetail;
                mClient.addParam("commencementDate",
                        Common.Helpers.getFinalDate(rentalDetailModel.commencementDate, "yyyy-MM-dd"));
                if (rentalDetailModel.termOfTheTenancy != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.termOfTheTenancy;
                    mClient.addParam("termsOfTenancyMonths", monthYearModel.month);
                    mClient.addParam("termsOfTenancyYears", monthYearModel.year);
                }
                mClient.addParam("isOptionToRenew", rentalDetailModel.isOptionToRenew);
                if (rentalDetailModel.optionToRenew != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.optionToRenew;
                    mClient.addParam("optionToRenewMonths", monthYearModel.month);
                    mClient.addParam("optionToRenewYears", monthYearModel.year);
                }
                if (rentalDetailModel.termsOfRenewal != null) {
                    TermsOfRenewalModel termsOfRenewalModel = rentalDetailModel.termsOfRenewal;
                    String optionToRenewCondition = null;
                    if (termsOfRenewalModel.isMarkerPrevailingRate) {
                        optionToRenewCondition = "isMarketPrevaillingRate";
                    } else if (termsOfRenewalModel.isIncreaseOf10Percent) {
                        optionToRenewCondition = "isIncreaseOf10percent";
                    } else if (termsOfRenewalModel.isIncreaseOf15Percent) {
                        optionToRenewCondition = "isIncreaseOf15percent";
                    } else if (termsOfRenewalModel.isAtMutuallyAgreeRate) {
                        optionToRenewCondition = "isMutuallyAgreedRate";
                    } else if (termsOfRenewalModel.isOther) {
                        optionToRenewCondition = "isOtherPercent";
                        mClient.addParam("otherTermsofRenewal", termsOfRenewalModel.totalOtherPercentage);
                    }
                    if (!TextUtils.isEmpty(optionToRenewCondition)) {
                        mClient.addParam("optionToRenewCondition", optionToRenewCondition);
                    }
                }
                if (rentalDetailModel.rental != null) {
                    MoneyModel moneyModel = rentalDetailModel.rental;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("rental", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.advanceRental != null) {
                    MoneyModel moneyModel = rentalDetailModel.advanceRental;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("advanceRental", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositRent != null) {
                    MoneyModel moneyModel = rentalDetailModel.securityDepositRent;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("securityDepositRent", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositUtilities != null) {
                    MoneyModel moneyModel = rentalDetailModel.securityDepositUtilities;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("securityDepositUtilities", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositOtherList != null && rentalDetailModel.securityDepositOtherList.size() != 0) {
                    OtherSecurityDepositCover otherSecurityDepositCover = new OtherSecurityDepositCover();
                    String json = otherSecurityDepositCover.getJson(rentalDetailModel.securityDepositOtherList);
                    if (!TextUtils.isEmpty(json)) {
                        mClient.addParam("otherSecurityDeposit", json);
                    }
                }
                if (rentalDetailModel.isRentalFreePeriodSpecialDate) {
                    if (!TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodFromDate) && !TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodToDate)) {
                        mClient.addParam("rentalFreePeriodStart",
                                Common.Helpers.getFinalDate(rentalDetailModel.rentalFreePeriodFromDate, "yyyy-MM-dd"));
                        mClient.addParam("rentalFreePeriodEnd",
                                Common.Helpers.getFinalDate(rentalDetailModel.rentalFreePeriodToDate, "yyyy-MM-dd"));
                    }
                } else {
                    if (rentalDetailModel.rentalFreePeriod != null) {
                        MonthYearModel monthYearModel = rentalDetailModel.rentalFreePeriod;
                        mClient.addParam("rentalFreePeriodWeeks", monthYearModel.week);
                        mClient.addParam("rentalFreePeriodMonths", monthYearModel.month);
                        mClient.addParam("rentalFreePeriodYears", monthYearModel.year);
                    }
                }

                if (rentalDetailModel.lotNumberList != null && rentalDetailModel.lotNumberList.size() != 0) {
                    mClient.addParam("carParkLots", new Gson().toJson(rentalDetailModel.lotNumberList));
                } else {
                    mClient.addParam("carParkLots", "[]");
                }
            }

            /**
             * Payment Detail
             */
            if (newTAModel.paymentDetailModel != null) {
                PaymentDetailModel paymentDetailModel = newTAModel.paymentDetailModel;
                String paymentMethod = null;
                if (paymentDetailModel.isDirectBankIn) {
                    paymentMethod = "isDirectBankIn";
                    if (paymentDetailModel.directBankInModel != null) {
                        DirectBankInModel directBankInModel = paymentDetailModel.directBankInModel;
                        mClient.addParam("paymentAccountName", directBankInModel.nameOfAccountHolder);
                        mClient.addParam("bank", directBankInModel.bank);
                        mClient.addParam("bankAccountNum", directBankInModel.bankAccountNumber);
                        String notificationMethod = null;
                        if (directBankInModel.isEmail) {
                            notificationMethod = "isEmail";
                        } else if (directBankInModel.isSMS) {
                            notificationMethod = "isSMS";
                        } else if (directBankInModel.isWhatsappText) {
                            notificationMethod = "isWhatsapp";
                        }
                        if (!TextUtils.isEmpty(notificationMethod)) {
                            mClient.addParam("notificationMethod", notificationMethod);
                        }
                    }
                } else if (paymentDetailModel.isPostDatedCheque) {
                    paymentMethod = "isPostDatedCheque";
                    if (paymentDetailModel.postDatedChequeModel != null) {
                        PostDatedChequeModel postDatedChequeModel = paymentDetailModel.postDatedChequeModel;
                        String postDatedMethod = null;
                        if (postDatedChequeModel.is6Months) {
                            postDatedMethod = "is6Months";
                        } else if (postDatedChequeModel.is12Months) {
                            postDatedMethod = "is12Months";
                        } else if (postDatedChequeModel.is24Months) {
                            postDatedMethod = "is24Months";
                        }
                        if (!TextUtils.isEmpty(postDatedMethod)) {
                            mClient.addParam("postDatedMethod", postDatedMethod);
                        }
                    }
                } else if (paymentDetailModel.isCollectionByLandlord) {
                    paymentMethod = "isCollectionByLandlord";
                }
                if (!TextUtils.isEmpty(paymentMethod)) {
                    mClient.addParam("paymentMethod", paymentMethod);
                }
            }

            /**
             * SpecialCondition
             */
            if (newTAModel.mSpecialRequestCover != null) {
                SpecialRequestCover mSpecialRequestCover = newTAModel.mSpecialRequestCover;
//                SpecialConditionModel specialConditionModel = newTAModel.specialCondition;
                if (mSpecialRequestCover != null &&
                        mSpecialRequestCover.getArrayList() != null &&
                        mSpecialRequestCover.getArrayList().size() != 0) {
                    mClient.addParam("specialRequest", new Gson().toJson(mSpecialRequestCover.getArrayList()));
                } else {
                    mClient.addParam("specialRequest", "[]");
                }
            } else {
                mClient.addParam("specialRequest", "[]");
            }

            /**
             * FixtureFittings
             */
            if (newTAModel.fixtureFittingsModel != null) {
                FixtureFittingsModel fixtureFittingsModel = newTAModel.fixtureFittingsModel;
                if (fixtureFittingsModel.fixtureFittingList != null && fixtureFittingsModel.fixtureFittingList.size() != 0) {
                    mClient.addParam("fixturesFitting", new Gson().toJson(fixtureFittingsModel.fixtureFittingList));
                } else {
                    mClient.addParam("fixturesFitting", "[]");
                }
            } else {
                mClient.addParam("fixturesFitting", "[]");
            }
            return this;
        }

        public Builder editOrder(NewTAModel newTAModel) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/edit";

            mClient.addParam("orderID", newTAModel.id);

            /**
             * Property Detail
             */
            mClient.addParam("isResidential", !newTAModel.isCommercial);
            if (newTAModel.propertyDetail != null) {
                PropertyDetailModel propertyDetailModel = newTAModel.propertyDetail;
                mClient.addParam("propertyStreet", propertyDetailModel.streetName);
                mClient.addParam("propertyTown", propertyDetailModel.town);
                mClient.addParam("propertyPostcode", propertyDetailModel.postCode);
                mClient.addParam("propertyState", propertyDetailModel.states);
                if (!propertyDetailModel.imagePath.contains("http")) {
                    File file = new File(propertyDetailModel.imagePath);
                    if (file != null && file.exists()) {
                        mClient.addParam("propertyPicture", file);
                    }
                }
            }

            /**
             * Owner Landlord Tenant Detail List
             */
            mClient.addParam("isLandlord", !newTAModel.isTenant);
            if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() != 0) {
                LandlordTenantCover landlordTenantCover = new LandlordTenantCover();
                String json = landlordTenantCover.getJson(newTAModel.ownerLandlordTenantDetailList);
                if (!TextUtils.isEmpty(json)) {
                    if (newTAModel.isTenant) {
                        mClient.addParam("tenants", json);
                    } else {
                        mClient.addParam("landlords", json);
                    }
                }
            }

            /**
             * Non Owner Landlord Tenant Detail List
             */
            if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() != 0) {
                LandlordTenantCover landlordTenantCover = new LandlordTenantCover();
                String json = landlordTenantCover.getJson(newTAModel.nonOwnerLandlordTenantDetailList);
                if (!TextUtils.isEmpty(json)) {
                    if (!newTAModel.isTenant) {
                        mClient.addParam("tenants", json);
                    } else {
                        mClient.addParam("landlords", json);
                    }
                }
            }

            /**
             * Rental Detail
             */
            if (newTAModel.rentalDetail != null) {
                RentalDetailModel rentalDetailModel = newTAModel.rentalDetail;
                mClient.addParam("commencementDate",
                        Common.Helpers.getFinalDate(rentalDetailModel.commencementDate, "yyyy-MM-dd"));
                if (rentalDetailModel.termOfTheTenancy != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.termOfTheTenancy;
                    mClient.addParam("termsOfTenancyMonths", monthYearModel.month);
                    mClient.addParam("termsOfTenancyYears", monthYearModel.year);
                }
                mClient.addParam("isOptionToRenew", rentalDetailModel.isOptionToRenew);
                if (rentalDetailModel.optionToRenew != null) {
                    MonthYearModel monthYearModel = rentalDetailModel.optionToRenew;
                    mClient.addParam("optionToRenewMonths", monthYearModel.month);
                    mClient.addParam("optionToRenewYears", monthYearModel.year);
                }
                if (rentalDetailModel.termsOfRenewal != null) {
                    TermsOfRenewalModel termsOfRenewalModel = rentalDetailModel.termsOfRenewal;
                    String optionToRenewCondition = null;
                    if (termsOfRenewalModel.isMarkerPrevailingRate) {
                        optionToRenewCondition = "isMarketPrevaillingRate";
                    } else if (termsOfRenewalModel.isIncreaseOf10Percent) {
                        optionToRenewCondition = "isIncreaseOf10percent";
                    } else if (termsOfRenewalModel.isIncreaseOf15Percent) {
                        optionToRenewCondition = "isIncreaseOf15percent";
                    } else if (termsOfRenewalModel.isAtMutuallyAgreeRate) {
                        optionToRenewCondition = "isMutuallyAgreedRate";
                    } else if (termsOfRenewalModel.isOther) {
                        optionToRenewCondition = "isOtherPercent";
                        mClient.addParam("otherTermsofRenewal", termsOfRenewalModel.totalOtherPercentage);
                    }
                    if (!TextUtils.isEmpty(optionToRenewCondition)) {
                        mClient.addParam("optionToRenewCondition", optionToRenewCondition);
                    }
                }
                if (rentalDetailModel.rental != null) {
                    MoneyModel moneyModel = rentalDetailModel.rental;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("rental", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.advanceRental != null) {
                    MoneyModel moneyModel = rentalDetailModel.advanceRental;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("advanceRental", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositRent != null) {
                    MoneyModel moneyModel = rentalDetailModel.securityDepositRent;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("securityDepositRent", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositUtilities != null) {
                    MoneyModel moneyModel = rentalDetailModel.securityDepositUtilities;
                    String priceStr;
                    if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                        priceStr = "00";
                    } else {
                        priceStr = moneyModel.ringgitMalaysia;
                    }
                    if (!Common.Helpers.isNumber(moneyModel.cent)) {
                        priceStr = priceStr + ".00";
                    } else {
                        priceStr = priceStr + "." + moneyModel.cent;
                    }
                    mClient.addParam("securityDepositUtilities", Double.valueOf(priceStr));
                }
                if (rentalDetailModel.securityDepositOtherList != null && rentalDetailModel.securityDepositOtherList.size() != 0) {
                    OtherSecurityDepositCover otherSecurityDepositCover = new OtherSecurityDepositCover();
                    String json = otherSecurityDepositCover.getJson(rentalDetailModel.securityDepositOtherList);
                    if (!TextUtils.isEmpty(json)) {
                        mClient.addParam("otherSecurityDeposit", json);
                    }
                }

                if (rentalDetailModel.isRentalFreePeriodSpecialDate) {
                    if (!TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodFromDate) && !TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodToDate)) {
                        mClient.addParam("rentalFreePeriodStart",
                                Common.Helpers.getFinalDate(rentalDetailModel.rentalFreePeriodFromDate, "yyyy-MM-dd"));
                        mClient.addParam("rentalFreePeriodEnd",
                                Common.Helpers.getFinalDate(rentalDetailModel.rentalFreePeriodToDate, "yyyy-MM-dd"));
                    }
                } else {
                    if (rentalDetailModel.rentalFreePeriod != null) {
                        MonthYearModel monthYearModel = rentalDetailModel.rentalFreePeriod;
                        mClient.addParam("rentalFreePeriodWeeks", monthYearModel.week);
                        mClient.addParam("rentalFreePeriodMonths", monthYearModel.month);
                        mClient.addParam("rentalFreePeriodYears", monthYearModel.year);
                    }
                }

                if (rentalDetailModel.lotNumberList != null && rentalDetailModel.lotNumberList.size() != 0) {
                    mClient.addParam("carParkLots", new Gson().toJson(rentalDetailModel.lotNumberList));
                } else {
                    mClient.addParam("carParkLots", "[]");
                }
            }

            /**
             * Payment Detail
             */
            if (newTAModel.paymentDetailModel != null) {
                PaymentDetailModel paymentDetailModel = newTAModel.paymentDetailModel;
                String paymentMethod = null;
                if (paymentDetailModel.isDirectBankIn) {
                    paymentMethod = "isDirectBankIn";
                    if (paymentDetailModel.directBankInModel != null) {
                        DirectBankInModel directBankInModel = paymentDetailModel.directBankInModel;
                        mClient.addParam("paymentAccountName", directBankInModel.nameOfAccountHolder);
                        mClient.addParam("bank", directBankInModel.bank);
                        mClient.addParam("bankAccountNum", directBankInModel.bankAccountNumber);
                        String notificationMethod = null;
                        if (directBankInModel.isEmail) {
                            notificationMethod = "isEmail";
                        } else if (directBankInModel.isSMS) {
                            notificationMethod = "isSMS";
                        } else if (directBankInModel.isWhatsappText) {
                            notificationMethod = "isWhatsapp";
                        }
                        if (!TextUtils.isEmpty(notificationMethod)) {
                            mClient.addParam("notificationMethod", notificationMethod);
                        }
                    }
                } else if (paymentDetailModel.isPostDatedCheque) {
                    paymentMethod = "isPostDatedCheque";
                    if (paymentDetailModel.postDatedChequeModel != null) {
                        PostDatedChequeModel postDatedChequeModel = paymentDetailModel.postDatedChequeModel;
                        String postDatedMethod = null;
                        if (postDatedChequeModel.is6Months) {
                            postDatedMethod = "is6Months";
                        } else if (postDatedChequeModel.is12Months) {
                            postDatedMethod = "is12Months";
                        } else if (postDatedChequeModel.is24Months) {
                            postDatedMethod = "is24Months";
                        }
                        if (!TextUtils.isEmpty(postDatedMethod)) {
                            mClient.addParam("postDatedMethod", postDatedMethod);
                        }
                    }
                } else if (paymentDetailModel.isCollectionByLandlord) {
                    paymentMethod = "isCollectionByLandlord";
                }
                if (!TextUtils.isEmpty(paymentMethod)) {
                    mClient.addParam("paymentMethod", paymentMethod);
                }
            }

            /**
             * SpecialCondition
             */
            if (newTAModel.mSpecialRequestCover != null) {
                SpecialRequestCover mSpecialRequestCover = newTAModel.mSpecialRequestCover;
//                SpecialConditionModel specialConditionModel = newTAModel.specialCondition;
                if (mSpecialRequestCover != null &&
                        mSpecialRequestCover.getArrayList() != null &&
                        mSpecialRequestCover.getArrayList().size() != 0) {
                    mClient.addParam("specialRequest", new Gson().toJson(mSpecialRequestCover.getArrayList()));
                } else {
                    mClient.addParam("specialRequest", "[]");
                }
            } else {
                mClient.addParam("specialRequest", "[]");
            }

            /**
             * FixtureFittings
             */
            if (newTAModel.fixtureFittingsModel != null) {
                FixtureFittingsModel fixtureFittingsModel = newTAModel.fixtureFittingsModel;
                if (fixtureFittingsModel.fixtureFittingList != null && fixtureFittingsModel.fixtureFittingList.size() != 0) {
                    mClient.addParam("fixturesFitting", new Gson().toJson(fixtureFittingsModel.fixtureFittingList));
                } else {
                    mClient.addParam("fixturesFitting", "[]");
                }
            } else {
                mClient.addParam("fixturesFitting", "[]");
            }
            return this;
        }

        public Builder listOrder(
                int pageLength,
                int page
        ) {

            mMethod = "post";
            withProgressDialog = false;
            mUrl = "/order/list";

            mClient.addParam("pageLength", pageLength);
            mClient.addParam("page", page);

            return this;
        }

        public Builder orderDetail(
                int orderID
        ) {

            mMethod = "post";
            withProgressDialog = false;
            mUrl = "/order/detail";

            mClient.addParam("orderID", orderID);

            return this;
        }

        public Builder sendSignInvitation(
                int orderID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/sendSignInvitation";

            mClient.addParam("orderID", orderID);

            return this;
        }

        public Builder loginViaPasscode(
                String passcode,
                String ic_num
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mIgnore403 = true;
            mUrl = "/order/loginViaPasscode";

            mClient.addParam("passcode", passcode);
            mClient.addParam("ic_num", ic_num);

            return this;
        }

        public Builder submitSignature(
                String orderUserID,
                File signatureImage
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mIgnore403 = true;
            mUrl = "/order/apiSubmitSignature";

            mClient.addParam("orderUserID", orderUserID);
            mClient.addParam("signatureImage", signatureImage);

            return this;
        }

        public Builder getEstampingPrice(
                long orderID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/getEstampingPrice";

            mClient.addParam("orderID", orderID);

            return this;
        }

        public Builder getExplanationAudio(
                long orderID,
                String language
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/getExplanationAudio";

            mClient.addParam("orderID", orderID);
            mClient.addParam("language", language);

            return this;
        }

        public Builder resendAgreement(
                long orderID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/resendAgreement";

            mClient.addParam("orderID", orderID);

            return this;
        }

        public Builder approve(
                long orderID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/approve";

            mClient.addParam("orderID", orderID);

            return this;
        }

        public Builder reject(
                long orderID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/reject";

            mClient.addParam("orderID", orderID);

            return this;
        }

        public Builder updateSpecialRequest(
                long orderID,
                String specialRequest
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/updateSpecialRequest";

            mClient.addParam("orderID", orderID);
            mClient.addParam("specialRequest", specialRequest);

            return this;
        }

        public Builder apiSubmitManualSignature(
                int orderID,
                File signatureImage
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/apiSubmitManualSignature";

            mClient.addParam("orderID", orderID);
            mClient.addParam("signatureImage", signatureImage);

            return this;
        }

        public Builder getFullDetail(
                long orderID
        ) {

            mMethod = "post";
            withProgressDialog = true;
            mUrl = "/order/fullDetail";

            mClient.addParam("orderID", orderID);

            return this;
        }
    }
}

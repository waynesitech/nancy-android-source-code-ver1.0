package com.lesys.lepro.ai.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.module.authentication.LoginActivity;
import com.lesys.lepro.ai.module.authentication.SignUpActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NicholasTan on 20/04/2016.
 */
public class SplashScreenView extends SplashScreenBaseView implements View.OnClickListener {
    // Game name
    public static final String NAME = "SpaceBlaster";

    private Context mContext;
    //
    private int screenWidth;
    private int screenHeight;
    private int totalPages;
    //
    private MBookView mBookView;
    private ScrollView scrollView;

    //    TextView mStartBtn;
    ImageView mSplashScreen;

    TextView mSignInBtn;
    TextView mSignUpBtn;
    TextView mESignBtn;
    Gson gson = new GsonBuilder().create();

    SplashScreenViewInterface mSplashScreenViewInterface;

    public SplashScreenView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public SplashScreenView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    //
    private void init() {

        totalPages = 2;
        Display display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();
        //
        mBookView = new MBookView(mContext, screenWidth, screenHeight);
        // prepare view to take all screenshoot
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);
        LayoutInflater factory = LayoutInflater.from(mContext);
        View view = factory.inflate(R.layout.splashscreen_view1, null);
        ll.addView(view, screenWidth, screenHeight);
        View view2 = factory.inflate(R.layout.activity_intro_page, null);
        ll.addView(view2, screenWidth, screenHeight);
        // hide scrollview's commponent to have clear bitmap
        scrollView = new ScrollView(mContext);
        scrollView.addView(ll);
        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setHorizontalScrollBarEnabled(false);
        scrollView.setFadingEdgeLength(0);
        addView(scrollView);
        // add true view and some actions
        View view3 = factory.inflate(R.layout.splashscreen_view1, null);
        addView(view3, screenWidth, screenHeight);
        View view4 = factory.inflate(R.layout.activity_intro_page, null);
        addView(view4, screenWidth, screenHeight);

        addView(mBookView);
        mBookView.setView(view3, view4);

        // view 1
//        mStartBtn = (TextView) view3.findViewById(R.id.start_btn);
        mSplashScreen = (ImageView) view3.findViewById(R.id.splashscreen);
//        mStartBtn.setVisibility(View.GONE);

        // view 2
        mSignInBtn = (TextView) view4.findViewById(R.id.sign_in_btn);
        mSignUpBtn = (TextView) view4.findViewById(R.id.sign_up_btn);
        mESignBtn = (TextView) view4.findViewById(R.id.e_sign_btn);
        view4.findViewById(R.id.sign_in_btn).setOnClickListener(this);
        view4.findViewById(R.id.sign_up_btn).setOnClickListener(this);
        view4.findViewById(R.id.e_sign_btn).setOnClickListener(this);
    }

    public void startBtnDisplay() {
//        mStartBtn.setVisibility(View.VISIBLE);
        mSplashScreen.setOnClickListener(SplashScreenView.this);
//        mStartBtn.setOnClickListener(SplashScreenView.this);
    }

    public void next() {
        mBookView.next();
    }

    private Bitmap mBitmap;
    private Bitmap page1, page2;

    public void getPageContent() {
        if (mBitmap == null) {
            setBitmap();
        }
    }

    private void setBitmap() {
        mBitmap = Bitmap.createBitmap(screenWidth, screenHeight * totalPages, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mBitmap);
        scrollView.draw(canvas);

        page1 = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas1 = new Canvas(page1);
        canvas1.drawBitmap(mBitmap, 0, 0, new Paint());
        page2 = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(page2);
        canvas2.drawBitmap(mBitmap, 0, -screenHeight, new Paint());
        //removeAllViews();
        scrollView.setVisibility(View.GONE);
        //
        mBookView.init(page1, page2);
        //
    }

    public void setSplashScreenViewInterface(SplashScreenViewInterface mSplashScreenViewInterface) {
        this.mSplashScreenViewInterface = mSplashScreenViewInterface;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
//            case R.id.start_btn:
            case R.id.splashscreen:
                next();
                break;
            case R.id.sign_in_btn: {
                if (mSplashScreenViewInterface != null)
                    mSplashScreenViewInterface.onClickLogin();
                break;
            }
            case R.id.sign_up_btn: {
                if (mSplashScreenViewInterface != null)
                    mSplashScreenViewInterface.onClickSignUp();
                break;
            }
            case R.id.e_sign_btn: {
                if (mSplashScreenViewInterface != null)
                    mSplashScreenViewInterface.onClickESign();
                break;
            }
        }
    }
}

package com.lesys.lepro.ai.model.generate_new_ta;

import com.lesys.lepro.ai.model.OrderFullDetail;

import java.io.Serializable;

/**
 * Created by Nicholas on 04/06/2017.
 */

public class DirectBankInModel implements Serializable {

    public String nameOfAccountHolder;
    public String bankAccountNumber;
    public String bank;
    public boolean isEmail;
    public boolean isSMS;
    public boolean isWhatsappText;

    public DirectBankInModel() {

    }

    public DirectBankInModel syncServerTA(OrderFullDetail order) {

        nameOfAccountHolder = order.getPaymentAccountName();
        bankAccountNumber = order.getBankAccountNum();
        bank = order.getBank();

        switch (order.getNotificationMethod()) {
            case "isEmail": {
                isEmail = true;
                isSMS = false;
                isWhatsappText = false;
                break;
            }
            case "isSMS": {
                isEmail = false;
                isSMS = true;
                isWhatsappText = false;
                break;
            }
            case "isWhatsapp": {
                isEmail = false;
                isSMS = false;
                isWhatsappText = true;
                break;
            }
        }

        return this;
    }
}

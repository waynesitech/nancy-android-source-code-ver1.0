package com.lesys.lepro.ai;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karumi.headerrecyclerview.HeaderRecyclerViewAdapter;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.TransformationUtil;
import com.squareup.picasso.Picasso;

/**
 * Created by NicholasTan on 28/04/2016.
 */
public abstract class MainFunctionAdapter extends
        HeaderRecyclerViewAdapter<RecyclerView.ViewHolder, User, String, String> {

    private final String TAG = MainFunctionAdapter.class.getName();
    private Context mContext;
    TypedArray nancyMainFunctionIconArray;

    public MainFunctionAdapter(final Context context) {
        this.mContext = context;

        nancyMainFunctionIconArray = context.getResources().obtainTypedArray(R.array.nancy_main_function_icon_array);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_main_function, null);

        return new HeaderViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_function, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loadmore, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        final HeaderViewHolder viewHolder = (HeaderViewHolder) holder;
        final User user = getHeader();
        viewHolder.itemViewControl(user, position);
    }


    @Override
    protected void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CustomViewHolder viewHolder = (CustomViewHolder) holder;
        final String data = getItem(position);
        viewHolder.itemViewControl(data, position);
    }


    @Override
    protected void onBindFooterViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    protected void onHeaderViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onItemViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onFooterViewRecycled(RecyclerView.ViewHolder holder) {
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected View mRootView;
        protected TextView mMsg;

        public HeaderViewHolder(View v) {
            super(v);
            try {
                mRootView = v;
                this.mMsg = (TextView) v.findViewById(R.id.msg);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void itemViewControl(final User mUser, final int position) {
            try {
                mMsg.setText(mContext.getString(R.string.main_function_msg, mUser.getName()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        protected View mRootView;
        protected ImageView mIcon;
        protected TextView mName;

        public CustomViewHolder(View v) {
            super(v);
            try {
                mRootView = v;
                this.mIcon = (ImageView) v.findViewById(R.id.icon);
                this.mName = (TextView) v.findViewById(R.id.name);

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mIcon.getLayoutParams();
//                params.width = (Common.Helpers.getScreenSize((Activity) mContext).x / 2) - Common.Helpers.getDp(mContext, 32);
//                params.width = (Common.Helpers.getScreenSize((Activity) mContext).x / 3) + Common.Helpers.getDp(mContext, 32);
                params.width = (Common.Helpers.getScreenSize((Activity) mContext).x / 3);
                params.height = params.width;
                mIcon.setLayoutParams(params);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void itemViewControl(final String data, final int position) {
            try {
                mName.setText(data);
                mIcon.setImageResource(nancyMainFunctionIconArray.getResourceId(position - 1, -1));
                mRootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClick(position);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public abstract void onItemClick(int position);
}
package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.shared.Common;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GalleryAdapter extends BaseAdapter implements Picasso.Listener {

    private Context mContext;
    private LayoutInflater infalter;
    private ArrayList<CustomGallery> data = new ArrayList<CustomGallery>();


    private boolean isActionMultiplePick;
    private int maximumSelection;
    String[] dataT;
    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options;

    public GalleryAdapter(Context c) {
        infalter = (LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;

        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder_camera) // resource or drawable
                .showImageForEmptyUri(R.drawable.placeholder_camera) // resource or drawable
                .showImageOnFail(R.drawable.placeholder_camera) // resource or drawable
               .build();

//        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        // clearCache();
    }

    public GalleryAdapter(Context c, int maximumSelection) {
        infalter = (LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;

        this.maximumSelection = maximumSelection;
        dataT = new String[maximumSelection];

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        // clearCache();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CustomGallery getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setMultiplePick(boolean isMultiplePick) {
        this.isActionMultiplePick = isMultiplePick;
    }

    public void selectAll(boolean selection) {
        for (int i = 0; i < data.size(); i++) {
            data.get(i).isSelected = selection +"";

        }
        notifyDataSetChanged();
    }

    public boolean isAllSelected() {
        boolean isAllSelected = true;

        for (int i = 0; i < data.size(); i++) {
            if (!data.get(i).isSelected()) {
                isAllSelected = false;
                break;
            }
        }

        return isAllSelected;
    }

    public boolean isAnySelected() {
        boolean isAnySelected = false;

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isSelected()) {
                isAnySelected = true;
                break;
            }
        }

        return isAnySelected;
    }

    public ArrayList<CustomGallery> getSelected() {
        ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isSelected()) {
                dataT.add(data.get(i));
            }
        }

        return dataT;
    }

    public void initSequenceSelection() {
//		ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isSelected() && data.get(i).selectionIndex < dataT.length) {
                dataT[data.get(i).selectionIndex] = data.get(i).sdcardPath;

            }
        }
//        System.out.println("Kelvyndebug:initSequenceSelection->["+dataT[0]+"," + dataT[1]+"," + dataT[2]+"," + dataT[3]+"," + dataT[4] + "]");

    }

    public String[] getSequenceSelection() {
        for(int i = 0; i<dataT.length;i++) {
            if (dataT[i] == null || dataT[i].trim().length() == 0) {
                for(int x =i+1;x<dataT.length;x++){
                    if(dataT[x] != null && dataT[x].trim().length() > 0){
                        dataT[i] = dataT[x];
                        dataT[x] = "";
                        break;
                    }
                }
            }
        }
        return dataT;
    }

    public void addAll(ArrayList<CustomGallery> files) {

        try {
            this.data.clear();
            this.data.addAll(files);
            initSequenceSelection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        notifyDataSetChanged();
    }

    public void changeSelection(View v, int position) {

        if (data.get(position).isSelected()) {
            Log.d("Gallery", "this is image is selected");
            data.get(position).isSelected = false +"";
            if (dataT != null) {
                if (data.get(position).selectionIndex < dataT.length) {
                    dataT[data.get(position).selectionIndex] = "";
                }
            }
//            ((ViewHolder) v.getTag()).imgQueueMultiSelected.setSelected(data
//                    .get(position).isSelected());
            ((ViewHolder) v.getTag()).imgQueueMultiSelected.setVisibility(View.GONE);
        } else {
            Log.d("Gallery", "this is image is not selected");

            if (dataT != null) {
                for (int i = 0; i < dataT.length; i++) {
                    if (dataT[i] == null || dataT[i].trim().length() == 0) {
                        data.get(position).isSelected = true +"";
                        dataT[i] = data.get(position).sdcardPath;
                        data.get(position).selectionIndex = i;
                        ((ViewHolder) v.getTag()).imgQueueMultiSelected.setVisibility(View.VISIBLE);
                        ((ViewHolder) v.getTag()).imgQueueMultiSelectionIndex.setText((i + 1) + "");
                        break;
                    }
                }
            }
        }


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.gallery_item, null);
            holder = new ViewHolder();

            holder.imgQueue = (ImageView) convertView
                    .findViewById(R.id.imgQueue);

            holder.imgQueueMultiSelectionIndex = (TextView) convertView
                    .findViewById(R.id.imgQueueMultiSelectIndex);
            holder.imgQueueMultiSelected = (FrameLayout) convertView
                    .findViewById(R.id.imgQueueMultiSelected);

            Point point = Common.Helpers.getScreenSize((Activity)mContext);

            int imgWidth = (int)((point.x - Common.Helpers.getPx(mContext,4))/3);

            FrameLayout.LayoutParams flLP = new FrameLayout.LayoutParams(imgWidth,imgWidth);
            holder.imgQueue.setLayoutParams(flLP);


            RelativeLayout.LayoutParams rlLP = new RelativeLayout.LayoutParams(imgWidth,imgWidth);
            holder.imgQueueMultiSelected.setLayoutParams(rlLP);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        holder.imgQueue.setTag(position);

        if (data.get(position).isSelected()) {
            holder.imgQueueMultiSelected.setVisibility(View.VISIBLE);
            holder.imgQueueMultiSelectionIndex.setText((data.get(position).selectionIndex+1)+"");
        } else {
            holder.imgQueueMultiSelected.setVisibility(View.GONE);
        }

        try {

//            imageLoader.displayImage("file://" + data.get(position).sdcardPath,
//                    holder.imgQueue, new SimpleImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//                            holder.imgQueue
//                                    .setImageResource(R.drawable.placeholder_camera);
//                            super.onLoadingStarted(imageUri, view);
//                        }
//                    });

            Log.d("Gallery", "data.get(position).sdcardPath->" + data.get(position).sdcardPath);

//            Picasso.with(mContext)
//                    .load("file://" + url2)
//                    .fit()
//                    .centerInside()
////                    .networkPolicy(NetworkPolicy.NO_CACHE)
////                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .error(R.drawable.placeholder_camera)
//                    .into(holder.imgQueue);

            try {

                imageLoader.displayImage("file://" + data.get(position).sdcardPath, holder.imgQueue,options);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (isActionMultiplePick) {

                holder.imgQueueMultiSelected
                        .setSelected(data.get(position).isSelected());

            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
        exception.printStackTrace();
        Log.e("picasso", "exception", exception);
    }



    public class ViewHolder {
        ImageView imgQueue;
        FrameLayout imgQueueMultiSelected;
        TextView imgQueueMultiSelectionIndex;
    }


    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }
}

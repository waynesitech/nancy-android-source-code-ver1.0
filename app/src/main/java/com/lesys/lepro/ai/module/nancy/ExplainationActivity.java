package com.lesys.lepro.ai.module.nancy;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.module.explaination.ExplainationListActivity;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class ExplainationActivity extends BaseActivity {

    private static final String TAG = ExplainationActivity.class.getName();

    TextView mMsg;
    TextView mEnglishBtn;
    TextView mMandarinBtn;

    Order mOrder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_nancy_explaination);

        mOrder = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);

        mMsg = (TextView) findViewById(R.id.msg);
        mEnglishBtn = (TextView) findViewById(R.id.english_btn);
        mMandarinBtn = (TextView) findViewById(R.id.mandarin_btn);

        mEnglishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExplainationActivity.this, ExplainationListActivity.class);
                intent.putExtra(GlobalParam.INTENT_IS_ENGLISH, true);
                intent.putExtra(GlobalParam.INTENT_ORDER_DATA, mOrder);
                startActivity(intent);
            }
        });
        mMandarinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExplainationActivity.this, ExplainationListActivity.class);
                intent.putExtra(GlobalParam.INTENT_IS_ENGLISH, false);
                intent.putExtra(GlobalParam.INTENT_ORDER_DATA, mOrder);
                startActivity(intent);
            }
        });
    }

}

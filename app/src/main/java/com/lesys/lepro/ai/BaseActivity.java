package com.lesys.lepro.ai;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.shared.ClickActionInterface;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.widget.CustomTypefaceSpan;

/**
 * Created by NicholasTan on 26/04/2016.
 */
public class BaseActivity extends AppCompatActivity {

    public static final String TAG = "BaseActivity";
    public BaseApplication getData;

    public AlertDialog mNotificationDialog;
    String messageType = "";
    String messageID;
    TextView mToolbarTitle;
    Toolbar toolbar;
    public User mUser = new User();

    public final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
//                    String newMessage = intent.getExtras().getString(CommonUtilities.EXTRA_MESSAGE);
//                    messageID = intent.getExtras().getString(CommonUtilities.EXTRA_ID);
//                    messageType = intent.getExtras().getString(CommonUtilities.EXTRA_TYPE);
//
//                    mNotificationDialog = new AlertDialog.Builder(context).create();
//                    mNotificationDialog.setTitle(R.string.notification);
//                    mNotificationDialog.setMessage(newMessage);
//                    mNotificationDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            mNotificationDialog.dismiss();
//
//                            System.out.println("messageID:" + messageID);
//                            if (messageType.equals("broadcast")) {
////                                Intent intent = new Intent(DefaultActivity.this, PropertyDetailActivity.class);
////                                intent.putExtra(PropertyDetailActivity.INTENT_PROP_ID, Integer.valueOf(messageID));
////                                startActivity(intent);
//                            }
//                        }
//                    });
//                    if (newMessage != null && !newMessage.equals(""))
//                        mNotificationDialog.show();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_landing));
        super.onCreate(savedInstanceState);
        sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
        sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));
        getData = (BaseApplication) getApplication();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
            setSupportActionBar(toolbar);
            try {
                int labelRes = getPackageManager().getActivityInfo(getComponentName(), 0).labelRes;
                getSupportActionBar().setTitle("");
                setTitle(labelRes);
//                if (layoutResID > 0) {
//                    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/SanFranciscoText-Medium.otf");
//                    SpannableStringBuilder ss = new SpannableStringBuilder(getString(labelRes));
//                    final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(android.R.color.white));
//                    ss.setSpan(fcs, 0, getString(labelRes).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//                    ss.setSpan(new CustomTypefaceSpan("", font), 0, getString(labelRes).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
//
//                    setTitle(ss);
//                }
//                toolbar.setNavigationIcon(R.drawable.ic_back);
//                Common.Helpers.displayRevealAnimation(this, findViewById(R.id.appbar));

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTitle(String title) {
        try {
//            Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/SanFranciscoText-Medium.otf");
//            SpannableStringBuilder ss = new SpannableStringBuilder(title);
//            final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(android.R.color.white));
//            ss.setSpan(fcs, 0, title.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//            ss.setSpan(new CustomTypefaceSpan("", typeface), 0, title.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

            mToolbarTitle.setText(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTitle(int titleRes) {
        try {
//            Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/SanFranciscoText-Medium.otf");
//            SpannableStringBuilder ss = new SpannableStringBuilder(getString(titleRes));
//            final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(android.R.color.white));
//            ss.setSpan(fcs, 0, getString(titleRes).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//            ss.setSpan(new CustomTypefaceSpan("", typeface), 0, getString(titleRes).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

            mToolbarTitle.setText(titleRes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayLoading(final boolean isArrayListEmpty) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isArrayListEmpty) {
                        findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.loading).setVisibility(View.VISIBLE);
                        findViewById(R.id.retry_layout).setVisibility(View.GONE);
                        findViewById(R.id.no_data_msg).setVisibility(View.GONE);
                    }
                }
            });

        } catch (Exception e) {
        }
    }

    public void displayRetryLayout(final boolean isArrayListEmpty, final ClickActionInterface.RetryActionInterface action) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isArrayListEmpty) {
                        findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        findViewById(R.id.retry_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.no_data_msg).setVisibility(View.GONE);
//                        CommonUtil.setRadiusColor(BaseActivity.this, findViewById(R.id.retry_btn), 80, R.color.button_primary, R.color.button_primary_light, R.color.grey);
                        findViewById(R.id.retry_btn).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (action != null)
                                    action.onRetryClickListener();
                            }
                        });
                    }
                }
            });

        } catch (Exception e) {
        }
    }

    public void dismissLoading(final boolean isArrayListEmpty) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.loading_layout).setVisibility(isArrayListEmpty ? View.VISIBLE : View.GONE);
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    findViewById(R.id.retry_layout).setVisibility(View.GONE);
                    findViewById(R.id.no_data_msg).setVisibility(isArrayListEmpty ? View.VISIBLE : View.GONE);
                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Uri uriData = getIntent().getData();
        Log.e(TAG , "uriData = " + uriData);
        if (uriData != null) {
            switch (uriData.getHost()) {
                case "product": {

                    break;
                }
            }
        }
/*        if(mNotificationDialog != null)
            mNotificationDialog.dismiss();*/

//        registerReceiver(mHandleMessageReceiver,
//                new IntentFilter(CommonUtilities.DISPLAY_MESSAGE_ACTION));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(mHandleMessageReceiver);
        } catch (IllegalArgumentException ex) {
        }
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mHandleMessageReceiver);
//            GCMRegistrar.onDestroy(this);
        } catch (IllegalArgumentException ex) {
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Common.Helpers.hideSoftKeyboard(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

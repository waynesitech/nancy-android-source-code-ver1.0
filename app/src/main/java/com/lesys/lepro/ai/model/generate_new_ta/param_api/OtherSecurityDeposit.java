package com.lesys.lepro.ai.model.generate_new_ta.param_api;

import com.lesys.lepro.ai.model.generate_new_ta.MoneyModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nicholas on 25/06/2017.
 */

public class OtherSecurityDeposit implements Serializable {

    private String name;
    private double amount;

    public OtherSecurityDeposit(String name, double amount) {
        setName(name);
        setAmount(amount);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}

package com.lesys.lepro.ai.module.tenancy_agreement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.karumi.headerrecyclerview.HeaderRecyclerViewAdapter;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.module.tenancy_agreement.TADetailActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

/**
 * Created by NicholasTan on 28/04/2016.
 */
public class TenancyAgreementListAdapter extends
        HeaderRecyclerViewAdapter<RecyclerView.ViewHolder, String, Order, String> {

    private final String TAG = "CentreAdapter";
    private Context mContext;
    Transformation transformation = new RoundedTransformationBuilder()
            .borderColor(Color.WHITE)
            .borderWidthDp(2)
            .cornerRadiusDp(180)
            .oval(false)
            .build();

    public TenancyAgreementListAdapter(final Context context) {
        this.mContext = context;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tenancy_agreement_list, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tenancy_agreement_list, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loadmore, null);

        return new CustomViewHolder(view);
    }


    @Override
    protected void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
    }


    @Override
    protected void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CustomViewHolder viewHolder = (CustomViewHolder) holder;
        final Order data = getItem(position);
        viewHolder.itemViewControl(data, position);
    }


    @Override
    protected void onBindFooterViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    protected void onHeaderViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onItemViewRecycled(RecyclerView.ViewHolder holder) {
    }


    @Override
    protected void onFooterViewRecycled(RecyclerView.ViewHolder holder) {
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        protected View mRootView;
        protected ImageView mThumbnail;
        protected TextView mRefNo;
        protected TextView mName;
        protected TextView mTenancyAgreement;
        protected TextView mESign;
        protected TextView mEStamp;

        public CustomViewHolder(View v) {
            super(v);
            try {
                this.mRootView = v;
                this.mThumbnail = (ImageView) v.findViewById(R.id.image);
                this.mName = (TextView) v.findViewById(R.id.name);
                this.mTenancyAgreement = (TextView) v.findViewById(R.id.tenancyAgreement);
                this.mESign = (TextView) v.findViewById(R.id.e_sign);
                this.mEStamp = (TextView) v.findViewById(R.id.e_stamp);
                this.mRefNo = (TextView) v.findViewById(R.id.refNo);
            } catch (Exception e) {

            }
        }

        public void itemViewControl(final Order data, final int position) {
            try {
                Picasso.with(mContext).load(data.getPropertyPicture())
                        .error(R.color.transparent)
                        .placeholder(R.color.transparent)
                        .transform(transformation)
                        .centerCrop()
                        .resize(Common.Helpers.getDp(mContext, 100), Common.Helpers.getDp(mContext, 100))
                        .into(mThumbnail);

                mRefNo.setText(mContext.getString(R.string.ref_no_param, data.getOrder_num()));

                mName.setText(data.getPropertyStreet() + "\n" +
                        data.getPropertyTown() + "\n" +
                        data.getPropertyPostcode() + " " +
                        data.getPropertyState());


                mTenancyAgreement.setText(mContext.getString(R.string.tenancy_agreement_param, data.getTenancyAgreementStatus()));
                mESign.setText(mContext.getString(R.string.e_sign_param, data.geteSignStatus()));
                mEStamp.setText(mContext.getString(R.string.e_stamp_param, data.geteStampStatus()));

                mRootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, TADetailActivity.class);
                        intent.putExtra(GlobalParam.INTENT_ORDER_DATA, data);
                        mContext.startActivity(intent);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
package com.lesys.lepro.ai.model.generate_new_ta;

import java.io.Serializable;

/**
 * Created by Nicholas on 28/05/2017.
 */

public class MonthYearModel implements Serializable {

    public String month;
    public String year;
    public String week;

    public MonthYearModel() {

    }

    public MonthYearModel(String week, String month, String year) {
        this.week = week;
        this.month = month;
        this.year = year;
    }
}

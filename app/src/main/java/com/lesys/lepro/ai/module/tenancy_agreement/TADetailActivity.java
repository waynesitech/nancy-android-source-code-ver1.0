package com.lesys.lepro.ai.module.tenancy_agreement;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.ESignEStampSelectionActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.model.OrderFullDetailResultCover;
import com.lesys.lepro.ai.model.OrderResultCover;
import com.lesys.lepro.ai.model.SMSStatus;
import com.lesys.lepro.ai.model.SMSStatusListResultCover;
import com.lesys.lepro.ai.model.Simple;
import com.lesys.lepro.ai.module.nancy.AutoReminderActivity;
import com.lesys.lepro.ai.module.nancy.ExplainationActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.FileDownloader;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.InternalStorageUtil;
import com.lesys.lepro.ai.shared.ShareStorageUtil;
import com.lesys.lepro.ai.shared.TransformationUtil;
import com.noelchew.permisowrapper.PermisoWrapper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

/**
 * Created by Nicholas on 25/05/2017.
 */

public class TADetailActivity extends BaseActivity implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout mSwipeRefreshLayout;
    ImageView mImage;
    TextView mViewBtn;
    TextView mDownloadBtn;
    TextView mExplanationBtn;
    TextView mResendBtn;
    TextView mReference;
    TextView mProperty;
    TextView mLandlord;
    TextView mTenant;
    TextView mTaStatus;
    TextView mESignStatus;
    TextView mManualSignStatus;
    TableRow mESignLayout;
    TextView mKindlyViewTAHere;
    Button mTARenewBtn;
    Button mESignSendOTPBtn;
    Button mManualSignUploadBtn;
    LinearLayout mESignLandlordTenantLayout;
    TableRow mManualSignLayout;
    TableRow mTenancyAgreementLayout;
    TableRow mEStampLayout;
    TextView mEStampStatus;
    Button mEStampSubmitBtn;

    LinearLayout mUserRoleLesysTeamLayout;
    LinearLayout mUserRoleMemberLayout;
    TextView mEditSpecialConditionBtn;
    TextView mApproveBtn;
    TextView mRejectBtn;

    /**
     * loginWithPassCodeLayout
     */
    LinearLayout mSignStatusLayout;

    Order order;
    InternalStorageUtil mInternalStorageUtil;
    boolean isLoginWithPassCode;
    String orderUserID;
    File manualSignFile;
    private ArrayList<String> docPaths = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ta_detail);

        order = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);
        orderUserID = getIntent().getStringExtra(GlobalParam.INTENT_ORDER_USER_ID);
        isLoginWithPassCode = getIntent().getBooleanExtra(GlobalParam.INTENT_IS_LOGIN_WITH_PASSCODE, false);
        Permiso.getInstance().setActivity(this);
        mUser = mUser.getUser(this);

        if (order == null) {
            finish();
            return;
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mImage = (ImageView) findViewById(R.id.image);
        mViewBtn = (TextView) findViewById(R.id.view_btn);
        mDownloadBtn = (TextView) findViewById(R.id.download_btn);
        mExplanationBtn = (TextView) findViewById(R.id.explaination_btn);
        mResendBtn = (TextView) findViewById(R.id.resend_btn);
        mReference = (TextView) findViewById(R.id.reference);
        mProperty = (TextView) findViewById(R.id.property);
        mLandlord = (TextView) findViewById(R.id.landlord);
        mTenant = (TextView) findViewById(R.id.tenant);
        mTaStatus = (TextView) findViewById(R.id.ta_status);
        mESignStatus = (TextView) findViewById(R.id.e_sign_status);
        mManualSignStatus = (TextView) findViewById(R.id.manual_sign_status);
        mESignLayout = (TableRow) findViewById(R.id.e_sign_layout);
        mKindlyViewTAHere = (TextView) findViewById(R.id.kindly_view_ta_here);
        mTARenewBtn = (Button) findViewById(R.id.ta_renew_btn);
        mESignSendOTPBtn = (Button) findViewById(R.id.e_sign_send_otp_btn);
        mManualSignUploadBtn = (Button) findViewById(R.id.manual_sign_upload_btn);
        mESignLandlordTenantLayout = (LinearLayout) findViewById(R.id.e_sign_landlord_tenant_layout);
        mManualSignLayout = (TableRow) findViewById(R.id.manual_sign_layout);
        mSignStatusLayout = (LinearLayout) findViewById(R.id.sign_status_layout);
        mTenancyAgreementLayout = (TableRow) findViewById(R.id.tenancy_agreement_layout);
        mEStampLayout = (TableRow) findViewById(R.id.e_stamp_layout);
        mEStampStatus = (TextView) findViewById(R.id.e_stamp_status);
        mEStampSubmitBtn = (Button) findViewById(R.id.e_stamp_submit_btn);
        mUserRoleLesysTeamLayout = (LinearLayout) findViewById(R.id.user_role_lesys_team_layout);
        mUserRoleMemberLayout = (LinearLayout) findViewById(R.id.user_role_member_layout);
        mEditSpecialConditionBtn = (TextView) findViewById(R.id.edit_special_condition_btn);
        mApproveBtn = (TextView) findViewById(R.id.approve_btn);
        mRejectBtn = (TextView) findViewById(R.id.reject_btn);

        mUserRoleLesysTeamLayout.setVisibility(View.GONE);
        mUserRoleMemberLayout.setVisibility(View.GONE);

        mInternalStorageUtil = new InternalStorageUtil(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mViewBtn.setOnClickListener(this);
        mDownloadBtn.setOnClickListener(this);
        mResendBtn.setOnClickListener(this);
        mExplanationBtn.setOnClickListener(this);
        mTARenewBtn.setOnClickListener(this);
        mESignSendOTPBtn.setOnClickListener(this);
        mManualSignUploadBtn.setOnClickListener(this);
        mEStampSubmitBtn.setOnClickListener(this);
        mEditSpecialConditionBtn.setOnClickListener(this);
        mApproveBtn.setOnClickListener(this);
        mRejectBtn.setOnClickListener(this);

        initData();
        onRefresh();
        if (isLoginWithPassCode) {
            isLoginWithPassCodeDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    void initData() {
        mSignStatusLayout.setVisibility(View.GONE);
        Picasso.with(this).load(order.getPropertyPicture())
                .error(R.color.transparent)
                .placeholder(R.color.transparent)
                .transform(TransformationUtil.transformationRounded())
                .centerCrop()
                .resize(Common.Helpers.getDp(this, 150), Common.Helpers.getDp(this, 150))
                .into(mImage);
        mReference.setText(order.getOrder_num());
        mProperty.setText(order.getPropertyStreet() + "\n" +
                order.getPropertyTown() + "\n" +
                order.getPropertyPostcode() + " " +
                order.getPropertyState());

        if (order.getLandlords() != null && order.getLandlords().size() != 0) {
            List<String> nameList = new ArrayList<>();
            for (Simple simple : order.getLandlords()) {
                nameList.add(simple.getName());
            }
            String landlords = TextUtils.join("\n", nameList);
            mLandlord.setText(landlords);
        } else {
            mLandlord.setText("-");
        }
        if (order.getTenants() != null && order.getTenants().size() != 0) {
            List<String> nameList = new ArrayList<>();
            for (Simple simple : order.getTenants()) {
                nameList.add(simple.getName());
            }
            String tenants = TextUtils.join("\n", nameList);
            mTenant.setText(tenants);
        } else {
            mTenant.setText("-");
        }
        if (isLoginWithPassCode) {
            mSignStatusLayout.setVisibility(View.VISIBLE);
            mTenancyAgreementLayout.setVisibility(View.GONE);
            mManualSignLayout.setVisibility(View.GONE);
            mESignLayout.setVisibility(View.GONE);
            mDownloadBtn.setVisibility(View.GONE);
            mExplanationBtn.setVisibility(View.GONE);
            mResendBtn.setVisibility(View.GONE);
            mESignLandlordTenantLayout.removeAllViews();
            if (order.getLandlords() != null && order.getLandlords().size() != 0) {
                for (Simple landlord : order.getLandlords()) {
                    generateLandlordTenantView(landlord, true);
                }
            }
            if (order.getTenants() != null && order.getTenants().size() != 0) {
                for (Simple tenant : order.getTenants()) {
                    generateLandlordTenantView(tenant, false);
                }
            }
            return;
        }

//        order.seteSignStatus(GlobalParam.ESignStatus.ALL_SIGNED);//for testing
//        order.seteStampStatus(GlobalParam.EStampStatus.DONE);//for testing
//        order.setTenancyAgreementStatus(GlobalParam.TAStatus.READY);//for testing

        mTaStatus.setText(order.getTenancyAgreementStatus());
        mESignStatus.setText(order.geteSignStatus());
        mManualSignStatus.setText(order.geteSignStatus());
        mEStampStatus.setText(order.geteStampStatus());

        if (mUser.getRole().getName().equals(GlobalParam.UserRole.ROLE_LESYS_TEAM)) {
            mUserRoleLesysTeamLayout.setVisibility(View.VISIBLE);
            mUserRoleMemberLayout.setVisibility(View.GONE);

            mTARenewBtn.setVisibility(View.GONE);
            mESignLayout.setVisibility(View.GONE);
            mESignLandlordTenantLayout.setVisibility(View.GONE);
            mManualSignLayout.setVisibility(View.GONE);
            mEStampLayout.setVisibility(View.GONE);
            mTaStatus.setText(getString(R.string.rendering).toUpperCase());
            mTaStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_yellow));

            mEditSpecialConditionBtn.setVisibility(TextUtils.isEmpty(order.getSpecialRequest()) ? View.GONE : View.VISIBLE);

            try {
                JSONArray jsonArray = new JSONArray(order.getSpecialRequest());
                mEditSpecialConditionBtn.setVisibility(jsonArray.length() == 0 ? View.GONE : View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
                mEditSpecialConditionBtn.setVisibility(View.GONE);
            }
            return;
        }

        mUserRoleLesysTeamLayout.setVisibility(View.GONE);
        mUserRoleMemberLayout.setVisibility(View.VISIBLE);
        switch (order.getTenancyAgreementStatus()) {
            case GlobalParam.TAStatus.RECEIVED: {
                mTARenewBtn.setVisibility(View.GONE);
                mKindlyViewTAHere.setVisibility(View.GONE);
                mViewBtn.setVisibility(View.GONE);
                mDownloadBtn.setVisibility(View.GONE);
                mResendBtn.setVisibility(View.GONE);
                mExplanationBtn.setVisibility(View.GONE);
                mESignLayout.setVisibility(View.GONE);
                mTaStatus.setText(getString(R.string.rendering).toUpperCase());
                mTaStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_yellow));
//                mEStampLayout.setVisibility(View.GONE);
                break;
            }
            case GlobalParam.TAStatus.READY: {
                mTARenewBtn.setVisibility(View.GONE);
                mKindlyViewTAHere.setVisibility(View.VISIBLE);
                mViewBtn.setVisibility(View.VISIBLE);
                mDownloadBtn.setVisibility(View.GONE);
                mResendBtn.setVisibility(View.GONE);
                mExplanationBtn.setVisibility(View.GONE);
                mESignLayout.setVisibility(View.GONE);
                mTaStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_green));

//                mEStampLayout.setVisibility(View.GONE);
                break;
            }
            case GlobalParam.TAStatus.DONE: {
                mTARenewBtn.setVisibility(View.GONE);
                mKindlyViewTAHere.setVisibility(View.GONE);
                mViewBtn.setVisibility(View.VISIBLE);
                mDownloadBtn.setVisibility(View.VISIBLE);
                mResendBtn.setVisibility(View.VISIBLE);
                mExplanationBtn.setVisibility(View.VISIBLE);
                mESignLayout.setVisibility(View.GONE);
                mTaStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_green));

//                mEStampLayout.setVisibility(View.GONE);
                break;
            }
            case GlobalParam.TAStatus.EXPIRED: {
                mTARenewBtn.setVisibility(View.VISIBLE);
                mKindlyViewTAHere.setVisibility(View.GONE);
                mViewBtn.setVisibility(View.VISIBLE);
                mDownloadBtn.setVisibility(View.VISIBLE);
                mResendBtn.setVisibility(View.VISIBLE);
                mExplanationBtn.setVisibility(View.VISIBLE);
                mESignLayout.setVisibility(View.GONE);
                mTaStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_red));

//                mEStampLayout.setVisibility(View.GONE);
                break;
            }
        }


        switch (order.geteSignStatus()) {
            case GlobalParam.ESignStatus.MANUALLY_SIGNED: {
                mESignLayout.setVisibility(View.GONE);
                mESignStatus.setVisibility(View.GONE);
                mESignLandlordTenantLayout.setVisibility(View.GONE);
                mESignSendOTPBtn.setVisibility(View.VISIBLE);
                mESignSendOTPBtn.setText(R.string.send_otp);
                mESignStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_red));

                mManualSignLayout.setVisibility(View.VISIBLE);
                mManualSignStatus.setVisibility(View.VISIBLE);
                mManualSignUploadBtn.setVisibility(View.GONE);
                mManualSignStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_green));
                break;
            }
            case GlobalParam.ESignStatus.PARTIAL_SIGNED: {
                mESignLayout.setVisibility(View.VISIBLE);
                mESignStatus.setVisibility(View.GONE);
                mESignLandlordTenantLayout.setVisibility(View.VISIBLE);
                mESignSendOTPBtn.setVisibility(View.VISIBLE);
                mESignSendOTPBtn.setText(R.string.resend_otp);

                mManualSignLayout.setVisibility(View.GONE);
                break;
            }
            case GlobalParam.ESignStatus.ALL_SIGNED: {
                mESignLayout.setVisibility(View.VISIBLE);
                mESignLandlordTenantLayout.setVisibility(View.VISIBLE);
                mESignStatus.setVisibility(View.VISIBLE);
                mESignSendOTPBtn.setVisibility(View.GONE);
                mESignStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_green));

                mManualSignLayout.setVisibility(View.GONE);
                break;
            }
            case GlobalParam.ESignStatus.EMPTY: {
                if (order.getTenancyAgreementStatus().equals(GlobalParam.TAStatus.DONE)) {
                    mESignLayout.setVisibility(View.VISIBLE);
                    mESignStatus.setVisibility(View.GONE);
                    mESignLandlordTenantLayout.setVisibility(View.GONE);
                    mESignSendOTPBtn.setVisibility(View.VISIBLE);
                    mESignSendOTPBtn.setText(R.string.send_otp);

                    mManualSignLayout.setVisibility(View.VISIBLE);
                    mManualSignStatus.setVisibility(View.GONE);
                    mManualSignUploadBtn.setVisibility(View.VISIBLE);
                } else {
                    mESignLandlordTenantLayout.setVisibility(View.GONE);
                    mESignLayout.setVisibility(View.GONE);
                    mManualSignLayout.setVisibility(View.GONE);
                }
                break;
            }
        }
//        order.seteStampStatus(GlobalParam.EStampStatus.RECEIVED);//for testing
        switch (order.geteStampStatus()) {
            case GlobalParam.EStampStatus.RECEIVED: {
                mEStampLayout.setVisibility(View.VISIBLE);
                mEStampStatus.setVisibility(View.GONE);
                mEStampSubmitBtn.setVisibility(View.VISIBLE);
                break;
            }
            case GlobalParam.EStampStatus.REJECTED:
            case GlobalParam.EStampStatus.DONE:
            case GlobalParam.EStampStatus.PROCESSING: {
                mEStampLayout.setVisibility(View.VISIBLE);
                mEStampStatus.setVisibility(View.VISIBLE);
                mEStampSubmitBtn.setVisibility(View.GONE);
                if (order.geteStampStatus() == GlobalParam.EStampStatus.REJECTED) {
                    mEStampStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_red));
                } else if (order.geteStampStatus() == GlobalParam.EStampStatus.DONE) {
                    mEStampStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_green));
                } else {
                    mEStampStatus.setTextColor(ContextCompat.getColor(this, R.color.ta_status_yellow));
                }
//                mEStampStatus.setText(R.string.done);
                break;
            }
            case GlobalParam.EStampStatus.EMPTY: {
                if (order.geteSignStatus().equals(GlobalParam.ESignStatus.ALL_SIGNED)) {
                    mEStampLayout.setVisibility(View.VISIBLE);
                } else {
                    mEStampLayout.setVisibility(View.GONE);
                }
                break;
            }
        }

        if (mESignLandlordTenantLayout.getVisibility() == View.VISIBLE) {
            mESignLandlordTenantLayout.removeAllViews();
            if (order.getLandlords() != null && order.getLandlords().size() != 0) {
                for (Simple landlord : order.getLandlords()) {
                    generateLandlordTenantView(landlord, true);
                }
            }
            if (order.getTenants() != null && order.getTenants().size() != 0) {
                for (Simple tenant : order.getTenants()) {
                    generateLandlordTenantView(tenant, false);
                }
            }
        }
    }


    void generateLandlordTenantView(Simple simple, boolean isLandlord) {
        View v = LayoutInflater.from(this).inflate(R.layout.item_ta_details_e_sign_landlord_tenant, null);

        TextView mType = (TextView) v.findViewById(R.id.type);
        TextView mName = (TextView) v.findViewById(R.id.name);
        TextView mStatus = (TextView) v.findViewById(R.id.status);

        mType.setText(isLandlord ? R.string.landlord : R.string.tenant);
        mName.setText(simple.getName());
        mStatus.setText(simple.getStatus());

        mESignLandlordTenantLayout.addView(v);
    }

    void getOrderDetail() {
        BaseApi.init(this)
                .orderDetail((int) order.getId())
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        order = data.getResult().getOrder();
                        initData();

                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    void apiSubmitManualSignature(File file) {
        if (file != null && file.exists()) {
            BaseApi.init(this)
                    .apiSubmitManualSignature((int) order.getId(),
                            file)
                    .call(new DataHandler<MetaData>(MetaData.class) {
                        @Override
                        public void onSuccess(MetaData data) {
                            onRefresh();
                        }

                        @Override
                        public void onFailure(Throwable throwable) {

                        }
                    });
        }
    }

    void sendSignInvitation() {
        BaseApi.init(this)
                .sendSignInvitation((int) order.getId())
                .call(new DataHandler<SMSStatusListResultCover>(SMSStatusListResultCover.class) {
                    @Override
                    public void onSuccess(SMSStatusListResultCover data) {

                        if (data != null && data.getResult() != null) {
                            if (data.getResult().getSmsStatus() != null && data.getResult().getSmsStatus().size() != 0) {
                                String msg = "";
                                List<SMSStatus> smsStatusList = data.getResult().getSmsStatus();
                                List<SMSStatus> newSMSStatusList = new ArrayList<>();
                                for (SMSStatus smsStatus : smsStatusList) {
                                    if (smsStatus.getStatus() != 2) {
                                        newSMSStatusList.add(smsStatus);
                                    }
                                }
                                for (int i = 0; i < newSMSStatusList.size(); i++) {
                                    SMSStatus smsStatus = newSMSStatusList.get(i);
                                    String indexMsg;
                                    switch (smsStatus.getStatus()) {
                                        case 1: {
                                            indexMsg = getString(R.string.dialog_item_send_otp_success_status_1,
                                                    smsStatus.getUserRole(),
                                                    smsStatus.getUserName(),
                                                    smsStatus.getSmsMobile());
                                            break;
                                        }
                                        default: {
                                            indexMsg = smsStatus.getMsg();
                                            break;
                                        }
                                    }
                                    if (i == 0) {
                                        msg = indexMsg;
                                    } else if (i == (newSMSStatusList.size() - 1)) {
                                        msg = msg + ",\nand " + indexMsg;
                                    } else {
                                        msg = msg + ",\n" + indexMsg;
                                    }
                                }
                                successSentOTPDialog(msg);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                    }
                });
    }

    void approveTA() {
        BaseApi.init(this)
                .approve((int) order.getId())
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        Toast.makeText(TADetailActivity.this, R.string.toast_approved_successfully, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                    }
                });
    }

    void rejectTA() {
        BaseApi.init(this)
                .reject((int) order.getId())
                .call(new DataHandler<OrderResultCover>(OrderResultCover.class) {
                    @Override
                    public void onSuccess(OrderResultCover data) {
                        Toast.makeText(TADetailActivity.this, R.string.toast_rejected_successfully, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                    }
                });
    }

//    void confirmTADialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this)
//                .setTitle(R.string.alert)
//                .setMessage(R.string.dialog_confirm_ta)
//                .setCancelable(false)
//                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        mConfirmBtn.setEnabled(false);
//                    }
//                })
//                .setNegativeButton(R.string.no, null);
//        AlertDialog dialog = builder.create();
//        dialog.show();
//    }

    void processToESignIfYouWishDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_process_to_esign_if_you_wish)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onRefresh();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void processToESignDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_process_to_esign)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendSignInvitation();
                    }
                })
                .setNegativeButton(R.string.no, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void processToManualSignDialog(final File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_process_to_manual_sign)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        apiSubmitManualSignature(file);
                    }
                })
                .setNegativeButton(R.string.no, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void successSentOTPDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.dialog_send_otp_success, msg))
                .setCancelable(false)
                .setPositiveButton(R.string.ok_thanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onRefresh();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void isLoginWithPassCodeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.dialog_one_time_passcode_will_be_cleared))
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void loginWithPassCodeBeforeSignDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.dialog_e_sign_agreement))
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        downloadAndViewPDFInternalStorage();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void processToEStampDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_process_to_estamp)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(TADetailActivity.this, AutoReminderActivity.class);
                        intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.no, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void confirmApproveTADialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_confirm_approve_this_ta)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        approveTA();
                    }
                })
                .setNegativeButton(R.string.no, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void confirmRejectTADialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_confirm_reject_this_ta)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rejectTA();
                    }
                })
                .setNegativeButton(R.string.no, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void downloadAndViewPDFInternalStorage() {
        if (TextUtils.isEmpty(order.getPreviewDocumentPath())) {
            return;
        }
        PermisoWrapper.getPermissionSaveMediaToStorage(this, new PermisoWrapper.PermissionListener() {
            @Override
            public void onPermissionGranted() {
                final String fileName = order.getPreviewDocumentPath().substring(order.getPreviewDocumentPath().lastIndexOf("/") + 1);

                if (mInternalStorageUtil.getPDFFile(fileName) != null) {
                    Intent intent = new Intent(TADetailActivity.this, ViewPDFActivity.class);
                    intent.putExtra(GlobalParam.INTENT_PDF_PATH,
                            mInternalStorageUtil.getPDFFile(fileName));
                    intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                    intent.putExtra(GlobalParam.INTENT_IS_LOGIN_WITH_PASSCODE, isLoginWithPassCode);
                    if (!TextUtils.isEmpty(orderUserID)) {
                        intent.putExtra(GlobalParam.INTENT_ORDER_USER_ID, orderUserID);
                    }
                    startActivityForResult(intent, isLoginWithPassCode ? GlobalParam.RC_DIGITAL_SIGNATURE : GlobalParam.RC_PAYMENT);
                } else {
                    new FileDownloader(TADetailActivity.this,
                            order.getPreviewDocumentPath(),
                            mInternalStorageUtil.savePDFPath(fileName)) {
                        @Override
                        public void onSuccessDownloaded() {
                            Intent intent = new Intent(TADetailActivity.this, ViewPDFActivity.class);
                            intent.putExtra(GlobalParam.INTENT_PDF_PATH,
                                    mInternalStorageUtil.getPDFFile(fileName));
                            intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                            intent.putExtra(GlobalParam.INTENT_IS_LOGIN_WITH_PASSCODE, isLoginWithPassCode);
                            if (!TextUtils.isEmpty(orderUserID)) {
                                intent.putExtra(GlobalParam.INTENT_ORDER_USER_ID, orderUserID);
                            }
                            startActivityForResult(intent, isLoginWithPassCode ? GlobalParam.RC_DIGITAL_SIGNATURE : GlobalParam.RC_PAYMENT);
                        }
                    }.download();
                }
//                        savePdfFile();
            }

            @Override
            public void onPermissionDenied() {

            }
        });
    }

    public void onPickDoc() {
        String[] pdfs = {".pdf"};
        FilePickerBuilder.getInstance().setMaxCount(1)
//                .setSelectedFiles(docPaths)
                .setActivityTheme(R.style.FilePickerTheme)
                .addFileSupport("PDF", pdfs)
                .enableDocSupport(false)
                .pickFile(this);
    }

    void getFullDetail() {
        BaseApi.init(this)
                .getFullDetail(order.getId())
                .call(new DataHandler<OrderFullDetailResultCover>(OrderFullDetailResultCover.class) {
                    @Override
                    public void onSuccess(OrderFullDetailResultCover data) {
                        Intent intent = new Intent(TADetailActivity.this, GenerateNewTAActivity.class);
                        intent.putExtra(GlobalParam.INTENT_IS_TA_RENEW, true);
                        intent.putExtra(GlobalParam.INTENT_ORDER_FULL_DETAIL_DATA, data.getResult().getOrder());
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Toast.makeText(TADetailActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GlobalParam.RC_PAYMENT: {
                    processToESignIfYouWishDialog();
                    break;
                }
                case GlobalParam.RC_DIGITAL_SIGNATURE: {
                    finish();
                    break;
                }
                case FilePickerConst.REQUEST_CODE_DOC:
                    if (resultCode == Activity.RESULT_OK && data != null) {
                        docPaths.clear();
                        docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));

                        if (docPaths.size() != 0) {
                            manualSignFile = new File(docPaths.get(0));
                            processToManualSignDialog(manualSignFile);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_btn: {
                if (isLoginWithPassCode) {
                    loginWithPassCodeBeforeSignDialog();
                } else {
                    downloadAndViewPDFInternalStorage();
                }

                break;
            }
            case R.id.download_btn: {
                if (TextUtils.isEmpty(order.getPreviewDocumentPath())) {
                    return;
                }
                PermisoWrapper.getPermissionSaveMediaToStorage(this, new PermisoWrapper.PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        final String fileName = order.getPreviewDocumentPath().substring(order.getPreviewDocumentPath().lastIndexOf("/") + 1);

                        if (ShareStorageUtil.getPDFFile(fileName) != null) {
                            Intent intent = new Intent(TADetailActivity.this, ViewPDFActivity.class);
                            intent.putExtra(GlobalParam.INTENT_PDF_PATH,
                                    ShareStorageUtil.getPDFFile(fileName));
                            intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                            startActivityForResult(intent, GlobalParam.RC_PAYMENT);
                        } else {
                            new FileDownloader(TADetailActivity.this,
                                    order.getPreviewDocumentPath(),
                                    ShareStorageUtil.savePDFPath(fileName)) {
                                @Override
                                public void onSuccessDownloaded() {
                                    Intent intent = new Intent(TADetailActivity.this, ViewPDFActivity.class);
                                    intent.putExtra(GlobalParam.INTENT_PDF_PATH,
                                            ShareStorageUtil.getPDFFile(fileName));
                                    intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                                    startActivityForResult(intent, GlobalParam.RC_PAYMENT);
                                }
                            }.download();
                        }
                    }

                    @Override
                    public void onPermissionDenied() {

                    }
                });
                break;
            }
            case R.id.explaination_btn: {
                Intent intent = new Intent(this, ExplainationActivity.class);
                intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                startActivity(intent);
                break;
            }
            case R.id.resend_btn: {
                BaseApi.init(this)
                        .resendAgreement(order.getId())
                        .call(new DataHandler<MetaData>(MetaData.class) {
                            @Override
                            public void onSuccess(MetaData data) {

                            }

                            @Override
                            public void onFailure(Throwable throwable) {

                            }
                        });

                break;
            }
            case R.id.ta_renew_btn: {
                getFullDetail();
                break;
            }
            case R.id.e_sign_send_otp_btn: {
                processToESignDialog();
                break;
            }
            case R.id.manual_sign_upload_btn: {
                PermisoWrapper.getPermissionSaveMediaToStorage(this, new PermisoWrapper.PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        onPickDoc();
                    }

                    @Override
                    public void onPermissionDenied() {

                    }
                });
                break;
            }
            case R.id.e_stamp_submit_btn: {
                processToEStampDialog();
                break;
            }
            case R.id.edit_special_condition_btn: {
                Intent intent = new Intent(this, LesysTeamEditSpecialConditionActivity.class);
                intent.putExtra(GlobalParam.INTENT_ORDER_DATA, order);
                startActivity(intent);
                break;
            }
            case R.id.approve_btn: {
                confirmApproveTADialog();
                break;
            }
            case R.id.reject_btn: {
                confirmRejectTADialog();
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (isLoginWithPassCode && Common.Helpers.isAppIsInBackground(this)) {
                finish();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        getOrderDetail();
    }
}

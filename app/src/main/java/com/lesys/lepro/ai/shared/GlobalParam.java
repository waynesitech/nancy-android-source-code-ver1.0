package com.lesys.lepro.ai.shared;

/**
 * Created by Nicholas on 06/04/2017.
 */

public class GlobalParam {

    /* RequestCode */
    public static final int RC_LOGIN = 1001;
    public static final int RC_REGISTED = 1002;
    public static final int RC_VERIFIED = 1003;
    public static final int RC_ESIGN = 1004;
    public static final int RC_PAYMENT = 1005;
    public static final int RC_DETAIL = 1006;
    public static final int RC_DIGITAL_SIGNATURE = 1007;


    /* Share Preferences */
    public static final String KEY_IS_LOGINED = "KEY_IS_LOGINED";
    public static final String KEY_USER_DATA = "KEY_USER_DATA";
    public static final String KEY_LOCALE = "KEY_LOCALE";
    public static final String KEY_NEW_TA_MODEL = "KEY_NEW_TA_MODEL_V15";

    /* Param */
    public static final String USER_DATA = "USER_DATA";
    public static final String INTENT_IS_ENGLISH = "INTENT_IS_ENGLISH";
    public static final String INTENT_ORDER_DATA = "INTENT_ORDER_DATA";
    public static final String INTENT_PDF_PATH = "INTENT_PDF_PATH";
    public static final String INTENT_IS_LOGIN_WITH_PASSCODE = "INTENT_IS_LOGIN_WITH_PASSCODE";
    public static final String INTENT_ORDER_USER_ID = "INTENT_ORDER_USER_ID";
    public static final String INTENT_TITLE = "INTENT_TITLE";
    public static final String INTENT_UPLOAD_TYPE = "INTENT_UPLOAD_TYPE";
    public static final String INTENT_TA_PDF = "INTENT_TA_PDF";
    public static final String INTENT_IS_TA_EDIT = "INTENT_IS_TA_EDIT";
    public static final String INTENT_IS_TA_RENEW = "INTENT_IS_TA_RENEW";
    public static final String INTENT_ORDER_FULL_DETAIL_DATA = "INTENT_ORDER_FULL_DETAIL_DATA";

    /* ACTION */
    public static final String ACTION_PHONE_VERIFIED = "ACTION_PHONE_VERIFIED";
    public static final String ACTION_BACKGROUND = "ACTION_BACKGROUND";

    /* User Role */
    public static class UserRole {
        public static final String ROLE_MEMBER = "Member";
        public static final String ROLE_LESYS_TEAM = "Lesys Team";
    }

    /* Upload Type */
    public static class TAUploadType {
        public static final String UPLOAD_TYPE_E_SIGN = "E_SIGN";
        public static final String UPLOAD_TYPE_E_STAMP = "E_STAMP";
        public static final String UPLOAD_TYPE_AUTO_REMINDER = "AUTO_REMINDER";
        public static final String UPLOAD_TYPE_NORMAL = "NORMAL";
    }

    /* Status */
    public static class TAStatus {
        public static final String RECEIVED = "Received";
        public static final String READY = "Ready";
        public static final String DONE = "Done";
        public static final String EXPIRED = "Expired";
    }

    public static class ESignStatus {
        public static final String EMPTY = "-";
        public static final String ALL_SIGNED = "ALL_SIGNED";
        public static final String PARTIAL_SIGNED = "PARTIAL_SIGNED";
        public static final String MANUALLY_SIGNED = "MANUALLY_SIGNED";
    }

    public static class EStampStatus {
        public static final String EMPTY = "-";
        public static final String RECEIVED = "Received";
        public static final String PROCESSING = "Processing";
        public static final String REJECTED = "Rejected";
        public static final String DONE = "Done";
    }

}

package com.lesys.lepro.ai.api;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lesys.lepro.ai.model.MetaData;

import org.apache.http.Header;
import org.json.JSONObject;

import java.lang.reflect.Modifier;

public abstract class DataHandler<T> {

    private static Gson gson = new Gson();
    private Class<T> clazz;

    public DataHandler(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void onReady(String json) {

        Log.d("Call back", "data : " + json);
        try {
            GsonBuilder gsonb = new GsonBuilder();
            gsonb.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
            gson = gsonb.create();
            JSONObject jsonObject = new JSONObject(json);
            String result = jsonObject.toString();
            if (TextUtils.isEmpty(result)) {
                onSuccess(null);
            } else {
                T data = gson.fromJson(result, clazz);
                onSuccess(data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            T data = gson.fromJson(json, clazz);
            ex.fillInStackTrace();
            if (data instanceof MetaData) {
                MetaData metaData = (MetaData) data;
                if (metaData.meta.stat.equals("200")) {
                    onSuccess(data);
                } else {
                    onFailure(ex);
                }
            }
        }
    }

    public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        Log.d("Call back fail", "data : " + responseString);
        onFailure(throwable);
    }

    public abstract void onSuccess(T data);

    public abstract void onFailure(Throwable throwable);
}
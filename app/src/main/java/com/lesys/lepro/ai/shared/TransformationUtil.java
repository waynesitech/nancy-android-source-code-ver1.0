package com.lesys.lepro.ai.shared;

import android.graphics.Color;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class TransformationUtil {

    public static Transformation transformationRounded() {
        return new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(180)
                .oval(false)
                .build();
    }
}

package com.lesys.lepro.ai.shared.ipay;

import com.ipay.IpayResultDelegate;

import java.io.Serializable;

/**
 * Created by NicholasTan on 20/07/2016.
 */
public class IPayResultDelegateCover {

    public static class ResultDelegate implements IpayResultDelegate, Serializable {
        static IPayPaymentListener IPayPaymentListener;

        public ResultDelegate(IPayPaymentListener IPayPaymentListener) {
            this.IPayPaymentListener = IPayPaymentListener;
        }

        public void onPaymentSucceeded(String transId, String refNo, String amount, String remarks, String auth) {
            if (IPayPaymentListener != null)
                IPayPaymentListener.onSuccessed();
        }

        public void onPaymentFailed(String transId, String refNo, String amount, String remarks, String err) {
            if (IPayPaymentListener != null)
                IPayPaymentListener.onFailed(err);
        }

        public void onPaymentCanceled(String transId, String refNo, String amount, String remarks, String errDesc) {
            if (IPayPaymentListener != null)
                IPayPaymentListener.onCanceled();
        }

        public void onRequeryResult(String merchantCode, String refNo, String amount, String result) {

        }
    }
}

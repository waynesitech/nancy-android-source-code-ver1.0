package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.app.DatePickerDialog;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.joanzapata.iconify.widget.IconTextView;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.generate_new_ta.DayHourMinModel;
import com.lesys.lepro.ai.model.generate_new_ta.MoneyModel;
import com.lesys.lepro.ai.model.generate_new_ta.MonthYearModel;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.PropertyDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.RentalDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.TermsOfRenewalModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatus3Fragment extends BaseFragment implements View.OnClickListener {

    TextView mPropertyType;
    TextInputEditText mTermOfTenancyMonth;
    TextInputEditText mTermOfTenancyYear;
    LinearLayout mOptionToRenewLayout;
    CheckBox mOptionToRenewCheckBox;
    IconTextView mOptionRenewArrow;
    TextInputEditText mOptionToRenewMonth;
    TextInputEditText mOptionToRenewYear;
    RadioButton mRadio1;
    RadioButton mRadio2;
    RadioButton mRadio3;
    RadioButton mRadio4;
    TextInputEditText mRentalRM;
    TextInputEditText mRentalCent;
    TextInputEditText mAdvanceRentalRM;
    TextInputEditText mAdvanceRentalCent;
    TextInputEditText mSecurityDepositRentRM;
    TextInputEditText mSecurityDepositRentCent;
    TextInputEditText mSecurityDepositUtilitiesRM;
    TextInputEditText mSecurityDepositUtilitiesCent;
    TextInputEditText mRentalFreePeriodWeek;
    TextInputEditText mRentalFreePeriodMonth;
    TextInputEditText mRentalFreePeriodYear;
    AppCompatSpinner mCarParkAmount;
    LinearLayout mCommercialLayout;
    Spinner mNatureOfBusinessSpinner;
    TextInputEditText mOperatingHourFromDay;
    TextInputEditText mOperatingHourFromHour;
    TextInputEditText mOperatingHourFromMin;
    TextInputEditText mOperatingHourTillDay;
    TextInputEditText mOperatingHourTillHour;
    TextInputEditText mOperatingHourTillMin;
    Button mNextBtn;

    TextView mCommencementDate;
    ImageView mCommencementDateIcon;
    RadioButton mRadio5;
    TextInputEditText mRadio5OtherPercent;
    LinearLayout mSecurityDepositOtherLayout;
    LinearLayout mLotNumberLayout;
//    Button mPreviousBtn;

    RadioButton mRadioRentalFreePeriod1;
    RadioButton mRadioRentalFreePeriod2;
    LinearLayout mLayoutRentalFreePeriod1;
    LinearLayout mLayoutRentalFreePeriod2;
    TextView mRentalFreePeriodFrom;
    TextView mRentalFreePeriodTo;

    //    String propertyType;
    public NewTAModel newTAModel;
    GenerateNewTAActivity generateNewTAActivity;

    private Calendar calendar;
    private int year, month, day;
    private int rentalFromYear, rentalFromMonth, rentalFromDay;
    private int rentalToYear, rentalToMonth, rentalToDay;
    String[] totalCarPark;

    boolean isEdit;
    boolean isTARenew;

    public static GenerateNewTaStatus3Fragment newInstance() {
        GenerateNewTaStatus3Fragment f = new GenerateNewTaStatus3Fragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_3, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);

        mPropertyType = (TextView) v.findViewById(R.id.property_type);
        mTermOfTenancyMonth = (TextInputEditText) v.findViewById(R.id.term_of_tenancy_month);
        mTermOfTenancyYear = (TextInputEditText) v.findViewById(R.id.term_of_tenancy_year);
        mOptionToRenewCheckBox = (CheckBox) v.findViewById(R.id.option_to_renew_checkbox);
        mOptionRenewArrow = (IconTextView) v.findViewById(R.id.option_to_renew_arrow);
        mOptionToRenewLayout = (LinearLayout) v.findViewById(R.id.option_to_renew_layout);
        mOptionToRenewMonth = (TextInputEditText) v.findViewById(R.id.option_to_renew_month);
        mOptionToRenewYear = (TextInputEditText) v.findViewById(R.id.option_to_renew_year);
        mRadio1 = (RadioButton) v.findViewById(R.id.radio_1);
        mRadio2 = (RadioButton) v.findViewById(R.id.radio_2);
        mRadio3 = (RadioButton) v.findViewById(R.id.radio_3);
        mRadio4 = (RadioButton) v.findViewById(R.id.radio_4);
        mRentalRM = (TextInputEditText) v.findViewById(R.id.rental_rm);
        mRentalCent = (TextInputEditText) v.findViewById(R.id.rental_cent);
        mAdvanceRentalRM = (TextInputEditText) v.findViewById(R.id.advance_rental_rm);
        mAdvanceRentalCent = (TextInputEditText) v.findViewById(R.id.advance_rental_cent);
        mSecurityDepositRentRM = (TextInputEditText) v.findViewById(R.id.security_deposit_rent_rm);
        mSecurityDepositRentCent = (TextInputEditText) v.findViewById(R.id.security_deposit_rent_cent);
        mSecurityDepositUtilitiesRM = (TextInputEditText) v.findViewById(R.id.security_deposit_utilities_rm);
        mSecurityDepositUtilitiesCent = (TextInputEditText) v.findViewById(R.id.security_deposit_utilities_cent);
        mRentalFreePeriodMonth = (TextInputEditText) v.findViewById(R.id.rental_free_period_month);
        mRentalFreePeriodYear = (TextInputEditText) v.findViewById(R.id.rental_free_period_year);
        mRentalFreePeriodWeek = (TextInputEditText) v.findViewById(R.id.rental_free_period_week);
        mCarParkAmount = (AppCompatSpinner) v.findViewById(R.id.car_park_amount);
        mCommercialLayout = (LinearLayout) v.findViewById(R.id.commercial_view_layout);
        mNatureOfBusinessSpinner = (Spinner) v.findViewById(R.id.nature_of_business_spinner);
        mOperatingHourFromDay = (TextInputEditText) v.findViewById(R.id.from_day);
        mOperatingHourFromHour = (TextInputEditText) v.findViewById(R.id.from_hour);
        mOperatingHourFromMin = (TextInputEditText) v.findViewById(R.id.from_min);
        mOperatingHourTillDay = (TextInputEditText) v.findViewById(R.id.till_day);
        mOperatingHourTillHour = (TextInputEditText) v.findViewById(R.id.till_hour);
        mOperatingHourTillMin = (TextInputEditText) v.findViewById(R.id.till_min);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);
        mCommencementDate = (TextView) v.findViewById(R.id.commencement_date);
        mCommencementDateIcon = (ImageView) v.findViewById(R.id.commencement_date_icon);
        mRadio5 = (RadioButton) v.findViewById(R.id.radio_5);
        mRadio5OtherPercent = (TextInputEditText) v.findViewById(R.id.radio_5_other_percent);
        mSecurityDepositOtherLayout = (LinearLayout) v.findViewById(R.id.security_deposit_other_layout);
        mLotNumberLayout = (LinearLayout) v.findViewById(R.id.lot_number_layout);
        mRadioRentalFreePeriod1 = (RadioButton) v.findViewById(R.id.radio_rental_free_period_1);
        mRadioRentalFreePeriod2 = (RadioButton) v.findViewById(R.id.radio_rental_free_period_2);
        mLayoutRentalFreePeriod1 = (LinearLayout) v.findViewById(R.id.layout_rental_free_period_1);
        mLayoutRentalFreePeriod2 = (LinearLayout) v.findViewById(R.id.layout_rental_free_period_2);
        mRentalFreePeriodFrom = (TextView) v.findViewById(R.id.rental_free_period_from);
        mRentalFreePeriodTo = (TextView) v.findViewById(R.id.rental_free_period_to);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);

        mRadio5OtherPercent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);


        totalCarPark = getResources().getStringArray(R.array.total_car_park);
        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mOptionToRenewCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mOptionToRenewLayout.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                mOptionRenewArrow.setText(isChecked ? "{mdi-chevron-up}" : "{mdi-chevron-down}");
            }
        });
        mRadio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadio2.setChecked(false);
                    mRadio3.setChecked(false);
                    mRadio4.setChecked(false);
                    mRadio5.setChecked(false);
                }
            }
        });
        mRadio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadio1.setChecked(false);
                    mRadio3.setChecked(false);
                    mRadio4.setChecked(false);
                    mRadio5.setChecked(false);
                }
            }
        });
        mRadio3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadio1.setChecked(false);
                    mRadio2.setChecked(false);
                    mRadio4.setChecked(false);
                    mRadio5.setChecked(false);
                }
            }
        });
        mRadio4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadio1.setChecked(false);
                    mRadio2.setChecked(false);
                    mRadio3.setChecked(false);
                    mRadio5.setChecked(false);
                }
            }
        });
        mRadio5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadio1.setChecked(false);
                    mRadio2.setChecked(false);
                    mRadio3.setChecked(false);
                    mRadio4.setChecked(false);
                }
            }
        });
        mRadioRentalFreePeriod1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadioRentalFreePeriod2.setChecked(false);
                    mLayoutRentalFreePeriod1.setVisibility(View.VISIBLE);
                    mLayoutRentalFreePeriod2.setVisibility(View.GONE);
                }
            }
        });
        mRadioRentalFreePeriod2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadioRentalFreePeriod1.setChecked(false);
                    mLayoutRentalFreePeriod1.setVisibility(View.GONE);
                    mLayoutRentalFreePeriod2.setVisibility(View.VISIBLE);
                }
            }
        });

        mCarParkAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int lotNumberSize = 0;
                try {
                    lotNumberSize = Integer.valueOf(mCarParkAmount.getSelectedItem().toString());
                    mLotNumberLayout.removeAllViews();
                    if (newTAModel != null &&
                            newTAModel.rentalDetail.lotNumberList != null &&
                            newTAModel.rentalDetail.lotNumberList.size() != 0 &&
                            lotNumberSize <= newTAModel.rentalDetail.lotNumberList.size()) {
                        for (int i = 0; i < lotNumberSize; i++) {
                            generateLotNumberView(newTAModel.rentalDetail.lotNumberList.get(i));
                        }
                        return;
                    } else if (newTAModel != null &&
                            newTAModel.rentalDetail.lotNumberList != null &&
                            newTAModel.rentalDetail.lotNumberList.size() != 0 &&
                            lotNumberSize > newTAModel.rentalDetail.lotNumberList.size()) {

                        for (int i = 0; i < newTAModel.rentalDetail.lotNumberList.size(); i++) {
                            generateLotNumberView(newTAModel.rentalDetail.lotNumberList.get(i));
                        }
                        int leftSize = lotNumberSize - newTAModel.rentalDetail.lotNumberList.size();
                        for (int i = 0; i < leftSize; i++) {
                            generateLotNumberView(null);
                        }
                        return;
                    }
                } catch (Exception e) {
                }
                for (int i = 0; i < lotNumberSize; i++) {
                    generateLotNumberView(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        mCarParkAmount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                int lotNumberSize = 0;
//                try {
//                    lotNumberSize = Integer.valueOf(s.toString());
//                    if (lotNumberSize > 10) {
//                        lotNumberSize = 10;
//                        mCarParkAmount.setText("" + lotNumberSize);
//                        return;
//                    } else if (lotNumberSize < 0) {
//                        lotNumberSize = 0;
//                        mCarParkAmount.setText("" + lotNumberSize);
//                        return;
//                    }
//
//                    mLotNumberLayout.removeAllViews();
//                    if (newTAModel != null &&
//                            newTAModel.rentalDetail.lotNumberList != null &&
//                            newTAModel.rentalDetail.lotNumberList.size() != 0 &&
//                            lotNumberSize <= newTAModel.rentalDetail.lotNumberList.size()) {
//                        for (int i = 0; i < lotNumberSize; i++) {
//                            generateLotNumberView(newTAModel.rentalDetail.lotNumberList.get(i));
//                        }
//                        return;
//                    } else if (newTAModel != null &&
//                            newTAModel.rentalDetail.lotNumberList != null &&
//                            newTAModel.rentalDetail.lotNumberList.size() != 0 &&
//                            lotNumberSize > newTAModel.rentalDetail.lotNumberList.size()) {
//
//                        for (int i = 0; i < newTAModel.rentalDetail.lotNumberList.size(); i++) {
//                            generateLotNumberView(newTAModel.rentalDetail.lotNumberList.get(i));
//                        }
//                        int leftSize = lotNumberSize - newTAModel.rentalDetail.lotNumberList.size();
//                        for (int i = 0; i < leftSize; i++) {
//                            generateLotNumberView(null);
//                        }
//                        return;
//                    }
//                } catch (Exception e) {
//                }
//                for (int i = 0; i < lotNumberSize; i++) {
//                    generateLotNumberView(null);
//                }
//            }
//        });

        mRadioRentalFreePeriod1.setChecked(true);
        mNextBtn.setOnClickListener(this);
        mCommencementDate.setOnClickListener(this);
        mCommencementDateIcon.setOnClickListener(this);
        mRentalFreePeriodFrom.setOnClickListener(this);
        mRentalFreePeriodTo.setOnClickListener(this);
//        mPreviousBtn.setOnClickListener(this);

        loadLocalData();
        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    void initDate() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        rentalFromYear = calendar.get(Calendar.YEAR);
        rentalFromMonth = calendar.get(Calendar.MONTH);
        rentalFromDay = calendar.get(Calendar.DAY_OF_MONTH);

        rentalToYear = calendar.get(Calendar.YEAR);
        rentalToMonth = calendar.get(Calendar.MONTH);
        rentalToDay = calendar.get(Calendar.DAY_OF_MONTH);

        mCommencementDate.setText(new StringBuilder().append(day).append("/")
                .append(month + 1).append("/").append(year));
    }

    public void loadLocalData() {
        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        if (newTAModel != null) {
            mPropertyType.setText(!newTAModel.isCommercial ? R.string.residential : R.string.commercial);
            mRadio1.setChecked(true);
            mOptionToRenewLayout.setVisibility(View.GONE);
            mCommercialLayout.setVisibility(!newTAModel.isCommercial ? View.GONE : View.VISIBLE);
            if (newTAModel.rentalDetail != null) {
                RentalDetailModel rentalDetailModel = newTAModel.rentalDetail;
                if (!TextUtils.isEmpty(rentalDetailModel.commencementDate)) {
                    Date date;
                    if (isEdit || isTARenew) {
                        date = Common.Helpers.getDateFromStr(rentalDetailModel.commencementDate, "yyyy-MM-dd");
                    } else {
                        date = Common.Helpers.getDateFromStr(rentalDetailModel.commencementDate, null);
                    }
                    year = date.getYear() + 1900;
                    month = date.getMonth();
                    day = date.getDate();
                    mCommencementDate.setText(new StringBuilder().append(day).append("/")
                            .append(month + 1).append("/").append(year));
                }
                if (rentalDetailModel.termOfTheTenancy != null) {
                    mTermOfTenancyMonth.setText(rentalDetailModel.termOfTheTenancy.month);
                    mTermOfTenancyYear.setText(rentalDetailModel.termOfTheTenancy.year);
                }
                mOptionToRenewCheckBox.setChecked(rentalDetailModel.isOptionToRenew);
                if (rentalDetailModel.optionToRenew != null) {
                    mOptionToRenewMonth.setText(rentalDetailModel.optionToRenew.month);
                    mOptionToRenewYear.setText(rentalDetailModel.optionToRenew.year);
                }
                if (rentalDetailModel.termsOfRenewal != null) {
                    mRadio1.setChecked(rentalDetailModel.termsOfRenewal.isMarkerPrevailingRate);
                    mRadio2.setChecked(rentalDetailModel.termsOfRenewal.isIncreaseOf10Percent);
                    mRadio3.setChecked(rentalDetailModel.termsOfRenewal.isIncreaseOf15Percent);
                    mRadio4.setChecked(rentalDetailModel.termsOfRenewal.isAtMutuallyAgreeRate);
                    mRadio5.setChecked(rentalDetailModel.termsOfRenewal.isOther);
                    mRadio5OtherPercent.setText(rentalDetailModel.termsOfRenewal.totalOtherPercentage);
                }
                if (rentalDetailModel.rental != null) {
                    mRentalRM.setText(rentalDetailModel.rental.ringgitMalaysia);
                    mRentalCent.setText(rentalDetailModel.rental.cent);
                }
                if (rentalDetailModel.advanceRental != null) {
                    mAdvanceRentalRM.setText(rentalDetailModel.advanceRental.ringgitMalaysia);
                    mAdvanceRentalCent.setText(rentalDetailModel.advanceRental.cent);
                }
                if (rentalDetailModel.securityDepositRent != null) {
                    mSecurityDepositRentRM.setText(rentalDetailModel.securityDepositRent.ringgitMalaysia);
                    mSecurityDepositRentCent.setText(rentalDetailModel.securityDepositRent.cent);
                }
                if (rentalDetailModel.securityDepositUtilities != null) {
                    mSecurityDepositUtilitiesRM.setText(rentalDetailModel.securityDepositUtilities.ringgitMalaysia);
                    mSecurityDepositUtilitiesCent.setText(rentalDetailModel.securityDepositUtilities.cent);
                }

                mSecurityDepositOtherLayout.removeAllViews();
                if (rentalDetailModel.securityDepositOtherList != null && rentalDetailModel.securityDepositOtherList.size() != 0) {
                    for (int i = 0; i < rentalDetailModel.securityDepositOtherList.size(); i++) {
                        generateSecurityDepositOtherView(i, rentalDetailModel.securityDepositOtherList.get(i));
                    }
                } else {
                    generateSecurityDepositOtherView(0, null);
                }

                if (TextUtils.isEmpty(newTAModel.rentalDetail.rentalFreePeriodFromDate) && TextUtils.isEmpty(newTAModel.rentalDetail.rentalFreePeriodToDate)) {
                    rentalDetailModel.isRentalFreePeriodSpecialDate = false;
                } else {
                    rentalDetailModel.isRentalFreePeriodSpecialDate = true;
                }

                mRadioRentalFreePeriod1.setChecked(!rentalDetailModel.isRentalFreePeriodSpecialDate);
                mRadioRentalFreePeriod2.setChecked(rentalDetailModel.isRentalFreePeriodSpecialDate);
                mLayoutRentalFreePeriod1.setVisibility(rentalDetailModel.isRentalFreePeriodSpecialDate ? View.GONE : View.VISIBLE);
                mLayoutRentalFreePeriod2.setVisibility(rentalDetailModel.isRentalFreePeriodSpecialDate ? View.VISIBLE : View.GONE);
                if (!TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodFromDate)) {
                    Date date;
                    if (isEdit || isTARenew) {
                        date = Common.Helpers.getDateFromStr(rentalDetailModel.rentalFreePeriodFromDate, "yyyy-MM-dd");
                    } else {
                        date = Common.Helpers.getDateFromStr(rentalDetailModel.rentalFreePeriodFromDate, null);
                    }
                    rentalFromYear = date.getYear() + 1900;
                    rentalFromMonth = date.getMonth();
                    rentalFromDay = date.getDate();
                    mRentalFreePeriodFrom.setText(new StringBuilder().append(rentalFromDay).append("/")
                            .append(rentalFromMonth + 1).append("/").append(rentalFromYear));
                } else {
                    calendar = Calendar.getInstance();
                    rentalFromYear = calendar.get(Calendar.YEAR);
                    rentalFromMonth = calendar.get(Calendar.MONTH);
                    rentalFromDay = calendar.get(Calendar.DAY_OF_MONTH);
                }
                if (!TextUtils.isEmpty(rentalDetailModel.rentalFreePeriodToDate)) {
                    Date date;
                    if (isEdit || isTARenew) {
                        date = Common.Helpers.getDateFromStr(rentalDetailModel.rentalFreePeriodToDate, "yyyy-MM-dd");
                    } else {
                        date = Common.Helpers.getDateFromStr(rentalDetailModel.rentalFreePeriodToDate, null);
                    }
                    rentalToYear = date.getYear() + 1900;
                    rentalToMonth = date.getMonth();
                    rentalToDay = date.getDate();
                    mRentalFreePeriodTo.setText(new StringBuilder().append(rentalToDay).append("/")
                            .append(rentalToMonth + 1).append("/").append(rentalToYear));
                } else {
                    calendar = Calendar.getInstance();
                    rentalFromYear = calendar.get(Calendar.YEAR);
                    rentalFromMonth = calendar.get(Calendar.MONTH);
                    rentalFromDay = calendar.get(Calendar.DAY_OF_MONTH);
                }
                if (rentalDetailModel.rentalFreePeriod != null) {
                    mRentalFreePeriodMonth.setText(rentalDetailModel.rentalFreePeriod.month);
                    mRentalFreePeriodYear.setText(rentalDetailModel.rentalFreePeriod.year);
                    mRentalFreePeriodWeek.setText(rentalDetailModel.rentalFreePeriod.week);
                }

                try {
                    for (int i = 0; i < totalCarPark.length; i++) {
                        if (rentalDetailModel.carParkAmount.equals(totalCarPark[i])) {
                            mCarParkAmount.setSelection(i);
                            break;
                        }
                    }
                } catch (Exception e) {

                }

//                mCarParkAmount.setText(rentalDetailModel.carParkAmount);

                if (rentalDetailModel.operationHourFrom != null) {
                    mOperatingHourFromDay.setText(rentalDetailModel.operationHourFrom.day);
                    mOperatingHourFromHour.setText(rentalDetailModel.operationHourFrom.hour);
                    mOperatingHourFromMin.setText(rentalDetailModel.operationHourFrom.min);
                }
                if (rentalDetailModel.operationHourTo != null) {
                    mOperatingHourTillDay.setText(rentalDetailModel.operationHourTo.day);
                    mOperatingHourTillHour.setText(rentalDetailModel.operationHourTo.hour);
                    mOperatingHourTillMin.setText(rentalDetailModel.operationHourTo.min);
                }
            } else {
                initDate();
                generateSecurityDepositOtherView(0, null);
            }
        } else {
            initDate();
            generateSecurityDepositOtherView(0, null);
        }
    }

    void generateSecurityDepositOtherView(final int position, MoneyModel moneyModel) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_security_deposit_other, null);

        TextInputEditText mName = (TextInputEditText) v.findViewById(R.id.security_deposit_other_name);
        TextInputEditText mRiggit = (TextInputEditText) v.findViewById(R.id.security_deposit_other_rm);
        TextInputEditText mCent = (TextInputEditText) v.findViewById(R.id.security_deposit_other_cent);
        ImageView mAddOnBtn = (ImageView) v.findViewById(R.id.add_on_btn);

        mName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        if (moneyModel != null) {
            mName.setText(moneyModel.name);
            mRiggit.setText(moneyModel.ringgitMalaysia);
            mCent.setText(moneyModel.cent);
        }
        mAddOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSecurityDepositOtherLayout.getChildCount() >= 5) {
                    Toast.makeText(getActivity(), R.string.toast_max_security_deposit, Toast.LENGTH_LONG).show();
                    return;
                }
                generateSecurityDepositOtherView((position + 1), null);
            }
        });
        mSecurityDepositOtherLayout.addView(v, position);
    }

    void generateLotNumberView(String s) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_lot_number, null);

        TextInputEditText mLotNumber = (TextInputEditText) v.findViewById(R.id.lot_number);
        mLotNumber.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        if (!TextUtils.isEmpty(s)) {
            mLotNumber.setText(s);
        }
        mLotNumberLayout.addView(v);
    }

    void updateNewTAModel() {
        newTAModel.rentalDetail = new RentalDetailModel();

        newTAModel.rentalDetail.commencementDate = mCommencementDate.getText().toString();

        newTAModel.rentalDetail.termOfTheTenancy = new MonthYearModel();
        newTAModel.rentalDetail.termOfTheTenancy.month = mTermOfTenancyMonth.getText().toString();
        newTAModel.rentalDetail.termOfTheTenancy.year = mTermOfTenancyYear.getText().toString();
        newTAModel.rentalDetail.isOptionToRenew = mOptionToRenewCheckBox.isChecked();

        newTAModel.rentalDetail.optionToRenew = new MonthYearModel();
        newTAModel.rentalDetail.optionToRenew.month = mOptionToRenewMonth.getText().toString();
        newTAModel.rentalDetail.optionToRenew.year = mOptionToRenewYear.getText().toString();

        newTAModel.rentalDetail.termsOfRenewal = new TermsOfRenewalModel();
        newTAModel.rentalDetail.termsOfRenewal.isMarkerPrevailingRate = mRadio1.isChecked();
        newTAModel.rentalDetail.termsOfRenewal.isIncreaseOf10Percent = mRadio2.isChecked();
        newTAModel.rentalDetail.termsOfRenewal.isIncreaseOf15Percent = mRadio3.isChecked();
        newTAModel.rentalDetail.termsOfRenewal.isAtMutuallyAgreeRate = mRadio4.isChecked();
        newTAModel.rentalDetail.termsOfRenewal.isOther = mRadio5.isChecked();
        newTAModel.rentalDetail.termsOfRenewal.totalOtherPercentage = mRadio5OtherPercent.getText().toString();

        newTAModel.rentalDetail.rental = new MoneyModel();
        newTAModel.rentalDetail.rental.ringgitMalaysia = mRentalRM.getText().toString();
        newTAModel.rentalDetail.rental.cent = mRentalCent.getText().toString();

        newTAModel.rentalDetail.advanceRental = new MoneyModel();
        newTAModel.rentalDetail.advanceRental.ringgitMalaysia = mAdvanceRentalRM.getText().toString();
        newTAModel.rentalDetail.advanceRental.cent = mAdvanceRentalCent.getText().toString();

        newTAModel.rentalDetail.securityDepositRent = new MoneyModel();
        newTAModel.rentalDetail.securityDepositRent.ringgitMalaysia = mSecurityDepositRentRM.getText().toString();
        newTAModel.rentalDetail.securityDepositRent.cent = mSecurityDepositRentCent.getText().toString();

        newTAModel.rentalDetail.securityDepositUtilities = new MoneyModel();
        newTAModel.rentalDetail.securityDepositUtilities.ringgitMalaysia = mSecurityDepositUtilitiesRM.getText().toString();
        newTAModel.rentalDetail.securityDepositUtilities.cent = mSecurityDepositUtilitiesCent.getText().toString();

        newTAModel.rentalDetail.securityDepositOtherList.clear();
        for (int i = 0; i < mSecurityDepositOtherLayout.getChildCount(); i++) {
            View v = mSecurityDepositOtherLayout.getChildAt(i);
            TextInputEditText mName = (TextInputEditText) v.findViewById(R.id.security_deposit_other_name);
            TextInputEditText mRiggit = (TextInputEditText) v.findViewById(R.id.security_deposit_other_rm);
            TextInputEditText mCent = (TextInputEditText) v.findViewById(R.id.security_deposit_other_cent);

            if (!TextUtils.isEmpty(mName.getText().toString())) {
                MoneyModel moneyModel = new MoneyModel();
                moneyModel.name = mName.getText().toString();
                moneyModel.ringgitMalaysia = mRiggit.getText().toString();
                moneyModel.cent = mCent.getText().toString();
                newTAModel.rentalDetail.securityDepositOtherList.add(moneyModel);
            }
        }

        newTAModel.rentalDetail.isRentalFreePeriodSpecialDate = mRadioRentalFreePeriod2.isChecked();
        if (mRadioRentalFreePeriod1.isChecked()) {
            newTAModel.rentalDetail.rentalFreePeriod = new MonthYearModel();
            newTAModel.rentalDetail.rentalFreePeriod.year = mRentalFreePeriodYear.getText().toString();
            newTAModel.rentalDetail.rentalFreePeriod.month = mRentalFreePeriodMonth.getText().toString();
            newTAModel.rentalDetail.rentalFreePeriod.week = mRentalFreePeriodWeek.getText().toString();
        } else {
            newTAModel.rentalDetail.rentalFreePeriodFromDate = mRentalFreePeriodFrom.getText().toString();
            newTAModel.rentalDetail.rentalFreePeriodToDate = mRentalFreePeriodTo.getText().toString();
        }

        newTAModel.rentalDetail.carParkAmount = mCarParkAmount.getSelectedItem().toString();

        newTAModel.rentalDetail.lotNumberList.clear();
        for (int i = 0; i < mLotNumberLayout.getChildCount(); i++) {
            View v = mLotNumberLayout.getChildAt(i);
            TextInputEditText mLotNumber = (TextInputEditText) v.findViewById(R.id.lot_number);

            if (!TextUtils.isEmpty(mLotNumber.getText().toString()))
                newTAModel.rentalDetail.lotNumberList.add(mLotNumber.getText().toString());
        }

        newTAModel.rentalDetail.operationHourFrom = new DayHourMinModel();
        newTAModel.rentalDetail.operationHourFrom.day = mOperatingHourFromDay.getText().toString();
        newTAModel.rentalDetail.operationHourFrom.hour = mOperatingHourFromHour.getText().toString();
        newTAModel.rentalDetail.operationHourFrom.min = mOperatingHourFromMin.getText().toString();

        newTAModel.rentalDetail.operationHourTo = new DayHourMinModel();
        newTAModel.rentalDetail.operationHourTo.day = mOperatingHourTillDay.getText().toString();
        newTAModel.rentalDetail.operationHourTo.hour = mOperatingHourTillHour.getText().toString();
        newTAModel.rentalDetail.operationHourTo.min = mOperatingHourTillMin.getText().toString();

        newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[4] : generateNewTAActivity.mResidentialSlideFragmentTag[4];

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.commencement_date:
            case R.id.commencement_date_icon: {
                new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                GenerateNewTaStatus3Fragment.this.year = year;
                                GenerateNewTaStatus3Fragment.this.month = month;
                                GenerateNewTaStatus3Fragment.this.day = dayOfMonth;
                                mCommencementDate.setText(new StringBuilder().append(dayOfMonth).append("/")
                                        .append(month + 1).append("/").append(year));
                            }
                        }, year, month, day).show();
                break;
            }
            case R.id.rental_free_period_from: {
                new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                GenerateNewTaStatus3Fragment.this.rentalFromYear = year;
                                GenerateNewTaStatus3Fragment.this.rentalFromMonth = month;
                                GenerateNewTaStatus3Fragment.this.rentalFromDay = dayOfMonth;
                                mRentalFreePeriodFrom.setText(new StringBuilder().append(dayOfMonth).append("/")
                                        .append(month + 1).append("/").append(year));
                            }
                        }, rentalFromYear, rentalFromMonth, rentalFromDay).show();
                break;
            }
            case R.id.rental_free_period_to: {
                new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                GenerateNewTaStatus3Fragment.this.rentalToYear = year;
                                GenerateNewTaStatus3Fragment.this.rentalToMonth = month;
                                GenerateNewTaStatus3Fragment.this.rentalToDay = dayOfMonth;
                                mRentalFreePeriodTo.setText(new StringBuilder().append(dayOfMonth).append("/")
                                        .append(month + 1).append("/").append(year));
                            }
                        }, rentalToYear, rentalToMonth, rentalToDay).show();
                break;
            }
            case R.id.next_btn: {
                if (TextUtils.isEmpty(mCommencementDate.getText().toString()) ||
                        TextUtils.isEmpty(mTermOfTenancyMonth.getText().toString()) ||
                        TextUtils.isEmpty(mTermOfTenancyYear.getText().toString()) ||
                        TextUtils.isEmpty(mRentalRM.getText().toString()) ||
                        TextUtils.isEmpty(mRentalCent.getText().toString()) ||
                        TextUtils.isEmpty(mAdvanceRentalRM.getText().toString()) ||
                        TextUtils.isEmpty(mAdvanceRentalCent.getText().toString()) ||
                        TextUtils.isEmpty(mSecurityDepositRentRM.getText().toString()) ||
                        TextUtils.isEmpty(mSecurityDepositRentCent.getText().toString()) ||
                        TextUtils.isEmpty(mSecurityDepositUtilitiesRM.getText().toString()) ||
                        TextUtils.isEmpty(mSecurityDepositUtilitiesCent.getText().toString()) ||
                        (mRadioRentalFreePeriod2.isChecked() && TextUtils.isEmpty(mRentalFreePeriodFrom.getText().toString())) ||
                        (mRadioRentalFreePeriod2.isChecked() && TextUtils.isEmpty(mRentalFreePeriodTo.getText().toString()))
                        ) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    long yearToMonth = Long.valueOf(mTermOfTenancyYear.getText().toString()) * 12;
                    long month = Long.valueOf(mTermOfTenancyMonth.getText().toString());
                    if ((yearToMonth + month) > 36) {
                        Toast.makeText(getActivity(), R.string.toast_term_of_tenancy_cannot_more_than_3_year, Toast.LENGTH_LONG).show();
                        return;
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Term Of Tenancy month or year have some error.", Toast.LENGTH_LONG).show();
                    return;
                }

                boolean isOtherLayoutPassValidation = true;
                for (int i = 0; i < mSecurityDepositOtherLayout.getChildCount(); i++) {
                    View view = mSecurityDepositOtherLayout.getChildAt(i);
                    TextInputEditText mName = (TextInputEditText) view.findViewById(R.id.security_deposit_other_name);
                    TextInputEditText mRiggit = (TextInputEditText) view.findViewById(R.id.security_deposit_other_rm);
                    TextInputEditText mCent = (TextInputEditText) view.findViewById(R.id.security_deposit_other_cent);

                    if (!TextUtils.isEmpty(mName.getText().toString()) &&
                            (TextUtils.isEmpty(mRiggit.getText().toString()) ||
                                    TextUtils.isEmpty(mCent.getText().toString()))) {
                        Toast.makeText(getActivity(), getString(R.string.toast_security_deposit_param_require, mName.getText().toString()), Toast.LENGTH_LONG).show();
                        isOtherLayoutPassValidation = false;
                        break;
                    }
                }
                if (!isOtherLayoutPassValidation) {
                    return;
                }

                if (mCommercialLayout.getVisibility() == View.VISIBLE &&
                        (
                                TextUtils.isEmpty(mOperatingHourFromDay.getText().toString()) ||
                                        TextUtils.isEmpty(mOperatingHourFromHour.getText().toString()) ||
                                        TextUtils.isEmpty(mOperatingHourFromMin.getText().toString()) ||
                                        TextUtils.isEmpty(mOperatingHourTillDay.getText().toString()) ||
                                        TextUtils.isEmpty(mOperatingHourTillHour.getText().toString()) ||
                                        TextUtils.isEmpty(mOperatingHourTillMin.getText().toString())
                        )
                        ) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }

                if (mOptionToRenewCheckBox.isChecked() &&
                        (TextUtils.isEmpty(mOptionToRenewMonth.getText().toString()) ||
                                TextUtils.isEmpty(mOptionToRenewYear.getText().toString()))) {
                    Toast.makeText(getActivity(), R.string.toast_option_to_renew_require, Toast.LENGTH_LONG).show();
                    return;
                }

                if (mOptionToRenewCheckBox.isChecked()){
                    try {
                        long yearToMonth = Long.valueOf(mOptionToRenewYear.getText().toString()) * 12;
                        long month = Long.valueOf(mOptionToRenewMonth.getText().toString());
                        if ((yearToMonth + month) > 36) {
                            Toast.makeText(getActivity(), R.string.toast_option_to_renew_cannot_more_than_3_year, Toast.LENGTH_LONG).show();
                            return;
                        }
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Option to renew month or year have some error.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (mOptionToRenewCheckBox.isChecked() && (!mRadio1.isChecked() &&
                        !mRadio2.isChecked() &&
                        !mRadio3.isChecked() &&
                        !mRadio4.isChecked() &&
                        !mRadio5.isChecked())) {
                    Toast.makeText(getActivity(), R.string.toast_terms_of_renewal_require, Toast.LENGTH_LONG).show();
                    return;
                }
                if (mOptionToRenewCheckBox.isChecked() &&
                        mRadio5.isChecked() &&
                        TextUtils.isEmpty(mRadio5OtherPercent.getText().toString())) {
                    Toast.makeText(getActivity(), R.string.toast_terms_of_renewal_other_field_require, Toast.LENGTH_LONG).show();
                    return;
                }

                boolean isLotNumberLayoutPassValidation = true;
                for (int i = 0; i < mLotNumberLayout.getChildCount(); i++) {
                    View view = mLotNumberLayout.getChildAt(i);
                    TextInputEditText mLotNumber = (TextInputEditText) view.findViewById(R.id.lot_number);

                    if (TextUtils.isEmpty(mLotNumber.getText().toString())) {
                        Toast.makeText(getActivity(), R.string.toast_lot_number_field_require, Toast.LENGTH_LONG).show();
                        isLotNumberLayoutPassValidation = false;
                        break;
                    }
                }
                if (!isLotNumberLayoutPassValidation) {
                    return;
                }

                updateNewTAModel();
                generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[4] : generateNewTAActivity.mResidentialSlideFragmentTag[4]);
                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.heetch.countrypicker.Country;
import com.heetch.countrypicker.CountryPickerCallbacks;
import com.heetch.countrypicker.CountryPickerDialog;
import com.heetch.countrypicker.Utils;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.generate_new_ta.LandlordTenantDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.CustomPhoneNumberUtil;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.LocationUtil;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

import java.util.List;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatus2Fragment extends BaseFragment implements View.OnClickListener {

    public static final String BUNDLE_IS_OWNER = "BUNDLE_IS_OWNER";
    private static final String TAG = GenerateNewTaStatus2Fragment.class.getName();

    Button mLandlordBtn;
    Button mTenantBtn;
    TextView mUserType;
    TextInputEditText mFullName;
    TextInputEditText mICNum;
    TextInputEditText mCurrentAddress;
    TextInputEditText mContactNum;
    TextInputEditText mEmailAddress;
    Button mNextBtn;
    //    Button mPreviousBtn;
    LinearLayout mUserTypeLayout;
    //    Button mUserTypeNextBtn;
//    Button mUserTypePreviousBtn;
    LinearLayout mCompanyLayout;
    Button mAddAnotherBtn;
    RadioButton mIndividualRadioButton;
    RadioButton mCompanyRadioButton;
    TextInputEditText mCompanyName;
    TextInputEditText mCompanyRegNo;
    TextView mCountryCodeBtn;
    ScrollView mScrollView;

    boolean isOwner;
    GenerateNewTAActivity generateNewTAActivity;
    public NewTAModel newTAModel;
    //    String currentCountryCode;
    private String mCountryISOCode;

    public int currentIndex = -1;

    boolean isEdit;
    boolean isTARenew;

    public static GenerateNewTaStatus2Fragment newInstance(boolean isOwner) {
        GenerateNewTaStatus2Fragment f = new GenerateNewTaStatus2Fragment();
        Bundle args = new Bundle();
        args.putBoolean(BUNDLE_IS_OWNER, isOwner);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_2, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);
        isOwner = getArguments().getBoolean(BUNDLE_IS_OWNER);

        mLandlordBtn = (Button) v.findViewById(R.id.landlord_btn);
        mTenantBtn = (Button) v.findViewById(R.id.tenant_btn);
        mUserType = (TextView) v.findViewById(R.id.user_type);
        mFullName = (TextInputEditText) v.findViewById(R.id.username);
        mICNum = (TextInputEditText) v.findViewById(R.id.ic_no);
        mCurrentAddress = (TextInputEditText) v.findViewById(R.id.current_address);
        mContactNum = (TextInputEditText) v.findViewById(R.id.contact_number);
        mEmailAddress = (TextInputEditText) v.findViewById(R.id.email);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);
        mUserTypeLayout = (LinearLayout) v.findViewById(R.id.user_type_layout);

        mCompanyLayout = (LinearLayout) v.findViewById(R.id.company_layout);
        mAddAnotherBtn = (Button) v.findViewById(R.id.add_another_btn);
        mIndividualRadioButton = (RadioButton) v.findViewById(R.id.radioBtn_individual);
        mCompanyRadioButton = (RadioButton) v.findViewById(R.id.radioBtn_company);
        mCompanyName = (TextInputEditText) v.findViewById(R.id.company_name);
        mCompanyRegNo = (TextInputEditText) v.findViewById(R.id.company_reg_no);
        mCountryCodeBtn = (TextView) v.findViewById(R.id.country_code_btn);
        mScrollView = (ScrollView) v.findViewById(R.id.scrollView);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);
//        mUserTypeNextBtn = (Button) v.findViewById(R.id.user_type_next_btn);
//        mUserTypePreviousBtn = (Button) v.findViewById(R.id.user_type_previous_btn);

        mFullName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        mCompanyName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        mCompanyRegNo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mLandlordBtn.setOnClickListener(this);
        mTenantBtn.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);
        mAddAnotherBtn.setOnClickListener(this);
        mCountryCodeBtn.setOnClickListener(this);
        mIndividualRadioButton.setOnCheckedChangeListener(onCheckedChangeListener);
        mCompanyRadioButton.setOnCheckedChangeListener(onCheckedChangeListener);
        mIndividualRadioButton.setChecked(true);
//        mPreviousBtn.setOnClickListener(this);
//        mUserTypeNextBtn.setOnClickListener(this);
//        mUserTypePreviousBtn.setOnClickListener(this);

        if ((!isEdit && isOwner) || (!isTARenew && isOwner)) {
            mUserTypeLayout.setVisibility((newTAModel.ownerLandlordTenantDetailList != null &&
                    newTAModel.ownerLandlordTenantDetailList.size() != 0)
                    ? View.GONE : View.VISIBLE);
            User mUser = new User().getUser(getActivity());
            mFullName.setText(mUser.getName());
            mContactNum.setText(mUser.getMobile());
            mEmailAddress.setText(mUser.getEmail());
        } else {
            mUserTypeLayout.setVisibility(View.GONE);
        }

        loadLocalData();
        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    private void initData() {
        displayTitle();
        mIndividualRadioButton.setChecked(true);
        mCompanyName.setText("");
        mCompanyRegNo.setText("");
        mFullName.setText("");
        mICNum.setText("");
        mCurrentAddress.setText("");
        mCountryCodeBtn.setText("+60");
        mContactNum.setText("");
        mEmailAddress.setText("");

        mScrollView.fullScroll(View.FOCUS_UP);
        mScrollView.pageScroll(View.FOCUS_UP);
        if (isOwner) {
            if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() >= 9) {
                mAddAnotherBtn.setEnabled(false);
            }
        } else {
            if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() >= 9) {
                mAddAnotherBtn.setEnabled(false);
            }
        }
    }

    CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked)
                switch (buttonView.getId()) {
                    case R.id.radioBtn_individual: {
                        mCompanyRadioButton.setChecked(false);
                        mCompanyLayout.setVisibility(View.GONE);
                        break;
                    }
                    case R.id.radioBtn_company: {
                        mIndividualRadioButton.setChecked(false);
                        mCompanyLayout.setVisibility(View.VISIBLE);
                        break;
                    }
                }
        }
    };

    public boolean isOnBackPressNeedDisplayUserTypeLayout() {
        if (!isEdit && !isTARenew && isOwner && mUserTypeLayout.getVisibility() == View.GONE) {
            mUserTypeLayout.setVisibility(View.VISIBLE);
            return true;
        }
        return false;
    }

    public void loadLocalData() {
        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mAddAnotherBtn.setEnabled(true);
        if (newTAModel != null) {
            if (isOwner) {
                if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() >= 10) {
                    mAddAnotherBtn.setEnabled(false);
                }
            } else {
                if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() >= 10) {
                    mAddAnotherBtn.setEnabled(false);
                }
            }

            if (isOwner) {
                if (newTAModel.ownerLandlordTenantDetailList != null && newTAModel.ownerLandlordTenantDetailList.size() != 0) {
                    if (getCurrentIndex() == -1) {
                        if (!isEdit || !isTARenew) {
                            currentIndex = newTAModel.ownerLandlordTenantDetailList.size() - 1;
                        } else {
                            currentIndex = 0;
                        }
                    }
                    LandlordTenantDetailModel landlordTenantDetailModel = newTAModel.ownerLandlordTenantDetailList.get(getCurrentIndex());
                    mIndividualRadioButton.setChecked(!landlordTenantDetailModel.isCompany);
                    mCompanyRadioButton.setChecked(landlordTenantDetailModel.isCompany);
                    mCompanyName.setText(landlordTenantDetailModel.companyName);
                    mCompanyRegNo.setText(landlordTenantDetailModel.companyRegNum);
                    mFullName.setText(landlordTenantDetailModel.name);
                    mICNum.setText(landlordTenantDetailModel.icNum);
                    mCurrentAddress.setText(landlordTenantDetailModel.address);

                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                    try {
                        // phone must begin with '+'
                        Phonenumber.PhoneNumber numberProto = phoneUtil.parse(landlordTenantDetailModel.contactNum, "");
                        mCountryCodeBtn.setText("+" + numberProto.getCountryCode());
                        mContactNum.setText(numberProto.getNationalNumber() + "");
                    } catch (NumberParseException e) {
                        mCountryCodeBtn.setText(landlordTenantDetailModel.countryCode);
                        mContactNum.setText(landlordTenantDetailModel.contactNum);
                    }

                    mEmailAddress.setText(landlordTenantDetailModel.email);
                } else {
//                    currentCountryCode = mCountryCodeBtn.getText().toString();
                    currentIndex = 0;
                }
            } else {
                if (newTAModel.nonOwnerLandlordTenantDetailList != null && newTAModel.nonOwnerLandlordTenantDetailList.size() != 0) {
                    if (getCurrentIndex() == -1) {
                        if (!isEdit || !isTARenew) {
                            currentIndex = newTAModel.nonOwnerLandlordTenantDetailList.size() - 1;
                        } else {
                            currentIndex = 0;
                        }
                    }
                    LandlordTenantDetailModel landlordTenantDetailModel = newTAModel.nonOwnerLandlordTenantDetailList.get(getCurrentIndex());
                    mIndividualRadioButton.setChecked(!landlordTenantDetailModel.isCompany);
                    mCompanyRadioButton.setChecked(landlordTenantDetailModel.isCompany);
                    mCompanyName.setText(landlordTenantDetailModel.companyName);
                    mCompanyRegNo.setText(landlordTenantDetailModel.companyRegNum);
                    mFullName.setText(landlordTenantDetailModel.name);
                    mICNum.setText(landlordTenantDetailModel.icNum);
                    mCurrentAddress.setText(landlordTenantDetailModel.address);

                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                    try {
                        // phone must begin with '+'
                        Phonenumber.PhoneNumber numberProto = phoneUtil.parse(landlordTenantDetailModel.contactNum, "");
                        mCountryCodeBtn.setText("+" + numberProto.getCountryCode());
                        mContactNum.setText(numberProto.getNationalNumber() + "");
                    } catch (NumberParseException e) {
                        mCountryCodeBtn.setText(landlordTenantDetailModel.countryCode);
                        mContactNum.setText(landlordTenantDetailModel.contactNum);
                    }

                    mEmailAddress.setText(landlordTenantDetailModel.email);
                } else {
//                    currentCountryCode = mCountryCodeBtn.getText().toString();
                    currentIndex = 0;
                }
            }
        } else {
//            currentCountryCode = mCountryCodeBtn.getText().toString();
            currentIndex = 0;
        }

        displayTitle();
    }

    private void displayTitle() {
        int displayIndex = getCurrentIndex();
        displayIndex++;

        if (isOwner) {
            mUserType.setText(newTAModel.isTenant ? getString(R.string.tenant_details) + " (" + (displayIndex) + ")"
                    : getString(R.string.landlord_details) + " (" + (displayIndex) + ")");
        } else {
            mUserType.setText(newTAModel.isTenant ? getString(R.string.landlord_details) + " (" + (displayIndex) + ")"
                    : getString(R.string.tenant_details) + " (" + (displayIndex) + ")");
        }
    }

    private void setTvCountryCodeValue(String countryISOCode) {
        String countryCode = countryISOCode;
        String countryDialCode = "";
        List<Country> countries = Utils.parseCountries(Utils.getCountriesJSON(getActivity()));
        for (Country country : countries) {
            if (country.getIsoCode().equalsIgnoreCase(countryCode)) {
                countryDialCode = country.getDialingCode();
                break;
            }
        }
        Country country = new Country(countryCode, countryDialCode);
        mCountryCodeBtn.setText(country.getDialingCode());
//        currentCountryCode = mCountryCodeBtn.getText().toString();
//        mCountryISOCode = country.getIsoCode();
    }

    private void countryCodeSelected(Country country) {
        mCountryCodeBtn.setText("+" + country.getDialingCode());
        mCountryISOCode = country.getIsoCode();
//        currentCountryCode = mCountryCodeBtn.getText().toString();
        LocationUtil.cancelLocationListenerCallback();
        getPhoneNumber();
    }

    private String getPhoneNumber() {
        String phoneNum = mCountryCodeBtn.getText().toString() + mContactNum.getText().toString();
        return CustomPhoneNumberUtil.getFormattedPhoneNumber(phoneNum, mCountryISOCode);
    }

    boolean isGoToNextStep;

    void updateNewTAModel() {
        isGoToNextStep = false;
        if (isOwner) {
            LandlordTenantDetailModel tenantDetail = new LandlordTenantDetailModel();
            tenantDetail.name = mFullName.getText().toString();
            tenantDetail.icNum = mICNum.getText().toString();
            tenantDetail.address = mCurrentAddress.getText().toString();
            tenantDetail.contactNum = mContactNum.getText().toString();
            tenantDetail.email = mEmailAddress.getText().toString();
            tenantDetail.isCompany = mCompanyRadioButton.isChecked();
            tenantDetail.companyName = mCompanyName.getText().toString();
            tenantDetail.companyRegNum = mCompanyRegNo.getText().toString();
            tenantDetail.countryCode = mCountryCodeBtn.getText().toString();

            if (getCurrentIndex() >= newTAModel.ownerLandlordTenantDetailList.size() - 1) {
                try {
                    if (newTAModel.ownerLandlordTenantDetailList.get(getCurrentIndex()) != null) {
                        newTAModel.ownerLandlordTenantDetailList.set(getCurrentIndex(), tenantDetail);
                    } else {
                        newTAModel.ownerLandlordTenantDetailList.add(tenantDetail);
                    }
                } catch (Exception e) {
                    newTAModel.ownerLandlordTenantDetailList.add(tenantDetail);
                }
                isGoToNextStep = true;
            } else {
                newTAModel.ownerLandlordTenantDetailList.set(getCurrentIndex(), tenantDetail);
            }
        } else {
            LandlordTenantDetailModel landlordDetail = new LandlordTenantDetailModel();
            landlordDetail.name = mFullName.getText().toString();
            landlordDetail.icNum = mICNum.getText().toString();
            landlordDetail.address = mCurrentAddress.getText().toString();
            landlordDetail.contactNum = mContactNum.getText().toString();
            landlordDetail.email = mEmailAddress.getText().toString();
            landlordDetail.isCompany = mCompanyRadioButton.isChecked();
            landlordDetail.companyName = mCompanyName.getText().toString();
            landlordDetail.companyRegNum = mCompanyRegNo.getText().toString();
            landlordDetail.countryCode = mCountryCodeBtn.getText().toString();

            if (getCurrentIndex() >= newTAModel.nonOwnerLandlordTenantDetailList.size() - 1) {
                try {
                    if (newTAModel.nonOwnerLandlordTenantDetailList.get(getCurrentIndex()) != null) {
                        newTAModel.nonOwnerLandlordTenantDetailList.set(getCurrentIndex(), landlordDetail);
                    } else {
                        newTAModel.nonOwnerLandlordTenantDetailList.add(landlordDetail);
                    }
                } catch (Exception e) {
                    newTAModel.nonOwnerLandlordTenantDetailList.add(landlordDetail);
                }
                isGoToNextStep = true;
            } else {
                newTAModel.nonOwnerLandlordTenantDetailList.set(getCurrentIndex(), landlordDetail);
            }
        }

        if (isOwner) {
            newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[2] : generateNewTAActivity.mResidentialSlideFragmentTag[2];
        } else {
            newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[3] : generateNewTAActivity.mResidentialSlideFragmentTag[3];

        }

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    void addToAnother() {
        if (isOwner) {
            LandlordTenantDetailModel tenantDetail = new LandlordTenantDetailModel();
            tenantDetail.name = mFullName.getText().toString();
            tenantDetail.icNum = mICNum.getText().toString();
            tenantDetail.address = mCurrentAddress.getText().toString();
            tenantDetail.contactNum = mContactNum.getText().toString();
            tenantDetail.email = mEmailAddress.getText().toString();
            tenantDetail.isCompany = mCompanyRadioButton.isChecked();
            tenantDetail.companyName = mCompanyName.getText().toString();
            tenantDetail.companyRegNum = mCompanyRegNo.getText().toString();
            tenantDetail.countryCode = mCountryCodeBtn.getText().toString();

            newTAModel.ownerLandlordTenantDetailList.add(tenantDetail);
        } else {
            LandlordTenantDetailModel landlordDetail = new LandlordTenantDetailModel();
            landlordDetail.name = mFullName.getText().toString();
            landlordDetail.icNum = mICNum.getText().toString();
            landlordDetail.address = mCurrentAddress.getText().toString();
            landlordDetail.contactNum = mContactNum.getText().toString();
            landlordDetail.email = mEmailAddress.getText().toString();
            landlordDetail.isCompany = mCompanyRadioButton.isChecked();
            landlordDetail.companyName = mCompanyName.getText().toString();
            landlordDetail.companyRegNum = mCompanyRegNo.getText().toString();
            landlordDetail.countryCode = mCountryCodeBtn.getText().toString();

            newTAModel.nonOwnerLandlordTenantDetailList.add(landlordDetail);
        }

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void decreaseCurrentIndex() {
        currentIndex--;
        loadLocalData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.landlord_btn: {
                mUserTypeLayout.setVisibility(View.GONE);
                newTAModel.isTenant = false;
                displayTitle();
                break;
            }
            case R.id.tenant_btn: {
                mUserTypeLayout.setVisibility(View.GONE);
                newTAModel.isTenant = true;
                displayTitle();
                break;
            }
            case R.id.next_btn: {
                if (mCompanyRadioButton.isChecked() && (TextUtils.isEmpty(mCompanyName.getText().toString()) ||
                        TextUtils.isEmpty(mCompanyRegNo.getText().toString()))) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(mFullName.getText().toString()) ||
                        TextUtils.isEmpty(mICNum.getText().toString()) ||
                        TextUtils.isEmpty(mCurrentAddress.getText().toString()) ||
                        TextUtils.isEmpty(mContactNum.getText().toString())) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }
                updateNewTAModel();
                if (isGoToNextStep) {
                    if (isOwner) {
                        generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[2] : generateNewTAActivity.mResidentialSlideFragmentTag[2]);
                    } else {
                        generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[3] : generateNewTAActivity.mResidentialSlideFragmentTag[3]);
                    }
                } else {
                    currentIndex++;
                    loadLocalData();
                }
                break;
            }
            case R.id.country_code_btn: {
                CountryPickerDialog countryPicker =
                        new CountryPickerDialog(getActivity(), new CountryPickerCallbacks() {
                            @Override
                            public void onCountrySelected(Country country, int flagResId) {
                                countryCodeSelected(country);
                            }
                        });
                countryPicker.show();
                break;
            }
            case R.id.add_another_btn: {
                if (mCompanyRadioButton.isChecked() && (TextUtils.isEmpty(mCompanyName.getText().toString()) ||
                        TextUtils.isEmpty(mCompanyRegNo.getText().toString()))) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(mFullName.getText().toString()) ||
                        TextUtils.isEmpty(mICNum.getText().toString()) ||
                        TextUtils.isEmpty(mCurrentAddress.getText().toString()) ||
                        TextUtils.isEmpty(mContactNum.getText().toString())) {
                    Toast.makeText(getActivity(), R.string.please_fill_in_all_info, Toast.LENGTH_LONG).show();
                    return;
                }
                currentIndex++;
                addToAnother();

                if (isEdit || isTARenew) {
                    newTAModel = generateNewTAActivity.getNewTAModel();
                } else {
                    String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
                    if (!TextUtils.isEmpty(keyNewTAModel)) {
                        newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
                    }
                }

                initData();
                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
//            case R.id.user_type_next_btn: {
//                mUserTypeLayout.setVisibility(View.GONE);
//                break;
//            }
//            case R.id.user_type_previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

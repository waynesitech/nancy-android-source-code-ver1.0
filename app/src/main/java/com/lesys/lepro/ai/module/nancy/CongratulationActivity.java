package com.lesys.lepro.ai.module.nancy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.MainActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.User;
import com.lesys.lepro.ai.model.UserCover;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class CongratulationActivity extends BaseActivity {

    private static final String TAG = CongratulationActivity.class.getName();

    Button mDoneBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulation);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } catch (Exception e) {

        }
        mDoneBtn = (Button) findViewById(R.id.done_btn);

        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}

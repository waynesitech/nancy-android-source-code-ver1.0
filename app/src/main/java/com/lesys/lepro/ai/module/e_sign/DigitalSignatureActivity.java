package com.lesys.lepro.ai.module.e_sign;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.Button;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.greysonparrelli.permiso.Permiso;
import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.listener.OnFileCopyListener;
import com.lesys.lepro.ai.model.Order;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.InternalStorageUtil;
import com.noelchew.permisowrapper.PermisoWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Nicholas on 22/06/2017.
 */

public class DigitalSignatureActivity extends BaseActivity {

    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;

    Order order;
    InternalStorageUtil mInternalStorageUtil;
    String orderUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_signature);

        order = (Order) getIntent().getSerializableExtra(GlobalParam.INTENT_ORDER_DATA);
        orderUserID = getIntent().getStringExtra(GlobalParam.INTENT_ORDER_USER_ID);

        Permiso.getInstance().setActivity(this);
        mInternalStorageUtil = new InternalStorageUtil(this);
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton = (Button) findViewById(R.id.clear_button);
        mSaveButton = (Button) findViewById(R.id.save_button);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmSubmitDialog();
//                if (addJpgSignatureToGallery(signatureBitmap)) {
//                    setResult(RESULT_OK);
//                    finish();
////                    Toast.makeText(DigitalSignatureActivity.this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
//                } else {
////                    Toast.makeText(DigitalSignatureActivity.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    void confirmSubmitDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_digital_signature_confirm_action)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        acceptAgreementDialog();
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialogBuilder.create().show();
    }

    void acceptAgreementDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this)
                .setMessage(Html.fromHtml(getString(R.string.dialog_digital_signature_agreement, "Nicholas")))
                .setPositiveButton(R.string.i_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PermisoWrapper.getPermissionSaveMediaToStorage(DigitalSignatureActivity.this, new PermisoWrapper.PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
                                addJpgSignatureToGallery(signatureBitmap);
                            }

                            @Override
                            public void onPermissionDenied() {

                            }
                        });
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(R.string.decline, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialogBuilder.create().show();
    }

    void successfullyDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_digital_signature_successfully_title)
                .setMessage(R.string.dialog_digital_signature_successfully)
                .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                        dialog.dismiss();
                    }
                });
        alertDialogBuilder.create().show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (Common.Helpers.isAppIsInBackground(this)) {
            Intent intent = new Intent();
            intent.setAction(GlobalParam.ACTION_BACKGROUND);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    void submitSignature(final File signature) {
//        if (Common.Helpers.checkReadPhoneStatePermission(this)) {
//            Common.Helpers.openPermissionDialog(this).show();
//            return;
//        }
        BaseApi.init(this)
                .submitSignature(
                        orderUserID,
                        signature)
                .call(new BaseApi.DialogResponseListener() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        successfullyDialog();

                        if (signature.exists()) {
                            signature.delete();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, JSONObject response) {
//                        Intent intent = new Intent();
//                        setResult(RESULT_OK, intent);
//                        finish();
                    }
                });
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.TRANSPARENT);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        resize(newBitmap, 640, 600).compress(Bitmap.CompressFormat.PNG, 80, stream);
        stream.close();
        submitSignature(photo);
    }

    private Bitmap resize(Bitmap bp, int width, int height) {
        return Bitmap.createScaledBitmap(bp, width, height, false);
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;

        String fileName = "Signature_" + System.currentTimeMillis() + ".png";

        fileName = mInternalStorageUtil.saveSignaturePath(fileName);
        try {
            saveBitmapToJPG(signature, new File(fileName));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}
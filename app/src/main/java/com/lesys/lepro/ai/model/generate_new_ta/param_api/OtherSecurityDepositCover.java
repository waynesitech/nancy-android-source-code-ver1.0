package com.lesys.lepro.ai.model.generate_new_ta.param_api;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lesys.lepro.ai.model.generate_new_ta.MoneyModel;
import com.lesys.lepro.ai.shared.Common;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicholas on 25/06/2017.
 */

public class OtherSecurityDepositCover implements Serializable {

    private List<OtherSecurityDeposit> otherSecurityDepositList = new ArrayList<>();

    public String getJson(List<MoneyModel> securityDepositOtherList) {
        otherSecurityDepositList.clear();
        for (MoneyModel moneyModel : securityDepositOtherList) {
            if (!TextUtils.isEmpty(moneyModel.name)) {
                String priceStr;
                if (!Common.Helpers.isNumber(moneyModel.ringgitMalaysia)) {
                    priceStr = "00";
                } else {
                    priceStr = moneyModel.ringgitMalaysia;
                }
                if (!Common.Helpers.isNumber(moneyModel.cent)) {
                    priceStr = priceStr + ".00";
                } else {
                    priceStr = priceStr + "." + moneyModel.cent;
                }
                otherSecurityDepositList.add(new OtherSecurityDeposit(moneyModel.name, Double.valueOf(priceStr)));
            }
        }
        if (otherSecurityDepositList.size() != 0) {
            return new Gson().toJson(otherSecurityDepositList);
        } else {
            return null;
        }
    }

    public List<MoneyModel> fromJson(String jsonOutput) {
        if (TextUtils.isEmpty(jsonOutput)) {
            return null;
        }

        try {
            List<MoneyModel> moneyModelList = new ArrayList<>();
            Type listType = new TypeToken<List<OtherSecurityDeposit>>() {
            }.getType();
            List<OtherSecurityDeposit> arrayList = new Gson().fromJson(jsonOutput, listType);

            for (OtherSecurityDeposit otherSecurityDeposit : arrayList) {
                MoneyModel moneyModel = new MoneyModel(
                        otherSecurityDeposit.getName(),
                        otherSecurityDeposit.getAmount()
                );
                moneyModelList.add(moneyModel);
            }
            return moneyModelList;
        } catch (Exception e) {
            return null;
        }
    }

}

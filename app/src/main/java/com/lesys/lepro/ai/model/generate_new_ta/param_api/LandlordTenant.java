package com.lesys.lepro.ai.model.generate_new_ta.param_api;

import java.io.Serializable;

/**
 * Created by Nicholas on 25/06/2017.
 */

public class LandlordTenant implements Serializable {

    private boolean isCompany;
    private String companyName;
    private String companyRegNum;
    private String name;
    private String icNum;
    private String address;
    private String contactNum;
    private String email;

    public boolean isCompany() {
        return isCompany;
    }

    public void setCompany(boolean company) {
        isCompany = company;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyRegNum() {
        return companyRegNum;
    }

    public void setCompanyRegNum(String companyRegNum) {
        this.companyRegNum = companyRegNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNum() {
        return contactNum;
    }

    public void setContactNum(String contactNum) {
        this.contactNum = contactNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcNum() {
        return icNum;
    }

    public void setIcNum(String icNum) {
        this.icNum = icNum;
    }

}

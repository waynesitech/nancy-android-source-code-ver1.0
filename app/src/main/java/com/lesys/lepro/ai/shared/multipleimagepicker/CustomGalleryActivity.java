package com.lesys.lepro.ai.shared.multipleimagepicker;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.lesys.lepro.ai.BaseActivity;
import com.lesys.lepro.ai.R;

import java.util.ArrayList;
import java.util.Collections;

public class CustomGalleryActivity extends BaseActivity {

//    Toolbar toolbar;
//    TextView mToolBarTitle;
    public static final String SELECTED_STRING_ARRAY_KEY = "CUSTOM_GALLERY_SELECTED";
    public static final String SEQUENCE_SELECTION_BOOLEAN_KEY = "CUSTOM_GALLERY_SEQUENCE_SELECTION";
    public static final String MAXIMUM_SELECTION_INT_KEY = "CUSTOM_GALLERY_MAXIMUM_SELECTION";
    GridView gridGallery;
    Handler handler;
    GalleryAdapter adapter;

    ImageView imgNoMedia;
    Button btnGalleryOk;

    String action;
    ArrayList<Album> mAlbumsList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.gallery);

        action = getIntent().getAction();
        if (action == null) {
            finish();
        }
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
//
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(null);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mToolBarTitle.setText(R.string.select_photo);

        init();
//		listFolder();

    }

    private void listFolder() {

        mAlbumsList = new ArrayList<Album>();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        ArrayList<String> ids = new ArrayList<String>();


        mAlbumsList.clear();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Album album = new Album();

                int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID);
                album.id = cursor.getString(columnIndex);

                if (!ids.contains(album.id)) {
                    columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                    album.name = cursor.getString(columnIndex);

                    Log.d("Gallery", album.name);

                    columnIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    album.coverID = cursor.getLong(columnIndex);

                    mAlbumsList.add(album);
                    ids.add(album.id);
                } else {
                    mAlbumsList.get(ids.indexOf(album.id)).count++;
                }
            }
            cursor.close();
        }
    }


    private void init() {

        handler = new Handler();
        gridGallery = (GridView) findViewById(R.id.gridGallery);
        gridGallery.setFastScrollEnabled(true);
        if (getIntent().getBooleanExtra(SEQUENCE_SELECTION_BOOLEAN_KEY, false)) {
            adapter = new GalleryAdapter(getApplicationContext(), getIntent().getIntExtra(MAXIMUM_SELECTION_INT_KEY, 5));
        } else {
            adapter = new GalleryAdapter(getApplicationContext());
        }

//		PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader,
//				true, true);
//		gridGallery.setOnScrollListener(listener);

        if (action.equalsIgnoreCase(Action.ACTION_MULTIPLE_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
            gridGallery.setOnItemClickListener(mItemMulClickListener);
            adapter.setMultiplePick(true);

        } else if (action.equalsIgnoreCase(Action.ACTION_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
            gridGallery.setOnItemClickListener(mItemSingleClickListener);
            adapter.setMultiplePick(false);

        }

        gridGallery.setAdapter(adapter);
        imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

        btnGalleryOk = (Button) findViewById(R.id.btnGalleryOk);
        btnGalleryOk.setOnClickListener(mOkClickListener);

        new Thread() {

            @Override
            public void run() {
                Looper.prepare();
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        adapter.addAll(getGalleryPhotos());
                        checkImageStatus();
                    }
                });
                Looper.loop();
            }

            ;

        }.start();

    }

    private void checkImageStatus() {
        if (adapter.isEmpty()) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }

    View.OnClickListener mOkClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String[] allPath;
            if (getIntent().getBooleanExtra(SEQUENCE_SELECTION_BOOLEAN_KEY, false)) {
                allPath = adapter.getSequenceSelection();
            } else {
                ArrayList<CustomGallery> selected = adapter.getSelected();

                allPath = new String[selected.size()];
                for (int i = 0; i < allPath.length; i++) {
                    allPath[i] = selected.get(i).sdcardPath;
                }
            }


            Intent data = new Intent().putExtra("all_path", allPath);
            setResult(RESULT_OK, data);
            finish();

        }
    };
    AdapterView.OnItemClickListener mItemMulClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            adapter.changeSelection(v, position);

        }
    };

    AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            CustomGallery item = adapter.getItem(position);
            Intent data = new Intent().putExtra("single_path", item.sdcardPath);
            setResult(RESULT_OK, data);
            finish();
        }
    };

    private ArrayList<CustomGallery> getGalleryPhotos() {
        ArrayList<CustomGallery> galleryList = new ArrayList<CustomGallery>();
        String[] selectedPaths = getIntent().getStringArrayExtra(SELECTED_STRING_ARRAY_KEY);
        int addedCount = 0;
        try {
            final String[] columns = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID};
            final String orderBy = MediaStore.Images.Media._ID;

            Cursor imagecursor = managedQuery(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
                    null, null, orderBy);

            if (imagecursor != null && imagecursor.getCount() > 0) {

                while (imagecursor.moveToNext()) {
                    CustomGallery item = new CustomGallery();

                    int dataColumnIndex = imagecursor
                            .getColumnIndex(MediaStore.Images.Media.DATA);

                    item.sdcardPath = imagecursor.getString(dataColumnIndex);
                    for (int i = 0; i < selectedPaths.length; i++) {
                        System.out.println("Kelvyndebug: paths compare:" + item.sdcardPath + " vs " + selectedPaths[i]);
                        if (item.sdcardPath.equals(selectedPaths[i])) {
                            item.isSelected = true + "";
                            item.selectionIndex = i;
                            addedCount++;
                            selectedPaths[i] = "";
                            break;
                        }
                    }
                    galleryList.add(item);
                }
            }
            if (addedCount < selectedPaths.length) {
                for (int i = 0; i < selectedPaths.length; i++) {
                    if (selectedPaths[i].trim().length() > 0) {
                        CustomGallery item = new CustomGallery();
                        item.sdcardPath = selectedPaths[i];
                        item.isSelected = true + "";
                        item.selectionIndex = i;
                        galleryList.add(item);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // show newest photo at beginning of the list
        Collections.reverse(galleryList);
        return galleryList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

package com.lesys.lepro.ai.model;

import java.io.Serializable;

/**
 * Created by Nicholas on 10/04/2017.
 */

public class Simple implements Serializable {

    private String id;
    private String name;
    private String ren;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRen() {
        return ren;
    }

    public void setRen(String ren) {
        this.ren = ren;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

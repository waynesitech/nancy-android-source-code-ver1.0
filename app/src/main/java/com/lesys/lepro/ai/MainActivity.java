package com.lesys.lepro.ai;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.lesys.lepro.ai.api.BaseApi;
import com.lesys.lepro.ai.api.BaseRestClient;
import com.lesys.lepro.ai.api.DataHandler;
import com.lesys.lepro.ai.model.MetaData;
import com.lesys.lepro.ai.module.authentication.IntroPageActivity;
import com.lesys.lepro.ai.module.nancy.CongratulationActivity;
import com.lesys.lepro.ai.module.nancy.GenerateTAConfirmationActivity;
import com.lesys.lepro.ai.module.nancy.fragment.WelcomeBackFragment;
import com.lesys.lepro.ai.module.tenancy_agreement.TenancyAgreementListActivity;
import com.lesys.lepro.ai.shared.Common;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends BaseActivity {

    private FragmentManager mFragmentManager;
    private Fragment[] mSlideFragments = {DemoFragment.newInstance(0),
            DemoFragment.newInstance(1),
            DemoFragment.newInstance(1),
            DemoFragment.newInstance(1),
            DemoFragment.newInstance(1),
            DemoFragment.newInstance(1),
            DemoFragment.newInstance(1),
            DemoFragment.newInstance(1)
    };

    DrawerLayout mDrawerLayout;
    RecyclerView mRecyclerView;

    MainActivityDrawerAdapter mAdapter;
    String[] slideMenuArray;
    int mCurrentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (mUser.getUser(this) == null) {
            startActivity(new Intent(this, IntroPageActivity.class));
            finish();
            return;
        }
        String action = getIntent().getAction();
        if (!TextUtils.isEmpty(action)) {
            switch (action) {
                case GlobalParam.ACTION_PHONE_VERIFIED: {
                    startActivity(new Intent(this, CongratulationActivity.class));
                    break;
                }
            }
        }

        toolbar.setNavigationIcon(R.drawable.ic_menu);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mUser = mUser.getUser(this);
        slideMenuArray = getResources().getStringArray(R.array.slidemenu_array);
        mFragmentManager = getSupportFragmentManager();
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MainActivityDrawerAdapter(this) {
            @Override
            public void onItemClick(int position) {
                switch (position) {
                    case 6: {
                        logoutDialog();
                        break;
                    }
//                    default: {
//                        mFragmentManager.beginTransaction()
//                                .replace(R.id.content, mSlideFragments[position - 1], slideMenuArray[position - 1])
//                                .commit();
//                        setTitle(slideMenuArray[position - 1]);
//                        mCurrentPosition = position - 1;
//                        break;
//                    }
                }
                mDrawerLayout.closeDrawers();
            }
        };
        mAdapter.setHeader(mUser);
        mAdapter.setItems(new ArrayList<>(Arrays.asList(slideMenuArray)));
        mRecyclerView.setAdapter(mAdapter);

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        mFragmentManager.beginTransaction()
                .replace(R.id.content, WelcomeBackFragment.newInstance(), WelcomeBackFragment.class.getName())
                .commit();
        setTitle(R.string.app_name);
    }

    public void openMainFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.content, MainFunctionFragment.newInstance(), MainFunctionFragment.class.getName())
                .commit();
        setTitle(R.string.app_name);
    }

    private void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_logout_msg)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferencesUtil.setBoolean(MainActivity.this, GlobalParam.KEY_IS_LOGINED, false);
                        new BaseRestClient(MainActivity.this).clearCookies();
                        SharedPreferencesUtil.setString(MainActivity.this, GlobalParam.KEY_USER_DATA, null);
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setClass(MainActivity.this, IntroPageActivity.class);
                        startActivity(intent);
                        finish();
//                        BaseApi.init(MainActivity.this)
//                                .logout()
//                                .call(new DataHandler<MetaData>(MetaData.class) {
//                                    @Override
//                                    public void onSuccess(MetaData data) {
//                                        SharedPreferencesUtil.setBoolean(MainActivity.this, GlobalParam.KEY_IS_LOGINED, false);
//                                        new BaseRestClient(MainActivity.this).clearCookies();
//                                        SharedPreferencesUtil.setString(MainActivity.this, GlobalParam.KEY_USER_DATA, null);
//                                        Intent intent = new Intent();
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        intent.setClass(MainActivity.this, IntroPageActivity.class);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//
//                                    @Override
//                                    public void onFailure(Throwable throwable) {
//
//                                    }
//                                });
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mRecyclerView)) {
            mDrawerLayout.closeDrawers();
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(mRecyclerView)) {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

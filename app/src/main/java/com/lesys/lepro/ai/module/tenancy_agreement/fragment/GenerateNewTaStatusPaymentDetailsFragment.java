package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.joanzapata.iconify.widget.IconTextView;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.generate_new_ta.CollectionByLandlordModel;
import com.lesys.lepro.ai.model.generate_new_ta.DirectBankInModel;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.PaymentDetailModel;
import com.lesys.lepro.ai.model.generate_new_ta.PostDatedChequeModel;
import com.lesys.lepro.ai.model.generate_new_ta.SpecialConditionModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatusPaymentDetailsFragment extends BaseFragment implements View.OnClickListener {

    ScrollView mScrollView;
    TextView mPropertyType;

    RadioButton mDirectBankInRadioBtn;
    IconTextView mDirectBankInArrow;
    LinearLayout mDirectBankInLayout;
    TextInputEditText mDirectBankInFullName;
    TextInputEditText mDirectBankInBank;
    //    AppCompatSpinner mDirectBankInBankSpinner;
    TextInputEditText mDirectBankInBankAccountNumber;
    RadioButton mPaymentNotificationModeEmail;
    RadioButton mPaymentNotificationModeSMS;
    RadioButton mPaymentNotificationModeWhatsappText;

    RadioButton mPostDatedChequeRadioBtn;
    IconTextView mPostDatedChequeArrow;
    LinearLayout mPostDatedChequeLayout;
    RadioButton mMonth6RadioBtn;
    RadioButton mMonth12RadioBtn;
    RadioButton mMonth24RadioBtn;

    RadioButton mCollectionByLandlordRadioBtn;
//    LinearLayout mCollectionByLandlordLayout;
//    RadioButton mYesRadioBtn;
//    RadioButton mNoRadioBtn;

    Button mNextBtn;
//    Button mPreviousBtn;

    public NewTAModel newTAModel;
    GenerateNewTAActivity generateNewTAActivity;

    boolean isEdit;
    boolean isTARenew;

    public static GenerateNewTaStatusPaymentDetailsFragment newInstance() {
        GenerateNewTaStatusPaymentDetailsFragment f = new GenerateNewTaStatusPaymentDetailsFragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_payment_details, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);

        mScrollView = (ScrollView) v.findViewById(R.id.scrollView);
        mPropertyType = (TextView) v.findViewById(R.id.property_type);

        mDirectBankInRadioBtn = (RadioButton) v.findViewById(R.id.direct_bank_in_radioBtn);
        mDirectBankInArrow = (IconTextView) v.findViewById(R.id.direct_bank_in_arrow);
        mDirectBankInLayout = (LinearLayout) v.findViewById(R.id.direct_bank_in_layout);
        mDirectBankInFullName = (TextInputEditText) v.findViewById(R.id.direct_bank_in_full_name);
        mDirectBankInBank = (TextInputEditText) v.findViewById(R.id.direct_bank_in_bank);
//        mDirectBankInBankSpinner = (AppCompatSpinner) v.findViewById(R.id.direct_bank_in_bank_spinner);
        mDirectBankInBankAccountNumber = (TextInputEditText) v.findViewById(R.id.direct_bank_in_bank_account_number);
        mPaymentNotificationModeEmail = (RadioButton) v.findViewById(R.id.payment_notification_mode_email);
        mPaymentNotificationModeSMS = (RadioButton) v.findViewById(R.id.payment_notification_mode_sms);
        mPaymentNotificationModeWhatsappText = (RadioButton) v.findViewById(R.id.payment_notification_mode_whatsapp);

        mPostDatedChequeRadioBtn = (RadioButton) v.findViewById(R.id.post_dated_cheque_radioBtn);
        mPostDatedChequeArrow = (IconTextView) v.findViewById(R.id.post_dated_cheque_arrow);
        mPostDatedChequeLayout = (LinearLayout) v.findViewById(R.id.post_dated_cheque_layout);
        mMonth6RadioBtn = (RadioButton) v.findViewById(R.id.month_6_radioBtn);
        mMonth12RadioBtn = (RadioButton) v.findViewById(R.id.month_12_radioBtn);
        mMonth24RadioBtn = (RadioButton) v.findViewById(R.id.month_24_radioBtn);

        mCollectionByLandlordRadioBtn = (RadioButton) v.findViewById(R.id.collection_by_landlord_radioBtn);
//        mCollectionByLandlordLayout = (LinearLayout) v.findViewById(R.id.collection_by_landlord_layout);
//        mYesRadioBtn = (RadioButton) v.findViewById(R.id.yes_radioBtn);
//        mNoRadioBtn = (RadioButton) v.findViewById(R.id.no_radioBtn);

        mNextBtn = (Button) v.findViewById(R.id.next_btn);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);

        mDirectBankInFullName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        mDirectBankInBank.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        mDirectBankInRadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
        mPostDatedChequeRadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
        mCollectionByLandlordRadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
        mPaymentNotificationModeEmail.setOnCheckedChangeListener(onCheckedChangeListener);
        mPaymentNotificationModeSMS.setOnCheckedChangeListener(onCheckedChangeListener);
        mPaymentNotificationModeWhatsappText.setOnCheckedChangeListener(onCheckedChangeListener);
        mMonth6RadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
        mMonth12RadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
        mMonth24RadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
//        mYesRadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);
//        mNoRadioBtn.setOnCheckedChangeListener(onCheckedChangeListener);

        mNextBtn.setOnClickListener(this);
//        mPreviousBtn.setOnClickListener(this);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        loadLocalData();

        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked)
                switch (buttonView.getId()) {
                    case R.id.direct_bank_in_radioBtn: {
                        mPostDatedChequeRadioBtn.setChecked(false);
                        mCollectionByLandlordRadioBtn.setChecked(false);
                        viewController();
                        break;
                    }
                    case R.id.post_dated_cheque_radioBtn: {
                        mDirectBankInRadioBtn.setChecked(false);
                        mCollectionByLandlordRadioBtn.setChecked(false);
                        viewController();
                        break;
                    }
                    case R.id.collection_by_landlord_radioBtn: {
                        mDirectBankInRadioBtn.setChecked(false);
                        mPostDatedChequeRadioBtn.setChecked(false);
                        viewController();
                        break;
                    }
                    case R.id.payment_notification_mode_email: {
                        mPaymentNotificationModeSMS.setChecked(false);
                        mPaymentNotificationModeWhatsappText.setChecked(false);
                        break;
                    }
                    case R.id.payment_notification_mode_sms: {
                        mPaymentNotificationModeEmail.setChecked(false);
                        mPaymentNotificationModeWhatsappText.setChecked(false);
                        break;
                    }
                    case R.id.payment_notification_mode_whatsapp: {
                        mPaymentNotificationModeEmail.setChecked(false);
                        mPaymentNotificationModeSMS.setChecked(false);
                        break;
                    }
                    case R.id.month_6_radioBtn: {
                        mMonth12RadioBtn.setChecked(false);
                        mMonth24RadioBtn.setChecked(false);
                        break;
                    }
                    case R.id.month_12_radioBtn: {
                        mMonth6RadioBtn.setChecked(false);
                        mMonth24RadioBtn.setChecked(false);
                        break;
                    }
                    case R.id.month_24_radioBtn: {
                        mMonth6RadioBtn.setChecked(false);
                        mMonth12RadioBtn.setChecked(false);
                        break;
                    }
//                    case R.id.yes_radioBtn: {
//                        mNoRadioBtn.setChecked(false);
//                        break;
//                    }
//                    case R.id.no_radioBtn: {
//                        mYesRadioBtn.setChecked(false);
//                        break;
//                    }
                }

            mDirectBankInArrow.setText(mDirectBankInRadioBtn.isChecked() ? "{mdi-chevron-up}" : "{mdi-chevron-down}");
            mPostDatedChequeArrow.setText(mPostDatedChequeRadioBtn.isChecked() ? "{mdi-chevron-up}" : "{mdi-chevron-down}");
        }
    };

    void viewController() {
        mDirectBankInLayout.setVisibility(mDirectBankInRadioBtn.isChecked() ? View.VISIBLE : View.GONE);
        mPostDatedChequeLayout.setVisibility(mPostDatedChequeRadioBtn.isChecked() ? View.VISIBLE : View.GONE);
//        mCollectionByLandlordLayout.setVisibility(mCollectionByLandlordRadioBtn.isChecked() ? View.VISIBLE : View.GONE);
    }

    public void loadLocalData() {
        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mPropertyType.setText(!newTAModel.isCommercial ? R.string.residential : R.string.commercial);
        if (newTAModel.paymentDetailModel != null) {
            PaymentDetailModel paymentDetailModel = newTAModel.paymentDetailModel;
            mDirectBankInRadioBtn.setChecked(paymentDetailModel.isDirectBankIn ? true : false);
            mPostDatedChequeRadioBtn.setChecked(paymentDetailModel.isPostDatedCheque ? true : false);
            mCollectionByLandlordRadioBtn.setChecked(paymentDetailModel.isCollectionByLandlord ? true : false);

            if (paymentDetailModel.directBankInModel != null) {
                DirectBankInModel directBankInModel = paymentDetailModel.directBankInModel;
                mDirectBankInFullName.setText(directBankInModel.nameOfAccountHolder);
                mDirectBankInBank.setText(directBankInModel.bank);
                mDirectBankInBankAccountNumber.setText(directBankInModel.bankAccountNumber);
                mPaymentNotificationModeEmail.setChecked(directBankInModel.isEmail ? true : false);
                mPaymentNotificationModeSMS.setChecked(directBankInModel.isSMS ? true : false);
                mPaymentNotificationModeWhatsappText.setChecked(directBankInModel.isWhatsappText ? true : false);
            }

            if (paymentDetailModel.postDatedChequeModel != null) {
                PostDatedChequeModel postDatedChequeModel = paymentDetailModel.postDatedChequeModel;
                mMonth6RadioBtn.setChecked(postDatedChequeModel.is6Months ? true : false);
                mMonth12RadioBtn.setChecked(postDatedChequeModel.is12Months ? true : false);
                mMonth24RadioBtn.setChecked(postDatedChequeModel.is24Months ? true : false);
            }

//            if (paymentDetailModel.collectionByLandlordModel != null) {
//                CollectionByLandlordModel collectionByLandlordModel = paymentDetailModel.collectionByLandlordModel;
//                mYesRadioBtn.setChecked(collectionByLandlordModel.isCollectionByLandlord);
//                mNoRadioBtn.setChecked(!collectionByLandlordModel.isCollectionByLandlord);
//            }
        }
    }

    void updateNewTAModel() {
        newTAModel.paymentDetailModel = new PaymentDetailModel();
        PaymentDetailModel paymentDetailModel = newTAModel.paymentDetailModel;
        paymentDetailModel.isDirectBankIn = mDirectBankInRadioBtn.isChecked();
        paymentDetailModel.isPostDatedCheque = mPostDatedChequeRadioBtn.isChecked();
        paymentDetailModel.isCollectionByLandlord = mCollectionByLandlordRadioBtn.isChecked();

        paymentDetailModel.directBankInModel = new DirectBankInModel();
        DirectBankInModel directBankInModel = paymentDetailModel.directBankInModel;
        directBankInModel.nameOfAccountHolder = mDirectBankInFullName.getText().toString();
        directBankInModel.bank = mDirectBankInBank.getText().toString();
        directBankInModel.bankAccountNumber = mDirectBankInBankAccountNumber.getText().toString();
        directBankInModel.isEmail = mPaymentNotificationModeEmail.isChecked();
        directBankInModel.isSMS = mPaymentNotificationModeSMS.isChecked();
        directBankInModel.isWhatsappText = mPaymentNotificationModeWhatsappText.isChecked();

        paymentDetailModel.postDatedChequeModel = new PostDatedChequeModel();
        PostDatedChequeModel postDatedChequeModel = paymentDetailModel.postDatedChequeModel;
        postDatedChequeModel.is6Months = mMonth6RadioBtn.isChecked();
        postDatedChequeModel.is12Months = mMonth12RadioBtn.isChecked();
        postDatedChequeModel.is24Months = mMonth24RadioBtn.isChecked();

//        paymentDetailModel.collectionByLandlordModel = new CollectionByLandlordModel();
//        CollectionByLandlordModel collectionByLandlordModel = paymentDetailModel.collectionByLandlordModel;
//        collectionByLandlordModel.isCollectionByLandlord = mYesRadioBtn.isChecked();

        newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[6] : generateNewTAActivity.mResidentialSlideFragmentTag[5];

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                if (!mDirectBankInRadioBtn.isChecked() && !mPostDatedChequeRadioBtn.isChecked() && !mCollectionByLandlordRadioBtn.isChecked()) {
                    Toast.makeText(getActivity(), R.string.toast_atleast_tick_1_required, Toast.LENGTH_LONG).show();
                    return;
                }
                if (mDirectBankInRadioBtn.isChecked() && (TextUtils.isEmpty(mDirectBankInFullName.getText().toString()) ||
                        TextUtils.isEmpty(mDirectBankInBankAccountNumber.getText().toString()))) {
                    Toast.makeText(getActivity(), R.string.toast_direct_bank_required, Toast.LENGTH_LONG).show();
                    return;
                }
                if (mDirectBankInRadioBtn.isChecked() && (!mPaymentNotificationModeWhatsappText.isChecked() &&
                        !mPaymentNotificationModeEmail.isChecked() &&
                        !mPaymentNotificationModeSMS.isChecked())) {
                    Toast.makeText(getActivity(), R.string.toast_method_of_notification_upon_payment_made_required, Toast.LENGTH_LONG).show();
                    return;
                }
                if (mPostDatedChequeRadioBtn.isChecked() && (!mMonth6RadioBtn.isChecked() &&
                        !mMonth12RadioBtn.isChecked() &&
                        !mMonth24RadioBtn.isChecked())) {
                    Toast.makeText(getActivity(), R.string.toast_post_dated_cheque_required, Toast.LENGTH_LONG).show();
                    return;
                }
//                if (mCollectionByLandlordRadioBtn.isChecked() && (!mYesRadioBtn.isChecked() &&
//                        !mNoRadioBtn.isChecked())) {
//                    Toast.makeText(getActivity(), R.string.toast_collection_by_landlord_required, Toast.LENGTH_LONG).show();
//                    return;
//                }
                updateNewTAModel();
                generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[6] : generateNewTAActivity.mResidentialSlideFragmentTag[5]);

                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

package com.lesys.lepro.ai.model.generate_new_ta;

import com.lesys.lepro.ai.model.OrderFullDetail;

import java.io.Serializable;

/**
 * Created by Nicholas on 04/06/2017.
 */

public class PostDatedChequeModel implements Serializable {

    public boolean is6Months;
    public boolean is12Months;
    public boolean is24Months;

    public PostDatedChequeModel() {

    }

    public PostDatedChequeModel syncServerTA(OrderFullDetail order) {

        switch (order.getPostDatedMethod()) {
            case "is6Months": {
                is6Months = true;
                is12Months = false;
                is24Months = false;
                break;
            }
            case "is12Months": {
                is6Months = false;
                is12Months = true;
                is24Months = false;
                break;
            }
            case "is24Months": {
                is6Months = false;
                is12Months = false;
                is24Months = true;
                break;
            }
        }

        return this;
    }

}

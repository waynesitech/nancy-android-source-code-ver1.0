package com.lesys.lepro.ai.module.tenancy_agreement.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.joanzapata.iconify.widget.IconTextView;
import com.lesys.lepro.ai.BaseFragment;
import com.lesys.lepro.ai.R;
import com.lesys.lepro.ai.model.generate_new_ta.FixtureFittingsModel;
import com.lesys.lepro.ai.model.generate_new_ta.NewTAModel;
import com.lesys.lepro.ai.model.generate_new_ta.SpecialConditionModel;
import com.lesys.lepro.ai.module.tenancy_agreement.GenerateNewTAActivity;
import com.lesys.lepro.ai.shared.GlobalParam;
import com.lesys.lepro.ai.shared.SharedPreferencesUtil;

/**
 * Created by Nicholas on 14/05/2017.
 */

public class GenerateNewTaStatusFixturesAndFittingsFragment extends BaseFragment implements View.OnClickListener {

    ScrollView mScrollView;
    TextView mPropertyType;
    LinearLayout mFixtureFittinsLayout;
    Button mNextBtn;
//    Button mPreviousBtn;

    public NewTAModel newTAModel;
    GenerateNewTAActivity generateNewTAActivity;

    boolean isEdit;
    boolean isTARenew;

    public static GenerateNewTaStatusFixturesAndFittingsFragment newInstance() {
        GenerateNewTaStatusFixturesAndFittingsFragment f = new GenerateNewTaStatusFixturesAndFittingsFragment();
//        Bundle args = new Bundle();
//        args.putInt("banner_id", id);
//        f.setArguments(args);
        return f;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
        if (!hidden) {
            loadLocalData();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_generate_new_ta_status_fixture_fittings, container, false);

        isEdit = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_EDIT, false);
        isTARenew = getActivity().getIntent().getBooleanExtra(GlobalParam.INTENT_IS_TA_RENEW, false);

        mScrollView = (ScrollView) v.findViewById(R.id.scrollView);
        mPropertyType = (TextView) v.findViewById(R.id.property_type);
        mFixtureFittinsLayout = (LinearLayout) v.findViewById(R.id.fixture_fittings_layout);
        mNextBtn = (Button) v.findViewById(R.id.next_btn);
//        mPreviousBtn = (Button) v.findViewById(R.id.previous_btn);

        mNextBtn.setOnClickListener(this);
//        mPreviousBtn.setOnClickListener(this);

        generateNewTAActivity = ((GenerateNewTAActivity) getActivity());

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

        loadLocalData();
    }

    public void loadLocalData() {
        if (isEdit || isTARenew) {
            newTAModel = generateNewTAActivity.getNewTAModel();
        } else {
            String keyNewTAModel = SharedPreferencesUtil.getString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL);
            if (!TextUtils.isEmpty(keyNewTAModel)) {
                newTAModel = new Gson().fromJson(keyNewTAModel, NewTAModel.class);
            }
            if (newTAModel == null)
                newTAModel = new NewTAModel();
        }

        mPropertyType.setText(!newTAModel.isCommercial ? R.string.residential : R.string.commercial);
        if (newTAModel.fixtureFittingsModel != null && newTAModel.fixtureFittingsModel.fixtureFittingList.size() != 0) {
            mFixtureFittinsLayout.removeAllViews();
            for (int i = 0; i < newTAModel.fixtureFittingsModel.fixtureFittingList.size(); i++) {
                String s = newTAModel.fixtureFittingsModel.fixtureFittingList.get(i);
                generateFixtureFittingsView(i, s);
            }
            return;
        }

        mFixtureFittinsLayout.removeAllViews();
        generateFixtureFittingsView(0, "");
    }

    void generateFixtureFittingsView(int position, String s) {
        final View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_fixture_fittings_view, null);

        final TextView mIndex = (TextView) v.findViewById(R.id.index);
        TextInputEditText mSpecialConditionText = (TextInputEditText) v.findViewById(R.id.fixture_fittings_text);
        IconTextView mAddOnBtn = (IconTextView) v.findViewById(R.id.add_on_btn);
        IconTextView mRemoveBtn = (IconTextView) v.findViewById(R.id.remove_btn);
        final int index = mFixtureFittinsLayout.getChildCount();
        mSpecialConditionText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        mSpecialConditionText.setText(s);
        mSpecialConditionText.requestFocus();
        mIndex.setText((index + 1) + ".");
        mIndex.setTag(position);
        mAddOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFixtureFittinsLayout.getChildCount() >= 10) {
                    Toast.makeText(getActivity(), R.string.toast_max_fixture_fittings, Toast.LENGTH_LONG).show();
                    return;
                }
                generateFixtureFittingsView(((int) mIndex.getTag() + 1), "");
            }
        });
        mRemoveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View btnView) {
                mFixtureFittinsLayout.removeView(v);
                for (int i = 0; i < mFixtureFittinsLayout.getChildCount(); i++) {
                    IconTextView mRemoveButton = (IconTextView) mFixtureFittinsLayout.getChildAt(i).findViewById(R.id.remove_btn);
                    TextView mIndexText = (TextView) mFixtureFittinsLayout.getChildAt(i).findViewById(R.id.index);
                    mIndexText.setText((i + 1) + ".");
                    mIndexText.setTag(i);

                    mRemoveButton.setVisibility(mFixtureFittinsLayout.getChildCount() != 1 ? View.VISIBLE : View.GONE);
                }
            }
        });
        mSpecialConditionText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        mFixtureFittinsLayout.addView(v, position);
        mScrollView.fullScroll(View.FOCUS_DOWN);
        mSpecialConditionText.requestFocus();

        for (int i = 0; i < mFixtureFittinsLayout.getChildCount(); i++) {
            IconTextView mRemoveButton = (IconTextView) mFixtureFittinsLayout.getChildAt(i).findViewById(R.id.remove_btn);
            TextView mIndexText = (TextView) mFixtureFittinsLayout.getChildAt(i).findViewById(R.id.index);
            mIndexText.setText((i + 1) + ".");
            mIndexText.setTag(i);

            mRemoveButton.setVisibility(mFixtureFittinsLayout.getChildCount() != 1 ? View.VISIBLE : View.GONE);
        }
    }

    void updateNewTAModel() {
        newTAModel.fixtureFittingsModel = new FixtureFittingsModel();
        for (int i = 0; i < mFixtureFittinsLayout.getChildCount(); i++) {
            TextInputEditText mEditText = (TextInputEditText) mFixtureFittinsLayout.getChildAt(i).findViewById(R.id.fixture_fittings_text);
            if (!TextUtils.isEmpty(mEditText.getText().toString()))
                newTAModel.fixtureFittingsModel.fixtureFittingList.add(mEditText.getText().toString());
        }
        newTAModel.stepCaching = newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[8] : generateNewTAActivity.mResidentialSlideFragmentTag[7];

        if (isEdit || isTARenew) {
            generateNewTAActivity.setNewTAModel(newTAModel);
        } else {
            String newTAModelJson = new Gson().toJson(newTAModel);
            SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, newTAModelJson);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn: {
                updateNewTAModel();
                generateNewTAActivity.pageController(newTAModel.isCommercial ? generateNewTAActivity.mCommercialSlideFragmentTag[8] : generateNewTAActivity.mResidentialSlideFragmentTag[7]);

//                SharedPreferencesUtil.setString(getActivity(), GlobalParam.KEY_NEW_TA_MODEL, "");
//                generateNewTAActivity.finish();
                break;
            }
//            case R.id.previous_btn: {
//                getActivity().onBackPressed();
//                break;
//            }
        }
    }
}

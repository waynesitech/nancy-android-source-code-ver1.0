package com.heetch.countrypicker;

import android.content.Context;

import java.util.Locale;

/**
 * Created by GODARD Tuatini on 07/05/15.
 */
public class Country {
    private String isoCode;
    private String dialingCode;

    public Country() {

    }

    public Country(String isoCode, String dialingCode) {
        this.isoCode = isoCode;
        this.dialingCode = dialingCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public int getDrawableResourceId(Context context) {
        String drawableName = this.isoCode.toLowerCase(Locale.ENGLISH) + "_flag";
        return Utils.getMipmapResId(context, drawableName);
    }
}
